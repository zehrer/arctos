#ifndef IO_STM32HAL_HAL_H
#define IO_STM32HAL_HAL_H

#if   defined(STM32F4xx)
  #include "stm32f4xx_hal.h"
#endif

#endif /* IO_STM32HAL_HAL_H */
