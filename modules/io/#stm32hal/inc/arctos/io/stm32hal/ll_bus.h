#ifndef IO_STM32HAL_LL_BUS_H
#define IO_STM32HAL_LL_BUS_H

#if   defined(STM32F4xx)
  #include "stm32f4xx_ll_bus.h"
#endif

#endif /* IO_STM32HAL_LL_BUS_H */
