#ifndef IO_STM32HAL_LL_RCC_H
#define IO_STM32HAL_LL_RCC_H

#if   defined(STM32F4xx)
  #include "stm32f4xx_ll_rcc.h"
#endif

#endif /* IO_STM32HAL_LL_RCC_H */
