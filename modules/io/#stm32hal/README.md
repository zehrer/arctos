# Overview
This module provides the STM32Cube-HAL-Drivers to be used by other modules

## STM32F4
Based on the HAL-Drivers from [STM32CubeF4-Repository](https://github.com/STMicroelectronics/STM32CubeF4):

* All template files and the "hal_eth_legacy" files were removed
* Some header files ("stm32f4xx_hal_def.h" and most of the low level headers) were slightly changed.
* All changes are marked with an "#ARCTOS" comment

### Important
According to STMicroelectronics it is crucial to use a consistent set of CMSIS Core - CMSIS Device - HAL:

HAL Driver F4 | CMSIS Device F4 | CMSIS Core | Was delivered in the full MCU package
------------- | --------------- | ---------- | -------------------------------------
Tag v1.8.0 | Tag v2.6.8 | Tag v5.4.0_cm4 | Tag v1.27.0 (and following, if any, till HAL tag)

We use **v1.27.0** of the MCU package and therefore **v1.8.0** of the HAL Drivers.
But we DON'T use the prescribed CMSIS!
The CMSIS used by this modules is the one from the "arm" core segment. Nevertheless, they should be compatible.