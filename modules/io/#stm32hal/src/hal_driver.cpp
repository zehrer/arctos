/**
 * @file
 * @copyright Copyright (c) 2022 Michael Zehrer. This file is part of ARCTOS
 * @license MIT
 * @brief HAL module driver
 */
#include <arctos/io/stm32hal/hal.h>

#include <arctos/time.hpp>

extern "C" {

HAL_StatusTypeDef HAL_InitTick(uint32_t TickPriority) {
    /* nothing to do here */
    return HAL_OK;
}

void HAL_IncTick(void) {
    /* nothing to do here */
}

/**
 * @brief Provides a tick value in millisecond
 */
uint32_t HAL_GetTick(void) {
    return static_cast<uint32_t>(ROUGH_NOW() / MILLISECONDS);
}

/**
 * @brief This function provides a polling based minimum delay (in milliseconds)
 */
void HAL_Delay(uint32_t delay) {
    int64_t until = NOW() + (delay * MILLISECONDS);
    while (NOW() < until) { }
}

void HAL_SuspendTick(void) {
    /* nothing to do here */
}

void HAL_ResumeTick(void) {
    /* nothing to do here */
}

} /* extern "C" */
