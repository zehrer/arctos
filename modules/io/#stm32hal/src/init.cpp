/**
 * @file
 * @copyright Copyright (c) 2022 Michael Zehrer. This file is part of ARCTOS
 * @license MIT
 * @brief HAL initialization
 */
#include <arctos/io/stm32hal/init.hpp>
#include <arctos/io/stm32hal/hal.h>

namespace arctos {
namespace io {
namespace stm32hal {

void init(void) {
    HAL_Init();
}

} /* namespace stm32hal */
} /* namespace io */
} /* namespace arctos */
