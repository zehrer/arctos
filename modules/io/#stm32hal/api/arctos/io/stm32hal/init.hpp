/**
 * @file
 * @copyright Copyright (c) 2022 Michael Zehrer. This file is part of ARCTOS
 * @license MIT
 * @brief HAL initialization
 */
#ifndef IO_STM32HAL_INIT_HPP
#define IO_STM32HAL_INIT_HPP

#include <stdint.h>

namespace arctos {
namespace io {
namespace stm32hal {

void init(void);

} /* namespace stm32hal */
} /* namespace io */
} /* namespace arctos */

#endif /* IO_STM32HAL_INIT_HPP */
