/**
 * @file
 * @copyright Copyright (c) 2022 Michael Zehrer. This file is part of ARCTOS
 * @license MIT
 * @brief UART driver implementation for STM32F4xx
 */
#ifndef IO_SERIAL_STM32_UART_F4_HPP
#define IO_SERIAL_STM32_UART_F4_HPP
#include <stddef.h>
#include <stdint.h>

#include <arctos/io/serial/serial.hpp>

#include <arctos/io/pin/pin.hpp>

#include CMSIS_DEVICE

#define UART_IRQ_PRIORITY   0x2

namespace arctos {
namespace io {
namespace serial {

class StmUartF4 : public StmSerial {
public:
    constexpr StmUartF4(pin::StmPin &default_rx, pin::StmPin &default_tx,
                        USART_TypeDef *uart, IRQn_Type irqn) :
        StmSerial(default_rx, default_tx),
        uart_(uart), irqn_(irqn) { }

    /**
     * @brief Get the number of bytes already available for reading
     * @return Number of bytes available
     */
    size_t availableForRead(void) const override;
    /**
     * @brief Get the number of bytes available for buffered writing
     * @return Number of bytes available
     */
    size_t availableForWrite(void) const override;

    /**
     * @brief Initializes the serial communication
     * @param[in] speed Baudrate
     * @param[in] length Word length
     * @param[in] parity Parity
     * @param[in] stop Number of stop bits
     * @return Actual baudrate (which might be slightly different from the given one)
     *         OR '0' on failure
     */
    uint32_t begin(uint32_t speed  = 115200,
                   uint32_t length = 8,
                   Parity   parity = Parity::kNone,
                   uint32_t stop   = 1) override;

    /**
     * @brief Changes the receive (RX) pin
     *
     * Two conditions need to be met:
     * 1) The given pin must support the RX alternate pin function
     * 2) The serial driver must not be initialized / used at the moment
     *
     * @param[in] new_rx New rx pin
     * @return true on success, false otherwise
     */
    bool changeRxPin(pin::StmPin &new_rx) override;

    /**
     * @brief Changes the transmission (TX) pin
     *
     * Two conditions need to be met:
     * 1) The given pin must support the TX alternate pin function
     * 2) The serial driver must not be initialized / used at the moment
     *
     * @param[in] new_tx New tx pin
     * @return true on success, false otherwise
     */
    bool changeTxPin(pin::StmPin &new_tx) override;

    /**
     * @brief Disables serial communication
     *
     * Allowing the RX and TX pins to be used for general input and output
     */
    void end(void) override;

    /**
     * @brief Indicates if the serial communication is initialized
     * @return true, if initialized
     */
    bool isInitialized() const override;

    /**
     * @brief Waits for the transmission of outgoing serial data to complete
     */
    void flush(void) override;

    /**
     * @brief Reads one byte
     *
     * This function doesn't block, if availableForRead() is greater than zero.
     * Otherwise it will block until one byte is received, or a timeout occurred.
     * @see setTimeout()
     *
     * @param[out] b Byte read
     * @return 1 on success, or 0 on timeout
     */
    size_t read(uint8_t &b) override;
    /**
     * @brief Reads several bytes
     *
     * This function doesn't block, if availableForRead() is greater than or
     * equal to the given length. Otherwise it will block until the requested
     * amount of bytes are received, or a timeout occurred.
     * @see setTimeout()
     *
     * @param[out] buffer Buffer to store the bytes
     * @param[in] length Number of bytes to be read
     * @return Number of bytes placed in the buffer
     */
    size_t read(uint8_t *buffer, size_t length) override;

    /**
     * @brief Writes one byte
     *
     * The transmission is asynchronous. This function doesn't block, if
     * availableForWrite() is greater than zero. Otherwise it will block
     * until the given byte could be inserted into the transmit buffer.
     * @see flush()
     *
     * @param[in] b Byte to be sent
     * @return Number of bytes written
     */
    size_t write(uint8_t b) override;
    /**
     * @brief Writes several bytes
     *
     * The transmission is asynchronous. This function doesn't block, if there
     * is enough empty space in the transmit buffer. But if the available buffer
     * size is insufficient, it will block until all given data is transferred
     * to the buffer.
     * @see availableForWrite()
     * @see flush()
     *
     * @param[in] buffer Buffer containing the bytes
     * @param[in] length Number of bytes to be sent
     * @return Number of bytes written
     */
    size_t write(const uint8_t *buffer, size_t length) override;


    /**
     * @brief Interrupt service routine
     * @param[in] irq_source Associated USART
     */
    static void isr(StmUartF4 &irq_source);

protected:
    USART_TypeDef   *uart_;
    IRQn_Type        irqn_;

    /**
     * @brief Starts the data transfer to the hardware (TX)
     *
     * Both write(...)-functions are writing only to the transmit buffer!
     * This helper function is called afterwards. It initiates the transfer.
     */
    void writeTransfer(void);
};

} /* namespace serial */
} /* namespace io */
} /* namespace arctos */

#endif /* IO_SERIAL_STM32_UART_F4_HPP */
