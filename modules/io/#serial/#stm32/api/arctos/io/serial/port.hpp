/**
 * @file
 * @copyright Copyright (c) 2022 Michael Zehrer. This file is part of ARCTOS
 * @license MIT
 * @brief stm32 specific port of the "serial" module
 */
#ifndef IO_SERIAL_PORT_HPP
#define IO_SERIAL_PORT_HPP
#include <stddef.h>
#include <stdint.h>

#include <arctos/io/serial/stm32/params.h>
#include <arctos/io/serial/serial.hpp>

#include <arctos/io/pin/pin.hpp>

#include <arctos/fifo.hpp>

namespace arctos {
namespace io {
namespace serial {

class StmSerial : public Serial {
protected:
    pin::StmPin *rx_;
    pin::StmPin *tx_;

    arctos::BlockFifo<uint8_t, SERIAL_BUFFER_SIZE> rx_buffer_;
    arctos::BlockFifo<uint8_t, SERIAL_BUFFER_SIZE> tx_buffer_;

    constexpr StmSerial(pin::StmPin &default_rx, pin::StmPin &default_tx) :
        rx_(&default_rx), tx_(&default_tx) { }

public:
    /**
     * @brief Changes the receive (RX) pin
     *
     * Two conditions need to be met:
     * 1) The given pin must support the RX alternate pin function
     * 2) The serial driver must not be initialized / used at the moment
     *
     * @param[in] new_rx New rx pin
     * @return true on success, false otherwise
     */
    virtual bool changeRxPin(pin::StmPin &new_rx) = 0;

    /**
     * @brief Changes the transmission (TX) pin
     *
     * Two conditions need to be met:
     * 1) The given pin must support the TX alternate pin function
     * 2) The serial driver must not be initialized / used at the moment
     *
     * @param[in] new_tx New tx pin
     * @return true on success, false otherwise
     */
    virtual bool changeTxPin(pin::StmPin &new_tx) = 0;
};

} /* namespace serial */
} /* namespace io */
} /* namespace arctos */

#if STM_BOARD == STM_BOARD_STM32F411_DISCO
#  include "stm32/boards/disco_f411.hpp"
#elif    STM_BOARD == STM_BOARD_BLACK_PILL_F401V3 \
      || STM_BOARD == STM_BOARD_BLACK_PILL_F411V2
#  include "stm32/boards/blackpill.hpp"
#endif

#endif /* IO_SERIAL_PORT_HPP */
