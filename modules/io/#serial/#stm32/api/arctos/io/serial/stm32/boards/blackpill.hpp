/**
 * @file
 * @copyright Copyright (c) 2022 Michael Zehrer. This file is part of ARCTOS
 * @license MIT
 * @brief serial definitions for "WeAct BlackPill"
 */
#ifndef IO_SERIAL_STM32_BOARDS_BLACKPILL_HPP
#define IO_SERIAL_STM32_BOARDS_BLACKPILL_HPP
#include <stddef.h>
#include <stdint.h>

#include <arctos/io/serial/serial.hpp>
#include <arctos/io/serial/stm32/params.h>

#include CMSIS_DEVICE

namespace arctos {
namespace io {
namespace serial {

#if defined(SERIAL_ENABLE_IDX1) && defined(USART1_BASE)
extern StmSerial &usart01;
#endif

#if defined(SERIAL_ENABLE_IDX2) && defined(USART2_BASE)
extern StmSerial &usart02;
#endif

#if defined(SERIAL_ENABLE_IDX6) && defined(USART6_BASE)
extern StmSerial &usart06;
#endif

} /* namespace serial */
} /* namespace io */
} /* namespace arctos */

#endif /* IO_SERIAL_STM32_BOARDS_BLACKPILL_HPP */
