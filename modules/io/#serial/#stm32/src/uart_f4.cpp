/**
 * @file
 * @copyright Copyright (c) 2022 Michael Zehrer. This file is part of ARCTOS
 * @license MIT
 * @brief UART driver implementation for STM32F4xx
 */
/*
    TODO-list:
    #1 Error handling
    #2 flush currently uses polling => rework with task suspension
    #3 implement DMA transfers
*/
#include <arctos/io/serial/stm32/uart_f4.hpp>
#include <arctos/io/serial/stm32/params.h>

#include <arctos/io/stm32hal/ll_bus.h>
#include <arctos/io/stm32hal/ll_rcc.h>
#include <arctos/io/stm32hal/ll_usart.h>

#include CMSIS_DEVICE

#include <string.h>

namespace arctos {
namespace io {
namespace serial {

size_t StmUartF4::availableForRead() const {
    if (!this->isInitialized())
        return 0;
    return this->rx_buffer_.getElementCount();
}

size_t StmUartF4::availableForWrite() const {
    if (!this->isInitialized())
        return 0;
    return this->tx_buffer_.getFreeSpaceCount();
}

uint32_t StmUartF4::begin(uint32_t speed,
                          uint32_t length,
                          Parity   parity,
                          uint32_t stop) {
    if (this->isInitialized()) {
        // already running
        return 0;
    }
    else if (this->rx_ == nullptr || this->tx_ == nullptr) {
        // can't initialize without rx/tx pins
        return 0;
    }

    LL_RCC_ClocksTypeDef rcc_clocks;
    uint32_t             periph_clk;
    LL_RCC_GetSystemClocksFreq(&rcc_clocks);

    const pin::StmPin::Alternate *rx_alt = this->rx_->getAlternate(
        pin::StmPin::Alternate::Function::kUartRX, this->uart_);
    const pin::StmPin::Alternate *tx_alt = this->tx_->getAlternate(
        pin::StmPin::Alternate::Function::kUartTX, this->uart_);
    if (rx_alt == nullptr || tx_alt == nullptr) {
        // no UART support on the pin(s) (This case should not be possible!)
        return 0;
    }

    /* enable the USARTx interface clock */
    switch (this->irqn_) {
#if defined(SERIAL_ENABLE_IDX1) && defined(USART1_BASE)
        case USART1_IRQn:
            LL_APB2_GRP1_ForceReset(LL_APB2_GRP1_PERIPH_USART1);
            LL_APB2_GRP1_ReleaseReset(LL_APB2_GRP1_PERIPH_USART1);
            LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_USART1);
            periph_clk = rcc_clocks.PCLK2_Frequency;
            break;
#endif

#if defined(SERIAL_ENABLE_IDX2) && defined(USART2_BASE)
        case USART2_IRQn:
            LL_APB1_GRP1_ForceReset(LL_APB1_GRP1_PERIPH_USART2);
            LL_APB1_GRP1_ReleaseReset(LL_APB1_GRP1_PERIPH_USART2);
            LL_APB1_GRP1_EnableClock(LL_APB1_GRP1_PERIPH_USART2);
            periph_clk = rcc_clocks.PCLK1_Frequency;
            break;
#endif

#if defined(SERIAL_ENABLE_IDX6) && defined(USART6_BASE)
        case USART6_IRQn:
            LL_APB2_GRP1_ForceReset(LL_APB2_GRP1_PERIPH_USART6);
            LL_APB2_GRP1_ReleaseReset(LL_APB2_GRP1_PERIPH_USART6);
            LL_APB2_GRP1_EnableClock(LL_APB2_GRP1_PERIPH_USART6);
            periph_clk = rcc_clocks.PCLK2_Frequency;
            break;
#endif

        default:
            return 0;
    }

    LL_USART_Disable(this->uart_);

    /* configure the UART RX/TX pins as alternate function pull-up */
    this->rx_->mode(rx_alt->mode);
    this->tx_->mode(tx_alt->mode);

    /* configure the USARTx interrupt priority */
    NVIC_SetPriority(this->irqn_, UART_IRQ_PRIORITY);
    /* enable the NVIC USART IRQ handle */
    NVIC_EnableIRQ(this->irqn_);

    /* program the word length */
    if (length == 8)
        LL_USART_SetDataWidth(this->uart_, LL_USART_DATAWIDTH_8B);
    else
        return 0;

    /* program the stop bit */
    if (stop == 1)
        LL_USART_SetStopBitsLength(this->uart_, LL_USART_STOPBITS_1);
    else if (stop == 2)
        LL_USART_SetStopBitsLength(this->uart_, LL_USART_STOPBITS_2);
    else
        return 0;

    /* program the parity */
    switch (parity) {
        case Parity::kNone:
            LL_USART_SetParity(this->uart_, LL_USART_PARITY_NONE);
            break;
        case Parity::kEven:
            LL_USART_SetParity(this->uart_, LL_USART_PARITY_EVEN);
            break;
        case Parity::kOdd:
            LL_USART_SetParity(this->uart_, LL_USART_PARITY_ODD);
            break;
        default:
            return 0;
    }

    LL_USART_SetTransferDirection(this->uart_, LL_USART_DIRECTION_TX_RX);
    LL_USART_SetHWFlowCtrl       (this->uart_, LL_USART_HWCONTROL_NONE );
    LL_USART_SetOverSampling     (this->uart_, LL_USART_OVERSAMPLING_16);
    LL_USART_SetBaudRate(this->uart_,
                         periph_clk,
                         LL_USART_OVERSAMPLING_16,
                         speed);

    LL_USART_ConfigAsyncMode(this->uart_);
    LL_USART_Enable(this->uart_);
    LL_USART_EnableIT_RXNE(this->uart_);

    return LL_USART_GetBaudRate(this->uart_,
                                periph_clk,
                                LL_USART_OVERSAMPLING_16);
}

bool StmUartF4::changeRxPin(pin::StmPin &new_rx) {
    if (this->isInitialized()) {
        return false;
    }

    const pin::StmPin::Alternate *new_rx_alt = new_rx.getAlternate(
        pin::StmPin::Alternate::Function::kUartRX, this->uart_);
    if (new_rx_alt != nullptr) {
        this->rx_ = &new_rx;
        return true;
    }
    return false;
}

bool StmUartF4::changeTxPin(pin::StmPin &new_tx) {
    if (this->isInitialized()) {
        return false;
    }

    const pin::StmPin::Alternate *new_tx_alt = new_tx.getAlternate(
        pin::StmPin::Alternate::Function::kUartTX, this->uart_);
    if (new_tx_alt != nullptr) {
        this->tx_ = &new_tx;
        return true;
    }
    return false;
}

void StmUartF4::end() {
    if (!this->isInitialized()) {
        // already in reset state => nothing to do
        return;
    }

    LL_USART_Disable(this->uart_);
    LL_USART_DisableIT_TXE (this->uart_);
    LL_USART_DisableIT_TC  (this->uart_);
    LL_USART_DisableIT_RXNE(this->uart_);

    switch (this->irqn_) {
#if defined(SERIAL_ENABLE_IDX1) && defined(USART1_BASE)
        case USART1_IRQn:
            LL_APB2_GRP1_ForceReset(LL_APB2_GRP1_PERIPH_USART1);
            LL_APB2_GRP1_ReleaseReset(LL_APB2_GRP1_PERIPH_USART1);
            LL_APB2_GRP1_DisableClock(LL_APB2_GRP1_PERIPH_USART1);
            break;
#endif

#if defined(SERIAL_ENABLE_IDX2) && defined(USART2_BASE)
        case USART2_IRQn:
            LL_APB1_GRP1_ForceReset(LL_APB1_GRP1_PERIPH_USART2);
            LL_APB1_GRP1_ReleaseReset(LL_APB1_GRP1_PERIPH_USART2);
            LL_APB1_GRP1_DisableClock(LL_APB1_GRP1_PERIPH_USART2);
            break;
#endif

#if defined(SERIAL_ENABLE_IDX6) && defined(USART6_BASE)
        case USART6_IRQn:
            LL_APB2_GRP1_ForceReset(LL_APB2_GRP1_PERIPH_USART6);
            LL_APB2_GRP1_ReleaseReset(LL_APB2_GRP1_PERIPH_USART6);
            LL_APB2_GRP1_DisableClock(LL_APB2_GRP1_PERIPH_USART6);
            break;
#endif

        default:
            return;
    }

    this->rx_->mode(pin::mode::kInput);
    this->tx_->mode(pin::mode::kInput);

    this->rx_buffer_.clear();
    this->tx_buffer_.clear();
}

bool StmUartF4::isInitialized() const {
    return LL_USART_IsEnabled(this->uart_);
}

void StmUartF4::flush() {
    LL_USART_DisableIT_TXE(this->uart_);
    uint8_t byte;
    while (this->tx_buffer_.get(byte)) {
        while (!LL_USART_IsActiveFlag_TXE(this->uart_)) {}
        LL_USART_TransmitData8(this->uart_, byte);
    }
    while (!LL_USART_IsActiveFlag_TC(this->uart_)) {}
}

size_t StmUartF4::read(uint8_t &b)  {
    if (!this->isInitialized()) return 0;

    int64_t time_start = NOW();
    do {
        if (this->rx_buffer_.get(b))
            return 1;
    } while((NOW() - time_start) < this->timeout_);
    return 0;
}

size_t StmUartF4::read(uint8_t *buffer, size_t length) {
    if (length == 0 || !this->isInitialized()) return 0;

    int64_t time_start = NOW();
    size_t bytes_readable;
    size_t bytes_read = 0;
    do {
        uint8_t *p = this->rx_buffer_.getBufferToRead(bytes_readable);
        if (bytes_readable > 0) {
            time_start = NOW();
            if (bytes_readable > length)
                bytes_readable = length;

            memcpy(buffer, p, bytes_readable);
            this->rx_buffer_.readConcluded(bytes_readable);

            buffer     += bytes_readable;
            length     -= bytes_readable;
            bytes_read += bytes_readable;
        }
    } while (length > 0 && (NOW() - time_start) < this->timeout_);
    return bytes_read;
}

size_t StmUartF4::write(uint8_t b) {
    if (!this->isInitialized()) return 0;

    bool success = this->tx_buffer_.put(b);
    this->writeTransfer();
    return (success ? 1 : 0);
}

size_t StmUartF4::write(const uint8_t *buffer, size_t length) {
    if (!this->isInitialized()) return 0;

    size_t bytes_writable;
    size_t bytes_written = 0;

    do {
        uint8_t *p = this->tx_buffer_.getBufferToWrite(bytes_writable);
        if (bytes_writable > 0) {
            if (bytes_writable > length)
                bytes_writable = length;

            memcpy(p, buffer, bytes_writable);
            this->tx_buffer_.writeConcluded(bytes_writable);

            buffer        += bytes_writable;
            length        -= bytes_writable;
            bytes_written += bytes_writable;
        }
        this->writeTransfer();
    } while (length > 0 && !this->tx_buffer_.isFull());
    return bytes_written;
}

void StmUartF4::writeTransfer() {
    if (LL_USART_IsEnabledIT_TXE(this->uart_)) {
        return;
    }

    uint8_t b;
    if (LL_USART_IsActiveFlag_TXE(this->uart_) && this->tx_buffer_.get(b))
        LL_USART_TransmitData8(this->uart_, b);
    LL_USART_EnableIT_TXE(this->uart_);
}

void StmUartF4::isr(StmUartF4 &irq_source) {
    NVIC_ClearPendingIRQ(irq_source.irqn_);
    uint8_t b;

    if (LL_USART_IsActiveFlag_RXNE(irq_source.uart_)) {
        b = LL_USART_ReceiveData8(irq_source.uart_);
        if (!irq_source.rx_buffer_.put(b)) {
            // ERROR => buffer full
        }
    }

    if (LL_USART_IsActiveFlag_TXE(irq_source.uart_)) {
        if (irq_source.tx_buffer_.get(b)) {
            LL_USART_TransmitData8(irq_source.uart_, b);
        }
        else {
            LL_USART_DisableIT_TXE(irq_source.uart_);
            //LL_USART_EnableIT_TC(irq_source->uart_);
        }
    }
}

} /* namespace serial */
} /* namespace io */
} /* namespace arctos */
