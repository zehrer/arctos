/**
 * @file
 * @copyright Copyright (c) 2022 Michael Zehrer. This file is part of ARCTOS
 * @license MIT
 * @brief serial definitions for "STM32F411 Discovery Board"
 */
#include <arctos/io/serial/stm32/uart_f4.hpp>

#include <arctos/io/pin/pin.hpp>

#include CMSIS_DEVICE

namespace arctos {
namespace io {
namespace serial {

#if defined(SERIAL_ENABLE_IDX1) && defined(USART1_BASE)
static StmUartF4 usart01_obj = StmUartF4(pin::pa10, pin::pa09, USART1, USART1_IRQn);
StmSerial &usart01 = usart01_obj;
extern "C" void USART1_IRQHandler(void) {
    StmUartF4::isr(usart01_obj);
}
#endif

#if defined(SERIAL_ENABLE_IDX2) && defined(USART2_BASE)
static StmUartF4 usart02_obj = StmUartF4(pin::pa03, pin::pa02, USART2, USART2_IRQn);
StmSerial &usart02 = usart02_obj;
extern "C" void USART2_IRQHandler(void) {
    StmUartF4::isr(usart02_obj);
}
#endif

#if defined(SERIAL_ENABLE_IDX6) && defined(USART6_BASE)
static StmUartF4 usart06_obj = StmUartF4(pin::pa12, pin::pa11, USART6, USART6_IRQn);
StmSerial &usart06 = usart06_obj;
extern "C" void USART6_IRQHandler(void) {
    StmUartF4::isr(usart06_obj);
}
#endif

} /* namespace serial */
} /* namespace io */
} /* namespace arctos */
