/**
 * @file
 * @copyright Copyright (c) 2022 Michael Zehrer. This file is part of ARCTOS
 * @license MIT
 * @brief uart / usart driver abstraction
 */
#include <arctos/io/serial/serial.hpp>

namespace arctos {
namespace io {
namespace serial {

// Deprecated => TODO delete this file

Serial *get(size_t index) {
    switch(index) {
        case 0:
            // TODO return <UART>::instance();
        case 1:
            // TODO return <UART>::instance();
            // ...
        default:
            return nullptr;
    }
}

} /* namespace serial */
} /* namespace io */
} /* namespace arctos */
