/**
 * @file
 * @copyright Copyright (c) 2019-2022 Michael Zehrer. This file is part of ARCTOS
 * @license MIT
 * @brief serial communication module
 */
#ifndef IO_SERIAL_SERIAL_HPP
#define IO_SERIAL_SERIAL_HPP
#include <stddef.h>
#include <stdint.h>

#include <arctos/printable.hpp>
#include <arctos/time.hpp>

namespace arctos {
namespace io {
namespace serial {

class Serial : public arctos::Printable {
public:
    enum class Parity {
        kNone,
        kEven,
        kOdd
    };

    /**
     * @brief Get the number of bytes already available for reading
     * @return Number of bytes available
     */
    virtual size_t availableForRead(void) const = 0;
    /**
     * @brief Get the number of bytes available for buffered writing
     * @return Number of bytes available
     */
    virtual size_t availableForWrite(void) const = 0;

    /**
     * @brief Initializes the serial communication
     * @param[in] speed Baudrate
     * @param[in] length Word length
     * @param[in] parity Parity
     * @param[in] stop Number of stop bits
     * @return Actual baudrate (which might be slightly different from the given one)
     *         OR '0' on failure
     */
    virtual uint32_t begin(uint32_t speed  = 115200,
                           uint32_t length = 8,
                           Parity   parity = Parity::kNone,
                           uint32_t stop   = 1) = 0;
    /**
     * @brief Disables serial communication
     *
     * Allowing the RX and TX pins to be used for general input and output
     */
    virtual void end(void) = 0;

    /**
     * @brief Indicates if the serial communication is initialized
     * @return true, if initialized
     */
    virtual bool isInitialized() const = 0;

    /**
     * @brief Reads one byte
     *
     * This function doesn't block, if availableForRead() is greater than zero.
     * Otherwise it will block until one byte is received, or a timeout occurred.
     * @see setTimeout()
     *
     * @param[out] b Byte read
     * @return 1 on success, or 0 on timeout
     */
    virtual size_t read(uint8_t &b) = 0;
    /**
     * @brief Reads several bytes
     *
     * This function doesn't block, if availableForRead() is greater than or
     * equal to the given length. Otherwise it will block until the requested
     * amount of bytes are received, or a timeout occurred.
     * @see setTimeout()
     *
     * @param[out] buffer Buffer to store the bytes
     * @param[in] length Number of bytes to be read
     * @return Number of bytes placed in the buffer
     */
    virtual size_t read(uint8_t *buffer, size_t length) = 0;

    /**
     * @brief Sets the maximum time to wait for serial data
     * @param[in] timeout Time in nanoseconds
     */
    void setTimeout(int64_t timeout) { this->timeout_ = timeout; }

    /**
     * @brief Writes one byte
     *
     * The transmission is asynchronous. This function doesn't block, if
     * availableForWrite() is greater than zero. Otherwise it will block
     * until the given byte could be inserted into the transmit buffer.
     * @see flush()
     *
     * @param[in] b Byte to be sent
     * @return Number of bytes written
     */
    virtual size_t write(uint8_t b) = 0;
    /**
     * @brief Writes several bytes
     *
     * The transmission is asynchronous. This function doesn't block, if there
     * is enough empty space in the transmit buffer. But if the available buffer
     * size is insufficient, it will block until all given data is transferred
     * to the buffer.
     * @see availableForWrite()
     * @see flush()
     *
     * @param[in] buffer Buffer containing the bytes
     * @param[in] length Number of bytes to be sent
     * @return Number of bytes written
     */
    virtual size_t write(const uint8_t *buffer, size_t length) = 0;


/***********************************************************************
 * Interface functions from 'arctos::Printable'
 **********************************************************************/

    /**
     * @brief Print a character.
     *
     * @param[in] c Character to print.
     * @return The value one
     */
    virtual size_t print(char c) {
        return this->write(static_cast<uint8_t>(c));
    }

    /**
     * @brief Print a string.
     *
     * @param[in] str String to print
     * @param[in] len Length of the input string
     * @return Number of chars really printed
     */
    virtual size_t print(const char *str, size_t len) {
        return this->write(reinterpret_cast<const uint8_t *>(str), len);
    }

protected:
    constexpr Serial() : timeout_(1000 * MILLISECONDS) { }
    int64_t timeout_;
};

// Deprecated => TODO delete
Serial *get(size_t index);

} /* namespace serial */
} /* namespace io */
} /* namespace arctos */

#if defined __has_include
  #if __has_include (<arctos/io/serial/port.hpp>)
    // include hardware / SoC specific port of this module, if available
    #include <arctos/io/serial/port.hpp>
  #endif
#endif

#endif /* IO_SERIAL_SERIAL_HPP */
