/**
 * @file
 * @copyright Copyright (c) 2019-2022 Michael Zehrer. This file is part of ARCTOS
 * @license MIT
 * @brief serial communication module
 */
#ifndef IO_SERIAL_ALL_HPP
#define IO_SERIAL_ALL_HPP

#include "serial.hpp"

#endif /* IO_SERIAL_ALL_HPP */
