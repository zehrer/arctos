/**
 * @file
 * @copyright Copyright (c) 2020-2022 Michael Zehrer. This file is part of ARCTOS
 * @license MIT
 * @brief serial communication module
 */
#include <arctos/io/serial/serial.hpp>
#include <arctos/io/serial/raspberry/mini_uart.hpp>
#include <arctos/io/serial/raspberry/pl011_uart.hpp>

namespace arctos {
namespace io {
namespace serial {

Serial *get(size_t index) {
    switch(index) {
        case 0:
            return Pl011UART::instance();
        case 1:
            return MiniUART::instance();
        default:
            return nullptr;
    }
}

} /* namespace serial */
} /* namespace io */
} /* namespace arctos */
