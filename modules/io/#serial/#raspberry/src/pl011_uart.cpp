/**
 * @file
 * @copyright Copyright (c) 2019-2022 Michael Zehrer. This file is part of ARCTOS
 * @license MIT
 * @brief serial communication over PL011-UART
 */
#include <arctos/io/serial/raspberry/pl011_uart.hpp>

#include <raspberry/all.h>
#include <raspberry/interrupts.hpp>

namespace arctos {
namespace io {
namespace serial {

size_t Pl011UART::availableForRead() const {
    // TODO
    return 0;
}

size_t Pl011UART::availableForWrite() const {
    // TODO
    return 0;
}

uint32_t Pl011UART::begin(uint32_t speed, uint32_t length, Parity parity, uint32_t stop) {
    // TODO
    return 0;
}

void Pl011UART::end() {
    // TODO
}

void Pl011UART::flush() {
    // TODO
}

size_t Pl011UART::read(uint8_t &b) {
    // TODO
    return 0;
}

size_t Pl011UART::read(uint8_t *buffer, size_t length) {
    // TODO
    return 0;
}

size_t Pl011UART::write(uint8_t b) {
    // TODO
    return 0;
}

size_t Pl011UART::write(const uint8_t *buffer, size_t length) {
    // TODO
    return 0;
}


Pl011UART *Pl011UART::instance() {
    static Pl011UART pl011;
    return &pl011;
}


class Pl011UARTIsr : public ::raspberry::Isr {
public:
    Pl011UARTIsr() : Isr(::raspberry::Interrupts::kUart0) { }

    void run() {
        //Pl011UART *pl011 = Pl011UART::instance();

        // TODO
    }
};
static Pl011UARTIsr pl011_isr;

} /* namespace serial */
} /* namespace io */
} /* namespace arctos */
