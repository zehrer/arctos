/**
 * @file
 * @copyright Copyright (c) 2019-2022 Michael Zehrer. This file is part of ARCTOS
 * @license MIT
 * @brief serial communication over mini uart
 */
#include <string.h>

#include <arctos/io/serial/raspberry/mini_uart.hpp>
#include <arctos/io/serial/serial.hpp>

#include <raspberry/all.h>
#include <raspberry/interrupts.hpp>

#include <arm/registers/psr.hpp>

namespace arctos {
namespace io {
namespace serial {

size_t MiniUART::availableForRead() const {
    if (!this->state_.attr.is_initialized)
        return 0;
    return this->receive_buffer_.getElementCount() +
        ((AUXILIARY->MU_STAT & AUX_MUSTAT_RX_FIFO_LEVEL_MASK) >> AUX_MUSTAT_RX_FIFO_LEVEL_POS);
}

size_t MiniUART::availableForWrite() const {
    if (!this->state_.attr.is_initialized)
        return 0;
    return this->transmit_buffer_.getFreeSpaceCount() +
        ((AUXILIARY->MU_STAT & AUX_MUSTAT_TX_FIFO_LEVEL_MASK) >> AUX_MUSTAT_TX_FIFO_LEVEL_POS);
}

uint32_t MiniUART::begin(uint32_t speed, uint32_t length, Parity parity, uint32_t stop) {
    if (parity != Parity::kNone) {
        // hardware doesn't have parity support => cancel initialization
        return 0;
    }
    else if (stop != 1) {
        // hardware only supports exactly 1 stop bit => cancel initialization
        return 0;
    }

    if (!this->state_.attr.is_initialized) {
        // activate alternative pin functions
        RPI_GPIO_SetMultiplePinFunction(14, 15, FS_ALT5);
        // disable the pull up / pull down resistors
        RPI_GPIO_SetMultiplePinPUD(14, 15, PUD_OFF);

        // disable Auxiliary-Interrupts
        RPI_IRQ_Disable(Auxiliary_IRQn);

        // enable the miniUART (necessary for register access)
        uint32_t ra = AUXILIARY->ENABLES;
        ra |= AUXENB_MINIUART;
        AUXILIARY->ENABLES = ra;
        // disable transmit and receive interrupts
        AUXILIARY->MU_IER  = 0;
        // disable transmitter, receiver, hw-flow-ctrl., ...
        AUXILIARY->MU_CNTL = 0;
        // (re)enable Auxiliary-Interrupts
        RPI_IRQ_Enable(Auxiliary_IRQn);

        if (length == 7) {
            // set data size to 7-bit mode
            AUXILIARY->MU_LCR  = AUX_MULCR_DATA_SIZE_7BIT;
        }
        else if (length == 8) {
            // set data size to 8-bit mode
            AUXILIARY->MU_LCR  = AUX_MULCR_DATA_SIZE_8BIT;
        }
        else {
            // length not possible => cancel initialization
            return 0;
        }
        // clear UART1_RTS line to high
        AUXILIARY->MU_MCR  = 0;
        // enable reveice interrupts
        AUXILIARY->MU_IER  = AUX_MUIER_ENABLE_RXIRQ;
        // clear FIFO's
        AUXILIARY->MU_IIR  = AUX_MUIIR_ENABLE_FIFOS | AUX_MUIIR_CLEAR_RX_FIFO | AUX_MUIIR_CLEAR_TX_FIFO;
        // set baudrate
        AUXILIARY->MU_BAUD = (BSC_CLOCK_FREQ / (speed * 8)) - 1;
        // enable transmitter and receiver
        AUXILIARY->MU_CNTL = AUX_MUCNTL_RX_ENABLE | AUX_MUCNTL_TX_ENABLE;

        this->state_.attr.is_initialized = 1;
    }

    return BSC_CLOCK_FREQ / (8 * (AUXILIARY->MU_BAUD + 1));
}

void MiniUART::end() {
    if (!this->state_.attr.is_initialized)
        return;

    // disable transmit and receive interrupts
    AUXILIARY->MU_IER  = 0;
    // disable transmitter, receiver, hw-flow-ctrl., ...
    AUXILIARY->MU_CNTL = 0;
    // clear transmit and receive FIFO's
    AUXILIARY->MU_IIR  = AUX_MUIIR_CLEAR_RX_FIFO | AUX_MUIIR_CLEAR_TX_FIFO;

    // disable the miniUART
    uint32_t ra = AUXILIARY->ENABLES;
    ra &= ~AUXENB_MINIUART;
    AUXILIARY->ENABLES = ra;

    // reset alternative pin functions
    RPI_GPIO_SetMultiplePinFunction(14, 15, FS_INPUT);

    this->state_.value = 0;
    this->receive_buffer_.clear();
    this->transmit_buffer_.clear();
}

void MiniUART::flush() {
    if (!this->state_.attr.is_initialized)
        return;
    while (!this->transmit_buffer_.isEmpty()) {
        if (!::arm::registers::PSR_Type::readCPSR().isIRQEnabled())
            this->write_transfer();
    }

    while (!(AUXILIARY->MU_STAT & AUX_MUSTAT_TX_DONE)) { }
}

void MiniUART::read_transfer() {
    AUXILIARY->MU_IER &= ~AUX_MUIER_ENABLE_RXIRQ;
    while (!this->receive_buffer_.isFull() && (AUXILIARY->MU_STAT & AUX_MUSTAT_SYMBOL_AVAILABLE))
        this->receive_buffer_.put(static_cast<uint8_t>(AUXILIARY->MU_IO & 0xFF));
    AUXILIARY->MU_IER |= AUX_MUIER_ENABLE_RXIRQ;
}

size_t MiniUART::read(uint8_t &b) {
    if (!this->state_.attr.is_initialized) return 0;

    int64_t time_start = NOW();
    do {
        this->read_transfer();
        if (this->receive_buffer_.get(b))
            return 1;
    } while((NOW() - time_start) < this->timeout_);
    return 0;
}

size_t MiniUART::read(uint8_t *buffer, size_t length) {
    if (length == 0 || !this->state_.attr.is_initialized)
        return 0;

    int64_t time_start = NOW();
    size_t bytes_readable;
    size_t bytes_read = 0;
    do {
        this->read_transfer();
        uint8_t *p = this->receive_buffer_.getBufferToRead(bytes_readable);
        if (bytes_readable > 0) {
            time_start = NOW();
            if (bytes_readable > length)
                bytes_readable = length;

            memcpy(buffer, p, bytes_readable);
            this->receive_buffer_.readConcluded(bytes_readable);

            buffer     += bytes_readable;
            length     -= bytes_readable;
            bytes_read += bytes_readable;
        }
    } while (length > 0 && (NOW() - time_start) < this->timeout_);
    return bytes_read;
}

void MiniUART::write_transfer() {
    uint8_t ch;
    AUXILIARY->MU_IER &= ~AUX_MUIER_ENABLE_TXIRQ;

    while (AUXILIARY->MU_STAT & AUX_MUSTAT_SPACE_AVAILABLE) {
        if (this->transmit_buffer_.get(ch))
            AUXILIARY->MU_IO = ch;
        else
            break;
    }

    if (!this->transmit_buffer_.isEmpty() && ::arm::registers::PSR_Type::readCPSR().isIRQEnabled())
        AUXILIARY->MU_IER |= AUX_MUIER_ENABLE_TXIRQ;
}

size_t MiniUART::write(uint8_t b) {
    if (!this->state_.attr.is_initialized)
        return 0;

    bool inserted;
    do {
        inserted = this->transmit_buffer_.put(b);
        this->write_transfer();
    } while (!inserted);
    return 1;
}

size_t MiniUART::write(const uint8_t *buffer, size_t length) {
    if (length == 0 || !this->state_.attr.is_initialized)
        return 0;

    size_t bytes_writable;
    size_t bytes_written = 0;
    do {
        uint8_t *p = this->transmit_buffer_.getBufferToWrite(bytes_writable);
        if (bytes_writable > 0) {
            if (bytes_writable > length)
                bytes_writable = length;

            memcpy(p, buffer, bytes_writable);
            this->transmit_buffer_.writeConcluded(bytes_writable);

            buffer        += bytes_writable;
            length        -= bytes_writable;
            bytes_written += bytes_writable;
        }
        this->write_transfer();
    } while (length > 0);
    return bytes_written;
}

MiniUART *MiniUART::instance() {
    static MiniUART mini;
    return &mini;
}


class MiniUARTIsr : public ::raspberry::Isr {
public:
    MiniUARTIsr() : Isr(::raspberry::Interrupts::kUart1) { }

    void run() {
        MiniUART *mini = MiniUART::instance();

        uint8_t c;
        uint32_t iir = AUXILIARY->MU_IIR;
        // !!the pending bit indicates that there are no more interrupts!!
        while (!(iir & AUX_MUIIR_IRQ_PENDING)) {
            if ((iir & AUX_MUIIR_IRQID_MASK) == AUX_MUIIR_IRQID_RX_READY) {
                c = (uint8_t)(AUXILIARY->MU_IO & 0xFF); // read byte from rx fifo
                if (!mini->receive_buffer_.put(c))
                    mini->state_.attr.rx_error++; // BlockFifo Overflow
                // TODO
                // ...->upCallDataReady();
            }
            if ((iir & AUX_MUIIR_IRQID_MASK) == AUX_MUIIR_IRQID_TX_EMPTY) {
                uint8_t sent = 0;
                do {
                    if (mini->transmit_buffer_.get(c)) {
                        AUXILIARY->MU_IO = c;
                        sent++;
                    }
                    else
                        break;
                } while (AUXILIARY->MU_STAT & AUX_MUSTAT_SPACE_AVAILABLE);

                if (sent == 0) {
                    // Both the transmission buffer and the tx fifo are empty,
                    // and therefore the write operation must have finished.
                    // NOTE: To be 100% sure we must also check if the transmitter is in idle state
                    AUXILIARY->MU_IER &= ~AUX_MUIER_ENABLE_TXIRQ;
                    // TODO
                    // ...->upCallWriteFinished();
                }
            }

            iir = AUXILIARY->MU_IIR;
        }
    }
};
static MiniUARTIsr mini_isr;

} /* namespace serial */
} /* namespace io */
} /* namespace arctos */
