/**
 * @file
 * @copyright Copyright (c) 2022 Michael Zehrer. This file is part of ARCTOS
 * @license MIT
 * @brief pin definitions / mapping for "STM32F411 Discovery Board"
 */
#ifndef IO_PIN_STM32_BOARDS_DISCO_F411_HPP
#define IO_PIN_STM32_BOARDS_DISCO_F411_HPP
#include <stddef.h>
#include <stdint.h>

#include <arctos/io/pin/pin.hpp>
#include <arctos/io/pin/stm32/params.h>

#include CMSIS_DEVICE

namespace arctos {
namespace io {
namespace pin {

#if defined(PIN_ENABLE_GPIOA) && defined(GPIOA_BASE)
extern StmPin &pa00;
extern StmPin &pa01;
extern StmPin &pa02;
extern StmPin &pa03;
extern StmPin &pa04;
extern StmPin &pa05;
extern StmPin &pa06;
extern StmPin &pa07;
extern StmPin &pa08;
extern StmPin &pa09;
extern StmPin &pa10;
extern StmPin &pa11;
extern StmPin &pa12;
extern StmPin &pa13;
extern StmPin &pa14;
extern StmPin &pa15;
#endif

#if defined(PIN_ENABLE_GPIOB) && defined(GPIOB_BASE)
extern StmPin &pb00;
extern StmPin &pb01;
extern StmPin &pb02;
extern StmPin &pb03;
extern StmPin &pb04;
extern StmPin &pb05;
extern StmPin &pb06;
extern StmPin &pb07;
extern StmPin &pb08;
extern StmPin &pb09;
extern StmPin &pb10;

extern StmPin &pb12;
extern StmPin &pb13;
extern StmPin &pb14;
extern StmPin &pb15;
#endif

#if defined(PIN_ENABLE_GPIOC) && defined(GPIOC_BASE)
extern StmPin &pc00;
extern StmPin &pc01;
extern StmPin &pc02;
extern StmPin &pc03;
extern StmPin &pc04;
extern StmPin &pc05;
extern StmPin &pc06;
extern StmPin &pc07;
extern StmPin &pc08;
extern StmPin &pc09;
extern StmPin &pc10;
extern StmPin &pc11;
extern StmPin &pc12;
extern StmPin &pc13;
extern StmPin &pc14;
extern StmPin &pc15;
#endif

#if defined(PIN_ENABLE_GPIOD) && defined(GPIOD_BASE)
extern StmPin &pd00;
extern StmPin &pd01;
extern StmPin &pd02;
extern StmPin &pd03;
extern StmPin &pd04;
extern StmPin &pd05;
extern StmPin &pd06;
extern StmPin &pd07;
extern StmPin &pd08;
extern StmPin &pd09;
extern StmPin &pd10;
extern StmPin &pd11;
extern StmPin &pd12;
extern StmPin &pd13;
extern StmPin &pd14;
extern StmPin &pd15;
#endif

#if defined(PIN_ENABLE_GPIOE) && defined(GPIOE_BASE)
extern StmPin &pe00;
extern StmPin &pe01;
extern StmPin &pe02;
extern StmPin &pe03;
extern StmPin &pe04;
extern StmPin &pe05;
extern StmPin &pe06;
extern StmPin &pe07;
extern StmPin &pe08;
extern StmPin &pe09;
extern StmPin &pe10;
extern StmPin &pe11;
extern StmPin &pe12;
extern StmPin &pe13;
extern StmPin &pe14;
extern StmPin &pe15;
#endif

#if defined(PIN_ENABLE_GPIOH) && defined(GPIOH_BASE)
extern StmPin &ph00;
extern StmPin &ph01;
#endif

} /* namespace pin */
} /* namespace io */
} /* namespace arctos */

#endif /* IO_PIN_STM32_BOARDS_DISCO_F411_HPP */
