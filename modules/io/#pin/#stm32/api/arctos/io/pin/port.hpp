/**
 * @file
 * @copyright Copyright (c) 2022 Michael Zehrer. This file is part of ARCTOS
 * @license MIT
 * @brief stm32 specific port of the "pin" module
 */
#ifndef IO_PIN_PORT_HPP
#define IO_PIN_PORT_HPP
#include <stddef.h>
#include <stdint.h>

#include <arctos/io/pin/pin.hpp>
#include <arctos/io/pin/stm32/params.h>

#include CMSIS_DEVICE

namespace arctos {
namespace io {
namespace pin {

class StmPin : public Pin {
public:
    struct Alternate {
        enum class Function {
              kNone = 0

            , kI2cSDA
            , kI2cSCL

            , kSpiMISO
            , kSpiMOSI
            , kSpiSCK
            , kSpiNSS

            , kUartRX
            , kUartTX
            , kUartCTS
            , kUartRTS
            , kUartCK
        };
        uint32_t mode;
        void    *peripheral;
        Function function;
    };
    struct Capabilities {
        /**
         * @brief Array of (supported) alternate functions
         */
        const Alternate * alts;
        /**
         * @brief Size of the 'alts' array
         */
        size_t alts_size;
    };

    /**
     * @brief Additional capabilities supported by this pin
     * (asides from the default kInput, kOutput, ...)
     */
    const Capabilities &capabilities;

    /**
     * @brief Searches an alternate pin definition
     * @param[in] function Alternate function to search for
     * @param[in] peripheral (Optional) underlying peripheral
     * @return Search result or 'nullptr'
     */
    const Alternate *getAlternate(Alternate::Function function,
                                  void *peripheral = nullptr) const;
protected:
    constexpr StmPin(const Capabilities &caps) : capabilities(caps) {}
};


namespace mode {
extern const uint32_t kInput;
extern const uint32_t kInputFloating;
extern const uint32_t kInputPullUp;
extern const uint32_t kInputPullDown;

extern const uint32_t kOutput;
extern const uint32_t kOutputPushPull;
extern const uint32_t kOutputPushPullPullUp;
extern const uint32_t kOutputPushPullPullDown;

extern const uint32_t kOutputOpenDrain;
extern const uint32_t kOutputOpenDrainPullUp;
extern const uint32_t kOutputOpenDrainPullDown;
} /* namespace mode */

} /* namespace pin */
} /* namespace io */
} /* namespace arctos */

#if STM_BOARD == STM_BOARD_STM32F411_DISCO
#  include "stm32/boards/disco_f411.hpp"
#elif    STM_BOARD == STM_BOARD_BLACK_PILL_F401V3 \
      || STM_BOARD == STM_BOARD_BLACK_PILL_F411V2
#  include "stm32/boards/blackpill.hpp"
#endif

#endif /* IO_PIN_PORT_HPP */
