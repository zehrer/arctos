/**
 * @file
 * @copyright Copyright (c) 2022 Michael Zehrer. This file is part of ARCTOS
 * @license MIT
 * @brief pin definitions / mapping for "WeAct BlackPill"
 */
#ifndef IO_PIN_STM32_BOARDS_BLACKPILL_HPP
#define IO_PIN_STM32_BOARDS_BLACKPILL_HPP
#include <stddef.h>
#include <stdint.h>

#include <arctos/io/pin/pin.hpp>
#include <arctos/io/pin/stm32/params.h>

#include CMSIS_DEVICE

namespace arctos {
namespace io {
namespace pin {

#if defined(PIN_ENABLE_GPIOA) && defined(GPIOA_BASE)
extern StmPin &pa00;
extern StmPin &pa01;
extern StmPin &pa02;
extern StmPin &pa03;
extern StmPin &pa04;
extern StmPin &pa05;
extern StmPin &pa06;
extern StmPin &pa07;
extern StmPin &pa08;
extern StmPin &pa09;
extern StmPin &pa10;
extern StmPin &pa11;
extern StmPin &pa12;
extern StmPin &pa13;
extern StmPin &pa14;
extern StmPin &pa15;
#endif

#if defined(PIN_ENABLE_GPIOB) && defined(GPIOB_BASE)
extern StmPin &pb00;
extern StmPin &pb01;
extern StmPin &pb02;
extern StmPin &pb03;
extern StmPin &pb04;
extern StmPin &pb05;
extern StmPin &pb06;
extern StmPin &pb07;
extern StmPin &pb08;
extern StmPin &pb09;
extern StmPin &pb10;

extern StmPin &pb12;
extern StmPin &pb13;
extern StmPin &pb14;
extern StmPin &pb15;
#endif

#if defined(PIN_ENABLE_GPIOC) && defined(GPIOC_BASE)
extern StmPin &pc13;
extern StmPin &pc14;
extern StmPin &pc15;
#endif

#if defined(PIN_ENABLE_GPIOH) && defined(GPIOH_BASE)
extern StmPin &ph00;
extern StmPin &ph01;
#endif

} /* namespace pin */
} /* namespace io */
} /* namespace arctos */

#endif /* IO_PIN_STM32_BOARDS_BLACKPILL_HPP */
