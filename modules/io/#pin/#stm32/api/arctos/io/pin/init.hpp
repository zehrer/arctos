/**
 * @file
 * @copyright Copyright (c) 2022 Michael Zehrer. This file is part of ARCTOS
 * @license MIT
 * @brief pin initialization
 */
#ifndef IO_PIN_INIT_HPP
#define IO_PIN_INIT_HPP

#include <stdint.h>

namespace arctos {
namespace io {
namespace pin {

void init(void);

} /* namespace pin */
} /* namespace io */
} /* namespace arctos */

#endif /* IO_PIN_INIT_HPP */
