/**
 * @file
 * @copyright Copyright (c) 2022 Michael Zehrer. This file is part of ARCTOS
 * @license MIT
 * @brief stm32 specific port of the "pin" module
 */
#include <arctos/io/pin/port.hpp>

namespace arctos {
namespace io {
namespace pin {

const StmPin::Alternate *StmPin::getAlternate(
        StmPin::Alternate::Function function, void *peripheral) const {
    const StmPin::Alternate *alt = this->capabilities.alts;
    for (size_t i = 0; i < this->capabilities.alts_size; ++i, ++alt) {
        if (alt->function != function)
            continue;
        if (peripheral != nullptr && alt->peripheral != peripheral)
            continue;

        return alt;
    }
    return nullptr;
}

} /* namespace pin */
} /* namespace io */
} /* namespace arctos */
