/**
 * @file
 * @copyright Copyright (c) 2022 Michael Zehrer. This file is part of ARCTOS
 * @license MIT
 * @brief stm32 GPIO driver
 */
#include <arctos/io/pin/stm32/gpio.hpp>
#include <arctos/io/pin/stm32/setting.hpp>
#include <arctos/io/pin/stm32/params.h>

#include <arctos/io/stm32hal/ll_gpio.h>

#include CMSIS_DEVICE

namespace arctos {
namespace io {
namespace pin {

bool StmGpio::checkModeCompatibility(const uint32_t m) {
    StmPinSetting check;
    check.mode = m;
    bool rv;

    /* check i/o port */
    rv = (    (check.attr.port == STM_PIN_SETTING_PORT_ANY)
    #if defined(PIN_ENABLE_GPIOA) && defined(GPIOA_BASE)
           || (check.attr.port == STM_PIN_SETTING_PORT_GPIOA
                               && this->peripheral_ == GPIOA)
    #endif
    #if defined(PIN_ENABLE_GPIOB) && defined(GPIOB_BASE)
           || (check.attr.port == STM_PIN_SETTING_PORT_GPIOB
                               && this->peripheral_ == GPIOB)
    #endif
    #if defined(PIN_ENABLE_GPIOC) && defined(GPIOC_BASE)
           || (check.attr.port == STM_PIN_SETTING_PORT_GPIOC
                               && this->peripheral_ == GPIOC)
    #endif
    #if defined(PIN_ENABLE_GPIOD) && defined(GPIOD_BASE)
           || (check.attr.port == STM_PIN_SETTING_PORT_GPIOD
                               && this->peripheral_ == GPIOD)
    #endif
    #if defined(PIN_ENABLE_GPIOE) && defined(GPIOE_BASE)
           || (check.attr.port == STM_PIN_SETTING_PORT_GPIOE
                               && this->peripheral_ == GPIOE)
    #endif
    #if defined(PIN_ENABLE_GPIOF) && defined(GPIOF_BASE)
           || (check.attr.port == STM_PIN_SETTING_PORT_GPIOF
                               && this->peripheral_ == GPIOF)
    #endif
    #if defined(PIN_ENABLE_GPIOG) && defined(GPIOG_BASE)
           || (check.attr.port == STM_PIN_SETTING_PORT_GPIOG
                               && this->peripheral_ == GPIOG)
    #endif
    #if defined(PIN_ENABLE_GPIOH) && defined(GPIOH_BASE)
           || (check.attr.port == STM_PIN_SETTING_PORT_GPIOH
                               && this->peripheral_ == GPIOH)
    #endif
    #if defined(PIN_ENABLE_GPIOI) && defined(GPIOI_BASE)
           || (check.attr.port == STM_PIN_SETTING_PORT_GPIOI
                               && this->peripheral_ == GPIOI)
    #endif
    #if defined(PIN_ENABLE_GPIOJ) && defined(GPIOJ_BASE)
           || (check.attr.port == STM_PIN_SETTING_PORT_GPIOJ
                               && this->peripheral_ == GPIOJ)
    #endif
    #if defined(PIN_ENABLE_GPIOK) && defined(GPIOK_BASE)
           || (check.attr.port == STM_PIN_SETTING_PORT_GPIOK
                               && this->peripheral_ == GPIOK)
    #endif
    #if defined(PIN_ENABLE_GPIOZ) && defined(GPIOZ_BASE)
           || (check.attr.port == STM_PIN_SETTING_PORT_GPIOZ
                               && this->peripheral_ == GPIOZ)
    #endif
         );
    if (!rv) return false;

    /* check pin number */
    rv = (    (check.attr.pin == STM_PIN_SETTING_PIN_ANY)
           || (check.attr.pin | STM_PIN_SETTING_PIN_00
                       && this->pin_ == LL_GPIO_PIN_0)
           || (check.attr.pin | STM_PIN_SETTING_PIN_01
                       && this->pin_ == LL_GPIO_PIN_1)
           || (check.attr.pin | STM_PIN_SETTING_PIN_02
                       && this->pin_ == LL_GPIO_PIN_2)
           || (check.attr.pin | STM_PIN_SETTING_PIN_03
                       && this->pin_ == LL_GPIO_PIN_3)
           || (check.attr.pin | STM_PIN_SETTING_PIN_04
                       && this->pin_ == LL_GPIO_PIN_4)
           || (check.attr.pin | STM_PIN_SETTING_PIN_05
                       && this->pin_ == LL_GPIO_PIN_5)
           || (check.attr.pin | STM_PIN_SETTING_PIN_06
                       && this->pin_ == LL_GPIO_PIN_6)
           || (check.attr.pin | STM_PIN_SETTING_PIN_07
                       && this->pin_ == LL_GPIO_PIN_7)
           || (check.attr.pin | STM_PIN_SETTING_PIN_08
                       && this->pin_ == LL_GPIO_PIN_8)
           || (check.attr.pin | STM_PIN_SETTING_PIN_09
                       && this->pin_ == LL_GPIO_PIN_9)
           || (check.attr.pin | STM_PIN_SETTING_PIN_10
                       && this->pin_ == LL_GPIO_PIN_10)
           || (check.attr.pin | STM_PIN_SETTING_PIN_11
                       && this->pin_ == LL_GPIO_PIN_11)
           || (check.attr.pin | STM_PIN_SETTING_PIN_12
                       && this->pin_ == LL_GPIO_PIN_12)
           || (check.attr.pin | STM_PIN_SETTING_PIN_13
                       && this->pin_ == LL_GPIO_PIN_13)
           || (check.attr.pin | STM_PIN_SETTING_PIN_14
                       && this->pin_ == LL_GPIO_PIN_14)
           || (check.attr.pin | STM_PIN_SETTING_PIN_15
                       && this->pin_ == LL_GPIO_PIN_15)
         );
    return rv;
}

bool StmGpio::isMode(const uint32_t m) {
    if (!this->checkModeCompatibility(m))
        return false;

    StmPinSetting check;
    check.mode = m;
    bool rv;

    /* check pin function */
    uint32_t ll_mode = LL_GPIO_GetPinMode(this->peripheral_, this->pin_);
    rv = (    (check.attr.function == STM_PIN_SETTING_FUNCTION_INPUT
                                    && ll_mode == LL_GPIO_MODE_INPUT)
           || (check.attr.function == STM_PIN_SETTING_FUNCTION_OUTPUT
                                    && ll_mode == LL_GPIO_MODE_OUTPUT)
           || (check.attr.function == STM_PIN_SETTING_FUNCTION_ALTERNATE
                                    && ll_mode == LL_GPIO_MODE_ALTERNATE)
           || (check.attr.function == STM_PIN_SETTING_FUNCTION_ANALOG
                                    && ll_mode == LL_GPIO_MODE_ANALOG)
         );

    if (check.attr.function == STM_PIN_SETTING_FUNCTION_OUTPUT ||
        check.attr.function == STM_PIN_SETTING_FUNCTION_ALTERNATE) {
        /* check pin output type (if applicable) */
        uint32_t ll_otype = LL_GPIO_GetPinOutputType(this->peripheral_,
                                                     this->pin_);
        rv &= (    (check.attr.otype == STM_PIN_SETTING_OTYPE_PUSH_PULL
                                && ll_otype == LL_GPIO_OUTPUT_PUSHPULL)
                || (check.attr.otype == STM_PIN_SETTING_OTYPE_OPEN_DRAIN
                                && ll_otype == LL_GPIO_OUTPUT_OPENDRAIN)
              );
    }

    /* check pull-up / pull-down configuration */
    uint32_t ll_pull = LL_GPIO_GetPinPull(this->peripheral_, this->pin_);
    rv &= (    (check.attr.pupd == STM_PIN_SETTING_PUPD_NONE
                                  && ll_pull == LL_GPIO_PULL_NO)
            || (check.attr.pupd == STM_PIN_SETTING_PUPD_PULL_UP
                                  && ll_pull == LL_GPIO_PULL_UP)
            || (check.attr.pupd == STM_PIN_SETTING_PUPD_PULL_DOWN
                                  && ll_pull == LL_GPIO_PULL_DOWN)
          );

    if (check.attr.function == STM_PIN_SETTING_FUNCTION_ALTERNATE) {
        /* check alternate pin function (if applicable) */
        uint32_t ll_alt = 0xFF;
        switch (this->pin_) {
            case LL_GPIO_PIN_0:
            case LL_GPIO_PIN_1:
            case LL_GPIO_PIN_2:
            case LL_GPIO_PIN_3:
            case LL_GPIO_PIN_4:
            case LL_GPIO_PIN_5:
            case LL_GPIO_PIN_6:
            case LL_GPIO_PIN_7:
                ll_alt = LL_GPIO_GetAFPin_0_7(this->peripheral_, this->pin_);
                break;
            case LL_GPIO_PIN_8:
            case LL_GPIO_PIN_9:
            case LL_GPIO_PIN_10:
            case LL_GPIO_PIN_11:
            case LL_GPIO_PIN_12:
            case LL_GPIO_PIN_13:
            case LL_GPIO_PIN_14:
            case LL_GPIO_PIN_15:
                ll_alt = LL_GPIO_GetAFPin_8_15(this->peripheral_, this->pin_);
                break;
        }
        rv &= (    (check.attr.alt == STM_PIN_SETTING_ALT_AF0
                                    && ll_alt == LL_GPIO_AF_0)
                || (check.attr.alt == STM_PIN_SETTING_ALT_AF1
                                    && ll_alt == LL_GPIO_AF_1)
                || (check.attr.alt == STM_PIN_SETTING_ALT_AF2
                                    && ll_alt == LL_GPIO_AF_2)
                || (check.attr.alt == STM_PIN_SETTING_ALT_AF3
                                    && ll_alt == LL_GPIO_AF_3)
                || (check.attr.alt == STM_PIN_SETTING_ALT_AF4
                                    && ll_alt == LL_GPIO_AF_4)
                || (check.attr.alt == STM_PIN_SETTING_ALT_AF5
                                    && ll_alt == LL_GPIO_AF_5)
                || (check.attr.alt == STM_PIN_SETTING_ALT_AF6
                                    && ll_alt == LL_GPIO_AF_6)
                || (check.attr.alt == STM_PIN_SETTING_ALT_AF7
                                    && ll_alt == LL_GPIO_AF_7)
                || (check.attr.alt == STM_PIN_SETTING_ALT_AF8
                                    && ll_alt == LL_GPIO_AF_8)
                || (check.attr.alt == STM_PIN_SETTING_ALT_AF9
                                    && ll_alt == LL_GPIO_AF_9)
                || (check.attr.alt == STM_PIN_SETTING_ALT_AF10
                                    && ll_alt == LL_GPIO_AF_10)
                || (check.attr.alt == STM_PIN_SETTING_ALT_AF11
                                    && ll_alt == LL_GPIO_AF_11)
                || (check.attr.alt == STM_PIN_SETTING_ALT_AF12
                                    && ll_alt == LL_GPIO_AF_12)
                || (check.attr.alt == STM_PIN_SETTING_ALT_AF13
                                    && ll_alt == LL_GPIO_AF_13)
                || (check.attr.alt == STM_PIN_SETTING_ALT_AF14
                                    && ll_alt == LL_GPIO_AF_14)
                || (check.attr.alt == STM_PIN_SETTING_ALT_AF15
                                    && ll_alt == LL_GPIO_AF_15)
              );
    }

    return rv;
}
void StmGpio::mode(const uint32_t m) {
    if (!this->checkModeCompatibility(m))
        return;

    StmPinSetting setting;
    setting.mode = m;

    if (setting.attr.function == STM_PIN_SETTING_FUNCTION_OUTPUT ||
        setting.attr.function == STM_PIN_SETTING_FUNCTION_ALTERNATE) {
        /* set pin output type (if applicable) */
        uint32_t ll_otype = LL_GPIO_OUTPUT_OPENDRAIN;
        if (setting.attr.otype == STM_PIN_SETTING_OTYPE_PUSH_PULL)
            ll_otype = LL_GPIO_OUTPUT_PUSHPULL;
        LL_GPIO_SetPinOutputType(this->peripheral_, this->pin_, ll_otype);
    }

    /* set pull-up / pull-down configuration */
    uint32_t ll_pull = LL_GPIO_PULL_NO;
    switch (setting.attr.pupd) {
        case STM_PIN_SETTING_PUPD_PULL_UP:
            ll_pull = LL_GPIO_PULL_UP;
            break;
        case STM_PIN_SETTING_PUPD_PULL_DOWN:
            ll_pull = LL_GPIO_PULL_DOWN;
            break;
    }
    LL_GPIO_SetPinPull(this->peripheral_, this->pin_, ll_pull);

    if (setting.attr.function == STM_PIN_SETTING_FUNCTION_ALTERNATE) {
        /* set alternate pin function (if applicable) */
        uint32_t ll_alt;
        switch (setting.attr.alt) {
            case STM_PIN_SETTING_ALT_AF0:  ll_alt = LL_GPIO_AF_0;  break;
            case STM_PIN_SETTING_ALT_AF1:  ll_alt = LL_GPIO_AF_1;  break;
            case STM_PIN_SETTING_ALT_AF2:  ll_alt = LL_GPIO_AF_2;  break;
            case STM_PIN_SETTING_ALT_AF3:  ll_alt = LL_GPIO_AF_3;  break;
            case STM_PIN_SETTING_ALT_AF4:  ll_alt = LL_GPIO_AF_4;  break;
            case STM_PIN_SETTING_ALT_AF5:  ll_alt = LL_GPIO_AF_5;  break;
            case STM_PIN_SETTING_ALT_AF6:  ll_alt = LL_GPIO_AF_6;  break;
            case STM_PIN_SETTING_ALT_AF7:  ll_alt = LL_GPIO_AF_7;  break;
            case STM_PIN_SETTING_ALT_AF8:  ll_alt = LL_GPIO_AF_8;  break;
            case STM_PIN_SETTING_ALT_AF9:  ll_alt = LL_GPIO_AF_9;  break;
            case STM_PIN_SETTING_ALT_AF10: ll_alt = LL_GPIO_AF_10; break;
            case STM_PIN_SETTING_ALT_AF11: ll_alt = LL_GPIO_AF_11; break;
            case STM_PIN_SETTING_ALT_AF12: ll_alt = LL_GPIO_AF_12; break;
            case STM_PIN_SETTING_ALT_AF13: ll_alt = LL_GPIO_AF_13; break;
            case STM_PIN_SETTING_ALT_AF14: ll_alt = LL_GPIO_AF_14; break;
            case STM_PIN_SETTING_ALT_AF15: ll_alt = LL_GPIO_AF_15; break;
        }
        switch (this->pin_) {
            case LL_GPIO_PIN_0:
            case LL_GPIO_PIN_1:
            case LL_GPIO_PIN_2:
            case LL_GPIO_PIN_3:
            case LL_GPIO_PIN_4:
            case LL_GPIO_PIN_5:
            case LL_GPIO_PIN_6:
            case LL_GPIO_PIN_7:
                LL_GPIO_SetAFPin_0_7(this->peripheral_, this->pin_, ll_alt);
                break;
            case LL_GPIO_PIN_8:
            case LL_GPIO_PIN_9:
            case LL_GPIO_PIN_10:
            case LL_GPIO_PIN_11:
            case LL_GPIO_PIN_12:
            case LL_GPIO_PIN_13:
            case LL_GPIO_PIN_14:
            case LL_GPIO_PIN_15:
                LL_GPIO_SetAFPin_8_15(this->peripheral_, this->pin_, ll_alt);
                break;
        }
    }

    /* set pin function */
    uint32_t ll_mode = LL_GPIO_MODE_INPUT;
    switch (setting.attr.function) {
        case STM_PIN_SETTING_FUNCTION_OUTPUT:
            ll_mode = LL_GPIO_MODE_OUTPUT;
            break;
        case STM_PIN_SETTING_FUNCTION_ALTERNATE:
            ll_mode = LL_GPIO_MODE_ALTERNATE;
            break;
        case STM_PIN_SETTING_FUNCTION_ANALOG:
            ll_mode = LL_GPIO_MODE_ANALOG;
            break;
    }
    LL_GPIO_SetPinMode(this->peripheral_, this->pin_, ll_mode);
}

void StmGpio::set() {
    LL_GPIO_SetOutputPin(this->peripheral_, this->pin_);
}
void StmGpio::clear() {
    LL_GPIO_ResetOutputPin(this->peripheral_, this->pin_);
}

bool StmGpio::isHigh(void) {
    return LL_GPIO_IsInputPinSet(this->peripheral_, this->pin_);
}

} /* namespace pin */
} /* namespace io */
} /* namespace arctos */
