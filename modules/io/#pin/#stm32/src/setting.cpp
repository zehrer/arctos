/**
 * @file
 * @copyright Copyright (c) 2022 Michael Zehrer. This file is part of ARCTOS
 * @license MIT
 * @brief pin settings
 */
#include <arctos/io/pin/stm32/setting.hpp>

namespace arctos {
namespace io {
namespace pin {
namespace mode {

extern const uint32_t kInputFloating =
(
    (STM_PIN_SETTING_PIN_ANY                << STM_PIN_SETTING_PIN_SHIFT)      |
    (STM_PIN_SETTING_PORT_ANY               << STM_PIN_SETTING_PORT_SHIFT)     |
    (STM_PIN_SETTING_FUNCTION_INPUT         << STM_PIN_SETTING_FUNCTION_SHIFT) |
    (STM_PIN_SETTING_PUPD_NONE              << STM_PIN_SETTING_PUPD_SHIFT)
);
extern const uint32_t kInput = kInputFloating;
extern const uint32_t kInputPullUp =
(
    (STM_PIN_SETTING_PIN_ANY                << STM_PIN_SETTING_PIN_SHIFT)      |
    (STM_PIN_SETTING_PORT_ANY               << STM_PIN_SETTING_PORT_SHIFT)     |
    (STM_PIN_SETTING_FUNCTION_INPUT         << STM_PIN_SETTING_FUNCTION_SHIFT) |
    (STM_PIN_SETTING_PUPD_PULL_UP           << STM_PIN_SETTING_PUPD_SHIFT)
);
extern const uint32_t kInputPullDown =
(
    (STM_PIN_SETTING_PIN_ANY                << STM_PIN_SETTING_PIN_SHIFT)      |
    (STM_PIN_SETTING_PORT_ANY               << STM_PIN_SETTING_PORT_SHIFT)     |
    (STM_PIN_SETTING_FUNCTION_INPUT         << STM_PIN_SETTING_FUNCTION_SHIFT) |
    (STM_PIN_SETTING_PUPD_PULL_DOWN         << STM_PIN_SETTING_PUPD_SHIFT)
);

extern const uint32_t kOutputPushPull =
(
    (STM_PIN_SETTING_PIN_ANY                << STM_PIN_SETTING_PIN_SHIFT)      |
    (STM_PIN_SETTING_PORT_ANY               << STM_PIN_SETTING_PORT_SHIFT)     |
    (STM_PIN_SETTING_FUNCTION_OUTPUT        << STM_PIN_SETTING_FUNCTION_SHIFT) |
    (STM_PIN_SETTING_OTYPE_PUSH_PULL        << STM_PIN_SETTING_OTYPE_SHIFT)    |
    (STM_PIN_SETTING_PUPD_NONE              << STM_PIN_SETTING_PUPD_SHIFT)
);
extern const uint32_t kOutput = kOutputPushPull;
extern const uint32_t kOutputPushPullPullUp =
(
    (STM_PIN_SETTING_PIN_ANY                << STM_PIN_SETTING_PIN_SHIFT)      |
    (STM_PIN_SETTING_PORT_ANY               << STM_PIN_SETTING_PORT_SHIFT)     |
    (STM_PIN_SETTING_FUNCTION_OUTPUT        << STM_PIN_SETTING_FUNCTION_SHIFT) |
    (STM_PIN_SETTING_OTYPE_PUSH_PULL        << STM_PIN_SETTING_OTYPE_SHIFT)    |
    (STM_PIN_SETTING_PUPD_PULL_UP           << STM_PIN_SETTING_PUPD_SHIFT)
);
extern const uint32_t kOutputPushPullPullDown =
(
    (STM_PIN_SETTING_PIN_ANY                << STM_PIN_SETTING_PIN_SHIFT)      |
    (STM_PIN_SETTING_PORT_ANY               << STM_PIN_SETTING_PORT_SHIFT)     |
    (STM_PIN_SETTING_FUNCTION_OUTPUT        << STM_PIN_SETTING_FUNCTION_SHIFT) |
    (STM_PIN_SETTING_OTYPE_PUSH_PULL        << STM_PIN_SETTING_OTYPE_SHIFT)    |
    (STM_PIN_SETTING_PUPD_PULL_DOWN         << STM_PIN_SETTING_PUPD_SHIFT)
);

extern const uint32_t kOutputOpenDrain =
(
    (STM_PIN_SETTING_PIN_ANY                << STM_PIN_SETTING_PIN_SHIFT)      |
    (STM_PIN_SETTING_PORT_ANY               << STM_PIN_SETTING_PORT_SHIFT)     |
    (STM_PIN_SETTING_FUNCTION_OUTPUT        << STM_PIN_SETTING_FUNCTION_SHIFT) |
    (STM_PIN_SETTING_OTYPE_OPEN_DRAIN       << STM_PIN_SETTING_OTYPE_SHIFT)    |
    (STM_PIN_SETTING_PUPD_NONE              << STM_PIN_SETTING_PUPD_SHIFT)
);
extern const uint32_t kOutputOpenDrainPullUp =
(
    (STM_PIN_SETTING_PIN_ANY                << STM_PIN_SETTING_PIN_SHIFT)      |
    (STM_PIN_SETTING_PORT_ANY               << STM_PIN_SETTING_PORT_SHIFT)     |
    (STM_PIN_SETTING_FUNCTION_OUTPUT        << STM_PIN_SETTING_FUNCTION_SHIFT) |
    (STM_PIN_SETTING_OTYPE_OPEN_DRAIN       << STM_PIN_SETTING_OTYPE_SHIFT)    |
    (STM_PIN_SETTING_PUPD_PULL_UP           << STM_PIN_SETTING_PUPD_SHIFT)
);
extern const uint32_t kOutputOpenDrainPullDown =
(
    (STM_PIN_SETTING_PIN_ANY                << STM_PIN_SETTING_PIN_SHIFT)      |
    (STM_PIN_SETTING_PORT_ANY               << STM_PIN_SETTING_PORT_SHIFT)     |
    (STM_PIN_SETTING_FUNCTION_OUTPUT        << STM_PIN_SETTING_FUNCTION_SHIFT) |
    (STM_PIN_SETTING_OTYPE_OPEN_DRAIN       << STM_PIN_SETTING_OTYPE_SHIFT)    |
    (STM_PIN_SETTING_PUPD_PULL_DOWN         << STM_PIN_SETTING_PUPD_SHIFT)
);

} /* namespace mode */
} /* namespace pin */
} /* namespace io */
} /* namespace arctos */
