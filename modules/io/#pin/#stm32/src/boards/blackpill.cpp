/**
 * @file
 * @copyright Copyright (c) 2022 Michael Zehrer. This file is part of ARCTOS
 * @license MIT
 * @brief pin definitions / mapping for "WeAct BlackPill"
 */
#include <arctos/io/pin/stm32/gpio.hpp>
#include <arctos/io/pin/stm32/setting.hpp>

#include <arctos/io/stm32hal/ll_gpio.h>

#include CMSIS_DEVICE

namespace arctos {
namespace io {
namespace pin {

#define STM_PIN_CAPABILITIES(ALTS) \
    .alts = ALTS,\
    .alts_size = (sizeof(ALTS) / sizeof(StmPin::Alternate))

static const StmPin::Capabilities no_cap { .alts = nullptr, .alts_size = 0 };

#if defined(PIN_ENABLE_GPIOA) && defined(GPIOA_BASE)
/*******************************************************************************
 * PA00
 ******************************************************************************/
static const StmPin::Alternate pa00_alts[] = {
{
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_00,
                            STM_PIN_SETTING_PORT_GPIOA,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_PUSH_PULL,
                            STM_PIN_SETTING_PUPD_PULL_UP,
                            STM_PIN_SETTING_ALT_AF7),
    .peripheral = USART2,
    .function   = StmPin::Alternate::Function::kUartCTS
}
};
static const StmPin::Capabilities pa00_cap { STM_PIN_CAPABILITIES(pa00_alts) };
static StmGpio pa00_obj(GPIOA, LL_GPIO_PIN_0, pa00_cap);
StmPin &pa00 = pa00_obj;


/*******************************************************************************
 * PA01
 ******************************************************************************/
static const StmPin::Alternate pa01_alts[] = {
#ifdef STM32F411xE
{
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_01,
                            STM_PIN_SETTING_PORT_GPIOA,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_PUSH_PULL,
                            STM_PIN_SETTING_PUPD_PULL_UP,
                            STM_PIN_SETTING_ALT_AF5),
    .peripheral = SPI4,
    .function   = StmPin::Alternate::Function::kSpiMOSI
},
#endif
{
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_01,
                            STM_PIN_SETTING_PORT_GPIOA,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_PUSH_PULL,
                            STM_PIN_SETTING_PUPD_PULL_UP,
                            STM_PIN_SETTING_ALT_AF7),
    .peripheral = USART2,
    .function   = StmPin::Alternate::Function::kUartRTS
}
};
static const StmPin::Capabilities pa01_cap { STM_PIN_CAPABILITIES(pa01_alts) };
static StmGpio pa01_obj(GPIOA, LL_GPIO_PIN_1, pa01_cap);
StmPin &pa01 = pa01_obj;


/*******************************************************************************
 * PA02
 ******************************************************************************/
static const StmPin::Alternate pa02_alts[] = {
{
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_02,
                            STM_PIN_SETTING_PORT_GPIOA,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_PUSH_PULL,
                            STM_PIN_SETTING_PUPD_PULL_UP,
                            STM_PIN_SETTING_ALT_AF7),
    .peripheral = USART2,
    .function   = StmPin::Alternate::Function::kUartTX
}
};
static const StmPin::Capabilities pa02_cap { STM_PIN_CAPABILITIES(pa02_alts) };
static StmGpio pa02_obj(GPIOA, LL_GPIO_PIN_2, pa02_cap);
StmPin &pa02 = pa02_obj;


/*******************************************************************************
 * PA03
 ******************************************************************************/
static const StmPin::Alternate pa03_alts[] = {
{
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_03,
                            STM_PIN_SETTING_PORT_GPIOA,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_PUSH_PULL,
                            STM_PIN_SETTING_PUPD_PULL_UP,
                            STM_PIN_SETTING_ALT_AF7),
    .peripheral = USART2,
    .function   = StmPin::Alternate::Function::kUartRX
}
};
static const StmPin::Capabilities pa03_cap { STM_PIN_CAPABILITIES(pa03_alts) };
static StmGpio pa03_obj(GPIOA, LL_GPIO_PIN_3, pa03_cap);
StmPin &pa03 = pa03_obj;


/*******************************************************************************
 * PA04
 ******************************************************************************/
static const StmPin::Alternate pa04_alts[] = {
{
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_04,
                            STM_PIN_SETTING_PORT_GPIOA,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_PUSH_PULL,
                            STM_PIN_SETTING_PUPD_PULL_UP,
                            STM_PIN_SETTING_ALT_AF5),
    .peripheral = SPI1,
    .function   = StmPin::Alternate::Function::kSpiNSS
}, {
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_04,
                            STM_PIN_SETTING_PORT_GPIOA,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_PUSH_PULL,
                            STM_PIN_SETTING_PUPD_PULL_UP,
                            STM_PIN_SETTING_ALT_AF6),
    .peripheral = SPI3,
    .function   = StmPin::Alternate::Function::kSpiNSS
}, {
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_04,
                            STM_PIN_SETTING_PORT_GPIOA,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_PUSH_PULL,
                            STM_PIN_SETTING_PUPD_PULL_UP,
                            STM_PIN_SETTING_ALT_AF7),
    .peripheral = USART2,
    .function   = StmPin::Alternate::Function::kUartCK
}
};
static const StmPin::Capabilities pa04_cap { STM_PIN_CAPABILITIES(pa04_alts) };
static StmGpio pa04_obj(GPIOA, LL_GPIO_PIN_4, pa04_cap);
StmPin &pa04 = pa04_obj;


/*******************************************************************************
 * PA05
 ******************************************************************************/
static const StmPin::Alternate pa05_alts[] = {
{
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_05,
                            STM_PIN_SETTING_PORT_GPIOA,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_PUSH_PULL,
                            STM_PIN_SETTING_PUPD_PULL_UP,
                            STM_PIN_SETTING_ALT_AF5),
    .peripheral = SPI1,
    .function   = StmPin::Alternate::Function::kSpiSCK
}
};
static const StmPin::Capabilities pa05_cap { STM_PIN_CAPABILITIES(pa05_alts) };
static StmGpio pa05_obj(GPIOA, LL_GPIO_PIN_5, pa05_cap);
StmPin &pa05 = pa05_obj;


/*******************************************************************************
 * PA06
 ******************************************************************************/
static const StmPin::Alternate pa06_alts[] = {
{
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_06,
                            STM_PIN_SETTING_PORT_GPIOA,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_PUSH_PULL,
                            STM_PIN_SETTING_PUPD_PULL_UP,
                            STM_PIN_SETTING_ALT_AF5),
    .peripheral = SPI1,
    .function   = StmPin::Alternate::Function::kSpiMISO
}
};
static const StmPin::Capabilities pa06_cap { STM_PIN_CAPABILITIES(pa06_alts) };
static StmGpio pa06_obj(GPIOA, LL_GPIO_PIN_6, pa06_cap);
StmPin &pa06 = pa06_obj;


/*******************************************************************************
 * PA07
 ******************************************************************************/
static const StmPin::Alternate pa07_alts[] = {
{
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_07,
                            STM_PIN_SETTING_PORT_GPIOA,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_PUSH_PULL,
                            STM_PIN_SETTING_PUPD_PULL_UP,
                            STM_PIN_SETTING_ALT_AF5),
    .peripheral = SPI1,
    .function   = StmPin::Alternate::Function::kSpiMOSI
}
};
static const StmPin::Capabilities pa07_cap { STM_PIN_CAPABILITIES(pa07_alts) };
static StmGpio pa07_obj(GPIOA, LL_GPIO_PIN_7, pa07_cap);
StmPin &pa07 = pa07_obj;


/*******************************************************************************
 * PA08
 ******************************************************************************/
static const StmPin::Alternate pa08_alts[] = {
{
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_08,
                            STM_PIN_SETTING_PORT_GPIOA,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_OPEN_DRAIN,
                            STM_PIN_SETTING_PUPD_NONE,
                            STM_PIN_SETTING_ALT_AF4),
    .peripheral = I2C3,
    .function   = StmPin::Alternate::Function::kI2cSCL
}, {
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_08,
                            STM_PIN_SETTING_PORT_GPIOA,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_PUSH_PULL,
                            STM_PIN_SETTING_PUPD_PULL_UP,
                            STM_PIN_SETTING_ALT_AF7),
    .peripheral = USART1,
    .function   = StmPin::Alternate::Function::kUartCK
}
};
static const StmPin::Capabilities pa08_cap { STM_PIN_CAPABILITIES(pa08_alts) };
static StmGpio pa08_obj(GPIOA, LL_GPIO_PIN_8, pa08_cap);
StmPin &pa08 = pa08_obj;


/*******************************************************************************
 * PA09
 ******************************************************************************/
static const StmPin::Alternate pa09_alts[] = {
{
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_09,
                            STM_PIN_SETTING_PORT_GPIOA,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_PUSH_PULL,
                            STM_PIN_SETTING_PUPD_PULL_UP,
                            STM_PIN_SETTING_ALT_AF7),
    .peripheral = USART1,
    .function   = StmPin::Alternate::Function::kUartTX
}
};
static const StmPin::Capabilities pa09_cap { STM_PIN_CAPABILITIES(pa09_alts) };
static StmGpio pa09_obj(GPIOA, LL_GPIO_PIN_9, pa09_cap);
StmPin &pa09 = pa09_obj;


/*******************************************************************************
 * PA10
 ******************************************************************************/
static const StmPin::Alternate pa10_alts[] = {
#ifdef STM32F411xE
{
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_10,
                            STM_PIN_SETTING_PORT_GPIOA,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_PUSH_PULL,
                            STM_PIN_SETTING_PUPD_PULL_UP,
                            STM_PIN_SETTING_ALT_AF6),
    .peripheral = SPI5,
    .function   = StmPin::Alternate::Function::kSpiMOSI
},
#endif
{
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_10,
                            STM_PIN_SETTING_PORT_GPIOA,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_PUSH_PULL,
                            STM_PIN_SETTING_PUPD_PULL_UP,
                            STM_PIN_SETTING_ALT_AF7),
    .peripheral = USART1,
    .function   = StmPin::Alternate::Function::kUartRX
}
};
static const StmPin::Capabilities pa10_cap { STM_PIN_CAPABILITIES(pa10_alts) };
static StmGpio pa10_obj(GPIOA, LL_GPIO_PIN_10, pa10_cap);
StmPin &pa10 = pa10_obj;


/*******************************************************************************
 * PA11
 ******************************************************************************/
static const StmPin::Alternate pa11_alts[] = {
#ifdef STM32F411xE
{
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_11,
                            STM_PIN_SETTING_PORT_GPIOA,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_PUSH_PULL,
                            STM_PIN_SETTING_PUPD_PULL_UP,
                            STM_PIN_SETTING_ALT_AF6),
    .peripheral = SPI4,
    .function   = StmPin::Alternate::Function::kSpiMISO
},
#endif
{
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_11,
                            STM_PIN_SETTING_PORT_GPIOA,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_PUSH_PULL,
                            STM_PIN_SETTING_PUPD_PULL_UP,
                            STM_PIN_SETTING_ALT_AF7),
    .peripheral = USART1,
    .function   = StmPin::Alternate::Function::kUartCTS
}, {
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_11,
                            STM_PIN_SETTING_PORT_GPIOA,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_PUSH_PULL,
                            STM_PIN_SETTING_PUPD_PULL_UP,
                            STM_PIN_SETTING_ALT_AF8),
    .peripheral = USART6,
    .function   = StmPin::Alternate::Function::kUartTX
}
};
static const StmPin::Capabilities pa11_cap { STM_PIN_CAPABILITIES(pa11_alts) };
static StmGpio pa11_obj(GPIOA, LL_GPIO_PIN_11, pa11_cap);
StmPin &pa11 = pa11_obj;


/*******************************************************************************
 * PA12
 ******************************************************************************/
static const StmPin::Alternate pa12_alts[] = {
#ifdef STM32F411xE
{
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_12,
                            STM_PIN_SETTING_PORT_GPIOA,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_PUSH_PULL,
                            STM_PIN_SETTING_PUPD_PULL_UP,
                            STM_PIN_SETTING_ALT_AF6),
    .peripheral = SPI5,
    .function   = StmPin::Alternate::Function::kSpiMISO
},
#endif
{
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_12,
                            STM_PIN_SETTING_PORT_GPIOA,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_PUSH_PULL,
                            STM_PIN_SETTING_PUPD_PULL_UP,
                            STM_PIN_SETTING_ALT_AF7),
    .peripheral = USART1,
    .function   = StmPin::Alternate::Function::kUartRTS
}, {
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_12,
                            STM_PIN_SETTING_PORT_GPIOA,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_PUSH_PULL,
                            STM_PIN_SETTING_PUPD_PULL_UP,
                            STM_PIN_SETTING_ALT_AF8),
    .peripheral = USART6,
    .function   = StmPin::Alternate::Function::kUartRX
}
};
static const StmPin::Capabilities pa12_cap { STM_PIN_CAPABILITIES(pa12_alts) };
static StmGpio pa12_obj(GPIOA, LL_GPIO_PIN_12, pa12_cap);
StmPin &pa12 = pa12_obj;


/*******************************************************************************
 * PA13
 ******************************************************************************/
static StmGpio pa13_obj(GPIOA, LL_GPIO_PIN_13, no_cap);
StmPin &pa13 = pa13_obj;


/*******************************************************************************
 * PA14
 ******************************************************************************/
static StmGpio pa14_obj(GPIOA, LL_GPIO_PIN_14, no_cap);
StmPin &pa14 = pa14_obj;


/*******************************************************************************
 * PA15
 ******************************************************************************/
static const StmPin::Alternate pa15_alts[] = {
{
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_15,
                            STM_PIN_SETTING_PORT_GPIOA,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_PUSH_PULL,
                            STM_PIN_SETTING_PUPD_PULL_UP,
                            STM_PIN_SETTING_ALT_AF5),
    .peripheral = SPI1,
    .function   = StmPin::Alternate::Function::kSpiNSS
}, {
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_15,
                            STM_PIN_SETTING_PORT_GPIOA,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_PUSH_PULL,
                            STM_PIN_SETTING_PUPD_PULL_UP,
                            STM_PIN_SETTING_ALT_AF6),
    .peripheral = SPI3,
    .function   = StmPin::Alternate::Function::kSpiNSS
}
#ifdef STM32F411xE
, {
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_15,
                            STM_PIN_SETTING_PORT_GPIOA,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_PUSH_PULL,
                            STM_PIN_SETTING_PUPD_PULL_UP,
                            STM_PIN_SETTING_ALT_AF7),
    .peripheral = USART1,
    .function   = StmPin::Alternate::Function::kUartTX
}
#endif
};
static const StmPin::Capabilities pa15_cap { STM_PIN_CAPABILITIES(pa15_alts) };
static StmGpio pa15_obj(GPIOA, LL_GPIO_PIN_15, pa15_cap);
StmPin &pa15 = pa15_obj;
#endif /* defined(PIN_ENABLE_GPIOA) && defined(GPIOA_BASE) */

#if defined(PIN_ENABLE_GPIOB) && defined(GPIOB_BASE)
/*******************************************************************************
 * PB00
 ******************************************************************************/
#ifdef STM32F411xE
static const StmPin::Alternate pb00_alts[] = {
{
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_00,
                            STM_PIN_SETTING_PORT_GPIOB,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_PUSH_PULL,
                            STM_PIN_SETTING_PUPD_PULL_UP,
                            STM_PIN_SETTING_ALT_AF6),
    .peripheral = SPI5,
    .function   = StmPin::Alternate::Function::kSpiSCK
}
};
static const StmPin::Capabilities pb00_cap { STM_PIN_CAPABILITIES(pb00_alts) };
static StmGpio pb00_obj(GPIOB, LL_GPIO_PIN_0, pb00_cap);
#else
static StmGpio pb00_obj(GPIOB, LL_GPIO_PIN_0, no_cap);
#endif
StmPin &pb00 = pb00_obj;


/*******************************************************************************
 * PB01
 ******************************************************************************/
#ifdef STM32F411xE
static const StmPin::Alternate pb01_alts[] = {
{
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_01,
                            STM_PIN_SETTING_PORT_GPIOB,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_PUSH_PULL,
                            STM_PIN_SETTING_PUPD_PULL_UP,
                            STM_PIN_SETTING_ALT_AF6),
    .peripheral = SPI5,
    .function   = StmPin::Alternate::Function::kSpiNSS
}
};
static const StmPin::Capabilities pb01_cap { STM_PIN_CAPABILITIES(pb01_alts) };
static StmGpio pb01_obj(GPIOB, LL_GPIO_PIN_1, pb01_cap);
#else
static StmGpio pb01_obj(GPIOB, LL_GPIO_PIN_1, no_cap);
#endif
StmPin &pb01 = pb01_obj;


/*******************************************************************************
 * PB02
 ******************************************************************************/
static StmGpio pb02_obj(GPIOB, LL_GPIO_PIN_2, no_cap);
StmPin &pb02 = pb02_obj;


/*******************************************************************************
 * PB03
 ******************************************************************************/
static const StmPin::Alternate pb03_alts[] = {
{
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_03,
                            STM_PIN_SETTING_PORT_GPIOB,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_PUSH_PULL,
                            STM_PIN_SETTING_PUPD_PULL_UP,
                            STM_PIN_SETTING_ALT_AF5),
    .peripheral = SPI1,
    .function   = StmPin::Alternate::Function::kSpiSCK
}, {
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_03,
                            STM_PIN_SETTING_PORT_GPIOB,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_PUSH_PULL,
                            STM_PIN_SETTING_PUPD_PULL_UP,
                            STM_PIN_SETTING_ALT_AF6),
    .peripheral = SPI3,
    .function   = StmPin::Alternate::Function::kSpiSCK
},
#ifdef STM32F411xE
{
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_03,
                            STM_PIN_SETTING_PORT_GPIOB,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_PUSH_PULL,
                            STM_PIN_SETTING_PUPD_PULL_UP,
                            STM_PIN_SETTING_ALT_AF7),
    .peripheral = USART1,
    .function   = StmPin::Alternate::Function::kUartRX
},
#endif
{
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_03,
                            STM_PIN_SETTING_PORT_GPIOB,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_OPEN_DRAIN,
                            STM_PIN_SETTING_PUPD_NONE,
                            STM_PIN_SETTING_ALT_AF9),
    .peripheral = I2C2,
    .function   = StmPin::Alternate::Function::kI2cSDA
}
};
static const StmPin::Capabilities pb03_cap { STM_PIN_CAPABILITIES(pb03_alts) };
static StmGpio pb03_obj(GPIOB, LL_GPIO_PIN_3, pb03_cap);
StmPin &pb03 = pb03_obj;


/*******************************************************************************
 * PB04
 ******************************************************************************/
static const StmPin::Alternate pb04_alts[] = {
{
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_04,
                            STM_PIN_SETTING_PORT_GPIOB,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_PUSH_PULL,
                            STM_PIN_SETTING_PUPD_PULL_UP,
                            STM_PIN_SETTING_ALT_AF5),
    .peripheral = SPI1,
    .function   = StmPin::Alternate::Function::kSpiMISO
}, {
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_04,
                            STM_PIN_SETTING_PORT_GPIOB,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_PUSH_PULL,
                            STM_PIN_SETTING_PUPD_PULL_UP,
                            STM_PIN_SETTING_ALT_AF6),
    .peripheral = SPI3,
    .function   = StmPin::Alternate::Function::kSpiMISO
}, {
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_04,
                            STM_PIN_SETTING_PORT_GPIOB,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_OPEN_DRAIN,
                            STM_PIN_SETTING_PUPD_NONE,
                            STM_PIN_SETTING_ALT_AF9),
    .peripheral = I2C3,
    .function   = StmPin::Alternate::Function::kI2cSDA
}
};
static const StmPin::Capabilities pb04_cap { STM_PIN_CAPABILITIES(pb04_alts) };
static StmGpio pb04_obj(GPIOB, LL_GPIO_PIN_4, pb04_cap);
StmPin &pb04 = pb04_obj;


/*******************************************************************************
 * PB05
 ******************************************************************************/
static const StmPin::Alternate pb05_alts[] = {
{
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_05,
                            STM_PIN_SETTING_PORT_GPIOB,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_PUSH_PULL,
                            STM_PIN_SETTING_PUPD_PULL_UP,
                            STM_PIN_SETTING_ALT_AF5),
    .peripheral = SPI1,
    .function   = StmPin::Alternate::Function::kSpiMOSI
}, {
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_05,
                            STM_PIN_SETTING_PORT_GPIOB,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_PUSH_PULL,
                            STM_PIN_SETTING_PUPD_PULL_UP,
                            STM_PIN_SETTING_ALT_AF6),
    .peripheral = SPI3,
    .function   = StmPin::Alternate::Function::kSpiMOSI
}
};
static const StmPin::Capabilities pb05_cap { STM_PIN_CAPABILITIES(pb05_alts) };
static StmGpio pb05_obj(GPIOB, LL_GPIO_PIN_5, pb05_cap);
StmPin &pb05 = pb05_obj;


/*******************************************************************************
 * PB06
 ******************************************************************************/
static const StmPin::Alternate pb06_alts[] = {
{
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_06,
                            STM_PIN_SETTING_PORT_GPIOB,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_OPEN_DRAIN,
                            STM_PIN_SETTING_PUPD_NONE,
                            STM_PIN_SETTING_ALT_AF4),
    .peripheral = I2C1,
    .function   = StmPin::Alternate::Function::kI2cSCL
}, {
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_06,
                            STM_PIN_SETTING_PORT_GPIOB,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_PUSH_PULL,
                            STM_PIN_SETTING_PUPD_PULL_UP,
                            STM_PIN_SETTING_ALT_AF7),
    .peripheral = USART1,
    .function   = StmPin::Alternate::Function::kUartTX
}
};
static const StmPin::Capabilities pb06_cap { STM_PIN_CAPABILITIES(pb06_alts) };
static StmGpio pb06_obj(GPIOB, LL_GPIO_PIN_6, pb06_cap);
StmPin &pb06 = pb06_obj;


/*******************************************************************************
 * PB07
 ******************************************************************************/
static const StmPin::Alternate pb07_alts[] = {
{
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_07,
                            STM_PIN_SETTING_PORT_GPIOB,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_OPEN_DRAIN,
                            STM_PIN_SETTING_PUPD_NONE,
                            STM_PIN_SETTING_ALT_AF4),
    .peripheral = I2C1,
    .function   = StmPin::Alternate::Function::kI2cSDA
}, {
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_07,
                            STM_PIN_SETTING_PORT_GPIOB,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_PUSH_PULL,
                            STM_PIN_SETTING_PUPD_PULL_UP,
                            STM_PIN_SETTING_ALT_AF7),
    .peripheral = USART1,
    .function   = StmPin::Alternate::Function::kUartRX
}
};
static const StmPin::Capabilities pb07_cap { STM_PIN_CAPABILITIES(pb07_alts) };
static StmGpio pb07_obj(GPIOB, LL_GPIO_PIN_7, pb07_cap);
StmPin &pb07 = pb07_obj;


/*******************************************************************************
 * PB08
 ******************************************************************************/
static const StmPin::Alternate pb08_alts[] = {
{
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_08,
                            STM_PIN_SETTING_PORT_GPIOB,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_OPEN_DRAIN,
                            STM_PIN_SETTING_PUPD_NONE,
                            STM_PIN_SETTING_ALT_AF4),
    .peripheral = I2C1,
    .function   = StmPin::Alternate::Function::kI2cSCL
}
#ifdef STM32F411xE
, {
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_08,
                            STM_PIN_SETTING_PORT_GPIOB,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_PUSH_PULL,
                            STM_PIN_SETTING_PUPD_PULL_UP,
                            STM_PIN_SETTING_ALT_AF6),
    .peripheral = SPI5,
    .function   = StmPin::Alternate::Function::kSpiMOSI
}, {
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_08,
                            STM_PIN_SETTING_PORT_GPIOB,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_OPEN_DRAIN,
                            STM_PIN_SETTING_PUPD_NONE,
                            STM_PIN_SETTING_ALT_AF9),
    .peripheral = I2C3,
    .function   = StmPin::Alternate::Function::kI2cSDA
}
#endif
};
static const StmPin::Capabilities pb08_cap { STM_PIN_CAPABILITIES(pb08_alts) };
static StmGpio pb08_obj(GPIOB, LL_GPIO_PIN_8, pb08_cap);
StmPin &pb08 = pb08_obj;


/*******************************************************************************
 * PB09
 ******************************************************************************/
static const StmPin::Alternate pb09_alts[] = {
{
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_09,
                            STM_PIN_SETTING_PORT_GPIOB,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_OPEN_DRAIN,
                            STM_PIN_SETTING_PUPD_NONE,
                            STM_PIN_SETTING_ALT_AF4),
    .peripheral = I2C1,
    .function   = StmPin::Alternate::Function::kI2cSDA
}, {
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_09,
                            STM_PIN_SETTING_PORT_GPIOB,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_PUSH_PULL,
                            STM_PIN_SETTING_PUPD_PULL_UP,
                            STM_PIN_SETTING_ALT_AF5),
    .peripheral = SPI2,
    .function   = StmPin::Alternate::Function::kSpiNSS
}
#ifdef STM32F411xE
, {
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_09,
                            STM_PIN_SETTING_PORT_GPIOB,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_OPEN_DRAIN,
                            STM_PIN_SETTING_PUPD_NONE,
                            STM_PIN_SETTING_ALT_AF9),
    .peripheral = I2C2,
    .function   = StmPin::Alternate::Function::kI2cSDA
}
#endif
};
static const StmPin::Capabilities pb09_cap { STM_PIN_CAPABILITIES(pb09_alts) };
static StmGpio pb09_obj(GPIOB, LL_GPIO_PIN_9, pb09_cap);
StmPin &pb09 = pb09_obj;


/*******************************************************************************
 * PB10
 ******************************************************************************/
static const StmPin::Alternate pb10_alts[] = {
{
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_10,
                            STM_PIN_SETTING_PORT_GPIOB,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_OPEN_DRAIN,
                            STM_PIN_SETTING_PUPD_NONE,
                            STM_PIN_SETTING_ALT_AF4),
    .peripheral = I2C2,
    .function   = StmPin::Alternate::Function::kI2cSCL
}, {
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_10,
                            STM_PIN_SETTING_PORT_GPIOB,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_PUSH_PULL,
                            STM_PIN_SETTING_PUPD_PULL_UP,
                            STM_PIN_SETTING_ALT_AF5),
    .peripheral = SPI2,
    .function   = StmPin::Alternate::Function::kSpiSCK
}
};
static const StmPin::Capabilities pb10_cap { STM_PIN_CAPABILITIES(pb10_alts) };
static StmGpio pb10_obj(GPIOB, LL_GPIO_PIN_10, pb10_cap);
StmPin &pb10 = pb10_obj;


/*******************************************************************************
 * PB11
 ******************************************************************************/
/*
static const StmPin::Alternate pb11_alts[] = {
{
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_11,
                            STM_PIN_SETTING_PORT_GPIOB,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_OPEN_DRAIN,
                            STM_PIN_SETTING_PUPD_NONE,
                            STM_PIN_SETTING_ALT_AF4),
    .peripheral = I2C2,
    .function   = StmPin::Alternate::Function::kI2cSDA
}
};
static const StmPin::Capabilities pb11_cap { STM_PIN_CAPABILITIES(pb11_alts) };
static StmGpio pb11_obj(GPIOB, LL_GPIO_PIN_11, pb11_cap);
StmPin &pb11 = pb11_obj;
*/


/*******************************************************************************
 * PB12
 ******************************************************************************/
static const StmPin::Alternate pb12_alts[] = {
{
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_12,
                            STM_PIN_SETTING_PORT_GPIOB,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_PUSH_PULL,
                            STM_PIN_SETTING_PUPD_PULL_UP,
                            STM_PIN_SETTING_ALT_AF5),
    .peripheral = SPI2,
    .function   = StmPin::Alternate::Function::kSpiNSS
}
#ifdef STM32F411xE
, {
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_12,
                            STM_PIN_SETTING_PORT_GPIOB,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_PUSH_PULL,
                            STM_PIN_SETTING_PUPD_PULL_UP,
                            STM_PIN_SETTING_ALT_AF6),
    .peripheral = SPI4,
    .function   = StmPin::Alternate::Function::kSpiNSS
}, {
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_12,
                            STM_PIN_SETTING_PORT_GPIOB,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_PUSH_PULL,
                            STM_PIN_SETTING_PUPD_PULL_UP,
                            STM_PIN_SETTING_ALT_AF7),
    .peripheral = SPI3,
    .function   = StmPin::Alternate::Function::kSpiSCK
}
#endif
};
static const StmPin::Capabilities pb12_cap { STM_PIN_CAPABILITIES(pb12_alts) };
static StmGpio pb12_obj(GPIOB, LL_GPIO_PIN_12, pb12_cap);
StmPin &pb12 = pb12_obj;


/*******************************************************************************
 * PB13
 ******************************************************************************/
static const StmPin::Alternate pb13_alts[] = {
{
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_13,
                            STM_PIN_SETTING_PORT_GPIOB,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_PUSH_PULL,
                            STM_PIN_SETTING_PUPD_PULL_UP,
                            STM_PIN_SETTING_ALT_AF5),
    .peripheral = SPI2,
    .function   = StmPin::Alternate::Function::kSpiSCK
}
#ifdef STM32F411xE
, {
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_13,
                            STM_PIN_SETTING_PORT_GPIOB,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_PUSH_PULL,
                            STM_PIN_SETTING_PUPD_PULL_UP,
                            STM_PIN_SETTING_ALT_AF6),
    .peripheral = SPI4,
    .function   = StmPin::Alternate::Function::kSpiSCK
}
#endif
};
static const StmPin::Capabilities pb13_cap { STM_PIN_CAPABILITIES(pb13_alts) };
static StmGpio pb13_obj(GPIOB, LL_GPIO_PIN_13, pb13_cap);
StmPin &pb13 = pb13_obj;


/*******************************************************************************
 * PB14
 ******************************************************************************/
static const StmPin::Alternate pb14_alts[] = {
{
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_14,
                            STM_PIN_SETTING_PORT_GPIOB,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_PUSH_PULL,
                            STM_PIN_SETTING_PUPD_PULL_UP,
                            STM_PIN_SETTING_ALT_AF5),
    .peripheral = SPI2,
    .function   = StmPin::Alternate::Function::kSpiMISO
}
};
static const StmPin::Capabilities pb14_cap { STM_PIN_CAPABILITIES(pb14_alts) };
static StmGpio pb14_obj(GPIOB, LL_GPIO_PIN_14, pb14_cap);
StmPin &pb14 = pb14_obj;


/*******************************************************************************
 * PB14
 ******************************************************************************/
static const StmPin::Alternate pb15_alts[] = {
{
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_15,
                            STM_PIN_SETTING_PORT_GPIOB,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_PUSH_PULL,
                            STM_PIN_SETTING_PUPD_PULL_UP,
                            STM_PIN_SETTING_ALT_AF5),
    .peripheral = SPI2,
    .function   = StmPin::Alternate::Function::kSpiMOSI
}
};
static const StmPin::Capabilities pb15_cap { STM_PIN_CAPABILITIES(pb15_alts) };
static StmGpio pb15_obj(GPIOB, LL_GPIO_PIN_15, pb15_cap);
StmPin &pb15 = pb15_obj;
#endif /* defined(PIN_ENABLE_GPIOB) && defined(GPIOB_BASE) */

#if defined(PIN_ENABLE_GPIOC) && defined(GPIOC_BASE)
/*******************************************************************************
 * PC13
 ******************************************************************************/
static StmGpio pc13_obj(GPIOC, LL_GPIO_PIN_13, no_cap);
StmPin &pc13 = pc13_obj;


/*******************************************************************************
 * PC14
 ******************************************************************************/
static StmGpio pc14_obj(GPIOC, LL_GPIO_PIN_14, no_cap);
StmPin &pc14 = pc14_obj;


/*******************************************************************************
 * PC15
 ******************************************************************************/
static StmGpio pc15_obj(GPIOC, LL_GPIO_PIN_15, no_cap);
StmPin &pc15 = pc15_obj;
#endif /* defined(PIN_ENABLE_GPIOC) && defined(GPIOC_BASE) */


#if defined(PIN_ENABLE_GPIOH) && defined(GPIOH_BASE)
/*******************************************************************************
 * PH00
 ******************************************************************************/
static StmGpio ph00_obj(GPIOH, LL_GPIO_PIN_0, no_cap);
StmPin &ph00 = ph00_obj;

/*******************************************************************************
 * PH01
 ******************************************************************************/
static StmGpio ph01_obj(GPIOH, LL_GPIO_PIN_1, no_cap);
StmPin &ph01 = ph01_obj;
#endif /* defined(PIN_ENABLE_GPIOH) && defined(GPIOH_BASE) */

} /* namespace pin */
} /* namespace io */
} /* namespace arctos */
