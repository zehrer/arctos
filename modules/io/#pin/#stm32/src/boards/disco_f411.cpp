/**
 * @file
 * @copyright Copyright (c) 2022 Michael Zehrer. This file is part of ARCTOS
 * @license MIT
 * @brief pin definitions / mapping for "STM32F411 Discovery Board"
 */
#include <arctos/io/pin/stm32/gpio.hpp>
#include <arctos/io/pin/stm32/setting.hpp>

#include <arctos/io/stm32hal/ll_gpio.h>

#include CMSIS_DEVICE

namespace arctos {
namespace io {
namespace pin {

#define STM_PIN_CAPABILITIES(ALTS) \
    .alts = ALTS,\
    .alts_size = (sizeof(ALTS) / sizeof(StmPin::Alternate))

static const StmPin::Capabilities no_cap { .alts = nullptr, .alts_size = 0 };

#if defined(PIN_ENABLE_GPIOA) && defined(GPIOA_BASE)
/*******************************************************************************
 * PA00
 ******************************************************************************/
static const StmPin::Alternate pa00_alts[] = {
{
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_00,
                            STM_PIN_SETTING_PORT_GPIOA,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_PUSH_PULL,
                            STM_PIN_SETTING_PUPD_PULL_UP,
                            STM_PIN_SETTING_ALT_AF7),
    .peripheral = USART2,
    .function   = StmPin::Alternate::Function::kUartCTS
}
};
static const StmPin::Capabilities pa00_cap { STM_PIN_CAPABILITIES(pa00_alts) };
static StmGpio pa00_obj(GPIOA, LL_GPIO_PIN_0, pa00_cap);
StmPin &pa00 = pa00_obj;


/*******************************************************************************
 * PA01
 ******************************************************************************/
static const StmPin::Alternate pa01_alts[] = {
{
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_01,
                            STM_PIN_SETTING_PORT_GPIOA,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_PUSH_PULL,
                            STM_PIN_SETTING_PUPD_PULL_UP,
                            STM_PIN_SETTING_ALT_AF5),
    .peripheral = SPI4,
    .function   = StmPin::Alternate::Function::kSpiMOSI
}, {
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_01,
                            STM_PIN_SETTING_PORT_GPIOA,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_PUSH_PULL,
                            STM_PIN_SETTING_PUPD_PULL_UP,
                            STM_PIN_SETTING_ALT_AF7),
    .peripheral = USART2,
    .function   = StmPin::Alternate::Function::kUartRTS
}
};
static const StmPin::Capabilities pa01_cap { STM_PIN_CAPABILITIES(pa01_alts) };
static StmGpio pa01_obj(GPIOA, LL_GPIO_PIN_1, pa01_cap);
StmPin &pa01 = pa01_obj;


/*******************************************************************************
 * PA02
 ******************************************************************************/
static const StmPin::Alternate pa02_alts[] = {
{
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_02,
                            STM_PIN_SETTING_PORT_GPIOA,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_PUSH_PULL,
                            STM_PIN_SETTING_PUPD_PULL_UP,
                            STM_PIN_SETTING_ALT_AF7),
    .peripheral = USART2,
    .function   = StmPin::Alternate::Function::kUartTX
}
};
static const StmPin::Capabilities pa02_cap { STM_PIN_CAPABILITIES(pa02_alts) };
static StmGpio pa02_obj(GPIOA, LL_GPIO_PIN_2, pa02_cap);
StmPin &pa02 = pa02_obj;


/*******************************************************************************
 * PA03
 ******************************************************************************/
static const StmPin::Alternate pa03_alts[] = {
{
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_03,
                            STM_PIN_SETTING_PORT_GPIOA,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_PUSH_PULL,
                            STM_PIN_SETTING_PUPD_PULL_UP,
                            STM_PIN_SETTING_ALT_AF7),
    .peripheral = USART2,
    .function   = StmPin::Alternate::Function::kUartRX
}
};
static const StmPin::Capabilities pa03_cap { STM_PIN_CAPABILITIES(pa03_alts) };
static StmGpio pa03_obj(GPIOA, LL_GPIO_PIN_3, pa03_cap);
StmPin &pa03 = pa03_obj;


/*******************************************************************************
 * PA04
 ******************************************************************************/
static const StmPin::Alternate pa04_alts[] = {
{
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_04,
                            STM_PIN_SETTING_PORT_GPIOA,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_PUSH_PULL,
                            STM_PIN_SETTING_PUPD_PULL_UP,
                            STM_PIN_SETTING_ALT_AF5),
    .peripheral = SPI1,
    .function   = StmPin::Alternate::Function::kSpiNSS
}, {
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_04,
                            STM_PIN_SETTING_PORT_GPIOA,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_PUSH_PULL,
                            STM_PIN_SETTING_PUPD_PULL_UP,
                            STM_PIN_SETTING_ALT_AF6),
    .peripheral = SPI3,
    .function   = StmPin::Alternate::Function::kSpiNSS
}, {
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_04,
                            STM_PIN_SETTING_PORT_GPIOA,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_PUSH_PULL,
                            STM_PIN_SETTING_PUPD_PULL_UP,
                            STM_PIN_SETTING_ALT_AF7),
    .peripheral = USART2,
    .function   = StmPin::Alternate::Function::kUartCK
}
};
static const StmPin::Capabilities pa04_cap { STM_PIN_CAPABILITIES(pa04_alts) };
static StmGpio pa04_obj(GPIOA, LL_GPIO_PIN_4, pa04_cap);
StmPin &pa04 = pa04_obj;


/*******************************************************************************
 * PA05
 ******************************************************************************/
static const StmPin::Alternate pa05_alts[] = {
{
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_05,
                            STM_PIN_SETTING_PORT_GPIOA,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_PUSH_PULL,
                            STM_PIN_SETTING_PUPD_PULL_UP,
                            STM_PIN_SETTING_ALT_AF5),
    .peripheral = SPI1,
    .function   = StmPin::Alternate::Function::kSpiSCK
}
};
static const StmPin::Capabilities pa05_cap { STM_PIN_CAPABILITIES(pa05_alts) };
static StmGpio pa05_obj(GPIOA, LL_GPIO_PIN_5, pa05_cap);
StmPin &pa05 = pa05_obj;


/*******************************************************************************
 * PA06
 ******************************************************************************/
static const StmPin::Alternate pa06_alts[] = {
{
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_06,
                            STM_PIN_SETTING_PORT_GPIOA,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_PUSH_PULL,
                            STM_PIN_SETTING_PUPD_PULL_UP,
                            STM_PIN_SETTING_ALT_AF5),
    .peripheral = SPI1,
    .function   = StmPin::Alternate::Function::kSpiMISO
}
};
static const StmPin::Capabilities pa06_cap { STM_PIN_CAPABILITIES(pa06_alts) };
static StmGpio pa06_obj(GPIOA, LL_GPIO_PIN_6, pa06_cap);
StmPin &pa06 = pa06_obj;


/*******************************************************************************
 * PA07
 ******************************************************************************/
static const StmPin::Alternate pa07_alts[] = {
{
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_07,
                            STM_PIN_SETTING_PORT_GPIOA,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_PUSH_PULL,
                            STM_PIN_SETTING_PUPD_PULL_UP,
                            STM_PIN_SETTING_ALT_AF5),
    .peripheral = SPI1,
    .function   = StmPin::Alternate::Function::kSpiMOSI
}
};
static const StmPin::Capabilities pa07_cap { STM_PIN_CAPABILITIES(pa07_alts) };
static StmGpio pa07_obj(GPIOA, LL_GPIO_PIN_7, pa07_cap);
StmPin &pa07 = pa07_obj;


/*******************************************************************************
 * PA08
 ******************************************************************************/
static const StmPin::Alternate pa08_alts[] = {
{
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_08,
                            STM_PIN_SETTING_PORT_GPIOA,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_OPEN_DRAIN,
                            STM_PIN_SETTING_PUPD_NONE,
                            STM_PIN_SETTING_ALT_AF4),
    .peripheral = I2C3,
    .function   = StmPin::Alternate::Function::kI2cSCL
}, {
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_08,
                            STM_PIN_SETTING_PORT_GPIOA,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_PUSH_PULL,
                            STM_PIN_SETTING_PUPD_PULL_UP,
                            STM_PIN_SETTING_ALT_AF7),
    .peripheral = USART1,
    .function   = StmPin::Alternate::Function::kUartCK
}
};
static const StmPin::Capabilities pa08_cap { STM_PIN_CAPABILITIES(pa08_alts) };
static StmGpio pa08_obj(GPIOA, LL_GPIO_PIN_8, pa08_cap);
StmPin &pa08 = pa08_obj;


/*******************************************************************************
 * PA09
 ******************************************************************************/
static const StmPin::Alternate pa09_alts[] = {
{
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_09,
                            STM_PIN_SETTING_PORT_GPIOA,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_PUSH_PULL,
                            STM_PIN_SETTING_PUPD_PULL_UP,
                            STM_PIN_SETTING_ALT_AF7),
    .peripheral = USART1,
    .function   = StmPin::Alternate::Function::kUartTX
}
};
static const StmPin::Capabilities pa09_cap { STM_PIN_CAPABILITIES(pa09_alts) };
static StmGpio pa09_obj(GPIOA, LL_GPIO_PIN_9, pa09_cap);
StmPin &pa09 = pa09_obj;


/*******************************************************************************
 * PA10
 ******************************************************************************/
static const StmPin::Alternate pa10_alts[] = {
{
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_10,
                            STM_PIN_SETTING_PORT_GPIOA,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_PUSH_PULL,
                            STM_PIN_SETTING_PUPD_PULL_UP,
                            STM_PIN_SETTING_ALT_AF6),
    .peripheral = SPI5,
    .function   = StmPin::Alternate::Function::kSpiMOSI
}, {
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_10,
                            STM_PIN_SETTING_PORT_GPIOA,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_PUSH_PULL,
                            STM_PIN_SETTING_PUPD_PULL_UP,
                            STM_PIN_SETTING_ALT_AF7),
    .peripheral = USART1,
    .function   = StmPin::Alternate::Function::kUartRX
}
};
static const StmPin::Capabilities pa10_cap { STM_PIN_CAPABILITIES(pa10_alts) };
static StmGpio pa10_obj(GPIOA, LL_GPIO_PIN_10, pa10_cap);
StmPin &pa10 = pa10_obj;


/*******************************************************************************
 * PA11
 ******************************************************************************/
static const StmPin::Alternate pa11_alts[] = {
{
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_11,
                            STM_PIN_SETTING_PORT_GPIOA,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_PUSH_PULL,
                            STM_PIN_SETTING_PUPD_PULL_UP,
                            STM_PIN_SETTING_ALT_AF6),
    .peripheral = SPI4,
    .function   = StmPin::Alternate::Function::kSpiMISO
}, {
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_11,
                            STM_PIN_SETTING_PORT_GPIOA,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_PUSH_PULL,
                            STM_PIN_SETTING_PUPD_PULL_UP,
                            STM_PIN_SETTING_ALT_AF7),
    .peripheral = USART1,
    .function   = StmPin::Alternate::Function::kUartCTS
}, {
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_11,
                            STM_PIN_SETTING_PORT_GPIOA,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_PUSH_PULL,
                            STM_PIN_SETTING_PUPD_PULL_UP,
                            STM_PIN_SETTING_ALT_AF8),
    .peripheral = USART6,
    .function   = StmPin::Alternate::Function::kUartTX
}
};
static const StmPin::Capabilities pa11_cap { STM_PIN_CAPABILITIES(pa11_alts) };
static StmGpio pa11_obj(GPIOA, LL_GPIO_PIN_11, pa11_cap);
StmPin &pa11 = pa11_obj;


/*******************************************************************************
 * PA12
 ******************************************************************************/
static const StmPin::Alternate pa12_alts[] = {
{
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_12,
                            STM_PIN_SETTING_PORT_GPIOA,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_PUSH_PULL,
                            STM_PIN_SETTING_PUPD_PULL_UP,
                            STM_PIN_SETTING_ALT_AF6),
    .peripheral = SPI5,
    .function   = StmPin::Alternate::Function::kSpiMISO
}, {
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_12,
                            STM_PIN_SETTING_PORT_GPIOA,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_PUSH_PULL,
                            STM_PIN_SETTING_PUPD_PULL_UP,
                            STM_PIN_SETTING_ALT_AF7),
    .peripheral = USART1,
    .function   = StmPin::Alternate::Function::kUartRTS
}, {
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_12,
                            STM_PIN_SETTING_PORT_GPIOA,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_PUSH_PULL,
                            STM_PIN_SETTING_PUPD_PULL_UP,
                            STM_PIN_SETTING_ALT_AF8),
    .peripheral = USART6,
    .function   = StmPin::Alternate::Function::kUartRX
}
};
static const StmPin::Capabilities pa12_cap { STM_PIN_CAPABILITIES(pa12_alts) };
static StmGpio pa12_obj(GPIOA, LL_GPIO_PIN_12, pa12_cap);
StmPin &pa12 = pa12_obj;


/*******************************************************************************
 * PA13
 ******************************************************************************/
static StmGpio pa13_obj(GPIOA, LL_GPIO_PIN_13, no_cap);
StmPin &pa13 = pa13_obj;


/*******************************************************************************
 * PA14
 ******************************************************************************/
static StmGpio pa14_obj(GPIOA, LL_GPIO_PIN_14, no_cap);
StmPin &pa14 = pa14_obj;


/*******************************************************************************
 * PA15
 ******************************************************************************/
static const StmPin::Alternate pa15_alts[] = {
{
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_15,
                            STM_PIN_SETTING_PORT_GPIOA,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_PUSH_PULL,
                            STM_PIN_SETTING_PUPD_PULL_UP,
                            STM_PIN_SETTING_ALT_AF5),
    .peripheral = SPI1,
    .function   = StmPin::Alternate::Function::kSpiNSS
}, {
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_15,
                            STM_PIN_SETTING_PORT_GPIOA,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_PUSH_PULL,
                            STM_PIN_SETTING_PUPD_PULL_UP,
                            STM_PIN_SETTING_ALT_AF6),
    .peripheral = SPI3,
    .function   = StmPin::Alternate::Function::kSpiNSS
}, {
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_15,
                            STM_PIN_SETTING_PORT_GPIOA,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_PUSH_PULL,
                            STM_PIN_SETTING_PUPD_PULL_UP,
                            STM_PIN_SETTING_ALT_AF7),
    .peripheral = USART1,
    .function   = StmPin::Alternate::Function::kUartTX
}
};
static const StmPin::Capabilities pa15_cap { STM_PIN_CAPABILITIES(pa15_alts) };
static StmGpio pa15_obj(GPIOA, LL_GPIO_PIN_15, pa15_cap);
StmPin &pa15 = pa15_obj;
#endif /* defined(PIN_ENABLE_GPIOA) && defined(GPIOA_BASE) */

#if defined(PIN_ENABLE_GPIOB) && defined(GPIOB_BASE)
/*******************************************************************************
 * PB00
 ******************************************************************************/
static const StmPin::Alternate pb00_alts[] = {
{
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_00,
                            STM_PIN_SETTING_PORT_GPIOB,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_PUSH_PULL,
                            STM_PIN_SETTING_PUPD_PULL_UP,
                            STM_PIN_SETTING_ALT_AF6),
    .peripheral = SPI5,
    .function   = StmPin::Alternate::Function::kSpiSCK
}
};
static const StmPin::Capabilities pb00_cap { STM_PIN_CAPABILITIES(pb00_alts) };
static StmGpio pb00_obj(GPIOB, LL_GPIO_PIN_0, pb00_cap);
StmPin &pb00 = pb00_obj;


/*******************************************************************************
 * PB01
 ******************************************************************************/
static const StmPin::Alternate pb01_alts[] = {
{
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_01,
                            STM_PIN_SETTING_PORT_GPIOB,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_PUSH_PULL,
                            STM_PIN_SETTING_PUPD_PULL_UP,
                            STM_PIN_SETTING_ALT_AF6),
    .peripheral = SPI5,
    .function   = StmPin::Alternate::Function::kSpiNSS
}
};
static const StmPin::Capabilities pb01_cap { STM_PIN_CAPABILITIES(pb01_alts) };
static StmGpio pb01_obj(GPIOB, LL_GPIO_PIN_1, pb01_cap);
StmPin &pb01 = pb01_obj;


/*******************************************************************************
 * PB02
 ******************************************************************************/
static StmGpio pb02_obj(GPIOB, LL_GPIO_PIN_2, no_cap);
StmPin &pb02 = pb02_obj;


/*******************************************************************************
 * PB03
 ******************************************************************************/
static const StmPin::Alternate pb03_alts[] = {
{
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_03,
                            STM_PIN_SETTING_PORT_GPIOB,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_PUSH_PULL,
                            STM_PIN_SETTING_PUPD_PULL_UP,
                            STM_PIN_SETTING_ALT_AF5),
    .peripheral = SPI1,
    .function   = StmPin::Alternate::Function::kSpiSCK
}, {
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_03,
                            STM_PIN_SETTING_PORT_GPIOB,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_PUSH_PULL,
                            STM_PIN_SETTING_PUPD_PULL_UP,
                            STM_PIN_SETTING_ALT_AF6),
    .peripheral = SPI3,
    .function   = StmPin::Alternate::Function::kSpiSCK
}, {
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_03,
                            STM_PIN_SETTING_PORT_GPIOB,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_PUSH_PULL,
                            STM_PIN_SETTING_PUPD_PULL_UP,
                            STM_PIN_SETTING_ALT_AF7),
    .peripheral = USART1,
    .function   = StmPin::Alternate::Function::kUartRX
}, {
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_03,
                            STM_PIN_SETTING_PORT_GPIOB,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_OPEN_DRAIN,
                            STM_PIN_SETTING_PUPD_NONE,
                            STM_PIN_SETTING_ALT_AF9),
    .peripheral = I2C2,
    .function   = StmPin::Alternate::Function::kI2cSDA
}
};
static const StmPin::Capabilities pb03_cap { STM_PIN_CAPABILITIES(pb03_alts) };
static StmGpio pb03_obj(GPIOB, LL_GPIO_PIN_3, pb03_cap);
StmPin &pb03 = pb03_obj;


/*******************************************************************************
 * PB04
 ******************************************************************************/
static const StmPin::Alternate pb04_alts[] = {
{
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_04,
                            STM_PIN_SETTING_PORT_GPIOB,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_PUSH_PULL,
                            STM_PIN_SETTING_PUPD_PULL_UP,
                            STM_PIN_SETTING_ALT_AF5),
    .peripheral = SPI1,
    .function   = StmPin::Alternate::Function::kSpiMISO
}, {
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_04,
                            STM_PIN_SETTING_PORT_GPIOB,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_PUSH_PULL,
                            STM_PIN_SETTING_PUPD_PULL_UP,
                            STM_PIN_SETTING_ALT_AF6),
    .peripheral = SPI3,
    .function   = StmPin::Alternate::Function::kSpiMISO
}, {
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_04,
                            STM_PIN_SETTING_PORT_GPIOB,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_OPEN_DRAIN,
                            STM_PIN_SETTING_PUPD_NONE,
                            STM_PIN_SETTING_ALT_AF9),
    .peripheral = I2C3,
    .function   = StmPin::Alternate::Function::kI2cSDA
}
};
static const StmPin::Capabilities pb04_cap { STM_PIN_CAPABILITIES(pb04_alts) };
static StmGpio pb04_obj(GPIOB, LL_GPIO_PIN_4, pb04_cap);
StmPin &pb04 = pb04_obj;


/*******************************************************************************
 * PB05
 ******************************************************************************/
static const StmPin::Alternate pb05_alts[] = {
{
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_05,
                            STM_PIN_SETTING_PORT_GPIOB,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_PUSH_PULL,
                            STM_PIN_SETTING_PUPD_PULL_UP,
                            STM_PIN_SETTING_ALT_AF5),
    .peripheral = SPI1,
    .function   = StmPin::Alternate::Function::kSpiMOSI
}, {
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_05,
                            STM_PIN_SETTING_PORT_GPIOB,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_PUSH_PULL,
                            STM_PIN_SETTING_PUPD_PULL_UP,
                            STM_PIN_SETTING_ALT_AF6),
    .peripheral = SPI3,
    .function   = StmPin::Alternate::Function::kSpiMOSI
}
};
static const StmPin::Capabilities pb05_cap { STM_PIN_CAPABILITIES(pb05_alts) };
static StmGpio pb05_obj(GPIOB, LL_GPIO_PIN_5, pb05_cap);
StmPin &pb05 = pb05_obj;


/*******************************************************************************
 * PB06
 ******************************************************************************/
static const StmPin::Alternate pb06_alts[] = {
{
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_06,
                            STM_PIN_SETTING_PORT_GPIOB,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_OPEN_DRAIN,
                            STM_PIN_SETTING_PUPD_NONE,
                            STM_PIN_SETTING_ALT_AF4),
    .peripheral = I2C1,
    .function   = StmPin::Alternate::Function::kI2cSCL
}, {
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_06,
                            STM_PIN_SETTING_PORT_GPIOB,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_PUSH_PULL,
                            STM_PIN_SETTING_PUPD_PULL_UP,
                            STM_PIN_SETTING_ALT_AF7),
    .peripheral = USART1,
    .function   = StmPin::Alternate::Function::kUartTX
}
};
static const StmPin::Capabilities pb06_cap { STM_PIN_CAPABILITIES(pb06_alts) };
static StmGpio pb06_obj(GPIOB, LL_GPIO_PIN_6, pb06_cap);
StmPin &pb06 = pb06_obj;


/*******************************************************************************
 * PB07
 ******************************************************************************/
static const StmPin::Alternate pb07_alts[] = {
{
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_07,
                            STM_PIN_SETTING_PORT_GPIOB,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_OPEN_DRAIN,
                            STM_PIN_SETTING_PUPD_NONE,
                            STM_PIN_SETTING_ALT_AF4),
    .peripheral = I2C1,
    .function   = StmPin::Alternate::Function::kI2cSDA
}, {
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_07,
                            STM_PIN_SETTING_PORT_GPIOB,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_PUSH_PULL,
                            STM_PIN_SETTING_PUPD_PULL_UP,
                            STM_PIN_SETTING_ALT_AF7),
    .peripheral = USART1,
    .function   = StmPin::Alternate::Function::kUartRX
}
};
static const StmPin::Capabilities pb07_cap { STM_PIN_CAPABILITIES(pb07_alts) };
static StmGpio pb07_obj(GPIOB, LL_GPIO_PIN_7, pb07_cap);
StmPin &pb07 = pb07_obj;


/*******************************************************************************
 * PB08
 ******************************************************************************/
static const StmPin::Alternate pb08_alts[] = {
{
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_08,
                            STM_PIN_SETTING_PORT_GPIOB,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_OPEN_DRAIN,
                            STM_PIN_SETTING_PUPD_NONE,
                            STM_PIN_SETTING_ALT_AF4),
    .peripheral = I2C1,
    .function   = StmPin::Alternate::Function::kI2cSCL
}, {
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_08,
                            STM_PIN_SETTING_PORT_GPIOB,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_PUSH_PULL,
                            STM_PIN_SETTING_PUPD_PULL_UP,
                            STM_PIN_SETTING_ALT_AF6),
    .peripheral = SPI5,
    .function   = StmPin::Alternate::Function::kSpiMOSI
}, {
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_08,
                            STM_PIN_SETTING_PORT_GPIOB,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_OPEN_DRAIN,
                            STM_PIN_SETTING_PUPD_NONE,
                            STM_PIN_SETTING_ALT_AF9),
    .peripheral = I2C3,
    .function   = StmPin::Alternate::Function::kI2cSDA
}
};
static const StmPin::Capabilities pb08_cap { STM_PIN_CAPABILITIES(pb08_alts) };
static StmGpio pb08_obj(GPIOB, LL_GPIO_PIN_8, pb08_cap);
StmPin &pb08 = pb08_obj;


/*******************************************************************************
 * PB09
 ******************************************************************************/
static const StmPin::Alternate pb09_alts[] = {
{
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_09,
                            STM_PIN_SETTING_PORT_GPIOB,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_OPEN_DRAIN,
                            STM_PIN_SETTING_PUPD_NONE,
                            STM_PIN_SETTING_ALT_AF4),
    .peripheral = I2C1,
    .function   = StmPin::Alternate::Function::kI2cSDA
}, {
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_09,
                            STM_PIN_SETTING_PORT_GPIOB,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_PUSH_PULL,
                            STM_PIN_SETTING_PUPD_PULL_UP,
                            STM_PIN_SETTING_ALT_AF5),
    .peripheral = SPI2,
    .function   = StmPin::Alternate::Function::kSpiNSS
}, {
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_09,
                            STM_PIN_SETTING_PORT_GPIOB,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_OPEN_DRAIN,
                            STM_PIN_SETTING_PUPD_NONE,
                            STM_PIN_SETTING_ALT_AF9),
    .peripheral = I2C2,
    .function   = StmPin::Alternate::Function::kI2cSDA
}
};
static const StmPin::Capabilities pb09_cap { STM_PIN_CAPABILITIES(pb09_alts) };
static StmGpio pb09_obj(GPIOB, LL_GPIO_PIN_9, pb09_cap);
StmPin &pb09 = pb09_obj;


/*******************************************************************************
 * PB10
 ******************************************************************************/
static const StmPin::Alternate pb10_alts[] = {
{
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_10,
                            STM_PIN_SETTING_PORT_GPIOB,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_OPEN_DRAIN,
                            STM_PIN_SETTING_PUPD_NONE,
                            STM_PIN_SETTING_ALT_AF4),
    .peripheral = I2C2,
    .function   = StmPin::Alternate::Function::kI2cSCL
}, {
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_10,
                            STM_PIN_SETTING_PORT_GPIOB,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_PUSH_PULL,
                            STM_PIN_SETTING_PUPD_PULL_UP,
                            STM_PIN_SETTING_ALT_AF5),
    .peripheral = SPI2,
    .function   = StmPin::Alternate::Function::kSpiSCK
}
};
static const StmPin::Capabilities pb10_cap { STM_PIN_CAPABILITIES(pb10_alts) };
static StmGpio pb10_obj(GPIOB, LL_GPIO_PIN_10, pb10_cap);
StmPin &pb10 = pb10_obj;


/*******************************************************************************
 * PB11
 ******************************************************************************/
/*
static const StmPin::Alternate pb11_alts[] = {
{
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_11,
                            STM_PIN_SETTING_PORT_GPIOB,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_OPEN_DRAIN,
                            STM_PIN_SETTING_PUPD_NONE,
                            STM_PIN_SETTING_ALT_AF4),
    .peripheral = I2C2,
    .function   = StmPin::Alternate::Function::kI2cSDA
}
};
static const StmPin::Capabilities pb11_cap { STM_PIN_CAPABILITIES(pb11_alts) };
static StmGpio pb11_obj(GPIOB, LL_GPIO_PIN_11, pb11_cap);
StmPin &pb11 = pb11_obj;
*/


/*******************************************************************************
 * PB12
 ******************************************************************************/
static const StmPin::Alternate pb12_alts[] = {
{
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_12,
                            STM_PIN_SETTING_PORT_GPIOB,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_PUSH_PULL,
                            STM_PIN_SETTING_PUPD_PULL_UP,
                            STM_PIN_SETTING_ALT_AF5),
    .peripheral = SPI2,
    .function   = StmPin::Alternate::Function::kSpiNSS
}, {
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_12,
                            STM_PIN_SETTING_PORT_GPIOB,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_PUSH_PULL,
                            STM_PIN_SETTING_PUPD_PULL_UP,
                            STM_PIN_SETTING_ALT_AF6),
    .peripheral = SPI4,
    .function   = StmPin::Alternate::Function::kSpiNSS
}, {
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_12,
                            STM_PIN_SETTING_PORT_GPIOB,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_PUSH_PULL,
                            STM_PIN_SETTING_PUPD_PULL_UP,
                            STM_PIN_SETTING_ALT_AF7),
    .peripheral = SPI3,
    .function   = StmPin::Alternate::Function::kSpiSCK
}
};
static const StmPin::Capabilities pb12_cap { STM_PIN_CAPABILITIES(pb12_alts) };
static StmGpio pb12_obj(GPIOB, LL_GPIO_PIN_12, pb12_cap);
StmPin &pb12 = pb12_obj;


/*******************************************************************************
 * PB13
 ******************************************************************************/
static const StmPin::Alternate pb13_alts[] = {
{
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_13,
                            STM_PIN_SETTING_PORT_GPIOB,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_PUSH_PULL,
                            STM_PIN_SETTING_PUPD_PULL_UP,
                            STM_PIN_SETTING_ALT_AF5),
    .peripheral = SPI2,
    .function   = StmPin::Alternate::Function::kSpiSCK
}, {
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_13,
                            STM_PIN_SETTING_PORT_GPIOB,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_PUSH_PULL,
                            STM_PIN_SETTING_PUPD_PULL_UP,
                            STM_PIN_SETTING_ALT_AF6),
    .peripheral = SPI4,
    .function   = StmPin::Alternate::Function::kSpiSCK
}
};
static const StmPin::Capabilities pb13_cap { STM_PIN_CAPABILITIES(pb13_alts) };
static StmGpio pb13_obj(GPIOB, LL_GPIO_PIN_13, pb13_cap);
StmPin &pb13 = pb13_obj;


/*******************************************************************************
 * PB14
 ******************************************************************************/
static const StmPin::Alternate pb14_alts[] = {
{
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_14,
                            STM_PIN_SETTING_PORT_GPIOB,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_PUSH_PULL,
                            STM_PIN_SETTING_PUPD_PULL_UP,
                            STM_PIN_SETTING_ALT_AF5),
    .peripheral = SPI2,
    .function   = StmPin::Alternate::Function::kSpiMISO
}
};
static const StmPin::Capabilities pb14_cap { STM_PIN_CAPABILITIES(pb14_alts) };
static StmGpio pb14_obj(GPIOB, LL_GPIO_PIN_14, pb14_cap);
StmPin &pb14 = pb14_obj;


/*******************************************************************************
 * PB14
 ******************************************************************************/
static const StmPin::Alternate pb15_alts[] = {
{
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_15,
                            STM_PIN_SETTING_PORT_GPIOB,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_PUSH_PULL,
                            STM_PIN_SETTING_PUPD_PULL_UP,
                            STM_PIN_SETTING_ALT_AF5),
    .peripheral = SPI2,
    .function   = StmPin::Alternate::Function::kSpiMOSI
}
};
static const StmPin::Capabilities pb15_cap { STM_PIN_CAPABILITIES(pb15_alts) };
static StmGpio pb15_obj(GPIOB, LL_GPIO_PIN_15, pb15_cap);
StmPin &pb15 = pb15_obj;
#endif /* defined(PIN_ENABLE_GPIOB) && defined(GPIOB_BASE) */

#if defined(PIN_ENABLE_GPIOC) && defined(GPIOC_BASE)
/*******************************************************************************
 * PC00
 ******************************************************************************/
static StmGpio pc00_obj(GPIOC, LL_GPIO_PIN_0, no_cap);
StmPin &pc00 = pc00_obj;


/*******************************************************************************
 * PC01
 ******************************************************************************/
static StmGpio pc01_obj(GPIOC, LL_GPIO_PIN_1, no_cap);
StmPin &pc01 = pc01_obj;


/*******************************************************************************
 * PC02
 ******************************************************************************/
static const StmPin::Alternate pc02_alts[] = {
{
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_02,
                            STM_PIN_SETTING_PORT_GPIOC,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_PUSH_PULL,
                            STM_PIN_SETTING_PUPD_PULL_UP,
                            STM_PIN_SETTING_ALT_AF5),
    .peripheral = SPI2,
    .function   = StmPin::Alternate::Function::kSpiMISO
}
};
static const StmPin::Capabilities pc02_cap { STM_PIN_CAPABILITIES(pc02_alts) };
static StmGpio pc02_obj(GPIOC, LL_GPIO_PIN_2, pc02_cap);
StmPin &pc02 = pc02_obj;


/*******************************************************************************
 * PC03
 ******************************************************************************/
static const StmPin::Alternate pc03_alts[] = {
{
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_03,
                            STM_PIN_SETTING_PORT_GPIOC,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_PUSH_PULL,
                            STM_PIN_SETTING_PUPD_PULL_UP,
                            STM_PIN_SETTING_ALT_AF5),
    .peripheral = SPI2,
    .function   = StmPin::Alternate::Function::kSpiMOSI
}
};
static const StmPin::Capabilities pc03_cap { STM_PIN_CAPABILITIES(pc03_alts) };
static StmGpio pc03_obj(GPIOC, LL_GPIO_PIN_3, pc03_cap);
StmPin &pc03 = pc03_obj;


/*******************************************************************************
 * PC04
 ******************************************************************************/
static StmGpio pc04_obj(GPIOC, LL_GPIO_PIN_4, no_cap);
StmPin &pc04 = pc04_obj;


/*******************************************************************************
 * PC05
 ******************************************************************************/
static StmGpio pc05_obj(GPIOC, LL_GPIO_PIN_5, no_cap);
StmPin &pc05 = pc05_obj;


/*******************************************************************************
 * PC06
 ******************************************************************************/
static const StmPin::Alternate pc06_alts[] = {
{
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_06,
                            STM_PIN_SETTING_PORT_GPIOC,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_PUSH_PULL,
                            STM_PIN_SETTING_PUPD_PULL_UP,
                            STM_PIN_SETTING_ALT_AF8),
    .peripheral = USART6,
    .function   = StmPin::Alternate::Function::kUartTX
}
};
static const StmPin::Capabilities pc06_cap { STM_PIN_CAPABILITIES(pc06_alts) };
static StmGpio pc06_obj(GPIOC, LL_GPIO_PIN_6, pc06_cap);
StmPin &pc06 = pc06_obj;


/*******************************************************************************
 * PC07
 ******************************************************************************/
static const StmPin::Alternate pc07_alts[] = {
{
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_07,
                            STM_PIN_SETTING_PORT_GPIOC,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_PUSH_PULL,
                            STM_PIN_SETTING_PUPD_PULL_UP,
                            STM_PIN_SETTING_ALT_AF5),
    .peripheral = SPI2,
    .function   = StmPin::Alternate::Function::kSpiSCK
}, {
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_07,
                            STM_PIN_SETTING_PORT_GPIOC,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_PUSH_PULL,
                            STM_PIN_SETTING_PUPD_PULL_UP,
                            STM_PIN_SETTING_ALT_AF8),
    .peripheral = USART6,
    .function   = StmPin::Alternate::Function::kUartRX
}
};
static const StmPin::Capabilities pc07_cap { STM_PIN_CAPABILITIES(pc07_alts) };
static StmGpio pc07_obj(GPIOC, LL_GPIO_PIN_7, pc07_cap);
StmPin &pc07 = pc07_obj;


/*******************************************************************************
 * PC08
 ******************************************************************************/
static const StmPin::Alternate pc08_alts[] = {
{
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_08,
                            STM_PIN_SETTING_PORT_GPIOC,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_PUSH_PULL,
                            STM_PIN_SETTING_PUPD_PULL_UP,
                            STM_PIN_SETTING_ALT_AF8),
    .peripheral = USART6,
    .function   = StmPin::Alternate::Function::kUartCK
}
};
static const StmPin::Capabilities pc08_cap { STM_PIN_CAPABILITIES(pc08_alts) };
static StmGpio pc08_obj(GPIOC, LL_GPIO_PIN_8, pc08_cap);
StmPin &pc08 = pc08_obj;


/*******************************************************************************
 * PC09
 ******************************************************************************/
static const StmPin::Alternate pc09_alts[] = {
{
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_09,
                            STM_PIN_SETTING_PORT_GPIOC,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_OPEN_DRAIN,
                            STM_PIN_SETTING_PUPD_NONE,
                            STM_PIN_SETTING_ALT_AF4),
    .peripheral = I2C3,
    .function   = StmPin::Alternate::Function::kI2cSDA
}
};
static const StmPin::Capabilities pc09_cap { STM_PIN_CAPABILITIES(pc09_alts) };
static StmGpio pc09_obj(GPIOC, LL_GPIO_PIN_9, pc09_cap);
StmPin &pc09 = pc09_obj;


/*******************************************************************************
 * PC10
 ******************************************************************************/
static const StmPin::Alternate pc10_alts[] = {
{
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_10,
                            STM_PIN_SETTING_PORT_GPIOC,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_PUSH_PULL,
                            STM_PIN_SETTING_PUPD_PULL_UP,
                            STM_PIN_SETTING_ALT_AF6),
    .peripheral = SPI3,
    .function   = StmPin::Alternate::Function::kSpiSCK
}
};
static const StmPin::Capabilities pc10_cap { STM_PIN_CAPABILITIES(pc10_alts) };
static StmGpio pc10_obj(GPIOC, LL_GPIO_PIN_10, pc10_cap);
StmPin &pc10 = pc10_obj;


/*******************************************************************************
 * PC11
 ******************************************************************************/
static const StmPin::Alternate pc11_alts[] = {
{
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_11,
                            STM_PIN_SETTING_PORT_GPIOC,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_PUSH_PULL,
                            STM_PIN_SETTING_PUPD_PULL_UP,
                            STM_PIN_SETTING_ALT_AF6),
    .peripheral = SPI3,
    .function   = StmPin::Alternate::Function::kSpiMISO
}
};
static const StmPin::Capabilities pc11_cap { STM_PIN_CAPABILITIES(pc11_alts) };
static StmGpio pc11_obj(GPIOC, LL_GPIO_PIN_11, pc11_cap);
StmPin &pc11 = pc11_obj;


/*******************************************************************************
 * PC12
 ******************************************************************************/
static const StmPin::Alternate pc12_alts[] = {
{
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_12,
                            STM_PIN_SETTING_PORT_GPIOC,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_PUSH_PULL,
                            STM_PIN_SETTING_PUPD_PULL_UP,
                            STM_PIN_SETTING_ALT_AF6),
    .peripheral = SPI3,
    .function   = StmPin::Alternate::Function::kSpiMOSI
}
};
static const StmPin::Capabilities pc12_cap { STM_PIN_CAPABILITIES(pc12_alts) };
static StmGpio pc12_obj(GPIOC, LL_GPIO_PIN_12, pc12_cap);
StmPin &pc12 = pc12_obj;


/*******************************************************************************
 * PC13
 ******************************************************************************/
static StmGpio pc13_obj(GPIOC, LL_GPIO_PIN_13, no_cap);
StmPin &pc13 = pc13_obj;


/*******************************************************************************
 * PC14
 ******************************************************************************/
static StmGpio pc14_obj(GPIOC, LL_GPIO_PIN_14, no_cap);
StmPin &pc14 = pc14_obj;


/*******************************************************************************
 * PC15
 ******************************************************************************/
static StmGpio pc15_obj(GPIOC, LL_GPIO_PIN_15, no_cap);
StmPin &pc15 = pc15_obj;
#endif /* defined(PIN_ENABLE_GPIOC) && defined(GPIOC_BASE) */

#if defined(PIN_ENABLE_GPIOD) && defined(GPIOD_BASE)
/*******************************************************************************
 * PD00
 ******************************************************************************/
static StmGpio pd00_obj(GPIOD, LL_GPIO_PIN_0, no_cap);
StmPin &pd00 = pd00_obj;


/*******************************************************************************
 * PD01
 ******************************************************************************/
static StmGpio pd01_obj(GPIOD, LL_GPIO_PIN_1, no_cap);
StmPin &pd01 = pd01_obj;


/*******************************************************************************
 * PD02
 ******************************************************************************/
static StmGpio pd02_obj(GPIOD, LL_GPIO_PIN_2, no_cap);
StmPin &pd02 = pd02_obj;


/*******************************************************************************
 * PD03
 ******************************************************************************/
static const StmPin::Alternate pd03_alts[] = {
{
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_03,
                            STM_PIN_SETTING_PORT_GPIOD,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_PUSH_PULL,
                            STM_PIN_SETTING_PUPD_PULL_UP,
                            STM_PIN_SETTING_ALT_AF5),
    .peripheral = SPI2,
    .function   = StmPin::Alternate::Function::kSpiSCK
}, {
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_03,
                            STM_PIN_SETTING_PORT_GPIOD,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_PUSH_PULL,
                            STM_PIN_SETTING_PUPD_PULL_UP,
                            STM_PIN_SETTING_ALT_AF7),
    .peripheral = USART2,
    .function   = StmPin::Alternate::Function::kUartCTS
}
};
static const StmPin::Capabilities pd03_cap { STM_PIN_CAPABILITIES(pd03_alts) };
static StmGpio pd03_obj(GPIOD, LL_GPIO_PIN_3, pd03_cap);
StmPin &pd03 = pd03_obj;


/*******************************************************************************
 * PD04
 ******************************************************************************/
static const StmPin::Alternate pd04_alts[] = {
{
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_04,
                            STM_PIN_SETTING_PORT_GPIOD,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_PUSH_PULL,
                            STM_PIN_SETTING_PUPD_PULL_UP,
                            STM_PIN_SETTING_ALT_AF7),
    .peripheral = USART2,
    .function   = StmPin::Alternate::Function::kUartRTS
}
};
static const StmPin::Capabilities pd04_cap { STM_PIN_CAPABILITIES(pd04_alts) };
static StmGpio pd04_obj(GPIOD, LL_GPIO_PIN_4, pd04_cap);
StmPin &pd04 = pd04_obj;


/*******************************************************************************
 * PD05
 ******************************************************************************/
static const StmPin::Alternate pd05_alts[] = {
{
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_05,
                            STM_PIN_SETTING_PORT_GPIOD,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_PUSH_PULL,
                            STM_PIN_SETTING_PUPD_PULL_UP,
                            STM_PIN_SETTING_ALT_AF7),
    .peripheral = USART2,
    .function   = StmPin::Alternate::Function::kUartTX
}
};
static const StmPin::Capabilities pd05_cap { STM_PIN_CAPABILITIES(pd05_alts) };
static StmGpio pd05_obj(GPIOD, LL_GPIO_PIN_5, pd05_cap);
StmPin &pd05 = pd05_obj;


/*******************************************************************************
 * PD06
 ******************************************************************************/
static const StmPin::Alternate pd06_alts[] = {
{
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_06,
                            STM_PIN_SETTING_PORT_GPIOD,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_PUSH_PULL,
                            STM_PIN_SETTING_PUPD_PULL_UP,
                            STM_PIN_SETTING_ALT_AF5),
    .peripheral = SPI3,
    .function   = StmPin::Alternate::Function::kSpiMOSI
}, {
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_06,
                            STM_PIN_SETTING_PORT_GPIOD,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_PUSH_PULL,
                            STM_PIN_SETTING_PUPD_PULL_UP,
                            STM_PIN_SETTING_ALT_AF7),
    .peripheral = USART2,
    .function   = StmPin::Alternate::Function::kUartRX
}
};
static const StmPin::Capabilities pd06_cap { STM_PIN_CAPABILITIES(pd06_alts) };
static StmGpio pd06_obj(GPIOD, LL_GPIO_PIN_6, pd06_cap);
StmPin &pd06 = pd06_obj;


/*******************************************************************************
 * PD07
 ******************************************************************************/
static const StmPin::Alternate pd07_alts[] = {
{
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_07,
                            STM_PIN_SETTING_PORT_GPIOD,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_PUSH_PULL,
                            STM_PIN_SETTING_PUPD_PULL_UP,
                            STM_PIN_SETTING_ALT_AF7),
    .peripheral = USART2,
    .function   = StmPin::Alternate::Function::kUartCK
}
};
static const StmPin::Capabilities pd07_cap { STM_PIN_CAPABILITIES(pd07_alts) };
static StmGpio pd07_obj(GPIOD, LL_GPIO_PIN_7, pd07_cap);
StmPin &pd07 = pd07_obj;


/*******************************************************************************
 * PD08
 ******************************************************************************/
static StmGpio pd08_obj(GPIOD, LL_GPIO_PIN_8, no_cap);
StmPin &pd08 = pd08_obj;


/*******************************************************************************
 * PD09
 ******************************************************************************/
static StmGpio pd09_obj(GPIOD, LL_GPIO_PIN_9, no_cap);
StmPin &pd09 = pd09_obj;


/*******************************************************************************
 * PD10
 ******************************************************************************/
static StmGpio pd10_obj(GPIOD, LL_GPIO_PIN_10, no_cap);
StmPin &pd10 = pd10_obj;


/*******************************************************************************
 * PD11
 ******************************************************************************/
static StmGpio pd11_obj(GPIOD, LL_GPIO_PIN_11, no_cap);
StmPin &pd11 = pd11_obj;


/*******************************************************************************
 * PD12
 ******************************************************************************/
static StmGpio pd12_obj(GPIOD, LL_GPIO_PIN_12, no_cap);
StmPin &pd12 = pd12_obj;


/*******************************************************************************
 * PD13
 ******************************************************************************/
static StmGpio pd13_obj(GPIOD, LL_GPIO_PIN_13, no_cap);
StmPin &pd13 = pd13_obj;


/*******************************************************************************
 * PD14
 ******************************************************************************/
static StmGpio pd14_obj(GPIOD, LL_GPIO_PIN_14, no_cap);
StmPin &pd14 = pd14_obj;


/*******************************************************************************
 * PD15
 ******************************************************************************/
static StmGpio pd15_obj(GPIOD, LL_GPIO_PIN_15, no_cap);
StmPin &pd15 = pd15_obj;
#endif /* defined(PIN_ENABLE_GPIOD) && defined(GPIOD_BASE) */

#if defined(PIN_ENABLE_GPIOE) && defined(GPIOE_BASE)
/*******************************************************************************
 * PE00
 ******************************************************************************/
static StmGpio pe00_obj(GPIOE, LL_GPIO_PIN_0, no_cap);
StmPin &pe00 = pe00_obj;


/*******************************************************************************
 * PE01
 ******************************************************************************/
static StmGpio pe01_obj(GPIOE, LL_GPIO_PIN_1, no_cap);
StmPin &pe01 = pe01_obj;


/*******************************************************************************
 * PE02
 ******************************************************************************/
static const StmPin::Alternate pe02_alts[] = {
{
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_02,
                            STM_PIN_SETTING_PORT_GPIOE,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_PUSH_PULL,
                            STM_PIN_SETTING_PUPD_PULL_UP,
                            STM_PIN_SETTING_ALT_AF5),
    .peripheral = SPI4,
    .function   = StmPin::Alternate::Function::kSpiSCK
}, {
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_02,
                            STM_PIN_SETTING_PORT_GPIOE,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_PUSH_PULL,
                            STM_PIN_SETTING_PUPD_PULL_UP,
                            STM_PIN_SETTING_ALT_AF6),
    .peripheral = SPI5,
    .function   = StmPin::Alternate::Function::kSpiSCK
}
};
static const StmPin::Capabilities pe02_cap { STM_PIN_CAPABILITIES(pe02_alts) };
static StmGpio pe02_obj(GPIOE, LL_GPIO_PIN_2, pe02_cap);
StmPin &pe02 = pe02_obj;


/*******************************************************************************
 * PE03
 ******************************************************************************/
static StmGpio pe03_obj(GPIOE, LL_GPIO_PIN_3, no_cap);
StmPin &pe03 = pe03_obj;


/*******************************************************************************
 * PE04
 ******************************************************************************/
static const StmPin::Alternate pe04_alts[] = {
{
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_04,
                            STM_PIN_SETTING_PORT_GPIOE,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_PUSH_PULL,
                            STM_PIN_SETTING_PUPD_PULL_UP,
                            STM_PIN_SETTING_ALT_AF5),
    .peripheral = SPI4,
    .function   = StmPin::Alternate::Function::kSpiNSS
}, {
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_04,
                            STM_PIN_SETTING_PORT_GPIOE,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_PUSH_PULL,
                            STM_PIN_SETTING_PUPD_PULL_UP,
                            STM_PIN_SETTING_ALT_AF6),
    .peripheral = SPI5,
    .function   = StmPin::Alternate::Function::kSpiNSS
}
};
static const StmPin::Capabilities pe04_cap { STM_PIN_CAPABILITIES(pe04_alts) };
static StmGpio pe04_obj(GPIOE, LL_GPIO_PIN_4, pe04_cap);
StmPin &pe04 = pe04_obj;


/*******************************************************************************
 * PE05
 ******************************************************************************/
static const StmPin::Alternate pe05_alts[] = {
{
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_05,
                            STM_PIN_SETTING_PORT_GPIOE,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_PUSH_PULL,
                            STM_PIN_SETTING_PUPD_PULL_UP,
                            STM_PIN_SETTING_ALT_AF5),
    .peripheral = SPI4,
    .function   = StmPin::Alternate::Function::kSpiMISO
}, {
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_05,
                            STM_PIN_SETTING_PORT_GPIOE,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_PUSH_PULL,
                            STM_PIN_SETTING_PUPD_PULL_UP,
                            STM_PIN_SETTING_ALT_AF6),
    .peripheral = SPI5,
    .function   = StmPin::Alternate::Function::kSpiMISO
}
};
static const StmPin::Capabilities pe05_cap { STM_PIN_CAPABILITIES(pe05_alts) };
static StmGpio pe05_obj(GPIOE, LL_GPIO_PIN_5, pe05_cap);
StmPin &pe05 = pe05_obj;


/*******************************************************************************
 * PE06
 ******************************************************************************/
static const StmPin::Alternate pe06_alts[] = {
{
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_06,
                            STM_PIN_SETTING_PORT_GPIOE,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_PUSH_PULL,
                            STM_PIN_SETTING_PUPD_PULL_UP,
                            STM_PIN_SETTING_ALT_AF5),
    .peripheral = SPI4,
    .function   = StmPin::Alternate::Function::kSpiMOSI
}, {
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_06,
                            STM_PIN_SETTING_PORT_GPIOE,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_PUSH_PULL,
                            STM_PIN_SETTING_PUPD_PULL_UP,
                            STM_PIN_SETTING_ALT_AF6),
    .peripheral = SPI5,
    .function   = StmPin::Alternate::Function::kSpiMOSI
}
};
static const StmPin::Capabilities pe06_cap { STM_PIN_CAPABILITIES(pe06_alts) };
static StmGpio pe06_obj(GPIOE, LL_GPIO_PIN_6, pe06_cap);
StmPin &pe06 = pe06_obj;


/*******************************************************************************
 * PE07
 ******************************************************************************/
static StmGpio pe07_obj(GPIOE, LL_GPIO_PIN_7, no_cap);
StmPin &pe07 = pe07_obj;


/*******************************************************************************
 * PE08
 ******************************************************************************/
static StmGpio pe08_obj(GPIOE, LL_GPIO_PIN_8, no_cap);
StmPin &pe08 = pe08_obj;


/*******************************************************************************
 * PE09
 ******************************************************************************/
static StmGpio pe09_obj(GPIOE, LL_GPIO_PIN_9, no_cap);
StmPin &pe09 = pe09_obj;


/*******************************************************************************
 * PE10
 ******************************************************************************/
static StmGpio pe10_obj(GPIOE, LL_GPIO_PIN_10, no_cap);
StmPin &pe10 = pe10_obj;


/*******************************************************************************
 * PE11
 ******************************************************************************/
static const StmPin::Alternate pe11_alts[] = {
{
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_11,
                            STM_PIN_SETTING_PORT_GPIOE,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_PUSH_PULL,
                            STM_PIN_SETTING_PUPD_PULL_UP,
                            STM_PIN_SETTING_ALT_AF5),
    .peripheral = SPI4,
    .function   = StmPin::Alternate::Function::kSpiNSS
}, {
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_11,
                            STM_PIN_SETTING_PORT_GPIOE,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_PUSH_PULL,
                            STM_PIN_SETTING_PUPD_PULL_UP,
                            STM_PIN_SETTING_ALT_AF6),
    .peripheral = SPI5,
    .function   = StmPin::Alternate::Function::kSpiNSS
}
};
static const StmPin::Capabilities pe11_cap { STM_PIN_CAPABILITIES(pe11_alts) };
static StmGpio pe11_obj(GPIOE, LL_GPIO_PIN_11, pe11_cap);
StmPin &pe11 = pe11_obj;


/*******************************************************************************
 * PE12
 ******************************************************************************/
static const StmPin::Alternate pe12_alts[] = {
{
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_12,
                            STM_PIN_SETTING_PORT_GPIOE,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_PUSH_PULL,
                            STM_PIN_SETTING_PUPD_PULL_UP,
                            STM_PIN_SETTING_ALT_AF5),
    .peripheral = SPI4,
    .function   = StmPin::Alternate::Function::kSpiSCK
}, {
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_12,
                            STM_PIN_SETTING_PORT_GPIOE,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_PUSH_PULL,
                            STM_PIN_SETTING_PUPD_PULL_UP,
                            STM_PIN_SETTING_ALT_AF6),
    .peripheral = SPI5,
    .function   = StmPin::Alternate::Function::kSpiSCK
}
};
static const StmPin::Capabilities pe12_cap { STM_PIN_CAPABILITIES(pe12_alts) };
static StmGpio pe12_obj(GPIOE, LL_GPIO_PIN_12, pe12_cap);
StmPin &pe12 = pe12_obj;


/*******************************************************************************
 * PE13
 ******************************************************************************/
static const StmPin::Alternate pe13_alts[] = {
{
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_13,
                            STM_PIN_SETTING_PORT_GPIOE,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_PUSH_PULL,
                            STM_PIN_SETTING_PUPD_PULL_UP,
                            STM_PIN_SETTING_ALT_AF5),
    .peripheral = SPI4,
    .function   = StmPin::Alternate::Function::kSpiMISO
}, {
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_13,
                            STM_PIN_SETTING_PORT_GPIOE,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_PUSH_PULL,
                            STM_PIN_SETTING_PUPD_PULL_UP,
                            STM_PIN_SETTING_ALT_AF6),
    .peripheral = SPI5,
    .function   = StmPin::Alternate::Function::kSpiMISO
}
};
static const StmPin::Capabilities pe13_cap { STM_PIN_CAPABILITIES(pe13_alts) };
static StmGpio pe13_obj(GPIOE, LL_GPIO_PIN_13, pe13_cap);
StmPin &pe13 = pe13_obj;


/*******************************************************************************
 * PE14
 ******************************************************************************/
static const StmPin::Alternate pe14_alts[] = {
{
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_14,
                            STM_PIN_SETTING_PORT_GPIOE,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_PUSH_PULL,
                            STM_PIN_SETTING_PUPD_PULL_UP,
                            STM_PIN_SETTING_ALT_AF5),
    .peripheral = SPI4,
    .function   = StmPin::Alternate::Function::kSpiMOSI
}, {
    .mode = STM_PIN_SETTING(STM_PIN_SETTING_PIN_14,
                            STM_PIN_SETTING_PORT_GPIOE,
                            STM_PIN_SETTING_FUNCTION_ALTERNATE,
                            STM_PIN_SETTING_OTYPE_PUSH_PULL,
                            STM_PIN_SETTING_PUPD_PULL_UP,
                            STM_PIN_SETTING_ALT_AF6),
    .peripheral = SPI5,
    .function   = StmPin::Alternate::Function::kSpiMOSI
}
};
static const StmPin::Capabilities pe14_cap { STM_PIN_CAPABILITIES(pe14_alts) };
static StmGpio pe14_obj(GPIOE, LL_GPIO_PIN_14, pe14_cap);
StmPin &pe14 = pe14_obj;


/*******************************************************************************
 * PE15
 ******************************************************************************/
static StmGpio pe15_obj(GPIOE, LL_GPIO_PIN_15, no_cap);
StmPin &pe15 = pe15_obj;
#endif /* defined(PIN_ENABLE_GPIOE) && defined(GPIOE_BASE) */

#if defined(PIN_ENABLE_GPIOH) && defined(GPIOH_BASE)
/*******************************************************************************
 * PH00
 ******************************************************************************/
static StmGpio ph00_obj(GPIOH, LL_GPIO_PIN_0, no_cap);
StmPin &ph00 = ph00_obj;

/*******************************************************************************
 * PH01
 ******************************************************************************/
static StmGpio ph01_obj(GPIOH, LL_GPIO_PIN_1, no_cap);
StmPin &ph01 = ph01_obj;
#endif /* defined(PIN_ENABLE_GPIOH) && defined(GPIOH_BASE) */

} /* namespace pin */
} /* namespace io */
} /* namespace arctos */
