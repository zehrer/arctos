/**
 * @file
 * @copyright Copyright (c) 2022 Michael Zehrer. This file is part of ARCTOS
 * @license MIT
 * @brief pin initialization
 */
#include <arctos/io/pin/init.hpp>
#include <arctos/io/pin/stm32/params.h>

#include <arctos/io/stm32hal/hal.h>

#include CMSIS_DEVICE

namespace arctos {
namespace io {
namespace pin {

void init(void) {
#if defined(PIN_ENABLE_GPIOA) && defined(GPIOA_BASE)
    __HAL_RCC_GPIOA_CLK_ENABLE();
#endif

#if defined(PIN_ENABLE_GPIOB) && defined(GPIOB_BASE)
    __HAL_RCC_GPIOB_CLK_ENABLE();
#endif

#if defined(PIN_ENABLE_GPIOC) && defined(GPIOC_BASE)
    __HAL_RCC_GPIOC_CLK_ENABLE();
#endif

#if defined(PIN_ENABLE_GPIOD) && defined(GPIOD_BASE)
    __HAL_RCC_GPIOD_CLK_ENABLE();
#endif

#if defined(PIN_ENABLE_GPIOE) && defined(GPIOE_BASE)
    __HAL_RCC_GPIOE_CLK_ENABLE();
#endif

#if defined(PIN_ENABLE_GPIOF) && defined(GPIOF_BASE)
    __HAL_RCC_GPIOF_CLK_ENABLE();
#endif

#if defined(PIN_ENABLE_GPIOG) && defined(GPIOG_BASE)
    __HAL_RCC_GPIOG_CLK_ENABLE();
#endif

#if defined(PIN_ENABLE_GPIOH) && defined(GPIOH_BASE)
    __HAL_RCC_GPIOH_CLK_ENABLE();
#endif

#if defined(PIN_ENABLE_GPIOI) && defined(GPIOI_BASE)
    __HAL_RCC_GPIOI_CLK_ENABLE();
#endif

#if defined(PIN_ENABLE_GPIOJ) && defined(GPIOJ_BASE)
    __HAL_RCC_GPIOJ_CLK_ENABLE();
#endif

#if defined(PIN_ENABLE_GPIOK) && defined(GPIOK_BASE)
    __HAL_RCC_GPIOK_CLK_ENABLE();
#endif

#if defined(PIN_ENABLE_GPIOZ) && defined(GPIOZ_BASE)
    __HAL_RCC_GPIOZ_CLK_ENABLE();
#endif
}

} /* namespace pin */
} /* namespace io */
} /* namespace arctos */
