/**
 * @file
 * @copyright Copyright (c) 2022 Michael Zehrer. This file is part of ARCTOS
 * @license MIT
 * @brief pin settings
 */
#ifndef IO_PIN_STM32_SETTING_HPP
#define IO_PIN_STM32_SETTING_HPP
#include <stddef.h>
#include <stdint.h>

namespace arctos {
namespace io {
namespace pin {

union StmPinSetting {
    uint32_t mode;

    struct {
        uint16_t pin;       // bit:  0..15 pin (bit-mask)
        uint32_t port:5;    // bit: 16..20 i/o port
        uint32_t function:2;// bit: 21..22 function (as in MODER register)
        uint32_t otype:1;   // bit:     23 output Push-Pull / Open Drain (as in OTYPER reg)
        uint32_t pupd:2;    // bit: 24..25 pull configuration (as in PUPDR reg)
        uint32_t ospeed:2;  // bit: 26..27 speed config (as in OSPEEDR) [NOT USED!]
        uint32_t alt:4;     // bit: 28..31 alternate num (as in AFRL/AFRH reg)
    } attr;
};

#define STM_PIN_SETTING_PIN_MASK                0xFFFF
#define STM_PIN_SETTING_PIN_SHIFT               0
#define STM_PIN_SETTING_PIN_00                  (1 << 0)
#define STM_PIN_SETTING_PIN_01                  (1 << 1)
#define STM_PIN_SETTING_PIN_02                  (1 << 2)
#define STM_PIN_SETTING_PIN_03                  (1 << 3)
#define STM_PIN_SETTING_PIN_04                  (1 << 4)
#define STM_PIN_SETTING_PIN_05                  (1 << 5)
#define STM_PIN_SETTING_PIN_06                  (1 << 6)
#define STM_PIN_SETTING_PIN_07                  (1 << 7)
#define STM_PIN_SETTING_PIN_08                  (1 << 8)
#define STM_PIN_SETTING_PIN_09                  (1 << 9)
#define STM_PIN_SETTING_PIN_10                  (1 << 10)
#define STM_PIN_SETTING_PIN_11                  (1 << 11)
#define STM_PIN_SETTING_PIN_12                  (1 << 12)
#define STM_PIN_SETTING_PIN_13                  (1 << 13)
#define STM_PIN_SETTING_PIN_14                  (1 << 14)
#define STM_PIN_SETTING_PIN_15                  (1 << 15)
#define STM_PIN_SETTING_PIN_ANY                 STM_PIN_SETTING_PIN_MASK

#define STM_PIN_SETTING_PORT_MASK               0x1F
#define STM_PIN_SETTING_PORT_SHIFT              16
#define STM_PIN_SETTING_PORT_GPIOA              1
#define STM_PIN_SETTING_PORT_GPIOB              2
#define STM_PIN_SETTING_PORT_GPIOC              3
#define STM_PIN_SETTING_PORT_GPIOD              4
#define STM_PIN_SETTING_PORT_GPIOE              5
#define STM_PIN_SETTING_PORT_GPIOF              6
#define STM_PIN_SETTING_PORT_GPIOG              7
#define STM_PIN_SETTING_PORT_GPIOH              8
#define STM_PIN_SETTING_PORT_GPIOI              9
#define STM_PIN_SETTING_PORT_GPIOJ              10
#define STM_PIN_SETTING_PORT_GPIOK              11
#define STM_PIN_SETTING_PORT_GPIOZ              26
#define STM_PIN_SETTING_PORT_ANY                STM_PIN_SETTING_PORT_MASK

#define STM_PIN_SETTING_FUNCTION_MASK           0x03
#define STM_PIN_SETTING_FUNCTION_SHIFT          21
#define STM_PIN_SETTING_FUNCTION_INPUT          0x00
#define STM_PIN_SETTING_FUNCTION_OUTPUT         0x01
#define STM_PIN_SETTING_FUNCTION_ALTERNATE      0x02
#define STM_PIN_SETTING_FUNCTION_ANALOG         0x03

#define STM_PIN_SETTING_OTYPE_MASK              0x01
#define STM_PIN_SETTING_OTYPE_SHIFT             23
#define STM_PIN_SETTING_OTYPE_PUSH_PULL         0x00
#define STM_PIN_SETTING_OTYPE_OPEN_DRAIN        0x01

#define STM_PIN_SETTING_PUPD_MASK               0x03
#define STM_PIN_SETTING_PUPD_SHIFT              24
#define STM_PIN_SETTING_PUPD_NONE               0x00
#define STM_PIN_SETTING_PUPD_PULL_UP            0x01
#define STM_PIN_SETTING_PUPD_PULL_DOWN          0x02

#define STM_PIN_SETTING_OSPEED_MASK             0x03
#define STM_PIN_SETTING_OSPEED_SHIFT            26
#define STM_PIN_SETTING_OSPEED_LOW              0x00
#define STM_PIN_SETTING_OSPEED_MEDIUM           0x01
#define STM_PIN_SETTING_OSPEED_HIGH             0x02
#define STM_PIN_SETTING_OSPEED_VERY_HIGH        0x03

#define STM_PIN_SETTING_ALT_MASK                0x0F
#define STM_PIN_SETTING_ALT_SHIFT               28
#define STM_PIN_SETTING_ALT_AF0                 0x00
#define STM_PIN_SETTING_ALT_AF1                 0x01
#define STM_PIN_SETTING_ALT_AF2                 0x02
#define STM_PIN_SETTING_ALT_AF3                 0x03
#define STM_PIN_SETTING_ALT_AF4                 0x04
#define STM_PIN_SETTING_ALT_AF5                 0x05
#define STM_PIN_SETTING_ALT_AF6                 0x06
#define STM_PIN_SETTING_ALT_AF7                 0x07
#define STM_PIN_SETTING_ALT_AF8                 0x08
#define STM_PIN_SETTING_ALT_AF9                 0x09
#define STM_PIN_SETTING_ALT_AF10                0x0A
#define STM_PIN_SETTING_ALT_AF11                0x0B
#define STM_PIN_SETTING_ALT_AF12                0x0C
#define STM_PIN_SETTING_ALT_AF13                0x0D
#define STM_PIN_SETTING_ALT_AF14                0x0E
#define STM_PIN_SETTING_ALT_AF15                0x0F

#define STM_PIN_SETTING(PIN, PORT, FUNC, OTYPE, PULL, ALT) \
((uint32_t)(   (PIN   << STM_PIN_SETTING_PIN_SHIFT)\
             | (PORT  << STM_PIN_SETTING_PORT_SHIFT)\
             | (FUNC  << STM_PIN_SETTING_FUNCTION_SHIFT)\
             | (OTYPE << STM_PIN_SETTING_OTYPE_SHIFT)\
             | (PULL  << STM_PIN_SETTING_PUPD_SHIFT)\
             | (ALT   << STM_PIN_SETTING_ALT_SHIFT)\
           ))

} /* namespace pin */
} /* namespace io */
} /* namespace arctos */

#endif /* IO_PIN_STM32_SETTING_HPP */
