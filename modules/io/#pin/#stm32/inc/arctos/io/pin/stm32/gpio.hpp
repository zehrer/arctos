/**
 * @file
 * @copyright Copyright (c) 2022 Michael Zehrer. This file is part of ARCTOS
 * @license MIT
 * @brief stm32 GPIO driver
 */
#ifndef IO_PIN_STM32_GPIO_HPP
#define IO_PIN_STM32_GPIO_HPP
#include <stddef.h>
#include <stdint.h>

#include <arctos/io/pin/pin.hpp>

#include CMSIS_DEVICE

namespace arctos {
namespace io {
namespace pin {

class StmGpio : public StmPin {
public:
    constexpr StmGpio(GPIO_TypeDef *const peripheral,
                      const uint32_t pin,
                      const StmPin::Capabilities &caps) :
        StmPin(caps), peripheral_(peripheral), pin_(pin) {}

    /**
     * @brief Checks if the pin is in the specified mode
     * @param[in] mode Mode
     * @see (Sub-)Namespace "mode"
     * @return true, if the given mode corresponds to the current configuration
     */
    bool isMode(const uint32_t mode) override;
    /**
     * @brief (Re-)configures the pin mode
     * @param[in] mode New mode
     * @see (Sub-)Namespace "mode"
     */
    void mode(const uint32_t mode) override;

    /**
     * @brief Sets the pin
     * @important Only possible if the pin is configured as output
     */
    void set() override;
    /**
     * @brief Clears the pin
     * @important Only possible if the pin is configured as output
     */
    void clear() override;

    /**
     * @brief Reads the value from the pin
     * @return true, if pin is logical HIGH
     */
    bool isHigh(void) override;

protected:
    GPIO_TypeDef *const peripheral_;
    const uint32_t pin_;

private:
    /**
     * @brief Checks, if the given mode is applicable
     *
     * This function performs only some very basic checks!
     * (pin number and i/o port)
     *
     * @param[in] mode Mode to check for
     * @return true, if compatible
     */
    bool checkModeCompatibility(const uint32_t mode);
};

} /* namespace pin */
} /* namespace io */
} /* namespace arctos */

#endif /* IO_PIN_STM32_GPIO_HPP */
