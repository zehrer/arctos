/**
 * @file
 * @copyright Copyright (c) 2022 Michael Zehrer. This file is part of ARCTOS
 * @license MIT
 * @brief pin (respectively gpio) control module
 */
#ifndef IO_PIN_PIN_HPP
#define IO_PIN_PIN_HPP
#include <stddef.h>
#include <stdint.h>

namespace arctos {
namespace io {
namespace pin {

class Pin {
protected:
    Pin() = default;

public:
    /**
     * @brief Checks if the pin is in the specified mode
     * @param[in] mode Mode
     * @see (Sub-)Namespace "mode"
     * @return true, if the given mode corresponds to the current configuration
     */
    virtual bool isMode(const uint32_t mode) = 0;
    /**
     * @brief (Re-)configures the pin mode
     * @param[in] mode New mode
     * @see (Sub-)Namespace "mode"
     */
    virtual void mode(const uint32_t mode) = 0;

    /**
     * @brief Sets the pin
     * @important Only possible if the pin is configured as output
     */
    virtual void set() = 0;
    /**
     * @brief Clears the pin
     * @important Only possible if the pin is configured as output
     */
    virtual void clear() = 0;

    /**
     * @brief Reads the value from the pin
     * @return true, if pin is logical HIGH
     */
    virtual bool isHigh(void) = 0;
    /**
     * @brief Reads the value from the pin
     * @return true, if pin is logical LOW
     */
    virtual bool isLow(void) { return !isHigh(); }

    bool operator==(Pin const &other) const { return (this == &other); }
    bool operator!=(Pin const &other) const { return (this != &other); }
};

} /* namespace pin */
} /* namespace io */
} /* namespace arctos */

#if __has_include (<arctos/io/pin/port.hpp>)
#  include <arctos/io/pin/port.hpp> /* include SoC specific port of this module */
#endif

#endif /* IO_PIN_PIN_HPP */
