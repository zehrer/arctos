/**
 * Copyright (c) 2008-2018, DLR and University Wuerzburg
 * All rights reserved.
 *
 * @licence BSD
 * @brief   basis to rotate from one vector to another
 * @author  Unknown
 */
#include <arctos/matlib/misc.hpp>
#include <arctos/matlib/rotor.hpp>
#include <arctos/matlib/vector3d.hpp>

#include <arctos/debug.h>

namespace arctos {
namespace matlib {

Rotor::Rotor(const Vector3D& fromVector, const Vector3D& toVector) {
    Vector3D from = fromVector.normalize();
    Vector3D to   = toVector.normalize();
    cosAngle = dotProduct  (from, to);
    axis     = crossProduct(from, to);
    if (isAlmost0(axis.getLen())) { cosAngle = axis.x = axis.y = axis.z = 1.0; }
    axis = axis.normalize();
}

bool Rotor::resetIfNAN() {
    bool error = !isfinite(cosAngle) || !isfinite(axis.x) || !isfinite(axis.y) || !isfinite(axis.z);
    if (error) cosAngle = axis.x = axis.y = axis.z = 1.0;
    return error;
}

bool Rotor::isNoRotation() {
    return isAlmost0(1 - cosAngle)  || isAlmost0(axis.getLen());
}

void Rotor::print(){
    PRINTF("[%f (%f, %f, %f)]\n", cosAngle, axis.x, axis.y, axis.z);
}

} /* namespace matlib */
} /* namespace arctos */
