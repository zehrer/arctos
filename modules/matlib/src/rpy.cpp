/**
 * Copyright (c) 2008-2018, DLR and University Wuerzburg
 * All rights reserved.
 *
 * @licence BSD
 * @brief   Roll, Pitch, Yaw
 * @author  Unknown
 */
#include <arctos/matlib/angle_axis.hpp>
#include <arctos/matlib/matrix3d.hpp>
#include <arctos/matlib/quaternion.hpp>
#include <arctos/matlib/rotor.hpp>
#include <arctos/matlib/rpy.hpp>
#include <arctos/matlib/vector3d.hpp>

namespace arctos {
namespace matlib {

RPY::RPY(const Quaternion& q) {
    double m21 = 2*q.q.x*q.q.y + 2*q.q0*q.q.z;
    double m11 = 2*q.q0*q.q0-1 + 2*q.q.x*q.q.x;
    double m31 = 2*q.q.x*q.q.z - 2*q.q0*q.q.y;
    double m32 = 2*q.q.y*q.q.z + 2*q.q0*q.q.x;
    double m33 = 2*q.q0*q.q0-1 + 2*q.q.z*q.q.z;

    this->x = atan2(m32, m33);                      // roll
    this->y = atan2(-m31, sqrt(m11*m11 + m21*m21)); // pitch
    this->z = atan2(m21, m11);                      // yaw
}

RPY::RPY(const Matrix3D& M) {
    double m21 = M.r[1][0];
    double m11 = M.r[0][0];
    double m31 = M.r[2][0];
    double m32 = M.r[2][1];
    double m33 = M.r[2][2];

    this->x = atan2(m32, m33);                      // roll
    this->y = atan2(-m31, sqrt(m11*m11 + m21*m21)); // pitch
    this->z = atan2(m21, m11);                      // yaw
}

RPY::RPY(const AngleAxis& other) {
    Vector3D u = other.u;
    double phi = other.phi;
    double cp  = cos(phi);
    double sp  = sin(phi);


    double m21 = u.x * u.y *(1-cp) + u.z*sp;
    double m11 = u.x * u.x *(1-cp) + cp;
    double m31 = u.x * u.z *(1-cp) - u.y*sp;
    double m32 = u.y * u.z *(1-cp) + u.x*sp;
    double m33 = u.z * u.z *(1-cp) + cp;

    this->x = atan2(m32, m33);                      // roll
    this->y = atan2(-m31, sqrt(m11*m11 + m21*m21)); // pitch
    this->z = atan2(m21, m11);                      // yaw
}

RPY::RPY(const Rotor& rot) {
    Vector3D u = rot.axis;
    double cp  = rot.cosAngle;
    double sp  = sqrt(1 - cp*cp);

    double m21 = u.x * u.y *(1-cp) + u.z*sp;
    double m11 = u.x * u.x *(1-cp) + cp;
    double m31 = u.x * u.z *(1-cp) - u.y*sp;
    double m32 = u.y * u.z *(1-cp) + u.x*sp;
    double m33 = u.z * u.z *(1-cp) + cp;

    this->x = atan2(m32, m33);                      // roll
    this->y = atan2(-m31, sqrt(m11*m11 + m21*m21)); // pitch
    this->z = atan2(m21, m11);                      // yaw
}

/***********************************************************************/

RPY RPY::accumulateRotation(RPY& increment) {
    Quaternion orig(*this);
    Quaternion inc(increment);
    orig = orig * inc;
    return orig.toRPY();
}

/***********************************************************************/

Matrix3D RPY::toMatrix3D() const {
    Matrix3D M;
    double cr = cos(x);
    double cp = cos(y);
    double cy = cos(z);

    double sr = sin(x);
    double sp = sin(y);
    double sy = sin(z);

    M.r[0][0]= cy*cp;
    M.r[0][1]= cy*sp*sr - sy*cr;
    M.r[0][2]= cy*sp*cr + sy*sr;

    M.r[1][0]= sy*cp;
    M.r[1][1]= sy*sp*sr + cy*cr;
    M.r[1][2]= sy*sp*cr - cy*sr;

    M.r[2][0]= -sp;
    M.r[2][1]= cp*sr;
    M.r[2][2]= cp*cr;

    return M;
}

Quaternion RPY::toQuaternion() const {
    Quaternion q;
    double cr2 = cos(x/2);
    double cp2 = cos(y/2);
    double cy2 = cos(z/2);

    double sr2 = sin(x/2);
    double sp2 = sin(y/2);
    double sy2 = sin(z/2);

    q.q0  = cr2*cp2*cy2 + sr2*sp2*sy2;
    q.q.x = sr2*cp2*cy2 - cr2*sp2*sy2;
    q.q.y = cr2*sp2*cy2 + sr2*cp2*sy2;
    q.q.z = cr2*cp2*sy2 - sr2*sp2*cy2;

    return q;
}

AngleAxis RPY::toAngleAxis() const {
    double r = this->x;
    double p = this->y;
    double y = this->z;

    double phi = acos(0.5*(-1 + cos(p)*cos(y) + cos(r)*cos(y) + sin(r)*sin(p)*sin(y) + cos(r)*cos(p) ));

    double u_x = 1/(2*sin(phi))* (sin(r)*cos(p) -cos(r)*sin(p)*sin(y) +sin(r)*cos(y));
    double u_y = 1/(2*sin(phi))* (sin(r)*sin(y) +cos(r)*sin(p)*cos(y) +sin(p));
    double u_z = 1/(2*sin(phi))* (cos(p)*sin(y) -sin(r)*sin(p)*cos(y) + cos(r)*sin(y));

    Vector3D u(u_x, u_y, u_z);
    AngleAxis u_phi(phi, u);

    return u_phi;
}

} /* namespace matlib */
} /* namespace arctos */
