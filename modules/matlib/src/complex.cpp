/**
 * Copyright (c) 2008-2018, DLR and University Wuerzburg
 * All rights reserved.
 *
 * @licence BSD
 * @brief   complex numbers 
 * @author  Unknown
 */
#include <arctos/matlib/complex.hpp>

#include <math.h>

namespace arctos {
namespace matlib {

Complex::Complex() {
    this->Re = 0.0;
    this->Im = 0.0;
}

Complex::Complex(const double& Re, const double& Im) {
    this->Re = Re;
    this->Im = Im;
}

Complex::Complex(const Complex& other) {
    this->Re = other.Re;
    this->Im = other.Im;
}

/***********************************************************************/

Complex Complex::cAdd(const Complex& other) const {
    Complex z;
    z.Re = other.Re + this->Re;
    z.Im = other.Im + this->Im;
    return z;
}

Complex Complex::cSub(const Complex& other) const {
    Complex z;
    z.Re = other.Re - this->Re;
    z.Im = other.Im - this->Im;
    return z;
}

Complex Complex::cScale(const double &scale) const {
    Complex z =(*this);
    z.Re *= scale;
    z.Im *= scale;
    return z;
}

Complex Complex::cMult(const Complex& other) const {
    Complex z;
    z.Re = this->Re*other.Re -(other.Im*this->Im);
    z.Im = this->Re*other.Im +this->Im*other.Re;

    return z;
}

Complex Complex::cPow(const int &exponent) const {
    Complex z;
    if (exponent ==0) {
        z.Re =1;
        return z;
    }

    z= (*this);
    for (int i = 1; i<exponent; i++) {
        z=z.cMult(*this);
    }

    return z;
}

Complex Complex::cExp() const {
    Complex z;
    z.Re = exp(this->Re)*cos(this->Im);
    z.Im = exp(this->Re)*sin(this->Im);

    return z;
}

} /* namespace matlib */
} /* namespace arctos */
