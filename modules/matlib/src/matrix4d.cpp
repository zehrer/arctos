/**
 * Copyright (c) 2008-2018, DLR and University Wuerzburg
 * All rights reserved.
 *
 * @licence BSD
 * @brief   4x4 matrix 
 * @author  Unknown
 */
#include <arctos/matlib/matrix3d.hpp>
#include <arctos/matlib/matrix4d.hpp>
#include <arctos/matlib/vector3d.hpp>

#include <arctos/debug.h>

#include <math.h>

namespace arctos {
namespace matlib {

/***********************************************************************/
/*==== constructors ====*/

Matrix4D::Matrix4D() {
    //column1
    r[0][0] = 1.0;
    r[1][0] = 0.0;
    r[2][0] = 0.0;
    r[3][0] = 0.0;

    //column 2
    r[0][1] = 0.0;
    r[1][1] = 1.0;
    r[2][1] = 0.0;
    r[3][1] = 0.0;

    //column 3
    r[0][2] = 0.0;
    r[1][2] = 0.0;
    r[2][2] = 1.0;
    r[3][2] = 0.0;

    //column 4
    r[0][3] = 0.0;
    r[1][3] = 0.0;
    r[2][3] = 0.0;
    r[3][3] = 1.0;
}

Matrix4D::Matrix4D(const Matrix3D& rot, const Vector3D& trans) {
    for (int i = 0 ; i<3; i++) {
        for (int j = 0; j<3; j++) {
            r[i][j]=rot.r[i][j];
        }
    }

    //column 4
    r[0][3] = trans.x;
    r[1][3] = trans.y;
    r[2][3] = trans.z;
    r[3][3] = 1.0;

    //row 4
    r[3][0]=0.0;
    r[3][1]=0.0;
    r[3][2]=0.0;
}

Matrix4D::Matrix4D(double* arr) {
    int k = 0;
    for (int i = 0 ; i<4; i++) {
        for (int j = 0; j<4; j++) {
            r[i][j]=arr[k];
            k++;
        }
    }
}

Matrix4D::Matrix4D(const Matrix4D& other) {
    for (int i = 0 ; i<4; i++) {
        for (int j = 0; j<4; j++) {
            r[i][j]=other.r[i][j];
        }
    }
}

/***********************************************************************/

Matrix3D Matrix4D::getRotation() const {
    Matrix3D R;
    for (int i = 0 ; i<3; i++) {
        for (int j = 0; j<3; j++) {
            R.r[i][j]=this->r[i][j];
        }
    }

    return R;
}

Vector3D Matrix4D::getTranslation() const {
    Vector3D trans(this->r[0][3],this->r[1][3],this->r[2][3]);
    return trans;
}

/***********************************************************************/

Matrix4D Matrix4D::scale(const double &factor) const {
    Matrix4D scale;
    for (int i = 0 ; i<3; i++) {
        scale.r[i][i]=this->r[i][i]*factor;
    }
    return scale;
}

Matrix4D Matrix4D::mMult(const Matrix4D& other) const {
    Matrix4D mult;
    double sum;

    for (int i=0; i<4; i++)
        for (int j=0; j<4; j++) {
            sum = 0.0;
            for (int k=0; k<4; k++)
                sum += this->r[i][k] * other.r[k][j];
            mult.r[i][j] = sum;
        }
    return mult;
}

Matrix4D Matrix4D::invert() const {
    Matrix3D transpose = this->getRotation().transpose();
    Vector3D invtrans  = this->getTranslation().matVecMult(transpose);
    invtrans = invtrans.scale(-1);
    Matrix4D inverse(transpose,invtrans);

    return inverse;
}

/***********************************************************************/

void Matrix4D::print() const {
    for (int k=0; k<4; k++) {
        for (int l = 0; l<4; l++) {
            if (this->r[k][l]>=0) {
                PRINTF(" ");
            }
            PRINTF("%3.3f \t",this->r[k][l]);
        }
        PRINTF("\n");
    }
}

/***********************************************************************/

Matrix4D operator+(const Matrix4D& left, const Matrix4D& right) {
    Matrix4D sum;
    for ( int i = 0; i < 4; i++) {
        for (int j = 0; j < 4; j++) {
            sum.r[i][j] = left.r[i][j] + right.r[i][j];
        }
    }

    return sum;
}

Matrix4D operator*(const Matrix4D& left, const double& right) {
    Matrix4D res;

    for ( int i = 0; i < 4; i++ ) {
        for ( int j = 0; j < 4; j++ ) {
            res.r[i][j] = left.r[i][j] * right;
        }
    }
    return res;
}

Matrix4D operator/(const Matrix4D& left, const double& right) {
    Matrix4D res;

    for ( int i = 0; i < 4; i++ ) {
        for ( int j = 0; j < 4; j++ ) {
            res.r[i][j] = left.r[i][j] * 1.0/right;
        }
    }
    return res;
}

} /* namespace matlib */
} /* namespace arctos */
