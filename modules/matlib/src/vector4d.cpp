/**
 * Copyright (c) 2008-2018, DLR and University Wuerzburg
 * All rights reserved.
 *
 * @licence BSD
 * @brief   4-dim mathematical vector with x, y, z and an scaling factor 
 * @author  Unknown
 */
#include <arctos/matlib/matrix4d.hpp>
#include <arctos/matlib/vector3d.hpp>
#include <arctos/matlib/vector4d.hpp>

#include <arctos/debug.h>

namespace arctos {
namespace matlib {

Vector4D::Vector4D() {
    this->x=0;
    this->y=0;
    this->z=0;
    this->scale=1;
}

Vector4D::Vector4D(const double& x, const double& y, const double& z, const double& scale) {
    this->x=x;
    this->y=y;
    this->z=z;
    this->scale=scale;
}

Vector4D::Vector4D(const Vector4D& other)
    : Vector3D(other.x, other.y, other.z), scale(other.scale) { }

Vector4D::Vector4D (double* arr) {
    this->x = arr[0];
    this->y = arr[1];
    this->z = arr[2];
    this->scale=arr[3];
}

/***********************************************************************/

Vector4D Vector4D::matVecMult(const Matrix4D& r) const {
    double x = r.r[0][0]*this->x + r.r[0][1]*this->y + r.r[0][2]*this->z+r.r[0][3];
    double y = r.r[1][0]*this->x + r.r[1][1]*this->y + r.r[1][2]*this->z+r.r[1][3];
    double z = r.r[2][0]*this->x + r.r[2][1]*this->y + r.r[2][2]*this->z+r.r[2][3];
    Vector4D result(x,y,z,1);
    return result;
}

Vector3D Vector4D::to3D() const {
    Vector3D v(this->x,this->y,this->z);
    return v;
}

Vector4D Vector4D::mRotate(const Matrix4D& r) const {
    Vector4D w(this->matVecMult(r));
    return w;
}

void Vector4D::print() const {
    PRINTF("(%3.3f \t %3.3f \t %3.3f \t %3.3f)\n",this->x,this->y,this->z,this->scale);
}

} /* namespace matlib */
} /* namespace arctos */
