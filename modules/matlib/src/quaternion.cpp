/**
 * Copyright (c) 2008-2018, DLR and University Wuerzburg
 * All rights reserved.
 *
 * @licence BSD
 * @brief   quaternion
 * @author  Unknown
 */
#include <arctos/matlib/angle_axis.hpp>
#include <arctos/matlib/matrix3d.hpp>
#include <arctos/matlib/matrix4d.hpp>
#include <arctos/matlib/misc.hpp>
#include <arctos/matlib/quaternion.hpp>
#include <arctos/matlib/rotor.hpp>
#include <arctos/matlib/rpy.hpp>

#include <arctos/debug.h>

#include <math.h>

namespace arctos {
namespace matlib {

Quaternion::Quaternion() {
    Vector3D q;
    this->q0 = 1.0;
    this->q  = q;
}

Quaternion::Quaternion(const Quaternion& other) {
    this->q0 = other.q0;
    this->q  = other.q;
}

Quaternion::Quaternion(const double& q0, const double& q1, const double& q2, const double& q3) {
    Vector3D q(q1, q2, q3);
    this->q0 = q0;
    this->q  = q;
}

Quaternion::Quaternion(double* arr) {
    Vector3D vec(arr[1], arr[2], arr[3]);
    this->q0 = arr[0];
    this->q  = vec;
}

Quaternion::Quaternion(const double& q0,const Vector3D& q) {
    this->q = q;
    this->q0 = q0;

}

Quaternion::Quaternion(const AngleAxis& other) {
    this->q0 = cos(other.phi/2);
    this->q  = other.u.normalize().scale(sin(other.phi/2));
}

Quaternion::Quaternion(const Rotor& rot) {
    double angle = acos(rot.cosAngle);
    this->q0 = cos(angle/2);
    this->q  = rot.axis.normalize().scale(sin(angle/2));
}

Quaternion::Quaternion(const Matrix3D& other) {  //Algorithmus 1
    double q0,q1,q2,q3;
    q0 = q1 = q2 = q3 = 0;
    double c1 = 1+ other.r[0][0] + other.r[1][1] + other.r[2][2];
    double c2 = 1+ other.r[0][0] - other.r[1][1] - other.r[2][2];
    double c3 = 1- other.r[0][0] + other.r[1][1] - other.r[2][2];
    double c4 = 1- other.r[0][0] - other.r[1][1] + other.r[2][2];
    double c = c1;
    // Finde das Maximum
    if (c2>c) c = c2;
    if (c3>c) c = c3;
    if (c4>c) c = c4;
    //Fallunterscheidung
    if (c==c1) {
        c = 0.5 * sqrt(c);
        q0 = c;
        q1 = (other.r[2][1]-other.r[1][2])/(4*c);
        q2 = (other.r[0][2]-other.r[2][0])/(4*c);
        q3 = (other.r[1][0]-other.r[0][1])/(4*c);
    }

    if (c==c2) {
        c = 0.5 * sqrt(c);
        q0 = (other.r[2][1]-other.r[1][2])/(4*c);
        q1 = c;
        q2 = (other.r[1][0]+other.r[0][1])/(4*c);
        q3 = (other.r[0][2]+other.r[2][0])/(4*c);
    }

    if (c==c3) {
        c = 0.5 * sqrt(c);
        q0 = (other.r[0][2]-other.r[2][0])/(4*c);
        q1 = (other.r[1][0]+other.r[0][1])/(4*c);
        q2 = c;
        q3 = (other.r[2][1]+other.r[1][2])/(4*c);
    }

    if (c==c4) {
        c = 0.5 * sqrt(c);
        q0 = (other.r[1][0]-other.r[0][1])/(4*c);
        q1 = (other.r[0][2]+other.r[2][0])/(4*c);
        q2 = (other.r[2][1]+other.r[1][2])/(4*c);
        q3 = c;
    }
    Vector3D q(q1,q2,q3);

    this->q0 = q0;
    this->q = q;
}

Quaternion::Quaternion(const RPY& other) {

    double a = other.x/2;
    double b = other.y/2;
    double c = other.z/2;

    double cdx = cos(a);
    double sdx = sin(a);
    double cdy = cos(b);
    double sdy = sin(b);
    double cdz = cos(c);
    double sdz = sin(c);

    //% Transforms RPY EulerAngles into Quaternion
    //% dx angle of rotation about x-axis (using RAD) | cdx = cos(dx) etc.
    //% dy angle of rotation about y-axis
    //% dz angle of rotation about z-axis

    double q0 = cdz*cdy*cdx + sdz*sdy*sdx;
    double q1 = cdz*cdy*sdx - sdz*sdy*cdx;
    double q2 = cdz*sdy*cdx + sdz*cdy*sdx;
    double q3 = sdz*cdy*cdx - cdz*sdy*sdx;

    double len  = sqrt(q0*q0 + q1*q1 + q2*q2 + q3*q3);

    q0 = q0 / len;
    q1 = q1 / len;
    q2 = q2 / len;
    q3 = q3 / len;

    Vector3D q(q1, q2, q3);

    this->q0 = q0;
    this->q  = q;
}

/***********************************************************************/

bool Quaternion::resetIfNAN() {
    bool error = !isfinite(q0) || !isfinite(q.x) || !isfinite(q.y) || !isfinite(q.z);
    if (error) { q0 = 1;   q.x = q.y = q.z = 0; }
    return error;
}

/***********************************************************************/
/*====getter====*/

Vector3D Quaternion::getVec() const  {
    Vector3D u= this->q.normalize();
    return u;
}

double Quaternion::getAngle() const {
    return 2*acos(q0);
}

/***********************************************************************/
/*==== Add, Sub, qMult, scale====*/

Quaternion Quaternion::qAdd(const Quaternion& other) const {
    Quaternion sum;
    sum.q0 = this->q0 + other.q0;
    sum.q  = this->q.vecAdd(other.q);
    return sum;
}

Quaternion Quaternion::qSub(const Quaternion& other) const {
    Quaternion diff;
    diff.q0 = this->q0 - other.q0;
    diff.q  = this->q.vecSub(other.q);
    return diff;
}

Quaternion Quaternion::scale(const double &factor) const {
    Quaternion scale;
    scale.q0 = this->q0 *factor;
    scale.q  = this->q.scale(factor);
    return scale;
}

Quaternion Quaternion::qMult(const Quaternion& other) const {
    Quaternion mult, left, right;
    left = (*this);
    right = other;

    if (left.q.equals(right.q)) {
        mult.q0 = left.q0 + right.q0;
        mult.q = left.q;
    }

    // mult.q0 = left.q0*right.q0 - left.q.dot(right.q);
    // mult.q  = left.q.scale(right.q0).vecAdd(right.q.scale(left.q0))
    //         .vecAdd(left.q.cross(right.q));

    mult.q0 = left.q0 * right.q0 - dotProduct(left.q, right.q);

    mult.q  = left.q  * right.q0 + // Warning: vectro ops
              right.q * left.q0  + // warning: operator +, * etc
              crossProduct(left.q, right.q);
    return mult;
}

/***********************************************************************/
/*==== invert, conjugate, qdivide====*/
Quaternion Quaternion::invert() const {
    double revNorm = 1/this->getLen();
    return this->conjugate().scale(revNorm);
}

Quaternion Quaternion::conjugate() const {
    Quaternion conjugate;
    conjugate.q0 = this->q0;
    conjugate.q = this->q.scale(-1);
    return conjugate;
}

Quaternion Quaternion::qDivide(const Quaternion& other) const {
    Quaternion division = other;
    Quaternion invert = division.conjugate();
    division = this->qMult(invert);
    return division;
}

/***********************************************************************/
/*====Norm,normalize,isNormalized,equals====*/

double Quaternion::getLen() const {
    double quads = q0*q0 + q.x*q.x + q.y*q.y + q.z*q.z;
    return sqrt(quads);
}

Quaternion Quaternion::normalize() const {
    Quaternion unit;
    unit = this->scale(1.0/this->getLen());
    return unit;
}

bool Quaternion::isNormalized() const {
    double len=this->getLen();
    return isAlmost0(len-1);
}

bool Quaternion::equals(const Quaternion& other) const {
    return(q0 == other.q0 && q.equals(other.q));
}

AngleAxis Quaternion::toAngleAxis() const {
    AngleAxis u_phi(this->getAngle(), this->getVec());
    return u_phi;
}

Matrix3D Quaternion::toMatrix3D() const { // return matrix representation of quaternion
    Matrix3D R;
    //column1
    R.r[0][0] = 2*this->q0 * this->q0-1 + 2*this->q.x * this->q.x;
    R.r[1][0] = 2*this->q.y*this->q.x   + 2*this->q0*this->q.z;
    R.r[2][0] = 2*this->q.z*this->q.x   - 2*this->q0*this->q.y;

    //column 2
    R.r[0][1] = 2*this->q.y*this->q.x   - 2*this->q0*this->q.z;
    R.r[1][1] = 2*this->q0 * this->q0-1 + 2*this->q.y*this->q.y;
    R.r[2][1] = 2*this->q.y*this->q.z   + 2*this->q0*this->q.x;

    //column 3
    R.r[0][2] = 2*this->q.z*this->q.x   + 2*this->q0*this->q.y;
    R.r[1][2] = 2*this->q.y*this->q.z   - 2*this->q0*this->q.x;
    R.r[2][2] = 2*this->q0 * this->q0-1 + 2*this->q.z * this->q.z;
    return R;
}

RPY Quaternion::toRPY() const {
    RPY rpy;
    double m21 = 2*this->q.x * this->q.y  + 2*this->q0 *this->q.z;
    double m11 = 2*this->q0  * this->q0-1 + 2*this->q.x*this->q.x;
    double m31 = 2*this->q.x * this->q.z  - 2*this->q0 *this->q.y;
    double m32 = 2*this->q.y * this->q.z  + 2*this->q0 *this->q.x;
    double m33 = 2*this->q0  * this->q0-1 + 2*this->q.z*this->q.z;

    rpy.x  = atan2(m32, m33);
    rpy.y  = atan2(-m31, sqrt(m11*m11 + m21*m21));
    rpy.z  = atan2(m21, m11);

    return rpy;
}

/***********************************************************************/

void Quaternion::print() const {
    PRINTF("[ %3.3f \t (%3.3f \t %3.3f \t %3.3f)]\n",q0, q.x, q.y, q.z);
}

/***********************************************************************/

Quaternion operator*(const Matrix4D& left, const Quaternion& right) {
    double q[] = { right.q0, right.q.x, right.q.y, right.q.z };
    double res[4] = { 0.0 };

    for ( int i = 0; i < 4; i++ ) {
        for ( int j = 0; j < 4; j++ ) {
            res[i] += left.r[i][j] * q[j];
        }
    }
    return Quaternion( res );
}

} /* namespace matlib */
} /* namespace arctos */
