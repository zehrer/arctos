/**
 * Copyright (c) 2008-2018, DLR and University Wuerzburg
 * All rights reserved.
 *
 * @licence BSD
 * @brief   polar coordinate system
 * @author  Unknown
 */
#include <arctos/matlib/polar.hpp>
#include <arctos/matlib/vector3d.hpp>

#include <arctos/debug.h>

#include <math.h>

#ifndef M_PI
    #define M_PI 3.14159265358979323846
#endif

namespace arctos {
namespace matlib {

Polar::Polar() {
    this->r     = 0;
    this->phi   = 0;
    this->theta = 0;
}

Polar::Polar(const double& r,const double& phi,const double& theta) {
    this->r     = r;
    this->phi   = phi;
    this->theta = theta;
}

Polar::Polar(const Polar& other) {
    this->r     = other.r;
    this->phi   = other.phi;
    this->theta = other.theta;
}

Polar::Polar(const Vector3D& other) {
    this->r     = sqrt(other.x*other.x + other.y*other.y + other.z*other.z) ;
    this->phi   = atan2(other.y,other.x);
    this->theta = acos(other.z/this->r);
}

/***********************************************************************/

Vector3D Polar::toCartesian() const {
    double x = this->r*sin(this->theta)*cos(this->phi);
    double y = this->r*sin(this->theta)*sin(this->phi);
    double z = this->r*sin(this->theta);
    Vector3D cartesian(x,y,z);

    return cartesian;
}

/***********************************************************************/

void Polar::print() const {
    PRINTF("[r= %3.3f \t phi= %3.3f*PI \t theta= %3.3f*PI]\n",r, phi/M_PI, theta/M_PI);
}

} /* namespace matlib */
} /* namespace arctos */
