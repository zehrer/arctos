/**
 * Copyright (c) 2008-2018, DLR and University Wuerzburg
 * All rights reserved.
 *
 * @licence BSD
 * @brief   3-dim mathematical vector with x, y and z 
 * @author  Unknown
 */
#include <arctos/matlib/angle_axis.hpp>
#include <arctos/matlib/matrix3d.hpp>
#include <arctos/matlib/misc.hpp>
#include <arctos/matlib/polar.hpp>
#include <arctos/matlib/quaternion.hpp>
#include <arctos/matlib/rpy.hpp>
#include <arctos/matlib/vector3d.hpp>
#include <arctos/matlib/vector4d.hpp>

#include <arctos/debug.h>

#include <math.h>

namespace arctos {
namespace matlib {

Vector3D::Vector3D() {
    this->x = 0;
    this->y = 0;
    this->z = 0;
}

Vector3D::Vector3D(const double& x, const double& y, const double& z) {
    this->x = x;
    this->y = y;
    this->z = z;
}

Vector3D::Vector3D(const Vector3D& other) {
    this->x = other.x;
    this->y = other.y;
    this->z = other.z;
}

Vector3D::Vector3D(double* arr) {
    this->x = arr[0];
    this->y = arr[1];
    this->z = arr[2];
}

bool Vector3D::resetIfNAN() {
    bool error = !isfinite(x) || !isfinite(y) || !isfinite(z);
    if (error) x = y = z = 0.0;
    return error;
}

/***********************************************************************/

Vector3D Vector3D::vecAdd(const Vector3D& other) const {
    Vector3D sum;
    sum.x = this->x + other.x;
    sum.y = this->y + other.y;
    sum.z = this->z + other.z;
    return sum;
}

Vector3D Vector3D::vecSub(const Vector3D& other) const {
    Vector3D diff;
    diff.x= x - other.x;
    diff.y= y - other.y;
    diff.z= z - other.z;
    return diff;
}

Vector3D  Vector3D::scale(const double& factor) const {
    Vector3D scale;
    scale.x = x * factor;
    scale.y = y * factor;
    scale.z = z * factor;
    return scale;
}

/***********************************************************************/

Vector3D Vector3D::cross(const Vector3D& other) const {
    Vector3D cross;
    cross.x = this->y*other.z - this->z*other.y;
    cross.y = this->z*other.x - this->x*other.z;
    cross.z = this->x*other.y - this->y*other.x;
    return cross;
}

double Vector3D::dot(const Vector3D& other) const {
    double product;
    product = x*other.x + y*other.y + z*other.z;
    return product;
}

/***********************************************************************/

double Vector3D::getLen() const {
    double len;
    len = sqrt(x*x + y*y + z*z);
    return len;
}

double Vector3D::distance(const Vector3D& other) const {
    Vector3D diff = *this - other;
    return diff.getLen();
}

/***********************************************************************/

Vector3D Vector3D::normalize() const {
    Vector3D norm;
    double len = this->getLen();
    if (isAlmost0(len)) return Vector3D(0,0,0); // avoid division by 0
    norm.x = x/len;
    norm.y = y/len;
    norm.z = z/len;
    return norm;
}

bool Vector3D::isNormalized() const {
    double len = this->getLen();
    return isAlmost0(len-1);
}

bool Vector3D::equals(const Vector3D& other) const {
    return isAlmost0(x - other.x) && isAlmost0(y - other.y) &&  isAlmost0(z - other.z);
}

/***********************************************************************/

// if len is 0 you will get NaN
double Vector3D::getAngle(const Vector3D& other) const {
    double angle, product, len;
    len = this->getLen() * other.getLen() ;
    product = this->dot(other);
    angle = acos(product/len);
    return angle; // radians
}

bool Vector3D::isOrthogonal(const Vector3D& other) const {
    double cosAngle = this->dot(other);
    return isAlmost0(cosAngle);
}

/***********************************************************************/

// if len is 0 you wil get NaN
Polar Vector3D::carToPolar() const {  // polar(r,phi,theta)
    double r     = this->getLen();
    double phi   = atan2(y, x);
    double theta = acos(z/r);
    Polar polar(r,phi,theta);
    return polar;
}

/***********************************************************************/

Vector3D Vector3D::matVecMult(const Matrix3D& r) const { // M*v
    Vector3D temp = r.getRow1();
    double x = temp.dot(*this);
    temp = r.getRow2();
    double y = temp.dot(*this);
    temp = r.getRow3();
    double z = temp.dot(*this);
    Vector3D result(x, y, z);
    return result;
}

Vector3D Vector3D::qRotate(const Quaternion& rotQuat) const { // w = qvq^* Vektor rotiert um Frame
    Quaternion v(0,(*this)); // ohne normalize()
    Quaternion w  = rotQuat * v * -rotQuat;

    return w.q;
}

Vector3D Vector3D::mRotate(const Matrix3D& r) const { // w = M*v
    Vector3D w(this->matVecMult(r));
    return w;
}

Vector3D Vector3D::rpyRotate(const RPY& rpy) const { // w = M*v
    Matrix3D rotor = rpy.toMatrix3D();
    Vector3D w(this->matVecMult(rotor));
    return w;
}

Vector3D Vector3D::aRotate(const AngleAxis& u_phi) const { // Rodriguez-Formula
    Vector3D temp = this->scale(cos(u_phi.phi)).vecAdd(u_phi.u.cross(this->scale(sin(u_phi.phi))));
    Vector3D w = temp.vecAdd(u_phi.u.scale(u_phi.u.dot(*this) * (1-cos(u_phi.phi))));
    return w;
}

/***********************************************************************/

Vector4D Vector3D::to4D() const {
    Vector4D v;
    v.x=this->x;
    v.y=this->y;
    v.z=this->z;
    v.scale=1;
    return v;
}

/***********************************************************************/

void Vector3D::print() const {
    PRINTF("(%3.3f \t %3.3f \t %3.3f)\n",this->x,this->y,this->z);
}

/***********************************************************************/

Vector3D rotateX(const Vector3D& s,const double& angle) {
    Vector3D end;
    Matrix3D Rx;
    Rx.rotationX(angle);
    end = s.matVecMult(Rx);
    return end;
}

Vector3D rotateY(const Vector3D& s,const double& angle) {
    Vector3D end;
    Matrix3D Ry;
    Ry.rotationY(angle);
    end = s.matVecMult(Ry);
    return end;
}

Vector3D rotateZ(const Vector3D& s,const double& angle) {
    Vector3D end;
    Matrix3D Rz;
    Rz.rotationZ(angle);
    end = s.matVecMult(Rz);
    return end;
}

/***********************************************************************/

Vector3D elementWiseProduct(const Vector3D &left, const Vector3D &right) {

    Vector3D res;

    res.x = left.x * right.x;
    res.y = left.y * right.y;
    res.z = left.z * right.z;

    return res;
}


// if you have 0's you will get NaN
Vector3D elementWiseDivision(const Vector3D &left, const Vector3D &right) {

    Vector3D res;

    res.x = left.x / right.x;
    res.y = left.y / right.y;
    res.z = left.z / right.z;

    return res;
}

Matrix3D skewSymmetricMatrix( const Vector3D &v ) {
    double r[9];

    r[0] =  0.0;
    r[1] = -v.z;
    r[2] =  v.y;
    r[3] =  v.z;
    r[4] =  0.0;
    r[5] = -v.x;
    r[6] = -v.y;
    r[7] =  v.x;
    r[8] =  0.0;

    return Matrix3D(r);
}

} /* namespace matlib */
} /* namespace arctos */
