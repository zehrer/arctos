#include <arctos/matlib/matrix3d.hpp>
#include <arctos/matlib/misc.hpp>
#include <arctos/matlib/polar.hpp>
#include <arctos/matlib/vector3d.hpp>

#include <arctos/time.hpp>

#include <stdint.h>
#include <math.h>

namespace arctos {
namespace matlib {

int64_t faculty(const int &x) {
    uint64_t faculty = 1;
    for (int i = 2; i <= x; i++)
        faculty *= i;
    return faculty;
}

double FMod2p( const double &x) {
    double rval = fmod(x, 2*M_PI);
    if (rval < 0.0) rval+= 2*M_PI;
    return rval;
}

double fabs(const double &value) {
    double dval = value; 
    if (dval < 0) dval = -dval;
    return dval;
}

/***********************************************************************/

//===Rn nach WGS84
// angle in rad
double R_n(const double angle) {
    double a,e2,f;
    double Rn;
    a = A_WGS84; //m
    f = F_WGS84;
    
    e2 =1-(1-f)*(1-f);

    double phi = angle;
    double sinp = sin(phi);
 
    double tmp = sqrt(1-e2*sinp*sinp);
    Rn = a/tmp;
    return Rn;
}

/***********************************************************************/

double daysSinceY2k(int year,int month,int day,int hour,int minute,double second) {
    double days2k;
    double temp1,temp2;
    temp1 = -floor(0.25*7*(year+floor(1.0/12*(month+9))));
    temp2 = floor(275*month/9.0)+(1.0/24*(hour+minute/60.0+second/3600.0));
    days2k = 367* year +temp1 + temp2 + day - 730531.5;
    return days2k;
}

//===ECEF2ECI

double utcToJD(int year, int month, int date, int hour, int minute, double second){
    double JD;
    double d2k = daysSinceY2k(year,month,date,hour,minute,second);
    JD = d2k + JD2000;
    return JD;
}

/*
* Sidereal Time differs from solar time because of earth's precession
* GMST means Greenwhich Mean Sidereal Time defined as the hour angle of the vernal equinox
* GAST means Greenwhich Apparent Sidereal Time
* Both angles differ as the true vernal equinox' position is not uniformly but varies with nutation
*/

/*
* The functions jd2GMST and jd2GAST are borrowed
* from matlab versions
* http://de.mathworks.com/matlabcentral/fileexchange/28176-julian-date-to-greenwich-mean-sidereal-time
* http://de.mathworks.com/matlabcentral/fileexchange/28232-convert-julian-date-to-greenwich-apparent-sidereal-time
* Copyright (c) 2010, Darin Koblick
* All rights reserved.
*/

double jd2GMST(double jd){
    double gmst;
    double jdmin = floor(jd) - 0.5;
    double jdmax = floor(jd) + 0.5;
    double jd0 = jd;
    if (jd > jdmax) jd0 = jdmax;
    else jd0 = jdmin;
    double h = (jd - jd0) * 24;
    double d = jd - JD2000;
    double d0 = jd0 - JD2000;
    double t = d/36525;
    gmst = fmod(6.697374558 + 0.06570982441908*d0 
                    + 1.00273790935*h + 0.000026*(t*t),24);
    gmst = grad2Rad(gmst * 15);
    return gmst;
}

double jd2GAST(double jd){
    double gast;
    double thetam = jd2GMST(jd);
    double t = (jd-JD2000)/36525;
    
    double epsilonm = grad2Rad(23.439291-0.0130111*t - 1.64E-07*(t*t) + 5.04E-07*(t*t*t));
    double l = grad2Rad(280.4665 + 36000.7698 * t);
    double dL = grad2Rad(218.3165 + 481267.8813*t);
    double omega = grad2Rad(125.04452 - 1934.136261*t);
    
    double dPSI = -17.20*sin(omega) - 1.32*sin(2*l) - 0.23*sin(2*dL)
            + 0.21*sin(2*omega);
    double dEPSILON = 9.20*cos(omega) + 0.57*cos(2*l) + 0.10*cos(2*dL) 
            -0.09*cos(2*omega);
            
    dPSI = grad2Rad(dPSI*(1./3600));
    dEPSILON = grad2Rad(dEPSILON*(1./3600));
    gast = fmod(thetam+dPSI*cos(epsilonm+dEPSILON),2*M_PI);
    return gast;
}

Matrix3D eciToECEFrotmat(double jd){
    double theta = jd2GAST(jd);
    Matrix3D rot;
    rot.rotationZ(theta);
    return rot;
}

//====ECEF (meter, meter, meter) to ECI (meter, meter, meter)
Vector3D ecefToECI(const Vector3D ecef, int64_t utcNanoseconds){
    double jd =  ((double)utcNanoseconds / (double)DAYS) + JD2000;
    Matrix3D rot = eciToECEFrotmat(jd);
    return  ecef.matVecMult(rot);
}

//====ECI (meter, meter, meter) to ECEF (meter, meter, meter)
Vector3D eciToECEF(const Vector3D eci, int64_t utcNanoseconds){
    double jd =  ((double)utcNanoseconds / (double)DAYS) + JD2000;
    Matrix3D rot = eciToECEFrotmat(jd);
    return eci.matVecMult(rot.transpose());
}

//===Geodetic (height, longitude, latitude) to ECEF
Vector3D geodeticToECEF(const Polar& polar) {
    double x,y,z;
    double e2,f;
    f = F_WGS84;
    e2 = 1-(1-f)*(1-f);

    double h      = polar.r;
    double phi    = polar.phi;
    double lambda = polar.theta;
    double rn = R_n(phi);

    x = (rn+h) * cos(phi) * cos(lambda);
    y = (rn+h) * cos(phi) * sin(lambda);
    z = (rn*(1-e2)+h) * sin(phi);

    Vector3D ecef(x,y,z);
    return ecef;
}

//====ECEF to Geodetic===  latitude longitude height (phi,lambda,h)from x,y,z

Polar ecefToGeodetic(const Vector3D& other) {
    double lambda,phi,h;
    double a,b,e2,f;
    a = A_WGS84; //m
    f = F_WGS84;
    b = a*(1-f);
    e2 = 1-(b*b)/(a*a);

    double x,y,z;
    x = other.x;
    y = other.y;
    z = other.z;

    double p = sqrt(x*x+y*y);
    double phi0 = atan2(z,(p*(1-e2)));
    double n0 = R_n(phi0);
    double h0 = (p / cos(phi0)) - n0;

    double eht = 1e-5;
    double ephi = 1e-12;

    h = 0;
    phi = 0;
    double dh = 1;
    double dphi = 1;
    double n = 0;
    
    while ((dh>eht) | (dphi > ephi)){
        phi = phi0;
        n = n0;
        h = h0;
        phi0 = atan2(z,p*(1-e2*(n/(n+h))));
        n0 = R_n(phi);
        h0 = p/cos(phi) - n;
        dh = fabs(h0-h);
        dphi = fabs(phi0-phi);
    }
    
    lambda = atan2(y,x);   

    Polar llh(h, phi,lambda);
    return llh;
}

} /* namespace matlib */
} /* namespace arctos */
