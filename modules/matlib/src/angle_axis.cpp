/**
 * Copyright (c) 2008-2018, DLR and University Wuerzburg
 * All rights reserved.
 *
 * @licence BSD
 * @brief   Rotation angle and axis
 * @author  Unknown
 */
#include <arctos/matlib/angle_axis.hpp>
#include <arctos/matlib/matrix3d.hpp>
#include <arctos/matlib/quaternion.hpp>
#include <arctos/matlib/rpy.hpp>
#include <arctos/matlib/vector3d.hpp>

#include <arctos/debug.h>

#include <math.h>

#ifndef M_PI
    #define M_PI 3.14159265358979323846
#endif

namespace arctos {
namespace matlib {

/***********************************************************************/
/*==== constructors ====*/

AngleAxis::AngleAxis() {
    Vector3D u(1,0,0);
    this->u   = u;
    this->phi = 0;
}

AngleAxis::AngleAxis(const double& phi, const double& x, const double& y, const double& z) {
    this->phi = phi;
    this->u.x = x;
    this->u.y = y;
    this->u.z = z;
    this->u = this->u.normalize();
}

AngleAxis::AngleAxis(const AngleAxis& other) {
    this->u   = other.u;
    this->phi = other.phi;
}

AngleAxis::AngleAxis(const double &phi, const Vector3D& u) {
    this->u   = u;
    this->u   = this->u.normalize();
    this->phi = phi;
}

AngleAxis::AngleAxis(const Quaternion& q) {
    this->u   = q.getVec().normalize();
    this->phi = q.getAngle();
}

AngleAxis::AngleAxis(const Matrix3D& M) {
    this->phi= acos(0.5*(M.r[0][0] + M.r[1][1] + M.r[2][2]-1));

    double x = 1/(2*sin(phi))* (M.r[2][1] - M.r[1][2]);
    double y = 1/(2*sin(phi))* (M.r[0][2] - M.r[2][0]);
    double z = 1/(2*sin(phi))* (M.r[1][0] - M.r[0][1]);
    Vector3D u(x,y,z);
    this->u = u.normalize();
}

AngleAxis::AngleAxis(const RPY& rpy) {
    double r = rpy.x;
    double p = rpy.y;
    double y = rpy.z;

    double phi = acos(0.5*(-1 + cos(p)*cos(y) + cos(r)*cos(y) + sin(r)*sin(p)*sin(y) + cos(r)*cos(p) ));
    double u_x = 1/(2*sin(phi))* (sin(r)*cos(p) -cos(r)*sin(p)*sin(y) +sin(r)*cos(y));
    double u_y = 1/(2*sin(phi))* (sin(r)*sin(y) +cos(r)*sin(p)*cos(y) +sin(p));
    double u_z = 1/(2*sin(phi))* (cos(p)*sin(y) -sin(r)*sin(p)*cos(y) + cos(r)*sin(y));
    Vector3D u(u_x,u_y,u_z);

    this->phi = phi;
    this->u = u.normalize();
}

/***********************************************************************/

Quaternion AngleAxis::toQuaternion() const {
    double q0  = cos(this->phi/2);
    Vector3D q = this->u.scale(sin(this->phi/2));
    Quaternion quat(q0,q);
    return quat;
}

Matrix3D AngleAxis::toMatrix3D() const { // allgemeine Rotataionsmatrix
    Matrix3D R;
    Vector3D u = this->u;
    double phi = this->phi;

    // 1 Spalte
    R.r[0][0]= u.x *u.x *(1-cos(phi)) +cos(phi);
    R.r[1][0]= u.x *u.y *(1-cos(phi)) +u.z*sin(phi);
    R.r[2][0]= u.x *u.z *(1-cos(phi)) -u.y*sin(phi);

    // 2 Spalte
    R.r[0][1]= u.x *u.y *(1-cos(phi)) -u.z*sin(phi);
    R.r[1][1]= u.y *u.y *(1-cos(phi)) +cos(phi);
    R.r[2][1]= u.z *u.y *(1-cos(phi)) +u.x*sin(phi);

    // 3 Spalte
    R.r[0][2]= u.x *u.z *(1-cos(phi)) +u.y*sin(phi);
    R.r[1][2]= u.y *u.z *(1-cos(phi)) -u.x*sin(phi);
    R.r[2][2]= u.z *u.z *(1-cos(phi)) +cos(phi);

    return R;
}

RPY AngleAxis::toRPY() const { 
    RPY rpy;
    Vector3D u = this->u;
    double phi = this->phi;
    double m21 = u.x *u.y *(1-cos(phi)) + u.z*sin(phi);
    double m11 = u.x *u.x *(1-cos(phi)) + cos(phi);
    double m31 = u.x *u.z *(1-cos(phi)) - u.y*sin(phi);
    double m32 = u.y *u.z *(1-cos(phi)) + u.x*sin(phi);
    double m33 = u.z *u.z *(1-cos(phi)) + cos(phi);

    rpy.x = atan2(m32 , m33);
    rpy.y = atan2(-m31,sqrt(m11*m11+m21*m21)); // WARNING: Sind vielleicht x und y vertauscht???
    rpy.z = atan2(m21 , m11);

    return rpy;
}

/***********************************************************************/

void AngleAxis::print() const {
    PRINTF("[%3.3f*PI \t %3.3f \t %3.3f \t %3.3f]\n", phi/M_PI, u.x, u.y, u.z);
}

} /* namespace matlib */
} /* namespace arctos */
