/**
 * Copyright (c) 2008-2018, DLR and University Wuerzburg
 * All rights reserved.
 *
 * @licence BSD
 * @brief   cartesian coordinate system
 * @author  Unknown
 */
#include <arctos/matlib/coordinate_frame3d.hpp>
#include <arctos/matlib/matrix3d.hpp>
#include <arctos/matlib/matrix4d.hpp>
#include <arctos/matlib/quaternion.hpp>
#include <arctos/matlib/vector3d.hpp>

namespace arctos {
namespace matlib {

/***********************************************************************/
/*==== constructors ====*/

//changed to orthogonal normalized coordinate system
CoordinateFrame3D::CoordinateFrame3D() {
    Vector3D x(1,0,0);
    Vector3D y(0,1,0);
    Vector3D z(0,0,1);
    Vector3D o;

    this->x=x;
    this->y=y;
    this->z=z;
    this->origin = o;
}

CoordinateFrame3D::CoordinateFrame3D(const Vector3D& x, const Vector3D& y, const Vector3D& z, const Vector3D& origin) {
    this->x = x.normalize();
    this->y = y.normalize();
    this->z = z.normalize();
    this->origin=origin;
}

CoordinateFrame3D::CoordinateFrame3D(const Vector3D& x, const Vector3D& y,const Vector3D& origin) {
    this->x=x.normalize();
    this->y= x.cross(y).normalize();
    this->z = x.cross(this->y).normalize();
    this->origin = origin;
}

CoordinateFrame3D::CoordinateFrame3D(const CoordinateFrame3D& other) {
    this->x=other.x;
    this->y=other.y;
    this->z =other.z;
    this->origin=other.origin;
}

/***********************************************************************/

// derives rotation matrix A : [v]r = A[v]f [v]f given
Matrix4D CoordinateFrame3D::mapTo(const CoordinateFrame3D& rotated) const {
    Matrix3D R;
    //column1
    R.r[0][0] = this->x.dot(rotated.x);
    R.r[1][0] = this->y.dot(rotated.x);
    R.r[2][0] = this->z.dot(rotated.x);

    //column 2
    R.r[0][1] = this->x.dot(rotated.y);
    R.r[1][1] = this->y.dot(rotated.y);
    R.r[2][1] = this->z.dot(rotated.y);

    //column 3
    R.r[0][2] = this->x.dot(rotated.z);
    R.r[1][2] = this->y.dot(rotated.z);
    R.r[2][2] = this->z.dot(rotated.z);

    Vector3D trans;
    trans = rotated.origin.vecSub(this->origin);
    Matrix4D rot(R, trans);

    return rot;
}

/***********************************************************************/
/*==== translate, rotate ====*/

CoordinateFrame3D CoordinateFrame3D::translate(const Vector3D& trans) const {
    CoordinateFrame3D T((*this));
    T.origin = this->origin.vecAdd(trans);

    return T;
}

CoordinateFrame3D CoordinateFrame3D::rotate(const Matrix3D& rot) const {
    CoordinateFrame3D R;
    R.x = this->x.mRotate(rot);
    R.y = this->y.mRotate(rot);
    R.z = this->z.mRotate(rot);
    R.origin= this->origin;

    return R;
}

CoordinateFrame3D CoordinateFrame3D::rotate(const Quaternion& q) const {
    CoordinateFrame3D R;
    R.x = this->x.qRotate(q);
    R.y = this->y.qRotate(q);
    R.z = this->x.qRotate(q);
    R.origin= this->origin;

    return R;
}

} /* namespace matlib */
} /* namespace arctos */
