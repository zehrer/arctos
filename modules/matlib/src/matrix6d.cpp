/**
 * Copyright (c) 2008-2018, DLR and University Wuerzburg
 * All rights reserved.
 *
 * @licence BSD
 * @brief   6x6 matrix 
 * @author  Unknown
 */
#include <arctos/matlib/matrix3d.hpp>
#include <arctos/matlib/matrix6d.hpp>
#include <arctos/matlib/vector6d.hpp>

#include <math.h>

namespace arctos {
namespace matlib {

Matrix6D::Matrix6D() {
    for (int i=0; i<6; i++) {
        for (int j=0; j<6; j++) {
            if ( i == j ) {
                r[i][j]=1.0;
            }
            else {
                r[i][j]=0.0;
            }
        }
    }
}

Matrix6D::Matrix6D(const Matrix6D& other) {
    for (int i=0; i<6; i++) {
        for (int j=0; j<6; j++) {
            r[i][j]=other.r[i][j];
        }
    }
}

Matrix6D::Matrix6D(const double* arr) {
    for (int i=0; i<6; i++) {
        for (int j=0; j<6; j++) {
            r[i][j]=arr[i*6+j];
        }
    }
}

Matrix6D::Matrix6D(const Vector6D& diag) {
    // Initialization
    for (int i=0; i<6; i++) {
        for (int j=0; j<6; j++) {
            if ( i == j ) {
                r[i][j] = diag.v[i];
            }
            else {
                r[i][j]=0.0;
            }
        }
    }
}

Matrix6D::Matrix6D(const Matrix3D& upperLeft, const Matrix3D& upperRight, const Matrix3D& lowerLeft, const Matrix3D& lowerRight) {
    for ( int i = 0; i < 3; i++ ) {
        for ( int j = 0; j < 3; j++ ) {
            r[  i][  j] =  upperLeft.r[i][j];
            r[  i][3+j] = upperRight.r[i][j];
            r[3+i][  j] =  lowerLeft.r[i][j];
            r[3+i][3+j] = lowerRight.r[i][j];
        }
    }
}

/***********************************************************************/

Vector6D Matrix6D::getColumn(const int& j) const {
    Vector6D Res;
    for (int i=0; i<6; i++) {
        Res.v[i]=r[i][j];
    }
    return Res;
}

Vector6D Matrix6D::getRow(const int& i) const {
    Vector6D Res;
    for (int j=0; j<6; j++)  {
        Res.v[j]=r[i][j];
    }
    return Res;
}

/***********************************************************************/

void Matrix6D::setColumn(const int& j, const Vector6D& column) {
    if ((j<0) || (6<=j)) {
        return;
    }
    for (int i=0; i<6; i++) {
        r[i][j] = column.v[i];
    }
}

void Matrix6D::setRow(const int& i, const Vector6D& row) {
    if ((i<0) || (6<=i)) {
        return;
    }
    for (int j=0; j<6; j++) {
        r[i][j] = row.v[j];
    }
}

/***********************************************************************/

Vector6D Matrix6D::diag() const {
    Vector6D Vec;
    for (int i=0; i<6; i++) {
        Vec.v[i] = r[i][i];
    }
    return Vec;
}

Matrix6D Matrix6D::transpose() const {
    Matrix6D transpose;
    for (int i = 0 ; i<6; i++) {
        for (int j = 0; j<6; j++) {
            transpose.r[i][j]=this->r[j][i];
        }
    }
    return transpose;
}

Matrix6D Matrix6D::invert() const {
    int j = 0, i = 0;
    double d = 0;

    Matrix6D inverse;
    Vector6D col;
    Vector6D indx;
    Matrix6D a = *this;

    /* LU- decomposition */
    if ( ludcmp(a, indx, d) ) {
        for (j = 0; j < 6; j++) {
            for (i = 0; i < 6; i++) {
                col.v[i] = 0;
            };
            col.v[j] = 1.0;
            /* backsubstitution */
            lubksb(a, indx, col);
            for (i = 0; i < 6; i++) {
                inverse.r[i][j] = col.v[i];
            };
        };
    }

    return inverse;
}

/***********************************************************************/

Matrix6D Matrix6D::scale(const double& factor) const {
    Matrix6D Aux;
    for (int i=0; i<6; i++) {
        for (int j=0; j<6; j++) {
            Aux.r[i][j]=factor*r[i][j];
        }
    }
    return Aux;
}

Matrix6D Matrix6D::mAdd(const Matrix6D& other) const {
    Matrix6D Aux;
    for (int i=0; i<6; i++) {
        for (int j=0; j<6; j++) {
            Aux.r[i][j] = r[i][j] + other.r[i][j];
        }
    }
    return Aux;
}

Matrix6D Matrix6D::mSub(const Matrix6D& other) const {
    Matrix6D Aux;
    for (int i=0; i<6; i++) {
        for (int j=0; j<6; j++) {
            Aux.r[i][j] = r[i][j] - other.r[i][j];
        }
    }
    return Aux;
}

Matrix6D Matrix6D::mMult(const Matrix6D& other) const {
    Matrix6D Aux;
    double Sum;
    for (int i=0; i<6; i++) {
        for (int j=0; j<6; j++) {
            Sum = 0.0;
            for (int k=0; k<6; k++) {
                Sum += r[i][k] * other.r[k][j];
            }
            Aux.r[i][j] = Sum;
        }
    }
    return Aux;
}

/***********************************************************************/

Matrix3D Matrix6D::upperLeft() const {
    Matrix3D P;

    for (int i = 0 ; i<3; i++) {
        for (int j = 0; j<3; j++) {
            P.r[i][j] = this->r[  i][  j];
        }
    }
    return P;
}

Matrix3D Matrix6D::upperRight() const {
    Matrix3D Q;

    for (int i = 0 ; i<3; i++) {
        for (int j = 0; j<3; j++) {
            Q.r[i][j] = this->r[  i][3+j];
        }
    }
    return Q;
}

Matrix3D Matrix6D::lowerLeft() const {
    Matrix3D R;

    for (int i = 0 ; i<3; i++) {
        for (int j = 0; j<3; j++) {
            R.r[i][j] = this->r[3+i][  j];
        }
    }
    return R;
}

Matrix3D Matrix6D::lowerRight() const {
    Matrix3D S;

    for (int i = 0 ; i<3; i++) {
        for (int j = 0; j<3; j++) {
            S.r[i][j] = this->r[3+i][3+j];
        }
    }
    return S;
}

/***********************************************************************/
/*==== Matrix and vector functions ====*/

Matrix6D dyadic(const Vector6D& left, const Vector6D& right) {
    Matrix6D Mat;
    for (int i=0; i<6; i++) {
        for (int j=0; j<6; j++) {
            Mat.r[i][j] = left.v[i]*right.v[j];
        }
    }
    return Mat;
}

/*! \brief LU-decomposition of a matrix
 * \param a the matrix which is decomposed
 * \param indx vector that records the row permutation effected by partial pivoting
 * \param d +/-1 depending on whether the number of row interchanges was even or odd
 * \return LU-decomposed matrix
 *
 * Given the matrix a[0..n-1][0..n-1], this routine replaces it by the LU decomposition
 * of a rowwise permutation of itself. a is input. On Output, it is arranged as in Equation
 * (2.3.14) of Ref. [1]; indx [0..n-1] is an output vector that records the row permutation
 * effected by the partial pivoting; d is output as +/-1 depending on whether ther number of
 * row interchanges was even or odd, respectively. This routine is used in combination with
 * lubksb to solve linear equations or invert a matrix.
 *
 * Reference: [1] Press, W., Teukolsky, S., Vetterling, W., Flannery, B.: Numerical Recipes in C++, Second Edition,
 * Cambridge University Press, 1992
 */
bool ludcmp(Matrix6D& a, Vector6D& indx, double& d) {
    const double TINY = 1.0e-20; // a small number
    const int n = 6;
    int i, imax=0, j, k;
    double big, sum, dum, temp;

    Vector6D vv; // vv stores the implicit scaling of each row
    d = 1.0; // no row interchanges yet
    // loop over rows to get the implicit scaling information
    for (i = 0; i < n; i++) {
        big = 0.0;
        for (j = 0; j < n; j++) {
            if ( (temp = fabs(a.r[i][j])) > big ) {
                big = temp;
            }
        }; // end for j
        if ( big == 0.0 ) {
            // error: matrix is singular
            return false;
        };
        vv.v[i] = 1.0/big; // save the scaling

    };    //end for i
    // this is the loop over columns of Crout's method
    for (j = 0; j < n; j++) {
        for (i = 0; i < j; i++) {
            // this is equation (2.3.13) of Ref. [1] except i == j
            sum = a.r[i][j];
            for (k = 0; k < i; k++) {
                sum -= a.r[i][k]*a.r[k][j];
            }; // end for k
            a.r[i][j] = sum;
        }; // end for i
        big = 0.0; // initialize for the search for largest pivot element
        // this is i ==j of equation (2.3.13) an i == j+1..N-1 of equation (2.3.13)
        for (i = j; i < n; i++) {
            sum = a.r[i][j];
            for (k = 0; k < j; k++) {
                sum -= a.r[i][k]*a.r[k][j];
            }
            a.r[i][j] = sum;
            if ( (dum = vv.v[i]*fabs(sum)) >= big ) {
                // is the figure of merit for the pivot better than the best so far?
                big = dum;
                imax = i;
            };
        }; // end for i
        if (j != imax) {
            // do we need to interchange rows?
            for (k = 0; k < n; k++) {
                dum = a.r[imax][k];
                a.r[imax][k] = a.r[j][k];
                a.r[j][k] = dum;
            } // end for k
            d = -d; // change the parity of d
            vv.v[imax] = vv.v[j]; // interchange the scale factor
        }; // end if
        indx.v[j] = static_cast<double> (imax);
        if ( a.r[j][j] == 0.0 ) {
            // if the pivot element is zero the matrix is singular (at least to the
            // precision of the algorithm). For some applications on singular matrices,
            // it is desirable to substitute TINY for zero
            a.r[j][j] = TINY;
        };
        if ( j != n-1 ) {
            // devide the pivot element
            dum = 1.0/a.r[j][j];
            for (i = j+1; i < n; i++) {
                a.r[i][j] *= dum;
            };
        };
    }; // end for j
    return true;
}

/*! \brief solves a set of linear equations A*X = B with a LU decomposition of the matrix a
 * \param a the LU decomposition of the matrix A
 * \param indx permutation vector
 * \param b right-hand side vector B for
 * \return solution X for the set of linear equations
 *
 * Solves the set of n linear equations A*X = B. Here a[0..n-1][0..n-1] is input, not as the matrix a
 * but rather as its LU decomposition, determined ny tje routine ludcmp. b[0..n-1] is the
 * input as the right-hand side vector B, and returns with the solution vector X. a and indx are
 * not modified by this routine and can ne left in place for successive calls with different right-hand sides b.
 * This routine takes into account the possibility that b will begin with many zero elements, so it is efficient for
 * use in matrix inversion
 *
 * Reference: [1] Press, W., Teukolsky, S., Vetterling, W., Flannery, B.: Numerical Recipes in C++, Second Edition,
 * Cambridge University Press, 1992
 */
void lubksb(Matrix6D &a, Vector6D &indx, Vector6D &b) {
    const int n = 6;
    int i, ii=0, ip,j;

    double sum;

    for (i = 0; i < n; i++) {
        // when ii is set to a positive value, it will become the index of the first
        // nonvanishing element of b. We now do the forward substitution, equation (2.3.6)
        // of Ref [1] The only new wrinkle is to unscramble ther permutation as we go
        ip = static_cast<double> ( indx.v[i] );
        sum = b.v[ip];
        b.v[ip] = b.v[i];
        if ( ii != 0) {
            for (j = ii-1; j < i; j++) {
                sum -= a.r[i][j]*b.v[j];
            };
        } // end if
        else {
            if ( sum != 0 ) {
                // a nonzero element was encointered, so from now on we will have to
                // do the sums in the loop above
                ii = i+1;
            };
        }; // end else
        b.v[i] = sum;
    }; // end for i
    for (i = n-1; i >= 0; i--) {
        // now we do the backsubstitution, equation (2.3.7) of Ref [1]
        sum = b.v[i];
        for (j = i+1; j < n; j++) {
            sum -= a.r[i][j]*b.v[j];
        };
        // store a component of the solution vector X
        b.v[i] = sum/a.r[i][i];
    };
}
/***********************************************************************/
/*==== Matrix6D operators ====*/

Matrix6D operator/(const Matrix6D& Mat, double value) {
    Matrix6D Aux;
    for (int i=0; i<6; i++) {
        for (int j=0; j<6; j++) {
            Aux.r[i][j]=Mat.r[i][j]/value;
        }
    }
    return Aux;
}

} /* namespace matlib */
} /* namespace arctos */
