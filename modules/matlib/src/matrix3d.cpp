/**
 * Copyright (c) 2008-2018, DLR and University Wuerzburg
 * All rights reserved.
 *
 * @licence BSD
 * @brief   3x3 matrix 
 * @author  Unknown
 */
#include <arctos/matlib/angle_axis.hpp>
#include <arctos/matlib/matrix3d.hpp>
#include <arctos/matlib/quaternion.hpp>
#include <arctos/matlib/rpy.hpp>
#include <arctos/matlib/vector3d.hpp>

#include <arctos/debug.h>

#include <math.h>

namespace arctos {
namespace matlib {

/***********************************************************************/
/*==== constructors ====*/

Matrix3D::Matrix3D() {
    //column1
    r[0][0] = 1.0;
    r[1][0] = 0.0;
    r[2][0] = 0.0;

    //column 2
    r[0][1] = 0.0;
    r[1][1] = 1.0;
    r[2][1] = 0.0;

    //column 3
    r[0][2] = 0.0;
    r[1][2] = 0.0;
    r[2][2] = 1.0;
}

Matrix3D::Matrix3D(const Vector3D& column1,const Vector3D& column2,const Vector3D& column3) {
    //column 1
    r[0][0] = column1.x;
    r[1][0] = column1.y;
    r[2][0] = column1.z;

    //column 2
    r[0][1] = column2.x;
    r[1][1] = column2.y;
    r[2][1] = column2.z;

    //column 3
    r[0][2] = column3.x;
    r[1][2] = column3.y;
    r[2][2] = column3.z;
}

Matrix3D::Matrix3D(double* arr) {
    int k = 0;
    for (int i = 0 ; i<3; i++) {
        for (int j = 0; j<3; j++) {
            r[i][j]=arr[k];
            k++;
        }
    }
}

Matrix3D::Matrix3D(const Matrix3D& other) {
    for (int i = 0 ; i<3; i++) {
        for (int j = 0; j<3; j++) {
            r[i][j]=other.r[i][j];
        }
    }
}

Matrix3D::Matrix3D(const Vector3D& init) { //diagonalmatrix aus vektor
    //column 1
    r[0][0] = init.x;
    r[1][0] = 0.0;
    r[2][0] = 0.0;

    //column 2
    r[0][1] = 0.0;
    r[1][1] = init.y;
    r[2][1] = 0.0;

    //column 3
    r[0][2] = 0.0;
    r[1][2] = 0.0;
    r[2][2] = init.z;
}

Matrix3D::Matrix3D(const RPY& rpy) { 
    double cr = cos(rpy.x);
    double cp = cos(rpy.y);
    double cy = cos(rpy.z);

    double sr = sin(rpy.x);
    double sp = sin(rpy.y);
    double sy = sin(rpy.z);

    this->r[0][0]= cy*cp;
    this->r[0][1]= cy*sp*sr - sy*cr;
    this->r[0][2]= cy*sp*cr + sy*sr;

    this->r[1][0]= sy*cp;
    this->r[1][1]= sy*sp*sr + cy*cr;
    this->r[1][2]= sy*sp*cr - cy*sr;

    this->r[2][0]= -sp;
    this->r[2][1]= cp*sr;
    this->r[2][2]= cp*cr;
}

Matrix3D::Matrix3D(const AngleAxis& other) { // allgemeine Rotationsmatrix
    Vector3D u = other.u;
    double phi = other.phi;
    double cp  = cos(phi);
    double sp  = sin(phi);

    // 1 Spalte
    this->r[0][0]= u.x *u.x *(1-cp) + cp;
    this->r[1][0]= u.x *u.y *(1-cp) + u.z*sp;
    this->r[2][0]= u.x *u.z *(1-cp) - u.y*sp;

    // 2 Spalte
    this->r[0][1]= u.x *u.y *(1-cp) - u.z*sp;
    this->r[1][1]= u.y *u.y *(1-cp) + cp;
    this->r[2][1]= u.z *u.y *(1-cp) + u.x*sp;

    // 3 Spalte
    this->r[0][2]= u.x *u.z *(1-cp) + u.y*sp;
    this->r[1][2]= u.y *u.z *(1-cp) - u.x*sp;
    this->r[2][2]= u.z *u.z *(1-cp) + cp;
}

Matrix3D::Matrix3D(const Quaternion& other) {
    //column1
    this->r[0][0] = 2*other.q0*other.q0-1 + 2*other.q.x * other.q.x;
    this->r[1][0] = 2*other.q.y*other.q.x  + 2*other.q0*other.q.z;
    this->r[2][0] = 2*other.q.z*other.q.x  - 2*other.q0*other.q.y;

    //column 2
    this->r[0][1] = 2*other.q.y*other.q.x - 2*other.q0*other.q.z;
    this->r[1][1] = 2*other.q0*other.q0-1 + 2*other.q.y*other.q.y;
    this->r[2][1] = 2*other.q.y*other.q.z + 2*other.q0*other.q.x;

    //column 3
    this->r[0][2] = 2*other.q.z*other.q.x + 2*other.q0*other.q.y;
    this->r[1][2] = 2*other.q.y*other.q.z - 2*other.q0*other.q.x;
    this->r[2][2] = 2*other.q0*other.q0-1 + 2*other.q.z * other.q.z;
}

/***********************************************************************/
/*====getVec, getAngle====*/

Vector3D Matrix3D::getVec() const {
    double x,y,z,angle;
    angle = this->getAngle();
    x = 1.0/(2*sin(angle)) * (this->r[2][1]-this->r[1][2]);
    y = 1.0/(2*sin(angle)) * (this->r[0][2]-this->r[2][0]);
    z = 1.0/(2*sin(angle)) * (this->r[1][0]-this->r[0][1]);
    Vector3D u(x,y,z);

    return u;
}

double Matrix3D::getAngle() const {
    double angle;
    angle = acos(0.5*(this->r[0][0]+this->r[1][1]+this->r[2][2]-1));
    return angle;
}

/***********************************************************************/
/*====Getter für Spalten bzw Zeilen ====*/

Vector3D Matrix3D::getRow1() const {
    Vector3D row1(this->r[0][0],this->r[0][1],this->r[0][2]);
    return row1;
}

Vector3D Matrix3D::getRow2() const {
    Vector3D row1(this->r[1][0],this->r[1][1],this->r[1][2]);
    return row1;
}

Vector3D Matrix3D::getRow3() const {
    Vector3D row1(this->r[2][0],this->r[2][1],this->r[2][2]);
    return row1;
}

Vector3D Matrix3D::getColumn1() const {
    Vector3D row1(this->r[0][0],this->r[1][0],this->r[2][0]);
    return row1;
}

Vector3D Matrix3D::getColumn2() const {
    Vector3D row1(this->r[0][1],this->r[1][1],this->r[2][1]);
    return row1;
}

Vector3D Matrix3D::getColumn3() const {
    Vector3D row1(this->r[0][2],this->r[1][2],this->r[2][2]);
    return row1;
}

/***********************************************************************/
/*====Setter für Spalten und Reihen =====*/

void Matrix3D::setRow1(const Vector3D& row) {
    this->r[0][0]= row.x;
    this->r[0][1]= row.y;
    this->r[0][2]= row.z;
}

void Matrix3D::setRow2(const Vector3D& row) {
    this->r[1][0]= row.x;
    this->r[1][1]= row.y;
    this->r[1][2]= row.z;
}

void Matrix3D::setRow3(const Vector3D& row) {
    this->r[2][0]= row.x;
    this->r[2][1]= row.y;
    this->r[2][2]= row.z;
}

void Matrix3D::setColumn1(const Vector3D& column) {
    this->r[0][0]= column.x;
    this->r[1][0]= column.y;
    this->r[2][0]= column.z;
}

void Matrix3D::setColumn2(const Vector3D& column) {
    this->r[0][1]= column.x;
    this->r[1][1]= column.y;
    this->r[2][1]= column.z;
}

void Matrix3D::setColumn3(const Vector3D& column) {
    this->r[0][2]= column.x;
    this->r[1][2]= column.y;
    this->r[2][2]= column.z;
}

/***********************************************************************/
//====mAdd, mSub, scale, mMult, mDivide====

Matrix3D Matrix3D::mAdd(const Matrix3D& other) const {
    Matrix3D add;

    for (int i = 0 ; i<3; i++) {
        for (int j = 0; j<3; j++) {
            add.r[i][j] = this->r[i][j] + other.r[i][j];
        }
    }
    return add;
}

Matrix3D Matrix3D::mSub(const Matrix3D& other) const {
    Matrix3D sub;
    for (int i = 0 ; i<3; i++) {
        for (int j = 0; j<3; j++) {
            sub.r[i][j] = this->r[i][j] - other.r[i][j];
        }
    }
    return sub;
}

Matrix3D Matrix3D::scale(const double& factor) const {
    Matrix3D scale;
    for (int i = 0 ; i<3; i++) {
        for (int j = 0; j<3; j++) {
            scale.r[i][j] = this->r[i][j]*factor;
        }
    }
    return scale;
}

Matrix3D Matrix3D::mMult(const Matrix3D& other) const {
    Matrix3D mult;
    double sum;

    for (int i=0; i<3; i++)
        for (int j=0; j<3; j++) {
            sum = 0.0;
            for (int k=0; k<3; k++)
                sum += this->r[i][k] * other.r[k][j];
            mult.r[i][j] = sum;
        }
    return mult;
}

Matrix3D Matrix3D::mDivide(const Matrix3D& other) const {
    Matrix3D inverse,divide;

    inverse = other.invert();
    divide  = this->mMult(inverse);
    return divide;
}

/***********************************************************************/
/*==== cofac, adjoint, invert, transpose, trace====*/

Matrix3D Matrix3D::cofac() const {
    Matrix3D cofak;
    cofak.r[0][0]=this->r[1][1] * this->r[2][2]-this->r[2][1] * this->r[1][2];
    cofak.r[0][1]=this->r[1][2] * this->r[2][0]-this->r[1][0] * this->r[2][2];
    cofak.r[0][2]=this->r[1][0] * this->r[2][1]-this->r[2][0] * this->r[1][1];
    cofak.r[1][0]=this->r[2][1] * this->r[0][2]-this->r[0][1] * this->r[2][2];
    cofak.r[1][1]=this->r[0][0] * this->r[2][2]-this->r[2][0] * this->r[0][2];
    cofak.r[1][2]=this->r[2][0] * this->r[0][1]-this->r[0][0] * this->r[2][1];
    cofak.r[2][0]=this->r[0][1] * this->r[1][2]-this->r[1][1] * this->r[0][2];
    cofak.r[2][1]=this->r[1][0] * this->r[0][2]-this->r[1][2] * this->r[0][0];
    cofak.r[2][2]=this->r[0][0] * this->r[1][1]-this->r[1][0] * this->r[0][1];
    return cofak;
}

Matrix3D Matrix3D::adjoint() const {
    Matrix3D adjoint;
    adjoint = this->cofac().transpose();
    return adjoint;
}

Matrix3D Matrix3D::invert() const {
    double det;
    Matrix3D inverse;

    if (this->isOrthogonal()) {
        inverse = this->transpose();
        return inverse;
    }

    det = this->determinant();
    inverse = this->adjoint().scale(1/det);

    return inverse;
}

Matrix3D Matrix3D::transpose() const {
    Matrix3D transpose;
    for (int i = 0 ; i<3; i++) {
        for (int j = 0; j<3; j++) {
            transpose.r[i][j]=this->r[j][i];
        }
    }
    return transpose;
}

double Matrix3D::trace() const {
    return this->r[0][0] * this->r[1][1] * this->r[2][2];
}

/***********************************************************************/
/*====fundamental rotations====*/

void Matrix3D::rotationX(const double& angle) {
    Vector3D c1(1.0, 0.0, 0.0);
    setColumn1(c1);
    Vector3D c2(0.0, cos(angle), sin(angle));
    setColumn2(c2);
    Vector3D c3(0.0, -sin(angle), cos(angle));
    setColumn3(c3);
}

void Matrix3D::rotationY(const double& angle) {
    Vector3D c1(cos(angle), 0.0, -sin(angle));
    setColumn1(c1);
    Vector3D c2(0.0, 1.0, 0.0);
    setColumn2(c2);
    Vector3D c3(sin(angle), 0.0, cos(angle));
    setColumn3(c3);
}

void Matrix3D::rotationZ(const double& angle) {
    Vector3D c1(cos(angle), sin(angle), 0.0);
    setColumn1(c1);
    Vector3D c2(-sin(angle), cos(angle), 0.0);
    setColumn2(c2);
    Vector3D c3(0.0, 0.0, 1.0);
    setColumn3(c3);
}

/***********************************************************************/
/*==== determinant,isOrthogonal ,equals====*/

double Matrix3D::determinant() const {
    double det,a1,a2,a3,a4,a5,a6;
    a1 = this->r[0][0]*this->r[1][1]*this->r[2][2];
    a2 = this->r[0][1]*this->r[1][2]*this->r[2][0];
    a3 = this->r[0][2]*this->r[1][0]*this->r[2][1];
    a4 = -(this->r[2][0]*this->r[1][1]*this->r[0][2]);
    a5 = -(this->r[2][1]*this->r[1][2]*this->r[0][0]);
    a6 = -(this->r[2][2]*this->r[1][0]*this->r[0][1]);
    det = a1+a2+a3+a4+a5+a6;
    return det;
}

bool Matrix3D::isOrthogonal() const {
    Matrix3D Id;
    if (this->mMult(this->transpose()).equals(Id)) {
        return true;
    }
    return false;
}

bool Matrix3D::equals(const Matrix3D& other) const {
    for (int i = 0 ; i<3; i++) {
        for (int j = 0; j<3; j++) {
            if (other.r[i][j] != this->r[i][j]) {
                return false;
            }
        }
    }
    return true;
}

/***********************************************************************/
/*==== toQuaternion, toRPY, toAngleAxis ====*/

Quaternion Matrix3D::toQuaternion() const {

    double q0,q1,q2,q3;
    q0 = q1 = q2 = q3 = 0;
    double c1 = 1+ this->r[0][0] + this->r[1][1] + this->r[2][2];
    double c2 = 1+ this->r[0][0] - this->r[1][1] - this->r[2][2];
    double c3 = 1- this->r[0][0] + this->r[1][1] - this->r[2][2];
    double c4 = 1- this->r[0][0] - this->r[1][1] + this->r[2][2];
    double c = c1;
    // Finde das Maximum
    if (c2>c) c = c2;
    if (c3>c) c = c3;
    if (c4>c) c = c4;
    //Fallunterscheidung
    if (c==c1) {
        c = 0.5 * sqrt(c);
        q0 = c;
        q1 = (this->r[2][1]-this->r[1][2])/(4*c);
        q2 = (this->r[0][2]-this->r[2][0])/(4*c);
        q3 = (this->r[1][0]-this->r[0][1])/(4*c);
    }

    if (c==c2) {
        c = 0.5 * sqrt(c);
        q0 = (this->r[2][1]-this->r[1][2])/(4*c);
        q1 = c;
        q2 = (this->r[1][0]+this->r[0][1])/(4*c);
        q3 = (this->r[0][2]+this->r[2][0])/(4*c);
    }

    if (c==c3) {
        c = 0.5 * sqrt(c);
        q0 = (this->r[0][2]-this->r[2][0])/(4*c);
        q1 = (this->r[1][0]+this->r[0][1])/(4*c);
        q2 = c;
        q3 = (this->r[2][1]+this->r[1][2])/(4*c);
    }

    if (c==c4) {
        c = 0.5 * sqrt(c);
        q0 = (this->r[1][0]-this->r[0][1])/(4*c);
        q1 = (this->r[0][2]+this->r[2][0])/(4*c);
        q2 = (this->r[2][1]+this->r[1][2])/(4*c);
        q3 = c;
    }
    Quaternion q(q0,q1,q2,q3);

    return q;

}

RPY Matrix3D::toRPY() const { 
    RPY rpy;
    double m21 = this->r[1][0];
    double m11 = this->r[0][0];
    double m31 = this->r[2][0];
    double m32 = this->r[2][1];
    double m33 = this->r[2][2];
    rpy.x     = atan2(m32, m33);
    rpy.y     = atan2(-m31, sqrt(m11*m11 + m21*m21)); // WARNING: are y and z worng? please check
    rpy.z     = atan2(m21, m11);

    return rpy;
}

AngleAxis Matrix3D::toAngleAxis() const {
    double phi = acos(0.5*(this->r[0][0] + this->r[1][1] + this->r[2][2]-1));
    double sp = sin(phi);

    double x = 1/(2*sp)* (this->r[2][1] - this->r[1][2]);
    double y = 1/(2*sp)* (this->r[0][2] - this->r[2][0]);
    double z = 1/(2*sp)* (this->r[1][0] - this->r[0][1]);
    Vector3D u(x,y,z);
    AngleAxis u_phi(phi, u.normalize());
    return u_phi;
}

/***********************************************************************/

void Matrix3D::print() const {
    PRINTF(" __\t\t\t\t\t\t__\n");
    for (int k=0; k<3; k++) {
        PRINTF("| ");
        for (int l = 0; l<3; l++)
            PRINTF("%3.3f \t ",this->r[k][l]);
        PRINTF(" |\n");
    }
    PRINTF(" --\t\t\t\t\t\t--\n");
}

} /* namespace matlib */
} /* namespace arctos */
