/**
 * Copyright (c) 2008-2018, DLR and University Wuerzburg
 * All rights reserved.
 *
 * @licence BSD
 * @brief   6-dim mathematical vector
 * @author  Unknown
 */
#include <arctos/matlib/matrix6d.hpp>
#include <arctos/matlib/vector3d.hpp>
#include <arctos/matlib/vector6d.hpp>

#include <arctos/debug.h>

#include <math.h>

namespace arctos {
namespace matlib {

Vector6D::Vector6D() {
    for (int i=0; i<6; i++) {
        v[i]=0.0;
    }
}

Vector6D::Vector6D(const Vector6D& other) {
    for (int i=0; i<6; i++) {
        v[i]=other.v[i];
    }
}

Vector6D::Vector6D(const double* arr) {
    for (int i=0; i<6; i++) {
        v[i]=arr[i];
    }
}

Vector6D::Vector6D(double x_, double y_, double z_,
                   double u_, double v_, double w_) {
    v[0]=x_; v[1]=y_; v[2]=z_;
    v[3]=u_; v[4]=v_; v[5]=w_;
}

Vector6D::Vector6D( const Vector3D &upper, const Vector3D &lower ) {
    v[0] = upper.x;
    v[1] = upper.y;
    v[2] = upper.z;
    v[3] = lower.x;
    v[4] = lower.y;
    v[5] = lower.z;
}

/***********************************************************************/

double Vector6D::getLen() const {
    return sqrt(dotProduct(*this, *this));
}

Vector6D Vector6D::scale(const double& factor) const {
    Vector6D aux;
    for (int i=0; i<6; i++) {
        aux.v[i]=factor*v[i];
    }
    return aux;
}

Vector6D Vector6D::vecAdd(const Vector6D& other) const {
    Vector6D aux;
    for (int i=0; i<6; i++) {
        aux.v[i]=v[i]+other.v[i];
    }
    return aux;
}

Vector6D Vector6D::vecSub(const Vector6D& other) const {
    Vector6D aux;
    for (int i=0; i<6; i++) {
        aux.v[i]=v[i]-other.v[i];
    }
    return aux;
}

Vector6D Vector6D::matVecMult(const Matrix6D& M) const {
    Vector6D aux;
    double sum;
    for (int i=0; i<6; i++) {
        sum = 0.0;
        for (int j=0; j<6; j++) {
            sum += M.r[i][j] * v[j];
        }
        aux.v[i] = sum;
    }
    return aux;
}

/***********************************************************************/

void Vector6D::print() const {
    PRINTF("(%3.3f \t %3.3f \t %3.3f \t %3.3f \t %3.3f \t %3.3f)\n",
        this->v[0], this->v[1], this->v[2], this->v[3], this->v[4], this->v[5]);
}

/***********************************************************************/

double dotProduct(const Vector6D& left, const Vector6D& right) {
    double sum = 0.0;
    for (int i=0; i<6; i++) {
        sum+=left.v[i]*right.v[i];
    }
    return sum;
}

} /* namespace matlib */
} /* namespace arctos */
