/**
 * Copyright (c) 2008-2018, DLR and University Wuerzburg
 * All rights reserved.
 *
 * @licence BSD
 * @brief   complex numbers 
 * @author  Unknown
 */
#ifndef MATLIB_COMPLEX_HPP
#define MATLIB_COMPLEX_HPP

namespace arctos {
namespace matlib {

class Complex {
public:
    double Re;
    double Im;

    Complex(); ///< (0,0)
    Complex(const Complex& other);
    Complex(const double &Re, const double &Im);

    Complex cAdd(const Complex& other) const;
    Complex cSub(const Complex& other) const;
    Complex cScale(const double &scale) const;
    Complex cMult(const Complex& other) const;
    Complex cPow(const int &exponent) const; ///< Complex z = (a+bi)^exponent 
    Complex cExp() const;                    ///< Complex z = e^(*this) */
};

/********************          operators          ********************/
inline Complex operator+ (const Complex &left, const Complex &right) { return left.cAdd(right); }
inline Complex operator- (const Complex &left, const Complex &right) { return left.cSub(right); }
inline Complex operator* (const double  &left, const Complex &right) { return right.cScale(left); }
inline Complex operator* (const Complex &left, const double  &right) { return left.cScale(right); }
inline Complex operator/ (const Complex &left, const double  &right) { return left.cScale(1.0/right); }

} /* namespace matlib */
} /* namespace arctos */

#endif /* MATLIB_COMPLEX_HPP */
