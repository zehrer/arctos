/**
 * Copyright (c) 2008-2018, DLR and University Wuerzburg
 * All rights reserved.
 *
 * @licence BSD
 * @brief   3x3 matrix 
 * @author  Unknown
 */
#ifndef MATLIB_MATRIX3D_HPP
#define MATLIB_MATRIX3D_HPP

#include "vector3d.hpp"

namespace arctos {
namespace matlib {

class AngleAxis;
class Quaternion;
class RPY;

class Matrix3D {
public:
    double r[3][3];
    Matrix3D(); ///< identity matrix
    Matrix3D(const Vector3D& column1, const Vector3D& column2, const Vector3D& column3);
    Matrix3D(double* arr);             ///< from left to rigth from top to button
    Matrix3D(const Matrix3D& other);
    Matrix3D(const Vector3D& init);    ///< diagonalmatrix of vector
    Matrix3D(const RPY& other);        ///< rotation matrix from Euler
    Matrix3D(const AngleAxis& other);  ///< corresponding rotation matrix
    Matrix3D(const Quaternion& other); ///< corresponding rotation matrix

    Vector3D getVec() const; ///< return the rotations axis which is codded in the matrix
    double getAngle() const; ///< returns the rotation angle which is codded in the matrix

    Vector3D getRow1() const;
    Vector3D getRow2() const;
    Vector3D getRow3() const;

    Vector3D getColumn1() const;
    Vector3D getColumn2() const;
    Vector3D getColumn3() const;

    void setRow1(const Vector3D& row);
    void setRow2(const Vector3D& row);
    void setRow3(const Vector3D& row);

    void setColumn1(const Vector3D& column);
    void setColumn2(const Vector3D& column);
    void setColumn3(const Vector3D& column);

    Matrix3D mAdd(const Matrix3D& other) const; ///< adds element for element
    Matrix3D mSub(const Matrix3D& other) const; ///< subs element for element
    Matrix3D scale(const double &factor) const;
    Matrix3D mMult(const Matrix3D& other) const;
    Matrix3D mDivide(const Matrix3D& other) const; ///< multiply with inverse

    Matrix3D cofac() const; ///< jedes Eintrages aij erhält die Unterdeterminante Aij (für inverse matrixen)
    Matrix3D adjoint() const; ///< die Transponierte Kofaktormatrix
    Matrix3D invert() const;
    Matrix3D transpose() const;
    double trace() const; ///< return Die Spur der Matrix

    void rotationX(const double &angle); ///< reinit as rotation matrix
    void rotationY(const double &angle); ///< reinit as rotation matrix
    void rotationZ(const double &angle); ///< reinit as rotation matrix

    double determinant() const;
    bool isOrthogonal() const;
    bool equals(const Matrix3D& other) const;

    Quaternion toQuaternion() const;
    RPY toRPY() const;
    AngleAxis toAngleAxis() const;

    void print() const;
};

/********************          operators          ********************/
inline Matrix3D operator+(const Matrix3D &left, const Matrix3D &right) { return left.mAdd(right); }
inline Matrix3D operator-(const Matrix3D &left, const Matrix3D &right) { return left.mSub(right); }
inline Matrix3D operator*(const Matrix3D &left, const Matrix3D &right) { return left.mMult(right); } 

inline Matrix3D operator*(const double   &left, const Matrix3D &right) { return right.scale(left); }
inline Matrix3D operator*(const Matrix3D &left, const double   &right) { return left.scale(right); }
inline Matrix3D operator/(const Matrix3D &left, const double   &right) { return left.scale(1.0/right); }

inline Matrix3D operator*(const Vector3D &left, const Vector3D &right) { return Matrix3D( left*right.x, left*right.y, left*right.z ); }

// in matVecMult right and left are toggled
inline Vector3D operator*(const Matrix3D &left, const Vector3D &right) { return right.matVecMult(left); }

} /* namespace matlib */
} /* namespace arctos */

#endif /* MATLIB_MATRIX3D_HPP */
