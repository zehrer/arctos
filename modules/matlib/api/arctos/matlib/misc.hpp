/**
 * Copyright (c) 2008-2018, DLR and University Wuerzburg
 * All rights reserved.
 *
 * @licence BSD
 * @brief   useful functions
 * @author  Unknown
 */
#ifndef MATLIB_MISC_HPP
#define MATLIB_MISC_HPP

#include <stdint.h>
#include <math.h>

#ifndef M_PI
    #define M_PI 3.14159265358979323846
#endif

namespace arctos {
namespace matlib {

class Matrix3D;
class Polar;
class Vector3D;

const double EPSILON = 0.0000001;

inline bool   isAlmost0(const double& a)   { return fabs(a) < EPSILON; }
inline double grad2Rad(const double& grad) { return grad * M_PI / 180.0; }
inline double rad2Grad(const double& rad)  { return rad * 180.0 / M_PI; }
inline double square(const double& x)      { return x*x; }
inline double frac(const double& x)        { return x - floor(x); }
inline double mod(const double& dividend, const double& divisor ) {
    return (isAlmost0(divisor) ? 0.0 : divisor * frac( dividend/divisor ));
}
int64_t faculty(const int& x);
double FMod2p(const double& x); ///< floating point rest, after division with 2Pi 
double fabs(const double& value);


/**
 * jd means Julian Date, which describes the days from 1st January −4712 (4713 bc) 12:00 o' clock
 */
const double JD2000 = 2451545.0;  //days  -- Julian Date on 01.01.2000 12'o clock
const double A_WGS84 = 6378137.0; // m
const double F_WGS84 = 1./298.257223563;

double R_n(const double degreeOfLatitude); ///< earthradius in meter at given latitude (rad)

/**
Reference Coordinate Systems

ECEF - Earth Centered Earth Fixed, x-Axis pointing to International Reference Meridian
ECI - Earth Centered Inertial, x-Axis aligned to Earth's Mean Equator and Equinox at 12:00 Terrestrial Time on 1 January 2000
Geodetic System, based on WGS84 Ellipsoid, defined by latitude, longitude, altitude
*/
double daysSinceY2k(int year, int month, int day, int hour, int minute, double second);
double utcToJD(int year, int month, int date, int hour, int minute, double second);
double jd2GAST(double jd);
double jd2GMST(double jd);
Matrix3D eciToECEFrotmat(double jd);
Vector3D eciToECEF(const Vector3D ecef, int64_t utcNanoseconds); ///<(meter, meter, meter) -> (meter, meter, meter)
Vector3D ecefToECI(const Vector3D eci, int64_t utcNanoseconds);///<(meter, meter, meter) -> (meter, meter, meter)
Vector3D geodeticToECEF(const Polar& polar); ///< Warning! Earth-surface (meter, rad, rad) -> Eart-center (meter, meter, meter)

/**
  Converts cartesian coordinates to ellipsoidal.
  Uses iterative alogithm with Heiskanen and Moritz's Method (1967)
  This method has been proved fastest when convergence of both
  latitude and ellipsoidal height is required (see H. S. Fok and H.
  Baki Iz "A Comparative Analysis of the Performance of Iterative
  and Non-iterative Solutions to the Cartesian to Geodetic
  Coordinate Transformation")
  Computation time is only slightly worse than for the
  Bowring direct formula but accuracy is better.
*/
Polar    ecefToGeodetic(const Vector3D& other);///< Warning! Eart-center (meter, meter, meter) -> Earth-surface (meter, rad, rad)

} /* namespace matlib */
} /* namespace arctos */

#endif /* MATLIB_MISC_HPP */
