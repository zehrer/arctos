/**
 * Copyright (c) 2008-2018, DLR and University Wuerzburg
 * All rights reserved.
 *
 * @licence BSD
 * @brief   Rotation angle and axis
 * @author  Unknown
 */
#ifndef MATLIB_ANGLE_AXIS_HPP
#define MATLIB_ANGLE_AXIS_HPP

#include "vector3d.hpp"

namespace arctos {
namespace matlib {

class Matrix3D;
class Quaternion;
class RPY;

///stellt eine Rotation durch einen Drehwinkel und eine Rotationsachse dar
class AngleAxis {
public:
    Vector3D u; ///< Stellt den Einheitsvektor u der Rotation dar.
    double phi; ///< Stellt den Rotationswinkel phi in Radians dar.

    AngleAxis(); ///< (0, (1,0,0);
    AngleAxis(const double &phi, const double &x, const double &y, const double &z);
    AngleAxis(const double &phi, const Vector3D& u);
    AngleAxis(const AngleAxis& other);
    AngleAxis(const Quaternion& q);
    AngleAxis(const Matrix3D& M);
    AngleAxis(const RPY& rpy);

    Quaternion toQuaternion() const;
    Matrix3D toMatrix3D() const;
    RPY toRPY() const;

    void print() const;
};

} /* namespace matlib */
} /* namespace arctos */

#endif /* MATLIB_ANGLE_AXIS_HPP */
