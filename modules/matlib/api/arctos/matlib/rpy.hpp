/**
 * Copyright (c) 2008-2018, DLR and University Wuerzburg
 * All rights reserved.
 *
 * @licence BSD
 * @brief   Roll, Pitch, Yaw
 * @author  Unknown
 */
#ifndef MATLIB_RPY_HPP
#define MATLIB_RPY_HPP

#include "vector3d.hpp"

namespace arctos {
namespace matlib {

class AngleAxis;
class Matrix3D;
class Quaternion;
class Rotor;

class RPY  : public Vector3D {
/*
 * RPY: Roll, Pitch, Yaw ist not a vector, but we need to reuse all functions from vector, therefore 
 * we use  RPY as it were a vector
 */
public:
    RPY() { }
    RPY(const Vector3D& a) { x = a.x; y = a.y; z = a.z; }
    RPY(const RPY& a) : Vector3D(a.x, a.y, a.z) { }
    RPY(const double roll, const double pitch, const double yaw) { x = roll; y = pitch; z = yaw; };
    RPY(const Quaternion& q);
    RPY(const Matrix3D& M);
    RPY(const AngleAxis& other);
    RPY(const Rotor& rot);

    RPY accumulateRotation(RPY& increment); ///<  converts both to quaternion, mulitply and back to RPY

    Matrix3D   toMatrix3D() const;
    Quaternion toQuaternion() const;
    AngleAxis  toAngleAxis() const;
    double     getRotationAngle() const;
    Vector3D   getRoationAxis() const;

    inline double getRoll()  const   { return x; }
    inline double getPitch() const   { return y; }
    inline double getYaw()   const   { return z; }
    inline void   setRoll(double r)  { x = r;    }
    inline void   setPitch(double p) { y = p;    }
    inline void   setYaw(double yaw) { z = yaw;  }
};

} /* namespace matlib */
} /* namespace arctos */

#endif /* MATLIB_RPY_HPP */
