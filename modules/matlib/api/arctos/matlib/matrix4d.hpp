/**
 * Copyright (c) 2008-2018, DLR and University Wuerzburg
 * All rights reserved.
 *
 * @licence BSD
 * @brief   4x4 matrix 
 * @author  Unknown
 */
#ifndef MATLIB_MATRIX4D_HPP
#define MATLIB_MATRIX4D_HPP

namespace arctos {
namespace matlib {

class Matrix3D;
class Vector3D;

/// 4x4-Matrix Rotation und einer Translation 
class Matrix4D {
public:
    double r[4][4];

    Matrix4D(); ///< no rotation, not translation
    Matrix4D(const Matrix3D& rot, const Vector3D& trans);
    Matrix4D(double* arr); ///< arr[16], left to right, top  to bottom
    Matrix4D(const Matrix4D& other);

    Matrix3D getRotation() const;    ///< returns the corresponding rotation matrix
    Vector3D getTranslation() const; ///< returns the translation vector

    Matrix4D scale(const double &factor) const;
    Matrix4D mMult(const Matrix4D& right) const;
    Matrix4D invert() const; ///< inverted rotation and translation

    void print() const;
};

/********************          operators          ********************/
Matrix4D operator+(const Matrix4D &left, const Matrix4D &right);
Matrix4D operator*(const Matrix4D &left, const double   &right);
Matrix4D operator/(const Matrix4D &left, const double   &right);

} /* namespace matlib */
} /* namespace arctos */

#endif /* MATLIB_MATRIX4D_HPP */
