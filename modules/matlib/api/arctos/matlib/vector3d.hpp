/**
 * Copyright (c) 2008-2018, DLR and University Wuerzburg
 * All rights reserved.
 *
 * @licence BSD
 * @brief   3-dim mathematical vector with x, y and z 
 * @author  Unknown
 */
#ifndef MATLIB_VECTOR3D_HPP
#define MATLIB_VECTOR3D_HPP

namespace arctos {
namespace matlib {

class AngleAxis;
class Matrix3D;
class Polar;
class Quaternion;
class RPY;
class Vector4D;

class Vector3D {
public:
    double x;
    double y;
    double z;

    Vector3D();                         ///< generates (0,0,0)
    Vector3D(const double &x, const double &y, const double &z);
    Vector3D(const Vector3D& other);    ///< copy from vector
    Vector3D(double* arr);              ///< like Vector3D (arr[0],arr[1],arr[2])

    bool resetIfNAN(); ///< sets to (0,0,0) if any component is infinite or NAN

    Vector3D vecAdd(const Vector3D& other) const;
    Vector3D vecSub(const Vector3D& other) const;
    Vector3D  scale(const double &factor) const;

    Vector3D cross(const Vector3D& right) const;
    double dot(const Vector3D& other) const;

    double getLen() const;
    double distance(const Vector3D& other) const;
    
    Vector3D normalize() const;
    bool isNormalized() const;
    bool equals(const Vector3D& other) const;

    double getAngle(const Vector3D& other) const;
    bool isOrthogonal(const Vector3D& other) const;

    Polar carToPolar() const; ///< cartesian to polar coordinates

    Vector3D matVecMult(const Matrix3D& M) const;   ///< multiply with a 3x3 matrix can be a rotation
    Vector3D qRotate(const Quaternion& q) const;    ///< rotates using a quaternion
    Vector3D mRotate(const Matrix3D& M) const;      ///< rotate using a rotation matrix
    Vector3D aRotate(const AngleAxis& u_phi) const; ///< rotation using angelaxis
    Vector3D rpyRotate(const RPY& rpy) const;       ///< rotation using rpy

    Vector4D to4D() const; ///< generates (x,y,z,1) 

    void print() const;
};

/********************          functions          ********************/
Vector3D rotateX(const Vector3D& s, const double &angle); ///< rotates vector s angle (radians) arround x 
Vector3D rotateY(const Vector3D& s, const double &angle); ///< rotates vector s angle (radians) arround y
Vector3D rotateZ(const Vector3D& s, const double &angle); ///< rotates vector s angle (radians) arround z

Vector3D elementWiseProduct (const Vector3D &left, const Vector3D &right);
Vector3D elementWiseDivision(const Vector3D &left, const Vector3D &right);

Matrix3D skewSymmetricMatrix(const Vector3D &v);

/********************          operators          ********************/
inline Vector3D operator+   (const Vector3D &left, const Vector3D &right) { return left.vecAdd(right); }
inline Vector3D operator-   (const Vector3D &left, const Vector3D &right) { return left.vecSub(right); }
inline Vector3D operator*   (const double   &left, const Vector3D &right) { return right.scale(left); }
inline Vector3D operator*   (const Vector3D &left, const double   &right) { return left.scale(right); }
inline Vector3D operator/   (const Vector3D &left, const double   &right) { return left.scale(1.0/right); }
inline double   dotProduct  (const Vector3D &left, const Vector3D &right) { return left.dot(right);   }
inline Vector3D crossProduct(const Vector3D &left, const Vector3D &right) { return left.cross(right); }

} /* namespace matlib */
} /* namespace arctos */

#endif /* MATLIB_VECTOR3D_HPP */
