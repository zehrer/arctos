/**
 * Copyright (c) 2008-2018, DLR and University Wuerzburg
 * All rights reserved.
 *
 * @licence BSD
 * @brief   basic math functionalities from RODOS's matlib
 * @author  Unknown
 */
#ifndef MATLIB_ALL_HPP
#define MATLIB_ALL_HPP

#include "angle_axis.hpp"
#include "complex.hpp"
#include "coordinate_frame3d.hpp"
#include "matrix3d.hpp"
#include "matrix4d.hpp"
#include "matrix6d.hpp"
#include "misc.hpp"
#include "polar.hpp"
#include "quaternion.hpp"
#include "rotor.hpp"
#include "rpy.hpp"
#include "vector3d.hpp"
#include "vector4d.hpp"
#include "vector6d.hpp"

#endif /* MATLIB_ALL_HPP */
