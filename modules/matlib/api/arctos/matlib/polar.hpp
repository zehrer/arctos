/**
 * Copyright (c) 2008-2018, DLR and University Wuerzburg
 * All rights reserved.
 *
 * @licence BSD
 * @brief   polar coordinate system
 * @author  Unknown
 */
#ifndef MATLIB_POLAR_HPP
#define MATLIB_POLAR_HPP

namespace arctos {
namespace matlib {

class Vector3D;

class Polar {
public:
    double r;
    double phi; ///< radians
    double theta; ///< radians

    Polar(); ///< (0,0,0)
    Polar(const double& r, const double& phi, const double& theta);
    Polar(const Polar& other);
    Polar(const Vector3D& other); ///< corresponding polar to equivalent (x,y,z)

    Vector3D toCartesian() const;

    void print() const;
};

} /* namespace matlib */
} /* namespace arctos */

#endif /* MATLIB_POLAR_HPP */
