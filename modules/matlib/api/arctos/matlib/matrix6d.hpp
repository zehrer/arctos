/**
 * Copyright (c) 2008-2018, DLR and University Wuerzburg
 * All rights reserved.
 *
 * @licence BSD
 * @brief   6x6 matrix 
 * @author  Unknown
 */
#ifndef MATLIB_MATRIX6D_HPP
#define MATLIB_MATRIX6D_HPP

#include "vector6d.hpp"

namespace arctos {
namespace matlib {

class Matrix3D;

class Matrix6D {
public:
    double r[6][6];

    Matrix6D();
    Matrix6D(const Matrix6D& other);
    Matrix6D(const Vector6D& diag);
    Matrix6D(const double* arr);
    Matrix6D(const Matrix3D& upperLeft, const Matrix3D& upperRight, const Matrix3D& lowerLeft, const Matrix3D& lowerRight);

    Vector6D getColumn(const int& j) const;
    Vector6D getRow(const int& i) const;

    void setColumn(const int &j, const Vector6D& column);
    void setRow(const int &i, const Vector6D& row);

    Vector6D diag() const;
    Matrix6D transpose() const;
    Matrix6D invert() const;
    
    Matrix6D scale(const double &factor) const;
    Matrix6D mAdd(const Matrix6D& other) const;
    Matrix6D mSub(const Matrix6D& other) const;
    Matrix6D mMult(const Matrix6D& other) const;

    Matrix3D upperLeft() const;
    Matrix3D upperRight() const;
    Matrix3D lowerLeft() const;
    Matrix3D lowerRight() const;
};

/********************          functions          ********************/
Matrix6D dyadic(const Vector6D& left, const Vector6D& right);
bool ludcmp(Matrix6D &a, Vector6D &indx, double &d);
void lubksb(Matrix6D &a, Vector6D &indx, Vector6D &b);

/********************          operators          ********************/
inline Matrix6D operator*(const double &left,   const Matrix6D& right) { return right.scale(left); }
inline Matrix6D operator*(const Matrix6D& left, const double &right)   { return left.scale(right); }
inline Matrix6D operator/(const Matrix6D &left, const double &right)   { return left.scale(1.0/right); }
inline Matrix6D operator+(const Matrix6D& left, const Matrix6D& right) { return left.mAdd(right); }
inline Matrix6D operator-(const Matrix6D& left, const Matrix6D& right) { return left.mSub(right); }
inline Matrix6D operator*(const Matrix6D& left, const Matrix6D& right) { return left.mMult(right); }
inline Matrix6D operator*(const Vector6D &left, const Vector6D &right) { return dyadic(left, right); }
inline Vector6D operator*(const Matrix6D& left, const Vector6D& right) { return right.matVecMult(left); }
Matrix6D operator/(const Matrix6D& Mat, double value);

} /* namespace matlib */
} /* namespace arctos */

#endif /* MATLIB_MATRIX6D_HPP */
