/**
 * Copyright (c) 2008-2018, DLR and University Wuerzburg
 * All rights reserved.
 *
 * @licence BSD
 * @brief   quaternion
 * @author  Unknown
 */
#ifndef MATLIB_QUATERNION_HPP
#define MATLIB_QUATERNION_HPP

#include <math.h>

#include "vector3d.hpp"

namespace arctos {
namespace matlib {

class AngleAxis;
class Matrix3D;
class Matrix4D;
class Rotor;
class RPY;

class Quaternion {
public:
    double q0;
    Vector3D q;
    Quaternion(); ///< creates q = (1,(0,0,0))
    Quaternion(const Quaternion& q);
    Quaternion(const double &q0, const double &q1, const double &q2, const double &q3); ///< (q0, (q1,q2,q3))
    Quaternion(double* arr); /// < like Quaternion(arr[0],(arr[1],arr[2],arr[3]))
    Quaternion(const double &q0,const Vector3D& q);
    Quaternion(const AngleAxis& other); ///<  q = cos(phi/2) + u*sin(phi/2)*/
    Quaternion(const Rotor& rot);
    Quaternion(const Matrix3D& other);
    Quaternion(const RPY& other);

    bool resetIfNAN(); ///< sets to (1,(0,0,0)) if any component is infinit or NAN

    double    getAngle() const; ///< radians
    Vector3D  getVec() const;

    Quaternion qAdd(const Quaternion& other) const;
    Quaternion qSub(const Quaternion& other) const;
    Quaternion scale(const double &factor) const;
    Quaternion qMult(const Quaternion& right) const;

    Quaternion conjugate() const; ///< returns  (q0, -(q) )
    Quaternion invert() const; ///< returns multiplikative Inverse
    Quaternion qDivide(const Quaternion& denominator) const; ///< (*this)/(other.invert())*/

    double getLen() const;
    Quaternion normalize() const;
    bool isNormalized() const;
    bool equals(const Quaternion& other) const;
    AngleAxis toAngleAxis() const;
    Matrix3D toMatrix3D() const;
    RPY toRPY() const;

    void print() const;
};

/********************          operators          ********************/
inline Quaternion operator+(const Quaternion &left, const Quaternion &right){ return left.qAdd(right); }
inline Quaternion operator-(const Quaternion &left, const Quaternion &right){ return left.qSub(right); }
inline Quaternion operator*(const Quaternion &left, const Quaternion &right){ return left.qMult(right); }
inline Quaternion operator*(const double     &left, const Quaternion &right){ return right.scale(left); }
inline Quaternion operator*(const Quaternion &left, const double     &right){ return left.scale(right); }
inline Vector3D   operator*(const Quaternion &left, const Vector3D   &right){ return right.qRotate(left); }
inline Quaternion operator/(const Quaternion &left, const double     &right){ return left.scale(1.0/right); }
inline Quaternion operator-(const Quaternion &right)                        { return right.conjugate(); }

inline Quaternion qX(const double &phi) { return Quaternion(cos(phi/2), sin(phi/2), 0.0, 0.0); }
inline Quaternion qY(const double &phi) { return Quaternion(cos(phi/2), 0.0, sin(phi/2), 0.0); }
inline Quaternion qZ(const double &phi) { return Quaternion(cos(phi/2), 0.0, 0.0, sin(phi/2)); }
inline Quaternion q1()                  { return Quaternion(1,          0.0, 0.0, 0.0); }
Quaternion operator*(const Matrix4D &left, const Quaternion &right);

} /* namespace matlib */
} /* namespace arctos */

#endif /* MATLIB_QUATERNION_HPP */
