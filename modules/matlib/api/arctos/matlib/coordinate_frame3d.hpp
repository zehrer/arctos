/**
 * Copyright (c) 2008-2018, DLR and University Wuerzburg
 * All rights reserved.
 *
 * @licence BSD
 * @brief   cartesian coordinate system
 * @author  Unknown
 */
#ifndef MATLIB_COORDINATE_FRAME3D_HPP
#define MATLIB_COORDINATE_FRAME3D_HPP

#include "vector3d.hpp"

namespace arctos {
namespace matlib {

class Matrix3D;
class Matrix4D;
class Quaternion;

/** karstesisches Koordinatensystems im euklidischem Raum
    3 orthogonale Einheitsvektoren x,y und z + ein Urpsrung */
class CoordinateFrame3D {
public:
    Vector3D x; ///< Einheitsvektor in x-Richtung
    Vector3D y; ///< Einehitsvektor in y-Richtung
    Vector3D z; ///< Einheitsvektor in z-Richtung
    Vector3D origin; ///< Ursprung des Koordinatensystems

    CoordinateFrame3D(); ///< all elements as unit vectors
    CoordinateFrame3D(const Vector3D& x, const Vector3D& y, const Vector3D& z, const Vector3D& origin);
    CoordinateFrame3D(const Vector3D& x, const Vector3D& y, const Vector3D& origin); ///< z will be generated ortogonal to x, y
    CoordinateFrame3D(const CoordinateFrame3D& other);

    Matrix4D mapTo(const CoordinateFrame3D& other) const; ///< rotation and translation from this to other

    CoordinateFrame3D translate(const Vector3D& trans) const;
    CoordinateFrame3D rotate(const Matrix3D& rot) const;
    CoordinateFrame3D rotate(const Quaternion& q) const;
};

} /* namespace matlib */
} /* namespace arctos */

#endif /* MATLIB_COORDINATE_FRAME3D_HPP */
