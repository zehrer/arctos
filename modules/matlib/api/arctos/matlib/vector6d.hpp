/**
 * Copyright (c) 2008-2018, DLR and University Wuerzburg
 * All rights reserved.
 *
 * @licence BSD
 * @brief   6-dim mathematical vector
 * @author  Unknown
 */
#ifndef MATLIB_VECTOR6D_HPP
#define MATLIB_VECTOR6D_HPP

namespace arctos {
namespace matlib {

class Vector3D;
class Matrix6D;

class Vector6D {
public:
    double v[6];

    Vector6D();
    Vector6D(const Vector6D& other);
    Vector6D(const double* arr);
    Vector6D(double x_, double y_, double z_,
             double u_, double v_, double w_);
    Vector6D(const Vector3D& upper, const Vector3D& lower);

    double getLen() const;
    Vector6D scale(const double& factor) const;
    Vector6D vecAdd(const Vector6D& other) const;
    Vector6D vecSub(const Vector6D& other) const;
    Vector6D matVecMult(const Matrix6D& M) const;

    void print() const;
};

/********************          operators          ********************/
inline Vector6D operator+(const Vector6D& left, const Vector6D& right) { return left.vecAdd(right);    }
inline Vector6D operator-(const Vector6D& left, const Vector6D& right) { return left.vecSub(right);    }
inline Vector6D operator*(double value, const Vector6D& right)         { return right.scale(value);    }
inline Vector6D operator*(const Vector6D& left, const double &value)   { return left.scale(value);     }
inline Vector6D operator/(const Vector6D &left, const double &right)   { return left.scale(1.0/right); }

/********************          functions          ********************/
double dotProduct(const Vector6D& left, const Vector6D& right);

} /* namespace matlib */
} /* namespace arctos */

#endif /* MATLIB_VECTOR6D_HPP */
