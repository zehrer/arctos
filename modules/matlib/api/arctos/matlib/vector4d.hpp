/**
 * Copyright (c) 2008-2018, DLR and University Wuerzburg
 * All rights reserved.
 *
 * @licence BSD
 * @brief   4-dim mathematical vector with x, y, z and an scaling factor 
 * @author  Unknown
 */
#ifndef MATLIB_VECTOR4D_HPP
#define MATLIB_VECTOR4D_HPP

#include "vector3d.hpp"

namespace arctos {
namespace matlib {

class Matrix4D;

class Vector4D : public Vector3D {
public:
    double scale;
    Vector4D(); ///< creates (0,0,0,1)
    Vector4D(const double &x, const double &y, const double &z, const double &scale);
    Vector4D(const Vector4D& other);
    Vector4D(double* arr); ///< like Vector4D(arr[0],arr[1],arr[2],arr[3])

    Vector4D matVecMult(const Matrix4D& M) const;
    Vector3D to3D() const; ///< returns (x,y,z)
    Vector4D mRotate(const Matrix4D& M) const;
    void print() const;
};

} /* namespace matlib */
} /* namespace arctos */

#endif /* MATLIB_VECTOR4D_HPP */
