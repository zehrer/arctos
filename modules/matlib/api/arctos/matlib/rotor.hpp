/**
 * Copyright (c) 2008-2018, DLR and University Wuerzburg
 * All rights reserved.
 *
 * @licence BSD
 * @brief   basis to rotate from one vector to another
 * @author  Unknown
 */
#ifndef MATLIB_ROTOR_HPP
#define MATLIB_ROTOR_HPP

#include "vector3d.hpp"

namespace arctos {
namespace matlib {

class Rotor {
public:
    double cosAngle;
    Vector3D axis;
    Rotor()                                   { cosAngle = axis.x = axis.y = axis.z = 1.0; }
    Rotor(const Rotor& other)                 { cosAngle = other.cosAngle; axis = other.axis; }
    Rotor(const double ca, const Vector3D& u) { cosAngle = ca; axis = u; }
    Rotor(const Vector3D& fromVector, const Vector3D& toVector); ///< rotor to go from one vector ot other
    bool resetIfNAN(); ///< sets to (1,(1,1,1)) if any component is infinite or NAN
    bool isNoRotation();
    void print();
};

} /* namespace matlib */
} /* namespace arctos */

#endif /* MATLIB_ROTOR_HPP */
