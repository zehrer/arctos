/**
 * @file
 * @copyright Copyright (c) 2022-2024 Michael Zehrer. This file is part of ARCTOS
 * @license MIT
 * @brief context structure and defines
 */
#ifndef ARCTOS_SYSTEM_RUN_ARM_CONTEXT_HPP
#define ARCTOS_SYSTEM_RUN_ARM_CONTEXT_HPP

#include <stdint.h>
#include CMSIS_DEVICE

namespace arctos {
namespace system {
namespace run {

/***********************************************************************
 * Exception Return (EXC_RETURN)
 **********************************************************************/
#define EXC_RETURN_MASK             0x1F

// return to handler mode with main stack
#define EXC_RETURN_HANDLER_MAIN     0x1
// return to thread mode with main stack
#define EXC_RETURN_THREAD_MAIN      0x9
// return to thread mode with process stack
#define ECX_RETURN_THREAD_PROC      0xD

// return to a non-floating-point context
#define ECX_RETURN_NO_FP            0x10
// return to a floating-point context
#define ECX_RETURN_WITH_FP          0x0


/***********************************************************************
 * Context layout(s)
 **********************************************************************/

/**
 * @brief Automatically saved registers (no FPU used)
 */
struct [[gnu::packed]] ContextAutoNoFP {
   uint32_t r0;
   uint32_t r1;
   uint32_t r2;
   uint32_t r3;
   uint32_t r12;
   uint32_t lr;
   uint32_t pc;
   xPSR_Type xpsr;
};

/**
 * @brief Automatically saved registers (FPU used)
 */
struct [[gnu::packed]] ContextAutoWithFP {
   uint32_t r0;
   uint32_t r1;
   uint32_t r2;
   uint32_t r3;
   uint32_t r12;
   uint32_t lr;
   uint32_t pc;
   xPSR_Type xpsr;
   uint32_t s0;
   uint32_t s1;
   uint32_t s2;
   uint32_t s3;
   uint32_t s4;
   uint32_t s5;
   uint32_t s6;
   uint32_t s7;
   uint32_t s8;
   uint32_t s9;
   uint32_t s10;
   uint32_t s11;
   uint32_t s12;
   uint32_t s13;
   uint32_t s14;
   uint32_t s15;
   uint32_t fpscr;
};

/**
 * @brief Context layout without floating-point storage
 */
struct [[gnu::packed]] ContextNoFP {
   uint32_t r4;
   uint32_t r5;
   uint32_t r6;
   uint32_t r7;
   uint32_t r8;
   uint32_t r9;
   uint32_t r10;
   uint32_t r11;
   uint32_t exc_return;

   ContextAutoNoFP a; //!< automatically saved registers
};

/**
 * @brief Context layout with floating-point storage
 */
struct ContextWithFP {
   uint32_t r4;
   uint32_t r5;
   uint32_t r6;
   uint32_t r7;
   uint32_t r8;
   uint32_t r9;
   uint32_t r10;
   uint32_t r11;
   uint32_t exc_return;

   uint32_t s16;
   uint32_t s17;
   uint32_t s18;
   uint32_t s19;
   uint32_t s20;
   uint32_t s21;
   uint32_t s22;
   uint32_t s23;
   uint32_t s24;
   uint32_t s25;
   uint32_t s26;
   uint32_t s27;
   uint32_t s28;
   uint32_t s29;
   uint32_t s30;
   uint32_t s31;

   ContextAutoWithFP a; //!< automatically saved registers
};

} /* namespace run */
} /* namespace system */
} /* namespace arctos */

#endif /* ARCTOS_SYSTEM_RUN_ARM_CONTEXT_HPP */
