/**
 * @file
 * @copyright Copyright (c) 2024 Michael Zehrer. This file is part of ARCTOS
 * @license MIT
 * @brief scheduler implementation for ARMv7M ports
 */
#ifndef ARCTOS_SYSTEM_RUN_ARMV7M_SCHEDULER_HPP
#define ARCTOS_SYSTEM_RUN_ARMV7M_SCHEDULER_HPP
#include <arctos/system/run/scheduler.hpp>
#include <arctos/system/run/schedulable.hpp>

namespace arctos {
namespace system {
namespace run {

/**
 * @class ArmV7mScheduler
 * @brief ARMv7 implementation of the priority based scheduler
 */
class ArmV7mScheduler : public Scheduler {
protected:
   /**
    * @brief Prepares and assigns the given `entry` for the given `work`
    * @note It mainly sets up the task's context
    */
   void prepareEntry(Entry &entry, Schedulable *const work) override;

   /**
    * @brief Initializes all tasks and starts scheduling
    */
   [[noreturn]]
   void start(void) override;

   /**
    * @brief Helper function that wraps the call to `Schedulable::run()`
    *
    * The `prepareEntry(...)` function makes sure,
    * that this wrapper will be called at the startup of each task
    */
   [[noreturn]]
   void taskStartupWrapper(Scheduler::Entry *entry);
};

} /* namespace run */
} /* namespace system */
} /* namespace arctos */

#endif /* ARCTOS_SYSTEM_RUN_ARMV7M_SCHEDULER_HPP */
