/**
 * @file
 * @copyright Copyright (c) 2024 Michael Zehrer. This file is part of ARCTOS
 * @license MIT
 * @brief scheduler implementation for ARMv7M ports
 */
#include <stdbool.h>
#include <stdint.h>

#include <arctos/log/logging.hpp>

#include "arctos/system/run/port_scheduler.hpp"
#include "arctos/system/run/context.hpp"

namespace arctos {
namespace system {
namespace run {

static constinit ArmV7mScheduler core00_obj;
Scheduler &core00 = core00_obj;

extern "C" {

[[noreturn]] void abort(void);
extern uintptr_t __INITIAL_SP;

void* SVC_Handler_Main(ContextAutoNoFP *stk) {
   uint8_t svc_number = (reinterpret_cast<uint8_t*>(stk->pc))[-2];
   switch (svc_number) {
      case 0:
         /* start first task (context pointer is in r0) */
         return reinterpret_cast<void*>(stk->r0);
      default:
         break;
   }
   return nullptr;
}

/**
 * @note When the ISR enters the following registered have been saved automatically:
 * [FPSCR, S15 ... S0], xPSR, PC, LR, R12, R3, R2, R1, R0
 */
[[gnu::naked]]
void SVC_Handler(void) {
   __asm__ volatile(
      /* check which stack was used... */
      "tst lr, #4                \n\t"
      "ite eq                    \n\t"
      /* ...and point r0 accordingly */
      "mrseq r0, msp             \n\t"
      "mrsne r0, psp             \n\t"
      /* push lr to the stack (r7 = placeholder to keep 64-bit alignment) */
      "push {r7, lr}             \n\t"
      /* branch to the main handler (stacked registers as param) */
      "bl SVC_Handler_Main       \n\t"
      "pop {r7, lr}              \n\t"
      /* return on nullptr */
      "cbz r0, 1f                \n\t"
      /* pop the core registers */
      "ldmia r0!, {r4-r11, r14}  \n\t"
#if (__FPU_PRESENT == 1) && (__FPU_USED == 1)
      /* was the task using the FPU? */
      "tst r14, #0x10            \n\t"
      "it eq                     \n\t"
      /* if so, pop the high vfp registers too */
      "vldmiaeq r0!, {s16-s31}   \n\t"
#endif
      /* psp = r0 */
      "msr psp, r0               \n\t"
      "1:                        \n\t"
      "bx r14                    \n\t"
      ".align 2                  \n\t"
   );
}

/**
 * @note When the ISR enters the following registered have been saved automatically
 * [FPSCR, S15 ... S0], xPSR, PC, LR, R12, R3, R2, R1, R0
 * => the other registers (R11 ... R4, [S31 ... S16])
 *    must be saved and restored manually
 */
[[gnu::naked]]
void PendSV_Handler(void) {
   __asm__ volatile(
      /* r0 = psp (r0 was saved automatically => r0 can be used to push the rest */
      "mrs r0, psp                              \n\t"
#if (__FPU_PRESENT == 1) && (__FPU_USED == 1)
      /* was the task using the FPU? */
      "tst r14, #0x10                           \n\t"
      "it eq                                    \n\t"
      /* if so, push high vfp registers */
      "vstmdbeq r0!, {s16-s31}                  \n\t"
#endif
      /* save core registers */
      "stmdb r0!, {r4-r11, r14}                 \n\t"

      /* context is saved, now call the scheduler */
      "bl scheduler_wrapper                     \n\t"

      /* pop the core registers */
      "ldmia r0!, {r4-r11, r14}                 \n\t"
#if (__FPU_PRESENT == 1) && (__FPU_USED == 1)
      /* was the task using the FPU? */
      "tst r14, #0x10                           \n\t"
      "it eq                                    \n\t"
      /* if so, pop the high vfp registers too */
      "vldmiaeq r0!, {s16-s31}                  \n\t"
#endif
      /* psp = r0 */
      "msr psp, r0                              \n\t"
      "bx r14                                   \n\t"
      ".align 2                                 \n\t"
   );
}
} /* extern "C" */

void ArmV7mScheduler::prepareEntry(Entry &entry, Schedulable *const work) {
   entry.task = work;

   /* IMPORTANT: stack is growing downwards */

   /* mark bottom of stack */
   uint32_t *bos = work->stackLow() + (work->stackSize() / 4) - 1;
   *bos = 0xDEADDEAD;

   ContextNoFP *ctx = reinterpret_cast<ContextNoFP*>(bos - 1 - (sizeof(ContextNoFP) / 4));

   /* clear the whole xPSR... */
   ctx->a.xpsr.w = 0;
   /* ...but the Thumb bit */
   ctx->a.xpsr.b.T = 1;

   /* to access the address of a member function we need to move it into a
      temporary member-function pointer... */
   void (ArmV7mScheduler::*up)(Entry *) = &ArmV7mScheduler::taskStartupWrapper;
   /* ...cast it's address to the generic pointer type... */
   uintptr_t *pup = reinterpret_cast<uintptr_t*>(&up);
   /* ...and de-reference to get the actual value */
   ctx->a.pc  = *pup;

   ctx->a.lr  = 0;
   ctx->a.r12 = 0;
   ctx->a.r3  = 0;
   ctx->a.r2  = 0;
   /* the ARM calling convention for member functions:
      - the object is passed via a "hidden" parameter in r0
      - "normal" parameter(s) will be passed with r1 and r2 */
   ctx->a.r1  = reinterpret_cast<uintptr_t>(&entry);
   ctx->a.r0  = reinterpret_cast<uintptr_t>(this);

   ctx->exc_return = ((~EXC_RETURN_MASK) | ECX_RETURN_NO_FP | ECX_RETURN_THREAD_PROC);
   ctx->r11 = 0;
   ctx->r10 = 0;
   ctx->r9  = 0;
   ctx->r8  = 0;
   ctx->r7  = 0;
   ctx->r6  = 0;
   ctx->r5  = 0;
   ctx->r4  = 0;

   entry.context = ctx;
}

[[noreturn]]
void ArmV7mScheduler::start() {
   Scheduler::start();

   /* this system message is a bit too early */
   ::arctos::log::system("-------------- applications running --------------\n");

   /* set PendSV to the lowest, and SVCall to second lowest possible priority */
   NVIC_SetPriority(PendSV_IRQn, (1UL << __NVIC_PRIO_BITS) - 1UL);
   NVIC_SetPriority(SVCall_IRQn, (1UL << __NVIC_PRIO_BITS) - 2UL);

   /* the current values on the main stack are no longer needed */
   __set_MSP(reinterpret_cast<uintptr_t>(&__INITIAL_SP));

   register void *ctx asm ("r0") = this->selected_->context;
   __asm__ volatile(
      "svc 0   \n\t"
      "nop     \n\t"
   :
   : "r" (ctx)
   : "memory");

   abort();
}

[[noreturn]]
void ArmV7mScheduler::taskStartupWrapper(Entry *entry) {
   /*
   This function will be called once for every task (first context load).
   => The stack is clean and most of the registers are 0
      @see 'prepareEntry(...)'
      => we don't need prologue and epilogue here

   Unfortunately we can't use '[[gnu::naked]]', because Non-ASM statements
   aren't allowed inside 'naked' functions.

   It would be possible to manually clean up the stack here
   (kinda undo the prologue), but this would only free up a few bytes.
   */

   selected_ = entry;
   // TODO reset all signals!?

   selected_->task->run();

   // TODO remove task entry?

   while (true) {
      // TODO suspend till end of time
      // TODO yield();
   }
}

} /* namespace run */
} /* namespace system */
} /* namespace arctos */
