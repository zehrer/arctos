if (${ARM_ARCHITECTURE_PROFILE} STREQUAL "ARMv7M" OR
    ${ARM_ARCHITECTURE_PROFILE} STREQUAL "ARMv7E-M")
   add_subdirectory("${CMAKE_CURRENT_LIST_DIR}/v7m")
else ()
   message(FATAL_ERROR "[system/run/arm] No implementation for \"${ARM_ARCHITECTURE_PROFILE}\" found!")
endif ()
