/**
 * @file
 * @copyright Copyright (c) 2018-2024 Michael Zehrer. This file is part of ARCTOS
 * @license MIT
 * @brief a Task is an active schedulable object with own context and stack
 */
#ifndef ARCTOS_SYSTEM_RUN_TASK_HPP
#define ARCTOS_SYSTEM_RUN_TASK_HPP

#include <stdbool.h>
#include <stdint.h>
#include <stddef.h>

#include <arctos/system/run/params.h>
#include <arctos/system/run/schedulable.hpp>

namespace arctos {
namespace system {
namespace run {

template <size_t STACK_SIZE = DEFAULT_TASK_STACKSIZE>
class Task : public system::run::Schedulable {
private:
   alignas(4) uint32_t stack_[STACK_SIZE / 4];

public:
   Task(const char *const name = "unnamed task",
      const uint32_t priority = DEFAULT_TASK_PRIORITY)
       : Schedulable(stack_, STACK_SIZE, name, priority)
   { }
   virtual ~Task() = default;
};

} /* namespace run */
} /* namespace system */
} /* namespace arctos */
#endif /* ARCTOS_SYSTEM_RUN_TASK_HPP */
