/**
 * @file
 * @copyright Copyright (c) 2020-2023 Michael Zehrer. This file is part of ARCTOS
 * @license MIT
 * @brief short, low-level routines with highest priority
 * e.g. (hardware) interrupts, exceptions, supervisor calls, ...
 */
#ifndef ARCTOS_SYSTEM_RUN_EVENT_HPP
#define ARCTOS_SYSTEM_RUN_EVENT_HPP

#include "runnable.hpp"

namespace arctos {
namespace system {
namespace run {

class Event : public Runnable {
};

} /* namespace run */
} /* namespace system */
} /* namespace arctos */
#endif /* ARCTOS_SYSTEM_RUN_EVENT_HPP */
