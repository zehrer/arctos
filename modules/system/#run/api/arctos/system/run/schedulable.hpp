/**
 * @file
 * @copyright Copyright (c) 2021-2024 Michael Zehrer. This file is part of ARCTOS
 * @license MIT
 * @brief base class for active schedulable routines with own context.
 * @warning do not use directly!
 */
#ifndef ARCTOS_SYSTEM_RUN_SCHEDULABLE_HPP
#define ARCTOS_SYSTEM_RUN_SCHEDULABLE_HPP

#include <stdbool.h>
#include <stdint.h>
#include <stddef.h>

#include <arctos/system/run/runnable.hpp>

namespace arctos {
namespace system {
namespace run {

class Schedulable : public Runnable {
public:
   /**
    * @brief Creates a new schedulable work unit
    *
    * The specified stack space must have been allocated and aligned to 4-bytes!
    *
    * @param[in] stack_low Lowest address of the stack
    * @param[in] stack_size Size of the stack
    * @param[in] name Readable name
    * @param[in] priority Priority (higher values mean higher priority)
    */
   Schedulable(uint32_t *const stack_low, const size_t stack_size,
               const char *const name, const uint32_t priority);
   //~Schedulable() = delete; if the destructor is deleted, the constructor must also be deleted!

   const char *name(void)        const { return this->name_; }
   uint32_t    priority(void)    const { return this->priority_; }

   uint32_t *stackLow(void)      const { return this->stack_low_; }
   size_t    stackSize(void)     const { return this->stack_size_; }
   size_t    stackFree(void)     const;
   size_t    stackMaxUsage(void) const { return this->stackSize() - this->stackFree(); }

   /**
    * @brief Perform some initialization stuff
    *
    * This method is called at startup BEFORE run(...)
    * If you need to initialize something: Overwrite this function.
    *
    * @see run
    */
   virtual void init(void) { }


   static Schedulable *current(void);

   /**
    * @brief Pause and perform a reschedule.
    */
   static void yield(void);

protected:
   char const *const name_;
   uint32_t const    priority_;

   uint32_t *const   stack_low_;
   size_t const      stack_size_;
};

} /* namespace run */
} /* namespace system */
} /* namespace arctos */
#endif /* ARCTOS_SYSTEM_RUN_SCHEDULABLE_HPP */
