/**
 * @file
 * @copyright Copyright (c) 2020-2023 Michael Zehrer. This file is part of ARCTOS
 * @license MIT
 * @brief base class for user defined routines
 * @warning do not use directly!
 */
#ifndef ARCTOS_SYSTEM_RUN_RUNNABLE_HPP
#define ARCTOS_SYSTEM_RUN_RUNNABLE_HPP

namespace arctos {
namespace system {
namespace run {

class Runnable {
public:
   /**
    * @brief Entry point for user code
    *
    * The desired functionality shall be implemented by overloading this method
    */
   virtual void run() = 0;
};

} /* namespace run */
} /* namespace system */
} /* namespace arctos */
#endif /* ARCTOS_SYSTEM_RUN_RUNNABLE_HPP */
