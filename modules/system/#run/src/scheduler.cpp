/**
 * @file
 * @copyright Copyright (c) 2018-2024 Michael Zehrer. This file is part of ARCTOS
 * @license MIT
 * @brief scheduler
 */
#define __STDC_FORMAT_MACROS 1
#include <inttypes.h>
#include <stdbool.h>
#include <stdint.h>

#include <arctos/system/run/schedulable.hpp>
#include <arctos/system/run/scheduler.hpp>
#include <arctos/system/run/task.hpp>

#include <arctos/log/logging.hpp>
#include <arctos/time.hpp>

namespace arctos {
namespace system {
namespace run {

/***********************************************************************
 * Idle-Task
 ***********************************************************************/
/**
 * @class IdleTask
 * @brief This task will be executed when no other task wants to run
 */
class IdleTask : public Task<512> {
public:
   IdleTask() : Task("IDLE-Task", 0) { }
   void run();
};
void IdleTask::run() {
   // TODO save energy
   while (true) {
      Task::yield();
   }
}
static IdleTask idle_task;



/***********************************************************************
 * Scheduler
 ***********************************************************************/

void Scheduler::addTask(Schedulable *const work) {
   this->entries_cnt_++;
   if (this->entries_cnt_ > MAX_NUMBER_OF_TASKS) return;

   size_t pos = 0;
   // determine the position of the new entry...
   while (pos < (this->entries_cnt_ - 1) &&
          work->priority() <= this->entries_[pos].task->priority()) {
      pos++;
   }

   // ...and move all other entries (with lower prio) one slot further
   for (size_t i = (this->entries_cnt_ - 1); i > pos; i--) {
      this->entries_[i] = this->entries_[i - 1];
   }

   this->prepareEntry(this->entries_[pos], work);
}

void Scheduler::start(void) {
   if (this->entries_cnt_ > MAX_NUMBER_OF_TASKS) {
      log::warning("Task limit exceeded (%u/%u)! => try raising \"MAX_NUMBER_OF_TASKS\"\n",
         this->entries_cnt_, MAX_NUMBER_OF_TASKS);
      this->entries_cnt_ = MAX_NUMBER_OF_TASKS;
   }

   log::system("Task's in System:\n");
   for (size_t i = 0; i < this->entries_cnt_; ++i) {
      log::system("  Prio = %6" PRIu32 ", Stack = %6u: %s\n",
         this->entries_[i].task->priority(),
         this->entries_[i].task->stackSize(),
         this->entries_[i].task->name());

      this->entries_[i].task->init();
   }
}

void* Scheduler::wrapper(void *ctx) {
   core00.selected_->context = ctx;
   // TODO: do scheduling

   return core00.selected_->context;
}

} /* namespace run */
} /* namespace system */
} /* namespace arctos */
