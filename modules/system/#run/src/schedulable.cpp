/**
 * @file
 * @copyright Copyright (c) 2021-2024 Michael Zehrer. This file is part of ARCTOS
 * @license MIT
 * @brief base class for active schedulable routines with own context.
 * @warning do not use directly!
 */
#include <arctos/list.hpp>
#include <arctos/time.hpp>

#include <arctos/system/run/schedulable.hpp>

#include "arctos/system/run/scheduler.hpp"

static constexpr uint32_t kEmptyMemoryMarker = 0xDA7ADA7A;

namespace arctos {
namespace system {
namespace run {

/***********************************************************************
 * non-static stuff
 ***********************************************************************/

Schedulable::Schedulable(
   uint32_t *const stack_low, const size_t stack_size,
   const char *const name,
   const uint32_t priority)
    : name_(name), priority_(priority),
      stack_low_(stack_low), stack_size_(stack_size) {

   /* paint the stack space */
   for (uint32_t *stk = stack_low + (stack_size / 4) - 1; stk >= stack_low; stk--)
      *stk = kEmptyMemoryMarker;

   /* register the task for scheduling on core 0 */
   core00.addTask(this);
}

size_t Schedulable::stackFree() const {
   const uint32_t *stk = this->stack_low_;
   while (*stk == kEmptyMemoryMarker)
      ++stk;

   return (reinterpret_cast<uintptr_t>(stk)
         - reinterpret_cast<uintptr_t>(this->stack_low_));
}



/***********************************************************************
 * static stuff
 ***********************************************************************/

void Schedulable::yield() {
   /*
   Scheduler *scheduler = Scheduler::getInstance();
   Schedulable *preselection = scheduler->findNextToRun(NOW());

   if (preselection == Schedulable::current) {
      scheduler->clearCachedSelection();
      return;
   }

   ::arctos::porting::context::save();
   */
}

Schedulable *Schedulable::current(void) {
   return core00.current();
}

} /* namespace run */
} /* namespace system */
} /* namespace arctos */
