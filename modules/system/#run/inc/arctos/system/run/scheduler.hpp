/**
 * @file
 * @copyright Copyright (c) 2018-2024 Michael Zehrer. This file is part of ARCTOS
 * @license MIT
 * @brief scheduler
 */
#ifndef ARCTOS_SYSTEM_RUN_SCHEDULER_HPP
#define ARCTOS_SYSTEM_RUN_SCHEDULER_HPP

#include <stddef.h>
#include <stdint.h>

#include <arctos/system/run/params.h>
#include <arctos/system/run/schedulable.hpp>

namespace arctos {
namespace system {
namespace run {

/**
 * @class Scheduler
 * @brief priority based scheduler
 */
class Scheduler {
public:
   /**
    * @brief Adds a new task to the scheduling list
    */
   void addTask(Schedulable *const work);

   /**
    * @brief Initializes all tasks.
    * @note The derived class should overwrite this method,
    *       call this one and afterwards start the scheduling.
    */
   virtual void start(void);

   /**
    * @brief Current active task
    */
   Schedulable *current(void) { return this->selected_->task; }

   static void* wrapper(void *ctx) __asm__ ("scheduler_wrapper");

protected:
   struct Entry {
      Schedulable   *task    = nullptr;
      void *volatile context = nullptr;

      // needed? (partly) replace with signals?
      //volatile int64_t last_activation = 0;
      //volatile int64_t suspended_until = 0;
   };

   constexpr Scheduler(void) = default;

   /**
    * @brief Prepares and assigns the given "entry" for the given "work"
    * @note It SHOULDN'T initialize the "work", but prepare its context
    */
   virtual void prepareEntry(Entry &entry, Schedulable *const work) = 0;

   Entry entries_[MAX_NUMBER_OF_TASKS];

   /**
    * @brief Number of tasks defined by the user
    * @warning Might actually be greater than MAX_NUMBER_OF_TASKS at startup!
    */
   size_t entries_cnt_ = 0;

   /**
    * @brief Currently selected task
    */
   Entry *selected_ = entries_;
};

extern Scheduler &core00;

} /* namespace run */
} /* namespace system */
} /* namespace arctos */

#endif /* ARCTOS_SYSTEM_RUN_SCHEDULER_HPP */
