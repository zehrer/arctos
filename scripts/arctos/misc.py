# -*- coding: utf-8 -*-
# This file is part of ARCTOS
__copyright__ = "Copyright (c) 2018-2020 Michael Zehrer"
__license__   = "MIT"
__author__    = "Michael Zehrer"

import sys, os
import shutil, re

class Color:
    red       = ''
    green     = ''
    yellow    = ''
    blue      = ''
    magenta   = ''
    cyan      = ''
    white     = ''
    normal    = ''
    bold      = ''
    underline = ''
if (sys.platform != 'Pocket PC' and (sys.platform != 'win32' or 'ANSICON' in os.environ)) and \
    (hasattr(sys.stdout, 'isatty') and sys.stdout.isatty()):
    Color.red       = '\033[31m'
    Color.green     = '\033[32m'
    Color.yellow    = '\033[33m'
    Color.blue      = '\033[34m'
    Color.magenta   = '\033[35m'
    Color.cyan      = '\033[36m'
    Color.white     = '\033[37m'
    Color.normal    = '\033[0m'
    Color.bold      = '\033[1m'
    Color.underline = '\033[4m'

def print_formated(text, indent=0, startx=0, end=os.linesep):
    str_newline = os.linesep
    for i in range(0, indent):
        str_newline += ' '

    ts = shutil.get_terminal_size()
    for i in range(startx, indent):
        print(' ', end='')
    x = indent

    formated = []
    for word in text.split(' '):
        parts = word.split(os.linesep)
        for i in range(0, len(parts)):
            x += len(re.sub('{.*?}', '', parts[i])) + 1
            if i > 0 or x > ts.columns:
                formated.append(str_newline)
                x = indent

            formated.append(parts[i].format(
                red         = Color.red,
                green       = Color.green,
                yellow      = Color.yellow,
                blue        = Color.blue,
                magenta     = Color.magenta,
                cyan        = Color.cyan,
                white       = Color.white,
                normal      = Color.normal,
                bold        = Color.bold,
                underline   = Color.underline))
            formated.append(' ')
    print(''.join(formated)[:-1], end=end)

def print_note(text):
    print_formated('{green}{bold}[NOTE]    ', end='')
    print_formated(text + '{normal}', indent=10, startx=10)

def print_warning(text):
    print_formated('{yellow}{bold}[WARNING] ', end='')
    print_formated(text + '{normal}', indent=10, startx=10)

def print_error(text, exit_code = 1):
    print_formated('{red}{bold}[ERROR]   ', end='')
    print_formated(text + '{normal}', indent=10, startx=10)
    sys.exit(exit_code)
