# -*- coding: utf-8 -*-
# This file is part of ARCTOS
__copyright__ = "Copyright (c) 2018-2020 Michael Zehrer"
__license__   = "MIT"
__author__    = "Michael Zehrer"

import sys, os
if sys.version_info[0] < 3 or (sys.version_info[0] >= 3 and sys.version_info[1] < 5):
    raise ImportError('This package requires Python version >= 3.5')

dir_arctos              = dict()
dir_arctos['base']      = os.path.abspath(os.path.join(os.path.realpath(__file__),'..','..','..'))
dir_arctos['core']      = os.path.join(dir_arctos['base']       , 'core'             )
dir_arctos['modules']   = os.path.join(dir_arctos['base']       , 'modules'          )
dir_arctos['ports']     = os.path.join(dir_arctos['base']       , 'ports'            )
dir_arctos['tunings']   = os.path.join(dir_arctos['base']       , 'tunings'          )
dir_arctos['resources'] = os.path.join(dir_arctos['base']       , 'resources'        )
dir_arctos['templates'] = os.path.join(dir_arctos['resources']  , 'project-templates')

from .misc import print_error
for key, value in dir_arctos.items():
    if not os.path.isdir(value):
        print_error('{0}-directory "{1}" not found!'.format(key, value))
