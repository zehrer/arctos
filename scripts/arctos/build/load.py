# -*- coding: utf-8 -*-
# This file is part of ARCTOS
__copyright__ = "Copyright (c) 2018-2024 Michael Zehrer"
__license__   = "MIT"
__author__    = "Michael Zehrer"

from .helper import *
from .. import dir_arctos
from . import structure

import os
import datetime
import xml.etree.ElementTree as ET

class Loader:
    def __init__(self, path, name):
        self.path       = path
        self.name       = name
        self.root       = None
        self.object     = None

    def _get_attribute(xelem, attr_name, default = None):
        """
        By our convention an attribute could be specified either as a normal
        xml attribute or as a child '[parent tag name].[attribute name]'.
        This function considers both methods

        @type  xelem    : L{xml.etree.ElementTree.Element}
        @param xelem    : parsed xml tag
        @param attr_name: name of the attribute
        @param default  : default value if attribute was not found
        """
        a = xelem.get(attr_name, default=default)
        b = xelem.findtext(xelem.tag + '.' + attr_name, default=default)

        a = a.strip() if isinstance(a, str) else a

        if a is default and b is default:
            return default
        elif b is default or is_blank(b):
            return a
        elif a is default or is_blank(a):
            return rectify_xml_string(b) if isinstance(b, str) else b
        else:
            raise FormatError('Multiple definitions of attribute "{0}"!'.format(attr_name))

    def load(self):
        if self.object is not None:
            raise ProcessError('File "{0}" already loaded!'.format(self.path))

        self.root = ET.parse(self.path).getroot()
        if self.root.tag not in self.root_tag:
            raise FormatError('Unexpected root tag! Expected "{0}" but got "{1}".'
                              .format(self.root_tag, root.tag))

        xml_name = Loader._get_attribute(self.root, 'name')
        if xml_name != self.name:
            raise FormatError('Name mismatch! Filename says "{0}", but content says "{1}".'
                              .format(self.name, xml_name))

        self.object = self._manufacture()

    def _load_set_instruction(self, xelem):
        try:
            definition  = Loader._get_attribute(xelem, 'definition')
            value       = Loader._get_attribute(xelem, 'value')
            note        = Loader._get_attribute(xelem, 'note')

            return structure.SetInstruction(definition, value, note)
        except FormatError as fe:
            raise FormatError('set -> {1}'.format(fe.args[0]))

class Unit(Loader):
    def __init__(self, path, name):
        super().__init__(path, name)

    def load(self):
        super().load()

        # Normally each tag sould only appear once, but it
        # doesn't matter if it is specified multiple times

        # Dependencies should be evaluated first...
        for child in self.root.iterfind('dependencies'):
            self._load_dependecies(child)

        # ...and types second. But regardless of that, both have
        # to be evaluated before 'configuration' and 'paramaters'
        # because the latter might use them.
        for child in self.root.iterfind('types'):
            self._load_types(child)

        for child in self.root.iterfind('configuration'):
            self._load_configuration(child)

        for child in self.root.iterfind('parameters'):
            self._load_parameters(child)

        self.root   = None

    def _load_dependecies(self, xelem):
        global module, segment, target
        try:
            for child in xelem.iterfind('needs'):
                if child.text in module:
                    dep = module[child.text]
                else:
                    raise FormatError(('Could not satisfy dependency. '
                        'Essential module "{0}" not found!').format(child.text))
                self.object.add_module(dep)
        except FormatError as fe:
            raise FormatError('dependencies -> {0}'.format(fe.args[0]))

    def _load_types(self, xelem):
        try:
            for child in xelem.iterfind('type'):
                self.object.add_custom_type(self._load_custom_type(child))
        except FormatError as fe:
            raise FormatError('types -> {0}'.format(fe.args[0]))

    def _load_configuration(self, xelem):
        try:
            for child in xelem.iterfind('option'):
                self.object.add_option(self._load_param_or_option(child))
        except FormatError as fe:
            raise FormatError('configuration -> {0}'.format(fe.args[0]))

    def _load_parameters(self, xelem):
        try:
            for child in xelem.iterfind('param'):
                self.object.add_param(self._load_param_or_option(child))
        except FormatError as fe:
            raise FormatError('parameters -> {0}'.format(fe.args[0]))

    def _load_param_or_option(self, xelem):
        try:
            definition  = Loader._get_attribute(xelem, 'definition')
            readable    = Loader._get_attribute(xelem, 'readable')
            description = Loader._get_attribute(xelem, 'description')
            type_       = Loader._get_attribute(xelem, 'type')
            value       = Loader._get_attribute(xelem, 'value')
            apply_      = Loader._get_attribute(xelem, 'apply', 'default')
            minv        = float('-inf')
            maxv        = float('inf')

            if type_ in ['int', 'float']:
                minv    = Loader._get_attribute(xelem, 'min', float('-inf'))
                maxv    = Loader._get_attribute(xelem, 'max', float('inf'))
            elif type_ in self.object.custom_types:
                type_   = self.object.custom_types[type_]
                if (value is not None) and (value not in type_.items):
                    raise FormatError('Type "{0}" doesn\'t have an item with id "{1}".'
                                      .format(type_.name, value))
            elif type_ not in ['string', 'bool']:
                raise FormatError('Type "{0}" wasn\'t found or isn\'t applicable.'.format(type_))

            if xelem.tag == 'param':
                return structure.Param(definition, type_, value, minv, maxv,
                                       readable, description, apply_)
            elif xelem.tag == 'option':
                return structure.Option(definition, type_, value, minv, maxv,
                                        readable, description, apply_)
            else:
                return None
        except FormatError as fe:
            raise FormatError('{0} -> {1}'.format(xelem.tag, fe.args[0]))

    def _load_custom_type(self, xelem):
        try:
            name        = Loader._get_attribute(xelem, 'name')
            description = Loader._get_attribute(xelem, 'description')

            type_ = structure.CustomType(name, description)
            for child in xelem.iterfind('item'):
                type_.add_item(self._load_custom_type_item(child))

            return type_
        except FormatError as fe:
            raise FormatError('type -> {0}'.format(fe.args[0]))

    def _load_custom_type_item(self, xelem):
        try:
            id_         = Loader._get_attribute(xelem, 'id')
            value       = Loader._get_attribute(xelem, 'value')
            readable    = Loader._get_attribute(xelem, 'readable')
            description = Loader._get_attribute(xelem, 'description')

            itm = structure.CustomTypeItem(id_, value, readable, description)

            for child in xelem.iterfind('set'):
                itm.add_set_instruction(self._load_set_instruction(child))

            return itm
        except FormatError as fe:
            raise FormatError('item -> {0}'.format(fe.args[0]))

class Segment(Unit):
    root_tag = 'segment'

    def __init__(self, path, name):
        super().__init__(path, name)

    def _manufacture(self):
        return structure.Segment(self.name, os.path.dirname(self.path))

    def _load_dependecies(self, xelem):
        super()._load_dependecies(xelem)

        global module, segment, target
        try:
            for child in xelem.iterfind('extends'):
                if child.text in segment:
                    dep = segment[child.text]
                else:
                    raise FormatError(('Could not satisfy dependency. '
                        'Segment "{0}" not found!').format(child.text))
                self.object.add_parent(dep)
        except FormatError as fe:
            raise FormatError('dependencies -> {0}'.format(fe.args[0]))

class Target(Segment):
    root_tag = 'target'

    def __init__(self, path, name):
        super().__init__(path, name)

    def _manufacture(self):
        return structure.Target(self.name, os.path.dirname(self.path))

class Module(Unit):
    root_tag = 'module'

    def __init__(self, path, name):
        super().__init__(path, name)

    def _manufacture(self):
        module_type = Loader._get_attribute(self.root, 'type', default = 'normal')
        return structure.Module(self.name, os.path.dirname(self.path), module_type)

    def _load_dependecies(self, xelem):
        super()._load_dependecies(xelem)

        global module, segment, target
        try:
            for child in xelem.iterfind('extends'):
                if child.text in module:
                    dep = module[child.text]
                else:
                    raise FormatError(('Could not satisfy dependency. '
                        'Module "{0}" not found!').format(child.text))
                self.object.add_parent(dep)

            for child in xelem.iterfind('suits'):
                if child.text in target:
                    dep = target[child.text]
                elif child.text in segment:
                    dep = segment[child.text]
                else:
                    raise FormatError(('Could not satisfy dependency. '
                        'Target / Segment "{0}" not found!').format(child.text))
                self.object.add_compatibility(dep)
        except FormatError as fe:
            raise FormatError('dependencies -> {0}'.format(fe.args[0]))

class Tuning(Loader):
    root_tag = 'tuning'

    def __init__(self, path, name):
        super().__init__(path, name)

    def _manufacture(self):
        global target

        t_name = Loader._get_attribute(self.root, 'target')
        if t_name not in target:
            raise FormatError('Target "{0}" not found!'.format(t_name))

        description = Loader._get_attribute(self.root, 'description')
        return structure.Tuning(self.name, target[t_name], description)

    def load(self):
        super().load()

        for child in self.root.iterfind('modules'):
            self._load_modules(child)

        for child in self.root.iterfind('settings'):
            self._load_settings(child)

        self.root   = None

    def _load_modules(self, xelem):
        global module
        try:
            for child in xelem.iterfind('use'):
                if child.text not in module:
                    raise FormatError('Couldn\'t find Module "{0}"'.format(child.text))
                self.object.add_module(module[child.text])
        except FormatError as fe:
            raise FormatError('modules -> {0}'.format(fe.args[0]))

    def _load_settings(self, xelem):
        try:
            for_ = Loader._get_attribute(xelem, 'for')
            if is_blank(for_):
                raise FormatError('The attribute "for" is mandatory!')

            for child in xelem.iterfind('set'):
                self.object.add_set_instruction(for_, self._load_set_instruction(child))
        except FormatError as fe:
            raise FormatError('settings -> {0}'.format(fe.args[0]))

class Access:
    def __init__(self, search_dir, loader_class):
        self._files = dict()

        ending = '.{0}.xml'.format(loader_class.root_tag)
        for d, dirs, files in os.walk(search_dir):
            for f in files:
                if not f.endswith(ending):
                    continue

                path = os.path.join(d, f)
                name = f.removesuffix(ending)
                self._files[name] = loader_class(path, name)

    def ensure_load(self, loader):
        if loader.object is None:
            try:
                loader.load()
            except FormatError as fe:
                raise ProcessError('Failed to load file "{0}":\n{1} -> {2}'
                                   .format(loader.path, loader.root_tag, fe.args[0]))
        elif loader.root is not None:
            # loader is currently loading, but hasn't finished yet
            # => loader.object isn't complete
            #    => problem?
            pass

    def preload(self):
        for loader in self._files.values():
            self.ensure_load(loader)

    def __len__(self):
        return len(self._files)

    def __contains__(self, key):
        return key in self._files

    def __getitem__(self, key):
        itm = self._files[key]
        self.ensure_load(itm)
        return itm.object

    def __iter__(self):
        self._iter = iter(self._files)
        return self

    def __next__(self):
        key = next(self._iter)
        return self[key]

    def get_filepath(self, key):
        return self._files[key].path

segment = Access(dir_arctos['core']   , Segment)
target  = Access(dir_arctos['ports']  , Target )
module  = Access(dir_arctos['modules'], Module )
tuning  = Access(dir_arctos['tunings'], Tuning )

# the build system might rely on the double-linked-list (dep_parents <-> dep_childs)
# => at least for module's 'partial' type it is required
#   => preloading all modules is essential
module.preload()
