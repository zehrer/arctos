# -*- coding: utf-8 -*-
# This file is part of ARCTOS
__copyright__ = "Copyright (c) 2018-2024 Michael Zehrer"
__license__   = "MIT"
__author__    = "Michael Zehrer"

import os
import re

class FormatError(RuntimeError):
    pass

class ProcessError(RuntimeError):
    pass

def is_blank(string):
    """ checks if the given string is blank """
    if string and isinstance(string, str) and string.strip():
        return False
    return True

def is_acceptable_definition(string):
    return re.fullmatch('[0-9A-Za-z_\\-]+', string) is not None

def is_acceptable_name(string):
    return re.fullmatch('[0-9A-Za-z#_\\-]+', string) is not None

def rectify_xml_string(string : str):
    num_spaces = 0
    for char in string:
        if char in ['\n', '\r']:
            num_spaces = 0
        elif char in [' ', '\t']:
            num_spaces += 1
        else:
            break

    string = string.strip()

    if num_spaces > 0:
        pattern = r'(?<=\n)[\t ]{' + str(num_spaces) + '}'
        string = re.sub(pattern, '', string)
    
    return string

def scan_directory(directory, ends):
    """ Searches for files ending with 'ends' 
        recursively in the given 'directory' """
    rv = []
    for root, dirs, files in os.walk(directory):
        for filename in files:
            if filename.endswith(ends):
                rv.append(os.path.join(root, filename))
    return rv
