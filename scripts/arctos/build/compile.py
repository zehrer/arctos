# -*- coding: utf-8 -*-
# This file is part of ARCTOS
__copyright__ = "Copyright (c) 2018-2024 Michael Zehrer"
__license__   = "MIT"
__author__    = "Michael Zehrer"

from .helper import *
from . import structure
from .. import dir_arctos

import sys, os
import datetime

class OutputFile:
    def __init__(self, directory, filename):
        self.directory  = directory
        self.filename   = filename
        self.content    = ''
    def get_path(self):
        return os.path.join(self.directory, self.filename)

class Unit:
    _snippet_header = (
        '/**{n}'
        ' * @file{n}'
        ' * @brief  auto-generated params header{n}'
        ' *{n}'
        ' * Generated on {date}{n}'
        ' * DO NOT EDIT{n}'
        ' */{n}'
        '#ifndef {guard}_PARAMS_H{n}'
        '#define {guard}_PARAMS_H{n}'
        '{n}'
        '{types}'
        '{defines}'
        '#endif /* {guard}_PARAMS_H */{n}')
    _snippet_cmake = '' # to be overwritten by child class
    _snippet_cmake_empty = (
        '{i}{comment_begin:#^60}{n}'
        '{i}# module already present => nothing to do here{n}'
        '{i}{comment_end:#^60}{n}{n}')
    _snippet_cmake_link = (
        '{i}target_link_libraries({object_lib} {link}){n}')
    _snippet_header_param = (
        '#define {def:36} {val}{n}')
    _snippet_header_param_true = (
        '#define {def}{n}')
    _snippet_header_param_false = (
        '/* #undef {def} */{n}')
    _snippet_cmake_param = (
        '{i}set({def:36} {val}){n}')
    _snippet_cmake_param_linker = (
        '{i}target_link_options(iface_arctos INTERFACE LINKER:--defsym={def}={val}){n}')
    _snippet_header_type = (
        '// BEGIN of "{type_name}"{n}'
        '{items}'
        '// END of "{type_name}"{n}{n}')

    def __init__(self, unit, objlibs, dept, header):
        assert (isinstance(unit, structure.Unit)),'Wrong type'
        self.unit        = unit
        self.objlibs     = objlibs
        self.dept        = dept
        self.header      = header

        self.cmake       = None
        self.dep_parents = dict()

        str_type = type(self.unit).__name__
        str_name = self.unit.name.replace('#', '_')
        self._snippet_map = {
            'n'                     : os.linesep,
            'i'                     : self.dept * '  ',
            'object_lib'            : '{0}_{1}'.format(str_type.lower(), str_name),
            'comment_begin'         : ' BEGIN-{0}: {1} '.format(str_type.upper(), str_name),
            'comment_end'           : ' END-{0}: {1} '.format(str_type.upper(), str_name),
            'directory'             : os.path.relpath(self.unit.directory, start = dir_arctos['tunings']),
            'date'                  : datetime.datetime.now().strftime("%Y-%m-%d %H:%M"),
            'guard'                 : self.header.directory.replace(os.sep, '_').upper(),
            'params_header_path'    : self.header.get_path(),
            'parents'               : '',
            'links'                 : '',
            'params'                : '',
            'types'                 : '',
            'defines'               : '' }

        self._precompile_params_or_options(self.unit.configuration)
        self._precompile_params_or_options(self.unit.parameters)
        self._precompile_custom_types(self.unit.custom_types)

        if dept > 0:
            # we only need cmake 'params' at the leaves => discard all others
            self._snippet_map['params'] = ''

    def _precompile_custom_types(self, custom_types):
        for name, type_ in custom_types.items():
            type_map = {
                'n'         : os.linesep,
                'type_name' : name,
                'items'     : '' }

            for item in type_.items.values():
                if item.value is None:
                    continue

                tmp_map = {
                    'n'     : os.linesep,
                    'def'   : item.identifier,
                    'val'   : item.value }
                type_map['items'] += Unit._snippet_header_param.format_map(tmp_map)

            if type_map['items'] == '':
                continue
            self._snippet_map['types'] += Unit._snippet_header_type.format_map(type_map)

    def _precompile_params_or_options(self, param):
        for definition, option in param.items():
            value = option.get_value()
            if value is None:
                raise ProcessError('No value specified for option / param "{}"'.format(definition))

            # TODO description as comment?
            if option.is_string():
                value = str(value).strip()
                value = value.replace('\r\n', '\n')
                value = value.replace('\n', '\\n\\\n')
                value = '"{}"'.format(value)
            elif option.is_int():
                value = '0x{:08X}'.format(value)

            tmp_map = {
                'n'     : os.linesep,
                'i'     : self.dept * '  ',
                'def'   : definition }

            if option.apply_ in ['default', 'cmake']:
                tmp_map['val'] = str(value)
                if option.is_bool():
                    tmp_map['val'] = 'on' if value else 'off'
                self._snippet_map['params'] += Unit._snippet_cmake_param.format_map(tmp_map)

            if option.apply_ in ['default', 'cpp']:
                tmp_map['val'] = str(value)
                linker_snippet = Unit._snippet_header_param
                if option.is_bool():
                    if value:
                        linker_snippet = Unit._snippet_header_param_true
                    else:
                        linker_snippet = Unit._snippet_header_param_false
                self._snippet_map['defines'] += linker_snippet.format_map(tmp_map)

            if option.apply_ in ['linker']:
                tmp_map['val'] = str(value)
                if option.is_bool():
                    tmp_map['val'] = 'true' if value else 'false'
                self._snippet_map['params'] += Unit._snippet_cmake_param_linker.format_map(tmp_map)

        if len(param) > 0:
            self._snippet_map['defines'] += os.linesep
            self._snippet_map['params']  += os.linesep

    def compile(self):
        headers_list = []
        if self._snippet_map['object_lib'] in self.objlibs:
            self.cmake = Unit._snippet_cmake_empty.format_map(self._snippet_map)
            self.header = None
            return self.cmake, headers_list

        if len(self.dep_parents) == 0:
            self._snippet_map['links'] += Unit._snippet_cmake_link.format_map({
                'i'          : self.dept * '  ',
                'n'          : os.linesep,
                'object_lib' : self._snippet_map['object_lib'],
                'link'       : 'iface_arctos'})
        else:
            for dep in self.dep_parents.values():
                dep_cmake, dep_headers_list = dep.compile()
                self._snippet_map['links'] += Unit._snippet_cmake_link.format_map({
                    'i'          : self.dept * '  ',
                    'n'          : os.linesep,
                    'object_lib' : self._snippet_map['object_lib'],
                    'link'       : dep._snippet_map['object_lib']})
                self._snippet_map['parents'] += dep_cmake
                headers_list.extend(dep_headers_list)

        self.objlibs.append(self._snippet_map['object_lib'])
        self.cmake = self._snippet_cmake.format_map(self._snippet_map)
        self.header.content = Unit._snippet_header.format_map(self._snippet_map)
        headers_list.append(self.header)
        return self.cmake, headers_list

################################################################################

class Segment(Unit):
    _snippet_cmake = (
        '{i}{comment_begin:#^60}{n}'
        '{parents}'
        '{i}add_subdirectory("${{CMAKE_CURRENT_LIST_DIR}}/../{directory}"){n}'
        '{i}{comment_end:#^60}{n}{n}')

    def __init__(self, segment, dept = 1, header = None):
        assert (isinstance(segment, structure.Segment)),'Wrong type'
        super().__init__(segment, [], dept, header)

        for name, dep in segment.dep_parents.items():
            self.dep_parents[name] = Segment(dep, dept + 1, header)

class Target(Unit):
    _snippet_cmake = (
        '{i}{comment_begin:#^60}{n}'
        '{params}'
        '{parents}'
        '{i}add_subdirectory("${{CMAKE_CURRENT_LIST_DIR}}/../{directory}"){n}'
        '{i}{comment_end:#^60}{n}{n}')

    def __init__(self, target):
        assert (isinstance(target, structure.Target)),'Wrong type'
        super().__init__(target, [], 0, OutputFile('arctos', 'params.h'))

        for name, dep in target.dep_parents.items():
            self.dep_parents[name] = Segment(dep, header = self.header)

################################################################################

class Module(Unit):
    _snippet_cmake = (
        '{i}{comment_begin:#^60}{n}'
        '{params}'
        '{parents}'
        '{i}set(UNIT "{object_lib}"){n}'
        '{i}add_library({object_lib} OBJECT "${{CMAKE_CURRENT_LIST_DIR}}/{params_header_path}"){n}'
        '{i}add_subdirectory("${{CMAKE_CURRENT_LIST_DIR}}/../{directory}"){n}'
        '{links}'
        '{n}'
        '{i}add_library(iface_{object_lib} INTERFACE){n}'
        '{i}target_link_libraries(iface_{object_lib} INTERFACE {object_lib} $<TARGET_OBJECTS:{object_lib}>){n}'
        '{i}{comment_end:#^60}{n}{n}')

    def __init__(self, module, objlibs, dept = 0):
        assert (isinstance(module, structure.Module)),'Wrong type'
        name = module.name.lower()
        while '##' in name:
            name = name.replace('##', '#')
        name = os.path.join('arctos', *name.split('#'))
        super().__init__(module, objlibs, dept, OutputFile(name, 'params.h'))

        for name, dep in module.dep_parents.items():
            self.dep_parents[name] = Module(dep, objlibs, dept + 1)

################################################################################

class Tuning:
    _snippet_cmake = (
        '#{n}'
        '# This script is auto-generated{n}'
        '# DO NOT EDIT{n}'
        '#{n}'
        '# Generated on {date}{n}'
        '#{n}'
        'set({target_var:36} "{target}"){n}'
        '{n}'
        '{cmake_target}'
        '{cmake_modules}'
        'target_link_libraries(arctos PRIVATE{n}'
        '{cmake_objlibs}'
        '){n}')
    _snippet_cmake_objlib = (
        '{i}iface_{objlib}{n}' )

    def __init__(self, tuning):
        assert (isinstance(tuning, structure.Tuning)),'Wrong type'
        try:
            self.target  = tuning.bake_target()
            self.modules = tuning.bake_modules()
            self.files   = []

            objlibs = []
            cmake_modules = []
            for mod in self.modules:
                m = Module(mod, objlibs)
                cmake, headers = m.compile()

                cmake_modules.append(cmake)
                self.files.extend(headers)

            t = Target(self.target)
            t.compile()

            self.files.append(t.header)

            cmake_objlibs = []
            for obj in objlibs:
                cmake_objlibs.append(Tuning._snippet_cmake_objlib.format_map({
                    'i'      : '  ',
                    'n'      : os.linesep,
                    'objlib' : obj }))

            cmake = OutputFile('', 'tuning.cmake')
            cmake.content = Tuning._snippet_cmake.format_map({
                'n'             : os.linesep,
                'date'          : datetime.datetime.now().strftime('%Y-%m-%d %H:%M'),
                'target_var'    : 'ARCTOS_TARGET',
                'target'        : self.target.name,
                'cmake_modules' : ''.join(cmake_modules),
                'cmake_target'  : t.cmake,
                'cmake_objlibs' : ''.join(cmake_objlibs) })
            self.files.append(cmake)
        except (ProcessError, FormatError) as pe:
            raise ProcessError('Failed to compile tuning "{0}":\n{1}'.format(tuning.name, pe.args[0]))
