# -*- coding: utf-8 -*-
# This file is part of ARCTOS
__copyright__ = "Copyright (c) 2018-2021 Michael Zehrer"
__license__   = "MIT"
__author__    = "Michael Zehrer"

import sys
if sys.version_info[0] < 3 or (sys.version_info[0] >= 3 and sys.version_info[1] < 9):
    raise ImportError('This package requires Python version >= 3.9')

from .helper import FormatError, ProcessError

__all__ = ['structure', 'load', 'compile']
