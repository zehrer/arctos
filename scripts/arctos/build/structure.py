# -*- coding: utf-8 -*-
# This file is part of ARCTOS
__copyright__ = "Copyright (c) 2018-2024 Michael Zehrer"
__license__   = "MIT"
__author__    = "Michael Zehrer"

from .helper import *
from copy import *
from queue import Queue

class CustomTypeItem:
    def __init__(self, identifier, value = None, readable = None, description = None):
        if is_blank(identifier):
            raise FormatError('The identifier is mandatory!')
        elif not is_acceptable_definition(identifier):
            raise FormatError('The identifier must only contain alphanumeric chars (and "-", "_")!')
        self.identifier         = identifier
        self.value              = value
        self.readable           = readable
        self.description        = description
        self.set_instructions   = dict()

    def add_set_instruction(self, set_):
        assert (isinstance(set_, SetInstruction)),'Wrong type'
        if set_.definition not in self.set_instructions:
            self.set_instructions[set_.definition] = set_
        else:
            raise FormatError('({0}) Set instruction already exists in this item!'
                              .format(set_.definition))

class CustomType:
    def __init__(self, name, description = None):
        if is_blank(name):
            raise FormatError('The name is mandatory!')
        self.name           = name
        self.description    = description
        self.items          = dict()

    def add_item(self, item):
        assert (isinstance(item, CustomTypeItem)),'Wrong type'
        if item.identifier not in self.items:
            self.items[item.identifier] = item
        else:
            raise FormatError('({0}) Type item "{1}" already exists!'
                              .format(self.name, item.identifier))

################################################################################

class Param:
    def __init__(self, definition, type_,
                 value = None, minv = float('-inf'), maxv = float('inf'),
                 readable = None, description = None, apply_ = 'default'):
        if is_blank(definition):
            raise FormatError('The definition is mandatory!')
        elif not is_acceptable_definition(definition):
            raise FormatError('The definition must only contain alphanumeric chars (and "-", "_")!')
        elif type_ is None:
            raise FormatError('The type is mandatory!')
        elif type_ not in ['string', 'bool', 'int', 'float'] \
             and not isinstance(type_, CustomType):
            raise FormatError('Type must be either "string", "bool", "int", '
                              '"float" or an custom enumeration.')
        elif apply_ not in ['default', 'cmake', 'cpp', 'linker']:
            raise FormatError('The attribute "apply" must be either "default", '
                              '"cmake", "cpp" or "linker".')

        self.definition     = definition
        self.readable       = readable
        self.description    = description
        self.type_          = type_
        self.apply_         = apply_

        if self.type_ in ['int', 'float']:
            self._min_value = float('-inf')
            self._max_value = float('inf')
        self.change_value(value, minv, maxv)

    def change_value(self, value = None, minv = float('-inf'), maxv = float('inf')):
        num_val  = None

        if value is None:
            self._value = None
        else:
            if self.type_ == 'string':
                self._value = str(value)
            elif self.type_ == 'bool':
                self._value = self._convert_bool(value)
            elif self.type_ == 'int':
                num_val = self._convert_int(value, 'value')
            elif self.type_ == 'float':
                num_val = self._convert_float(value, 'value')
            elif value in self.type_.items:
                self._value = self.type_.items[value]
            else:
                raise FormatError('({0}) Given value {1} is no element of type "{2}"'
                                   .format(self.definition, value, self.type_.name))

        if self.type_ == 'int':
            num_minv = float('-inf')
            if minv != float('-inf'):
                num_minv = self._convert_int(minv, 'min_value')
            num_maxv = float('inf')
            if maxv != float('inf'):
                num_maxv = self._convert_int(maxv, 'max_value')
            self._change_numeric_value(num_val, num_minv, num_maxv)
        elif self.type_ == 'float':
            num_minv = self._convert_float(minv, 'min_value')
            num_maxv = self._convert_float(maxv, 'max_value')
            self._change_numeric_value(num_val, num_minv, num_maxv)

    def _change_numeric_value(self, new_value = None,
                              new_minv = float('-inf'), new_maxv = float('inf')):
        """
        Performs sanity checks and sets '_value', '_min_value' and '_max_value'.

        @param new_value: new value (None if unset)
        @param new_minv : new minimum value (-inf if unset)
        @param new_maxv : new maximum value (inf if unset)
        """
        changed_minv = self._min_value
        if new_minv != float('-inf'):
            if new_minv >= self._min_value:
                changed_minv = new_minv
            else:
                raise FormatError('({0}) Boundaries doesn\'t fit! New minimum '
                                  'value "{2}" is smaller than current one "{1}".'
                                  .format(self.definition, self._min_value, new_minv))

        changed_maxv = self._max_value
        if new_maxv != float('inf'):
            if new_maxv <= self._max_value:
                changed_maxv = new_maxv
            else:
                raise FormatError('({0}) Boundaries doesn\'t fit! New maximum '
                                  'value "{2}" is greater than current one "{1}".'
                                  .format(self.definition, self._max_value, new_maxv))

        if changed_maxv < changed_minv:
            raise FormatError('({0}) The maximum value "{2}" is smaller than '
                              'the minimum value "{1}".'
                              .format(self.definition, str(changed_minv), str(changed_maxv)))

        changed_value = self._value if new_value is None else new_value
        if changed_value is not None:
            if changed_value < changed_minv:
                raise FormatError('({0}) The specified value {1} is less than '
                                  'the minimum value {2}.'
                                  .format(self.definition, str(changed_value), str(changed_minv)))
            elif changed_value > changed_maxv:
                raise FormatError('({0}) The specified value {1} is greater than '
                                  'the maximum value {1}.'
                                  .format(self.definition, str(changed_value), str(changed_maxv)))

        self._min_value = changed_minv
        self._max_value = changed_maxv
        self._value     = changed_value

    def _convert_bool(self, val):
        """
        Converts the value stored in 'val' into an boolean.
        
        @param val  : value to be converted
        @return     : bool value
        """
        if (str(val)).lower() in ['true', 'on']:
            return True
        elif (str(val)).lower() in ['false', 'off']:
            return False
        else:
            raise FormatError('({0}) The specified value "{1}" is not a boolean!'
                               .format(self.definition, str(val)))
    def _convert_int(self, val, name):
        """
        Converts the value stored in 'val' into an int.
        
        @param val  : value to be converted
        @param name : value name (only used for error message)
        @return     : int value
        """
        try:
            if val.startswith('0x') or val.startswith('0X'):
                int_val = int(val, 16)
            else:
                int_val = int(val)
        except:
            raise FormatError('({0}) "{1}" must be an int, but was {2}.'
                              .format(self.definition, name, str(val)))
        return int_val
    def _convert_float(self, val, name):
        """
        Converts the value stored in 'val' into an float.
        
        @param val  : value to be converted
        @param name : element name (only used for error message)
        @return     : float value
        """
        try:
            float_val = float(val)
        except:
            raise FormatError('({0}) "{1}" must be an float, but was {2}.'
                              .format(self.definition, name, str(val)))
        return float_val

    def get_boundaries(self):
        """
        Determines the Boundaries.
        Only suitable for numeric types ('float' or 'int')

        @return: boundary tuple (min_value, max_value)
                 min_value could be '-inf' and max_value 'inf'
        """
        if self.type_ not in ['float', 'int']:
            return (None, None)
        else:
            return (self._min_value, self._max_value)

    def get_inferred_set_instructions(self):
        if isinstance(self._value, CustomTypeItem):
            return self._value.set_instructions
        else:
            return dict()

    def get_value(self):
        if isinstance(self._value, CustomTypeItem):
            return self._value.identifier
        else:
            return self._value

    def is_custom_type(self):
        return isinstance(self.type_, CustomType)
    def is_string(self):
        return self.type_ == 'string'
    def is_bool(self):
        return self.type_ == 'bool'
    def is_int(self):
        return self.type_ == 'int'
    def is_float(self):
        return self.type_ == 'float'

    def merge(self, other):
        """
        Merges the parameter together:
            1. 'self' and 'other' must have the same 'definition' and 'type_'
            2. Changes are only made on 'self', not on 'other'
            3. 'other' has the higher priority and will therefore overwrite
               settings on 'self', but only if specified (not None)
            4. For types 'int' and 'float' boundaries must fit together

        @type  other: Must be L{Param}'
        @param other: An other parameter
        """
        assert (isinstance(other, Param)),'Wrong type'
        if self.definition != other.definition:
            raise FormatError('({0}) Definition mismatch! Can\'t merge with "{1}".'
                              .format(self.definition, other.definition))
        elif self.type_ != other.type_:
            raise FormatError('({0}) Type mismatch! Can\'t merge "{2}" into "{1}".'
                              .format(self.definition, self.type_, other.type_))

        if self.type_ in ['float', 'int']:
            self._change_numeric_value(other._value, other._min_value, other._max_value)
        elif other._value is not None:
            self._value = other._value

################################################################################

class Option(Param):
    pass

################################################################################

class SetInstruction:
    def __init__(self, definition, value, note = None):
        if is_blank(definition):
            raise FormatError('The definition is mandatory!')
        if is_blank(value):
            raise FormatError('The value is mandatory!')
        self.definition = definition
        self.value      = value
        self.note       = note

################################################################################

class Unit:
    def __init__(self, name, directory):
        if is_blank(name):
            raise FormatError('The name is mandatory!')
        elif not is_acceptable_name(name):
            raise FormatError('The name must only contain alphanumeric chars (and "-", "_", "#")!')
        if is_blank(directory):
            raise FormatError('The directory is mandatory!')
        self.name           = name
        self.directory      = directory
        self.dep_parents    = dict()
        self.dep_childs     = dict()
        self.dep_modules    = dict()
        self.custom_types   = dict()
        self.configuration  = dict()
        self.parameters     = dict()

    def __copy__(self):
        result              = object.__new__(type(self))
        result.name         = self.name
        result.directory    = self.directory
        result.dep_modules  = self.dep_modules
        result.custom_types = self.custom_types

        result.dep_parents  = dict()
        result.dep_childs   = dict()
        for key, value in self.dep_parents.items():
            cp_value = copy(value)
            cp_value.dep_childs[result.name] = result
            result.dep_parents[key] = cp_value

        result.configuration = dict()
        for key, value in self.configuration.items():
            result.configuration[key] = copy(value)

        result.parameters = dict()
        for key, value in self.parameters.items():
            result.parameters[key] = copy(value)

        return result

    def add_module(self, module):
        assert (isinstance(module, Module)),'Wrong type'
        if module.name not in self.dep_modules:
            self.dep_modules[module.name] = module

    def add_option(self, option):
        assert (isinstance(option, Option)),'Wrong type'
        if option.definition not in self.configuration:
            self.configuration[option.definition] = option
        else:
            self.configuration[option.definition].merge(option)

    def add_param(self, param):
        assert (isinstance(param, Param)),'Wrong type'
        if param.definition not in self.parameters:
            self.parameters[param.definition] = param
        else:
            self.parameters[param.definition].merge(param)

    def add_custom_type(self, type_):
        assert (isinstance(type_, CustomType)),'Wrong type'
        if type_.name not in self.custom_types:
            self.custom_types[type_.name] = type_
        elif type_ is not self.custom_types[type_.name]:
            raise FormatError('({0}) Type name collision: "{1}"'
                              .format(self.name, type_.name))

    def add_parent(self, parent):
        if parent.name not in self.dep_parents:
            self.dep_parents[parent.name] = parent
        else:
            raise FormatError('({0}) Parent "{1}" already exists!'.format(self.name, parent.name))

        if self.name not in parent.dep_childs:
            parent.dep_childs[self.name] = self

        for key, value in parent.dep_modules.items():
            self.add_module(value)
        for key, value in parent.custom_types.items():
            self.add_custom_type(value)
        for key, value in parent.configuration.items():
            self.add_option(copy(value))
        for key, value in parent.parameters.items():
            self.add_param(copy(value))

    def bake(self, setters):
        assert (isinstance(setters, dict)),'Wrong type'

        inferred_setters = dict()
        for definition, instr in setters.items():
            if definition not in self.configuration:
                raise ProcessError('({0}) Can\'t execute set '
                                   'instruction. Option "{1}" not found!'
                                   .format(self.name, definition))
            opt = self.configuration[definition]
            opt.change_value(instr.value)
            if opt.is_custom_type():
                inferred_setters.update(opt.get_inferred_set_instructions())

        for definition, instr in inferred_setters.items():
            if definition not in self.parameters:
                raise ProcessError('({0}) Can\'t execute inferred set '
                                   'instruction: "{1}" > "{2}" not found!'
                                   .format(self.name, opt.get_value(), definition))
            self.parameters[definition].change_value(instr.value)

        for param in self.parameters.values():
            if not param.is_custom_type():
                continue
            self._propagate_sets(param)

        self._propagate_to_parents()

    def _propagate_sets(self, param):
        for definition, instr in param.get_inferred_set_instructions().items():
            if definition not in self.parameters:
                raise ProcessError('({0}) Can\'t execute inferred set '
                                   'instruction: "{1}" > "{2}" not found!'
                                   .format(self.name, param.get_value(), definition))
            self.parameters[definition].change_value(instr.value)
            self._propagate_sets(self.parameters[definition])

    def _propagate_to_parents(self):
        for parent in self.dep_parents.values():
            for definition in (self.configuration.keys() & parent.configuration.keys()):
                parent.configuration[definition].merge(self.configuration[definition])

            for definition in (self.parameters.keys() & parent.parameters.keys()):
                parent.parameters[definition].merge(self.parameters[definition])

            for param in parent.parameters.values():
                if not param.is_custom_type():
                    continue
                parent._propagate_sets(param)

            parent._propagate_to_parents()

################################################################################

class Segment(Unit):
    def __init__(self, name, directory):
        super().__init__(name, directory)

    def __copy__(self):
        return super().__copy__()

    def add_parent(self, parent):
        assert (isinstance(parent, Segment)),'Wrong type'
        super().add_parent(parent)

class Target(Segment):
    def __init__(self, name, directory):
        super().__init__(name, directory)

    def __copy__(self):
        return super().__copy__()

################################################################################

class Module(Unit):
    def __init__(self, name, directory, module_type = 'normal'):
        super().__init__(name, directory)
        if module_type.lower() not in ['normal', 'partial']:
            raise FormatError('({0}) Module type must either be "normal" or "partial"!'
                              .format(name))
        self.module_type = module_type.lower()
        self.dep_compatibilities = dict()

    def __copy__(self):
        result = super().__copy__()
        result.module_type = self.module_type
        result.dep_compatibilities = self.dep_compatibilities
        return result

    def add_parent(self, parent):
        assert (isinstance(parent, Module)),'Wrong type'
        super().add_parent(parent)

    def add_compatibility(self, c):
        assert (isinstance(c, Target) or isinstance(c, Segment)),'Wrong type'
        self.dep_compatibilities[c.name] = c

################################################################################

class Tuning:
    def __init__(self, name, target, description = None):
        assert (isinstance(target, Target)),'Wrong type'
        if is_blank(name):
            raise FormatError('The name is mandatory!')
        self.name           = name
        self.description    = description
        self._target        = (target, dict())
        self._modules       = dict()
        for module in target.dep_modules.values():
            self.add_module(module)

    def _is_compatible(self, module):
        if len(module.dep_compatibilities) == 0:
            return True
        elif self._target[0].name in module.dep_compatibilities:
            return True
        else:
            q = Queue()
            for parent in self._target[0].dep_parents.values():
                q.put(parent)

            while not q.empty():
                seg = q.get()
                if seg.name in module.dep_compatibilities:
                    return True

                for parent in seg.dep_parents.values():
                    q.put(parent)

        return False

    def add_module(self, module):
        assert (isinstance(module, Module)),'Wrong type'
        if module.name in self._modules:
            return

        if module.module_type == 'partial':
            found = 0
            for child in module.dep_childs.values():
                if self._is_compatible(child):
                    self.add_module(child)
                    found += 1
            if found == 0:
                raise ProcessError(
                    '({0}) Can\'t statisfy module dependency! '
                    'Module "{1}" has no implementation suitable for target "{2}"!'
                    .format(self.name, module.name, self._target[0].name))
        else:
            if not self._is_compatible(module):
                raise ProcessError(
                    '({0}) Can\'t statisfy module dependency! '
                    'Module "{1}" isn\'t suitable for target "{2}"!'
                    .format(self.name, module.name, self._target[0].name))
            self._modules[module.name] = (module, dict())
            for dep in module.dep_modules.values():
                self.add_module(dep)

    def add_set_instruction(self, for_, set_):
        assert (isinstance(set_, SetInstruction)),'Wrong type'

        dest = None
        if for_ == self._target[0].name:
            dest = self._target[1]
        elif for_ in self._modules:
            dest = self._modules[for_][1]
        else:
            raise FormatError('({0}) Can\'t assign settings. "{1}" wasn\'t found.'
                              .format(self.name, for_))

        if set_.definition not in dest:
            dest[set_.definition] = set_
        else:
            raise FormatError('({0}) Set instruction "{1}" defined multiple times!'
                              .format(self.name, set_.definition))

    def bake_target(self):
        cp = copy(self._target[0])
        cp.bake(self._target[1])
        return cp

    def bake_modules(self):
        mods = []
        for module_name, value in self._modules.items():
            cp = copy(value[0])
            cp.bake(value[1])
            mods.append(cp)
        return mods
