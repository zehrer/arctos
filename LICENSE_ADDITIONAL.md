# !!! IMPORTANT !!!

This repository contains third party code or code based on such.
If so, the file is marked accordingly (incl. original copyright, license and origin).

| Component | Copyright | License |
|:--------- |:--------- |:------- |
| core/arm/cmsis/ | Copyright (c) 2009-2021 ARM Limited. All rights reserved. | Apache License 2.0 |
| core/arm/cmsis/src/device/st | Copyright (c) 2017 STMicroelectronics. All rights reserved | BSD 3-Clause license |
| modules/stm32hal/ | Copyright (c) 2016-2021 STMicroelectronics. All rights reserved | BSD 3-Clause license |
