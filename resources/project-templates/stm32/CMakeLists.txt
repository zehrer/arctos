cmake_minimum_required(VERSION 3.21)

if("${CMAKE_SOURCE_DIR}" STREQUAL "${CMAKE_BINARY_DIR}")
   message(FATAL_ERROR "In-source builds are not allowed.")
elseif(NOT IS_DIRECTORY $ENV{ARCTOS_DIR})
   message(FATAL_ERROR
      "It looks like arctos has not been initialized yet.\
      Please call 'source <path-to-arctos-scripts>/setup-environment' to do so.")
endif()
set(CMAKE_TOOLCHAIN_FILE "$ENV{ARCTOS_DIR}/resources/arm-gcc-toolchain.cmake")

set(CMAKE_RUNTIME_OUTPUT_DIRECTORY "${CMAKE_CURRENT_LIST_DIR}/bin")

project(
   "Example Project"
   VERSION 0.1.0
   LANGUAGES C CXX
)

######################################################################
# You may want to change the tuning (see "tunings" directory)
######################################################################
set(ARCTOS_CURRENT_TUNING "stm32f411-disco")

add_subdirectory($ENV{ARCTOS_DIR} "${CMAKE_BINARY_DIR}/arctos")
add_subdirectory(${CMAKE_CURRENT_LIST_DIR}/src)
