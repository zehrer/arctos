#include <arctos/log/logging.hpp>
#include <arctos/system/run/task.hpp>

#include <arctos/io/pin/pin.hpp>

using namespace arctos;
using namespace arctos::io;

class HelloWorldTask : public system::run::Task<1024> {
public:
   HelloWorldTask() : Task<1024>("HelloWorld-Task") {}

   void run() {
      // print debug message (to serial)
      log::debug("Hello World\n");

      // set mode of pin PD12 to "output"
      pin::pd12.mode(pin::mode::kOutput);
      // turn on PD12
      pin::pd12.set();
   }
};

// instantiate the task => this is enough for it to be executed
static HelloWorldTask hw_task;
