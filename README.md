# Overview

ARCTOS (another real-time capable, tiny operating system) aims to be an easy to use operating system for embedded applications. The code is written completely from scratch, but got inspired by the real-time operating system [RODOS](https://gitlab.com/rodos/rodos "RODOS Embedded Operating System").

## `ARCTOS` says "Hello World"

For a quick insight, on how things are done in `arctos`, it's probably best to show an example:

```cpp
#include <arctos/log/logging.hpp>
#include <arctos/system/run/task.hpp>

#include <arctos/io/pin/pin.hpp>

using namespace arctos;
using namespace arctos::io;

class HelloWorldTask : public system::run::Task<1024> {
public:
   HelloWorldTask() : Task<1024>("HelloWorld-Task") {}

   void run() {
      // print debug message (to serial)
      log::debug("Hello World\n");

      // set mode of pin PD12 to "output"
      pin::pd12.mode(pin::mode::kOutput);
      // turn on PD12
      pin::pd12.set();
   }
};

// instantiate the task => this is enough for it to be executed
static HelloWorldTask hw_task;
```

Note: The example is meant for the STM32F411-Discovery board, where pin `PD12` is wired to an green LED.

## Status and API
The project is currently work in progress (pre v1.0)

**IMPORTANT**: The API is NOT yet considered stable. Changes can happen at any time without prior notice!



# How to start

## Prerequisites

1.  **ARM-Toolchain** for bare-metal targets (arm-none-eabi)
2.  **cmake** >= 3.21
3.  **python** >= 3.5 (optional)

## First application

Due to the megalithic kernel design, the whole code (kernel and application) will be compiled into one single binary. This results in an highly dependent executable, targeting one specific platform (e.g. BlackPill F401 v3). A direct consequence of this design is, that your application needs to import the `arctos` source code. Otherwise it wouldn't be able to perform this kind of optimization.

To make a long story short, it is recommended to start a new arctos application by simply **copying** one of the templates from the
```
arctos/resources/project-templates/
```
directory and start from there.

### You may however want to adjust some options in the topmost `CMakeLists.txt`:

a) `ARCTOS_CURRENT_TUNING` variable

This variable defines the projects target platform. Have a look inside the `tunings` directory for predefined tunings

b) Location of the `arctos` source code

By default it's determined by an environment variable called `ARCTOS_DIR` (that contains the path). Either define this variable (shell: `source scripts/setup-environment`), or simply replace `$ENV{ARCTOS_DIR}` with the path to your local copy

### At this point compiling should be straight forward:

```bash
$ cd your/application/directory
$ mkdir build && cd build
$ cmake ..
$ make
```
