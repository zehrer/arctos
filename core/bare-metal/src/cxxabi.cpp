/**
 * Copyright (c) 2018-2020 Michael Zehrer
 * This file is part of ARCTOS
 *
 * @licence MIT
 * @brief   GCC requirements according to the Itanium C++ ABI
 * @author  Andre Bartke <dev@bartke.cc>
 *          adapted in 2018 by Michael Zehrer
 *
 * For reference, see http://refspecs.linux-foundation.org/cxxabi-1.83.html
 *
 * According to the Itanium C++ ABI the C runtime library shall maintain a
 * list of termination function entries containing the following information:
 *    - A pointer to a termination function.
 *    - An operand to be passed to the function.
 *    - A handle identifying the home shared library of the entry.
 *
 * The list is populated by entries of two kinds:
 *    - Destructors of global (or local static) C++ objects that require
 *      destruction on exit.
 *    - Functions registered by the user with atexit().
 */
#include <stddef.h>
#include <stdint.h>

#include <arctos/log/logging.hpp>
#include <arctos/porting/all.hpp>

namespace arctos {
namespace porting {
int32_t atomicCompareAndExchange(int32_t *v, int32_t o, int32_t n);
int64_t atomicCompareAndExchange(int64_t *v, int64_t o, int64_t n);
} /* namespace porting */
} /* namespace arctos */

#ifdef __cplusplus
namespace __cxxabiv1 {
extern "C" {
#endif

union __guard {
    int64_t counter;
    struct {
        // only the meaning of the first byte is specified in the ABI
        // 0 = uninitialized; 1 = initialized
        uint8_t state;
        uint8_t res[3];
        uint32_t other;
    } detail;
};


int __aeabi_atexit(void *object, void (*destructor)(void *), void *dso_handle) { 
    static_cast<void>(object); 
    static_cast<void>(destructor); 
    static_cast<void>(dso_handle); 
    return 0; 
}

/**
 * @brief Dynamic shared object handle.
 */
void *__dso_handle = nullptr;

/**
 * @brief If a function is called with an unfilled virtual function pointer.
 *
 * This should never be called since it is not possible to instantiate a
 * class that does not define all virtual functions. If the vtable
 * contains a NULL pointer we call it a 'pure virtual function'.
 */
void __cxa_pure_virtual() {
    ::arctos::log::critical("\n\nKERNEL PANIC: pure virtual called");
}

/**
 * @brief Register a destructor or cleanup function to be called on exit.
 *
 * @param func a pointer to a termination function.
 * @param arg an operand to be passed to the function.
 * @param dso_handle a handle identifying the library of the entry.
 *
 * @retval 0 on success
 * @retval -1 on failure
 */
int __cxa_atexit(void (*func) (void *), void *arg, void *dso_handle) noexcept {
    // unnecessary
    return 0;
}

/**
 * @brief Call destructors and exit functions registered with atexit.
 * According to the Itanium C++ ABI the termination function list is to be
 * executed in reverse order.
 *
 * @param func The destructor or exit function to be called.
 */
void __cxa_finalize(void *func) {
    // unnecessary
}



/**
 * Returns 1 if the initialization is not yet complete; 0 otherwise.
 * This function is called before initialization takes place.
 * If this function returns 1, either __cxa_guard_release or
 * __cxa_guard_abort must be called with the same argument.
 * The first byte of the guard_object is not modified by this function.
 *
 * A thread-safe implementation will probably guard access to the first
 * byte of the guard_object with a mutex. If this function returns 1,
 * the mutex will have been acquired by the calling thread.
 *
 * @return  0 if initialization is completed, 1 otherwise
 */
int __cxa_guard_acquire (__guard *g) {
    // check that the initializer has not already been run
    if (g->detail.state == 1)
        return 0;

    int32_t *guard_state = reinterpret_cast<int32_t *>(&g->detail.state);
    // if possible, we are ready to initialize, set state to 2 (PENDING) and return 1
    if (::arctos::porting::atomicCompareAndExchange(guard_state, 0, 2) == 0)
        return 1;
    else {
        // if not possible, wait for completion of the other thread
        while (g->detail.state != 1);;
        return 0;
    }
}

/**
 * Sets the first byte of the guard object to a non-zero value.
 * This function is called after initialization is complete.
 *
 * A thread-safe implementation will release the mutex acquired by
 * __cxa_guard_acquire after setting the first byte of the guard object.
 */
void __cxa_guard_release (__guard *g) noexcept {
    g->detail.state = 1;
}

/**
 * This function is called if the initialization terminates by
 * throwing an exception.
 *
 * A thread-safe implementation will release the mutex acquired by
 * __cxa_guard_acquire.
 */
void __cxa_guard_abort (__guard *g) noexcept {
    g->detail.state = 0;
}

/*
From http://refspecs.linux-foundation.org/cxxabi-1.83.html#once-ctor

The following is pseudo-code showing how these functions can be used:

    if (obj_guard.first_byte == 0) {
        if ( __cxa_guard_acquire (&obj_guard) ) {
            try {
                ... initialize the object ...;
            } catch (...) {
                __cxa_guard_abort (&obj_guard);
                throw;
            }
            ... queue object destructor with __cxa_atexit() ...;
            __cxa_guard_release (&obj_guard);
        }
    }

An implementation need not include the simple inline test of the
initialization flag in the guard variable around the above sequence.
If it does so, the cost of this scheme, when run single-threaded
with minimal versions of the above functions, will be two extra
function calls, each of them accessing the guard variable,
the first time the scope is entered.

An implementation supporting thread-safety on multiprocessor systems
must also guarantee that references to the initialized object do not
occur before the load of the initialization flag. On Itanium, this
can be done by using a ld1.acq operation to load the flag.

The intent of specifying an 8-byte structure for the guard variable,
but only describing one byte of its contents, is to allow flexibility
in the implementation of the API above. On systems with good small
lock support, the second word might be used for a mutex lock.
On others, it might identify (as a pointer or index) a more complex
lock structure to use. 
*/

#ifdef __cplusplus
} /* extern "C" */
} /* namespace __cxxabiv1 */
#endif
