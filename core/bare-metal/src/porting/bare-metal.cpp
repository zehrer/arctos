/**
 * @file
 * @copyright Copyright (c) 2018-2021 Michael Zehrer. This file is part of ARCTOS
 * @license MIT
 * @brief porting interface functions (bare-metal implementations)
 */
#include <arctos/porting/all.hpp>

namespace arctos {
namespace porting {

void init(void) {
    clock::init();
    hardware::init();
}

} /* namespace porting */
} /* namespace arctos */
