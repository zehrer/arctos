/**
 * Copyright (c) 2018 Michael Zehrer
 * This file is part of ARCTOS
 *
 * @licence MIT
 * @brief   Simplified and reduced reimplementation of standrad c-ctype functions
 *          (optimized for size, not for performance)
 * @author  Michael Zehrer
 */
#include <stddef.h>

/**
 * @brief   This function checks whether the passed character is alphanumeric
 *          [a letter (a-z or A-Z) or a digit (0-9)]
 */
__attribute__((weak))
int isalnum(int c) {
    if ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z') || (c >= '0' && c <= '9'))
        return 1;
    else
        return 0;
}

/**
 * @brief   This function checks whether the passed character is alphabetic
 */
__attribute__((weak))
int isalpha(int c) {
    if ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z'))
        return 1;
    else
        return 0;
}

/**
 * @brief   This function checks whether the passed character is decimal digit
 */
__attribute__((weak))
int isdigit(int c) {
    if (c >= '0' && c <= '9')
        return 1;
    else
        return 0;
}

/**
 * @brief   This function checks whether the passed character is lowercase letter
 */
__attribute__((weak))
int islower(int c) {
    if (c >= 'a' && c <= 'z')
        return 1;
    else
        return 0;
}

/**
 * @brief   This function checks whether the passed character is uppercase letter
 */
__attribute__((weak))
int isupper(int c) {
    if (c >= 'A' && c <= 'Z')
        return 1;
    else
        return 0;
}

/**
 * @brief   This function checks whether the passed character is white-space
 */
__attribute__((weak))
int isspace(int c) {
    //  htab, line feed, vtab, formfeed, carriage return, space)
    if ((c >= 0x09 && c <= 0x0D) || c == 0x20)
        return 1;
    else
        return 0;
}
