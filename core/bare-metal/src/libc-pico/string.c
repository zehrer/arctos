/**
 * Copyright (c) 2018 Michael Zehrer
 * This file is part of ARCTOS
 *
 * @licence MIT
 * @brief   Simplified and reduced reimplementation of standrad c-string functions
 *          (optimized for size, not for performance)
 * @author  Michael Zehrer
 */
#include <stddef.h>
#include <stdbool.h>

/**
 * @brief   Searches for the first occurrence of the character 'c'
 *          (an unsigned char) in the first n bytes of the string
 *          pointed to, by the argument 'str'
 */
__attribute__((weak))
void *memchr(const void *str, int c, size_t n) {
    const unsigned char *src = (const unsigned char *)str;
    for (const unsigned char d = c; n--; ++src) {
        if (*src == d) return (void *)src;
    }
    return NULL;
}

/**
 * @brief   Compares the first n bytes of 'str1' and 'str2'
 */
__attribute__((weak))
int memcmp(const void *str1, const void *str2, size_t n) {
    unsigned char *s1 = (unsigned char *)str1;
    unsigned char *s2 = (unsigned char *)str2;

    for (; n--; ++s1, ++s2) {
        if (*s1 != *s2) return *s1 - *s2;
    }
    return 0;
}

/**
 * @brief   Copies n characters from 'src' to 'dest'
 */
__attribute__((weak))
void *memcpy(void *dest, const void *src, size_t n) {
    char *d = (char *)dest;
    const char *s = (const char *)src;
    
    while (n--)
        *d++ = *s++;
    return dest;
}

/**
 * @brief   Another function to copy n characters from 'src' to 'dest'
 */
__attribute__((weak))
void *memmove(void *dest, const void *src, size_t n) {
    char *d = (char *)dest;
    const char *s = (const char *)src;

    if (s < d && d < (s + n)) {
        /* copy backwards */
        for (s+=n, d+=n; n--; )
            *--d = *--s;
    }
    else {
        while (n--)
            *d++ = *s++;
    }

    return dest;
}

/**
 * @brief   Copies the character 'c' (an unsigned char)
 *          to the first 'n' characters of the string pointed to,
 *          by the argument 'str'
 */
__attribute__((weak))
void *memset(void *str, int c, size_t n) {
    char *s = (char *)str;
    while (n--)
        *s++ = (char)c;
    return str;
}

/**
 * @brief   Appends the string pointed to, by 'src' to
 *          the end of the string pointed to by 'dest'
 */
__attribute__((weak))
char *strcat(char *dest, const char *src) {
    char *d = dest;
    while (*dest)
        ++dest;

    while (*src)
        *dest++ = *src++;
    return d;
}

/**
 * @brief   Appends the string pointed to, by 'src' to
 *          the end of the string pointed to, by 'dest'
 *          up to 'n' characters long
 */
__attribute__((weak))
char *strncat(char *dest, const char *src, size_t n) {
    char *d = dest;
    while (*dest)
        ++dest;
    
    while ((*src != '\0') && (n-- != 0))
        *dest++ = *src++;
    *dest = '\0';
    return d;
}

/**
 * @brief   Searches for the first occurrence of the character 'c' 
 *          (an unsigned char) in the string pointed to, by the argument 'str'
 */
__attribute__((weak))
char *strchr(const char *str, int c) {
    const unsigned char *src = (const unsigned char *)str;
    for (const unsigned char d = c; *str; ++src) {
        if (*src == d) return (void *)src;
    }
    return NULL;
}

/**
 * @brief   Searches for the last occurrence of the character 'c' 
 *          (an unsigned char) in the string pointed to, by the argument 'str'
 */
__attribute__((weak))
char *strrchr(const char *str, int c) {
    const unsigned char *src = (const unsigned char *)str;
    void * last = NULL;
    for (const unsigned char d = c; *str; ++src) {
        if (*src == d)
            last = (void *)src;
    }
    return last;
}

/**
 * @brief   Compares the string pointed to, by 'str1'
 *          to the string pointed to by 'str2'
 */
__attribute__((weak))
int strcmp(const char *str1, const char *str2) {
    while ((*str1 != '\0') && (*str1 == *str2)) {
        ++str1;
        ++str2;
    }
    return (*(unsigned char *)str1) - (*(unsigned char *)str2);
}

/**
 * @brief   Compares at most the first 'n' bytes of 'str1' and 'str2'
 */
__attribute__((weak))
int strncmp(const char *str1, const char *str2, size_t n) {
    if (n == 0)
        return 0;
    
    while ((n-- != 0) && (*str1 == *str2)) {
        if (n == 0 || *str1 == '\0')
            break;
        ++str1;
        ++str2;
    }
    return (*(unsigned char *)str1) - (*(unsigned char *)str2);
}

/**
 * @brief   Copies the string pointed to, by 'src' to 'dest'
 */
__attribute__((weak))
char *strcpy(char *dest, const char *src) {
    char *d = dest;
    while (*src)
        *dest++ = *src++;
    return d;
}

/**
 * @brief   Copies up to 'n' characters from the string pointed to,
 *          by 'src' to 'dest'
 */
__attribute__((weak))
char *strncpy(char *dest, const char *src, size_t n) {
    char *d = dest;    
    while ((*src != '\0') && (n-- != 0))
        *dest++ = *src++;
    *dest = '\0';
    return d;
}

/**
 * @brief   Computes the length of the string 'str' up to but not including
 *          the terminating null character.
 */
__attribute__((weak))
size_t strlen(const char *str) {
    const char *start = str;
    while (*str != '\0')
        ++str;
    return str - start;
}

/**
 * [strnlen is a GNU-Extension]
 * @brief   Returns the length of the string 'str' up to but not including
 *          the terminating null character with a maximum of 'n'
 */
__attribute__((weak))
size_t strnlen(const char *str, size_t n) {
    const char *start = str;
    while ((*str != '\0') && (n-- != 0))
        ++str;
    return str - start;
}

/**
 * @brief   Finds the first occurrence of the entire string 'needle'
 *          (not including the terminating null character)
 *          which appears in the string 'haystack'
 */
__attribute__((weak))
char *strstr(const char *haystack, const char *needle) {
    if (*haystack == '\0') {
        if (*needle == '\0')
            return (char *)haystack;
        else
            return (char *)NULL;
    }
    
    while (*haystack != '\0') {
        for (size_t i = 0; true; ++i) {
            if (needle[i] == '\0')
                return (char *)haystack;

            if (needle[i] != haystack[i])
                break;
        }
        haystack++;
    }
    return (char *)NULL;
}
