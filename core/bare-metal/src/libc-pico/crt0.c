/**
 * @file
 * @copyright Copyright (c) 2021 Michael Zehrer. This file is part of ARCTOS
 * @license MIT
 * @brief run-time initialization
 */
#include <stdint.h>

// defined by the linkerscript
extern uint32_t __bss_start__;
extern uint32_t __bss_end__;

extern void arctos_system_startup(void);

__attribute__((weak, noreturn))
void _start(void) {
    // clear the .bss section
    for (uint32_t *mem = &__bss_start__; mem < &__bss_end__; ++mem) {
        (*mem) = 0u;
    }

    arctos_system_startup();
    while (1) {}
}
