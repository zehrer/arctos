/**
 * @file
 * @copyright Copyright (c) 2018-2024 Michael Zehrer. This file is part of ARCTOS
 * @license MIT
 * @brief simplified and reduced reimplementation of standard c-stdlib functions
 */
#include <stddef.h>
#include <stdbool.h>

/**
 * @brief   Allocates the requested memory and returns a pointer to it
 */
__attribute__((weak))
void *calloc(size_t nitems, size_t size) {
   // TODO malloc and set the memory to zero
   return NULL;
}

/**
 * @brief   Allocates the requested memory and returns a pointer to it
 */
__attribute__((weak))
void *malloc(size_t size) {
   // TODO
   return NULL;
}

/**
 * @brief   Attempts to resize the memory block pointed to by 'ptr'
 *          that was previously allocated with a call to malloc or calloc
 */
__attribute__((weak))
void *realloc(void *ptr, size_t size) {
   // TODO
   return NULL;
}

/**
 * @brief   Deallocates the memory previously allocated by a call to
 *          calloc, malloc, or realloc
 */
__attribute__((weak))
void free(void *ptr) {
   // TODO
}

__attribute__((weak, noreturn))
void abort() {
   while (true)
      continue;
}
