/**
 * @file
 * @copyright Copyright (c) 2018-2024 Michael Zehrer. This file is part of ARCTOS
 * @license MIT
 * @brief initialization of global objects (call their constructors)
 */
#include <stddef.h>
#include <stdint.h>

// The following symbols are defined by the linker script:
extern void (*__preinit_array_start []) (void);
extern void (*__preinit_array_end []) (void);
extern void (*__init_array_start []) (void);
extern void (*__init_array_end []) (void);

[[gnu::weak]]
void __libc_init_array(void) {
   size_t count, i;

   count = __preinit_array_end - __preinit_array_start;
   for (i = 0; i < count; ++i)
      __preinit_array_start[i]();

   count = __init_array_end - __init_array_start;
   for (i = 0; i < count; ++i)
      __init_array_start[i]();
}
