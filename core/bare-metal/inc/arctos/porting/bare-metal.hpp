/**
 * @file
 * @copyright Copyright (c) 2020-2021 Michael Zehrer. This file is part of ARCTOS
 * @license MIT
 * @brief porting interface functions
 */
#ifndef ARCTOS_PORTING_BARE_METAL_HPP
#define ARCTOS_PORTING_BARE_METAL_HPP

#include <stdint.h>

namespace arctos {
namespace porting {

/**
 * @brief Atomic compare and exchange.
 *
 * @param[in/out] v volatile memory
 * @param[in] o old value
 * @param[in] n new value
 *
 * @return old value; test if successful by comparing old with return value
 */
int32_t atomicCompareAndExchange(int32_t *v, int32_t o, int32_t n);
/**
 * @brief Atomic compare and exchange.
 *
 * @param[in/out] v volatile memory
 * @param[in] o old value
 * @param[in] n new value
 *
 * @return old value; test if successful by comparing old with return value
 */
int64_t atomicCompareAndExchange(int64_t *v, int64_t o, int64_t n);

} /* namespace porting */
} /* namespace arctos */
#endif /* ARCTOS_PORTING_BARE_METAL_HPP */
