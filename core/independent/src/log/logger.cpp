/**
 * Copyright (c) 2019-2020 Michael Zehrer
 * This file is part of ARCTOS
 *
 * @licence MIT
 * @brief   Simple logging implementation
 * @author  Michael Zehrer
 */
#include <arctos/log/logging.hpp>
#include <arctos/log/logger.hpp>

namespace arctos {
namespace log {

#ifdef ARCTOS_LOG_DEBUG_AVAILABLE
size_t debug(const char *fmt, ...) {
    va_list vl;
    va_start(vl, fmt);
    size_t counter = Logger::instance()->notify(level::kDebug, fmt, vl);
    va_end(vl);
    return counter;
}
#endif
#ifdef ARCTOS_LOG_INFO_AVAILABLE
size_t info(const char *fmt, ...) {
    va_list vl;
    va_start(vl, fmt);
    size_t counter = Logger::instance()->notify(level::kInfo, fmt, vl);
    va_end(vl);
    return counter;
}
#endif
#ifdef ARCTOS_LOG_WARNING_AVAILABLE
size_t warning(const char *fmt, ...) {
    va_list vl;
    va_start(vl, fmt);
    size_t counter = Logger::instance()->notify(level::kWarning, fmt, vl);
    va_end(vl);
    return counter;
}
#endif
#ifdef ARCTOS_LOG_SYSTEM_AVAILABLE
size_t system(const char *fmt, ...) {
    va_list vl;
    va_start(vl, fmt);
    size_t counter = Logger::instance()->notify(level::kSystem, fmt, vl);
    va_end(vl);
    return counter;
}
#endif
#ifdef ARCTOS_LOG_ERROR_AVAILABLE
size_t error(const char *fmt, ...) {
    va_list vl;
    va_start(vl, fmt);
    size_t counter = Logger::instance()->notify(level::kError, fmt, vl);
    va_end(vl);
    return counter;
}
#endif
#ifdef ARCTOS_LOG_CRITICAL_AVAILABLE
size_t critical(const char *fmt, ...) {
    va_list vl;
    va_start(vl, fmt);
    Logger::instance()->notify(level::kCritical, fmt, vl);
    va_end(vl);
    while (true) { }
}
#endif

size_t Logger::notify(level::Level lvl, const char *fmt, va_list vl) {
    if (this->destination_ == nullptr)
        return 0;

    size_t counter = 0;
#ifdef ARCTOS_LOG_ENABLE_COLOR
    switch (lvl) {
    case level::kWarning:
        counter += this->destination_->print("\e[33m", 6);
        break;
    case level::kError:
        counter += this->destination_->print("\e[31m", 6);
        break;
    case level::kCritical:
        counter += this->destination_->print("\e[5m\e[31m", 11);
        break;
    default:
        break;
    }
#endif

    counter += this->destination_->vprintf(fmt, vl);

#ifdef ARCTOS_LOG_ENABLE_COLOR
    counter += this->destination_->print("\e[0m", 5);
#endif

    this->destination_->flush();

    return counter;
}


} /* namespace log */
} /* namespace arctos */
