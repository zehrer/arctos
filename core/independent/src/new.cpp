/**
 * @file
 * @copyright Copyright (c) 2018-2024 Michael Zehrer. This file is part of ARCTOS
 * @license MIT
 * @brief new and delete operators that don't throw std::bad_alloc exceptions
 */
#include <new>
#include <cstdint>

void* operator new(std::size_t size) noexcept {
   return operator new (size, std::nothrow);
}
void operator delete(void *p) noexcept {
   return operator delete(p, std::nothrow);
}

void* operator new[](std::size_t size) noexcept {
   return operator new(size, std::nothrow);
}
void operator delete[](void *p) noexcept {
   operator delete(p, std::nothrow);
}

void operator delete(void*, std::size_t) noexcept { }
void operator delete[](void*, std::size_t) noexcept { }
