/**
 * @file
 * @copyright Copyright (c) 2018-2021 Michael Zehrer. This file is part of ARCTOS
 * @license MIT
 * @brief Some of the functions from the Porting-Interface can be implemented
 *        independently from the hardware. They come in here.
 */
#include <arctos/porting/all.hpp>

namespace arctos {
namespace porting {

__attribute__((weak)) void init(void) {
    clock::init();
    hardware::init();
}

} /* namespace porting */
} /* namespace arctos */
