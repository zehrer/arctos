/**
 * @file
 * @copyright Copyright (c) 2018-2021 Michael Zehrer. This file is part of ARCTOS
 * @license MIT
 * @brief system clock related porting interface functions
 */
#include <arctos/porting/clock/clock.hpp>

namespace arctos {
namespace porting {
namespace clock {

__attribute__((weak)) void init(void) { }
__attribute__((weak)) int64_t rough(void) { return now(); }

} /* namespace clock */
} /* namespace porting */
} /* namespace arctos */
