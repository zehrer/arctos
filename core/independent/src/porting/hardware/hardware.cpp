/**
 * @file
 * @copyright Copyright (c) 2018-2021 Michael Zehrer. This file is part of ARCTOS
 * @license MIT
 * @brief hardware related porting interface functions
 */
#include <arctos/porting/hardware/hardware.hpp>

namespace arctos {
namespace porting {
namespace hardware {

__attribute__((weak)) void init(void) { }

} /* namespace hardware */
} /* namespace porting */
} /* namespace arctos */
