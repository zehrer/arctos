/**
 * @file
 * @copyright Copyright (c) 2018-2021 Michael Zehrer. This file is part of ARCTOS
 * @license MIT
 * @brief (system) time API
 */
#include <stdint.h>

#include <arctos/porting/all.hpp>
#include <arctos/time.hpp>

namespace arctos {

TimeModel sys_time;

int64_t TimeModel::now() {
    return porting::clock::now();
}

int64_t TimeModel::rough() {
    return porting::clock::rough();
}

} /* namespace arctos */
