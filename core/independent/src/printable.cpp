/**
 * Copyright (c) 2019-2020 Michael Zehrer
 * This file is part of ARCTOS
 *
 * @licence MIT
 * @brief   Printable base class
 * @author  Michael Zehrer
 */
#include <arctos/printable.hpp>

namespace arctos {

typedef union {
    uint16_t value;
    struct {
        uint16_t flags_hash:1;

        uint16_t precision_set:1;

        uint16_t length_l:1;
        uint16_t length_ll:1;

        uint16_t specifier_uppercase:1;

        uint16_t base:5;

        uint16_t is_signed:1;
        uint16_t is_float:1;
    } attr;
} Format;

size_t Printable::printf(const char *fmt, ...) {
    va_list vl;
    va_start(vl, fmt);
    size_t counter = this->vprintf(fmt, vl);
    va_end(vl);
    return counter;
}

size_t Printable::vprintf(const char *fmt, va_list vl) {
    size_t counter = 0;

    const char *start = fmt;
    char c = *fmt++;

    while (c != '\0') {
        if (c != '%') {
            c = *fmt++;
            continue;
        }
        counter += this->print(start, fmt - start - 1);
        c = *fmt++;

        // if the percent sign was the last character in the string
        // (which is btw. no proper format string) processing will be canceled
        if (c == '\0')
            break;

        Format   format    = { .value = 0 };
        int16_t  width     = 0;
        uint16_t log2base  = 0;
        uint16_t precision = 0;
        char     fill      = ' ';

        // FLAGS
        if (c == '0') {
            fill = c;
            c = *fmt++;
        }
        else if (c == '#') {
            format.attr.flags_hash = 1;
            c = *fmt++;
        }

        // WIDTH
        for (size_t j = 1; c >= '0' && c <= '9'; j *= 10, c = *fmt++) {
            width *= j;
            width += (c - '0');
        }

        // PRECISION
        if (c == '.') {
            format.attr.precision_set = 1;
            c = *fmt++;
            for (size_t j = 1; c >= '0' && c <= '9'; j *= 10, c = *fmt++) {
                precision *= j;
                precision += (c - '0');
            }
            // another incorrect format string
            if (c == '\0')
                return counter;
            c = *fmt++;
        }

        // LENGTH
        if (c == 'l') {
            format.attr.length_l = 1;
            c = *fmt++;
        }
        if (c == 'l') {
            format.attr.length_ll = 1;
            c = *fmt++;
        }

        switch(c) {
        case '\0':
            // another incorrect format string
            return counter;
        case 'c':
            c = va_arg(vl, int);
            [[fallthrough]];
        default:
            counter += this->print(c);
            start = fmt;
            c = *fmt++;
            continue;

        case 's':
            if (format.attr.precision_set == 0)
                counter += this->print(va_arg(vl, const char *));
            else
                counter += this->print(va_arg(vl, const char *), static_cast<size_t>(precision));
            start = fmt;
            c = *fmt++;
            continue;

        // Not in the standard, but it might be useful
        case 'b':
            format.attr.base = 2;
            log2base = 1;
            break;

        case 'o':
            format.attr.base = 8;
            log2base = 3;
            if (format.attr.flags_hash)
                counter += this->print('0');
            break;

        case 'F':
            format.attr.specifier_uppercase = 1;
            [[fallthrough]];
        case 'f':
            format.attr.is_float = 1;
            if (format.attr.precision_set == 0)
                precision = 6;
            [[fallthrough]];
        case 'd':
            [[fallthrough]];
        case 'i':
            format.attr.is_signed = 1;
            [[fallthrough]];
        case 'u':
            format.attr.base = 10;
            break;

        case 'X':
            format.attr.specifier_uppercase = 1;
            [[fallthrough]];
        case 'x':
            if (format.attr.flags_hash) {
                counter += this->print('0');
                counter += this->print(c);
            }
            format.attr.base = 16;
            log2base = 4;
            break;
        }

        {
            unsigned long long  val_ull;
            long long           val_ll = 0;
            double              val_f  = 0.0;

            if (format.attr.is_float) {
                val_f = va_arg(vl, double);
                val_ll = static_cast<long long>(val_f);
                // if the precision was explicitly set to zero...
                if (precision == 0) {
                    // ...we pretend that the value 'val_ll' was
                    // originally a normal signed integer
                    format.attr.is_float = 0;
                }
                else
                    width = width - precision - 1;
            }
            else if (format.attr.length_ll)
                val_ll = va_arg(vl, long long);
            else if (format.attr.length_l)
                val_ll = va_arg(vl, long);
            else
                val_ll = va_arg(vl, int);

            if (format.attr.is_signed) {
                if (val_ll < 0 || val_f < 0) {
                    val_ll = -val_ll;
                    val_f  = -val_f;
                    counter += this->print('-');
                    --width;
                }
            }

            if (format.attr.length_ll)
                val_ull = static_cast<unsigned long long>(val_ll);
            else if (format.attr.length_l)
                val_ull = static_cast<unsigned long long>(static_cast<unsigned long>(val_ll));
            else
                val_ull = static_cast<unsigned long long>(static_cast<unsigned int>(val_ll));

            if (format.attr.base == 10) {
                constexpr size_t size = __builtin_ceil(__builtin_log10(2)*sizeof(unsigned long long)*8);

                char buffer[size];
                char *const end = buffer + size - 1;
                char *ptr = end;

                do {
                    *ptr-- = static_cast<char>(val_ull % 10) + '0';
                    val_ull /= 10;
                    --width;
                } while (val_ull);

                while (width-- > 0)
                    counter += this->print(fill);

                counter += this->print(ptr + 1, end - ptr);
            }
            else {
                // The base is a power of 2 and therefore some potential faster
                // operations for 'mod' and 'div' can be used.
                const size_t size = (sizeof(unsigned long long) * 8) / log2base;
                char buffer[size];
                char *const end = buffer + size - 1;
                char *ptr = end;

                uint8_t mod_mask = format.attr.base - 1;
                do {
                    c = static_cast<char>((val_ull & mod_mask) + '0');
                    if (c > '9')
                        c += (format.attr.specifier_uppercase == 1 ? 'A' : 'a') - '9' - 1;
                    *ptr-- = c;
                    val_ull >>= log2base;
                    --width;
                } while (val_ull);

                while (width-- > 0)
                    counter += this->print(fill);

                counter += this->print(ptr + 1, end - ptr);
            }

            if (format.attr.is_float) {
                // Print the digits after the decimal point
                counter += this->print('.');
                while (precision--) {
                    val_f = (val_f - val_ll) * 10;
                    val_ll = static_cast<long long>(val_f);
                    counter += this->print(val_ll + '0');
                }
            }
        }

        start = fmt;
        c = *fmt++;
    }

    // On loop exit 'fmt' is one char "behind" the terminating null
    // character. We have to correct that...
    --fmt;
    if (fmt > start)
        //...and print out the tail if necessary
        counter += this->print(start, fmt - start);

    return counter;
}

} /* namespace arctos */
