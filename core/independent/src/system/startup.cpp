/**
 * @file
 * @copyright Copyright (c) 2018-2024 Michael Zehrer. This file is part of ARCTOS
 * @license MIT
 * @brief system startup
 */
#include <arctos/porting/all.hpp>

#include <arctos/system/startup.hpp>
#include <arctos/system/run/scheduler.hpp>
#include <arctos/log/logging.hpp>

extern "C" void __libc_init_array(void);
extern "C" [[noreturn]] void abort(void);

namespace arctos {
namespace system {

void startup() {
   __libc_init_array();

   ::arctos::porting::init();
   ::arctos::log::system("arctos (version: " ARCTOS_VERSION ")\n\n");

   ::arctos::system::run::core00.start();

   abort();
}

} /* namespace system */
} /* namespace arctos */
