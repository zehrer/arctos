add_subdirectory("${CMAKE_CURRENT_LIST_DIR}/src")

# internal includes
target_include_directories(arctos PRIVATE
   "${CMAKE_CURRENT_LIST_DIR}/inc/"
)

target_sources(arctos PRIVATE
   "${CMAKE_CURRENT_LIST_DIR}/inc/arctos/log/logger.hpp"
   "${CMAKE_CURRENT_LIST_DIR}/inc/arctos/porting/all.hpp"
   "${CMAKE_CURRENT_LIST_DIR}/inc/arctos/porting/independent.hpp"
   "${CMAKE_CURRENT_LIST_DIR}/inc/arctos/porting/clock/clock.hpp"
   "${CMAKE_CURRENT_LIST_DIR}/inc/arctos/porting/hardware/hardware.hpp"
   "${CMAKE_CURRENT_LIST_DIR}/inc/arctos/system/startup.hpp"
)


# public includes
target_include_directories(iface_arctos INTERFACE
   "${CMAKE_CURRENT_LIST_DIR}/api/"
)

target_sources(arctos PUBLIC
   "${CMAKE_CURRENT_LIST_DIR}/api/arctos/log/logging.hpp"

   "${CMAKE_CURRENT_LIST_DIR}/api/arctos/fifo.hpp"
   "${CMAKE_CURRENT_LIST_DIR}/api/arctos/list.hpp"
   "${CMAKE_CURRENT_LIST_DIR}/api/arctos/printable.hpp"
   "${CMAKE_CURRENT_LIST_DIR}/api/arctos/time.hpp"
   "${CMAKE_CURRENT_LIST_DIR}/api/new.hpp"
)
