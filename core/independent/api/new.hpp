/**
 * Copyright (c) 2018 Michael Zehrer
 * This file is part of ARCTOS
 *
 * @licence MIT
 * @brief   C++ placement new and delete
 * @author  Michael Zehrer
 */
#ifndef ARCTOS_NEW_HPP
#define ARCTOS_NEW_HPP
#include <stddef.h>

inline void *operator new(size_t, void *p)     { return p; }
inline void *operator new[](size_t, void *p)   { return p; }
inline void  operator delete  (void *, void *) { };
inline void  operator delete[](void *, void *) { };

#endif /* ARCTOS_NEW_HPP */
