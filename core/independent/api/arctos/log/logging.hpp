/**
 * Copyright (c) 2019-2020 Michael Zehrer
 * This file is part of ARCTOS
 *
 * @licence MIT
 * @brief   Simple logging and debugging interface
 * @author  Michael Zehrer
 */
#ifndef ARCTOS_LOG_LOGGING_HPP
#define ARCTOS_LOG_LOGGING_HPP

#include <stdint.h>
#include <stddef.h>

#define ARCTOS_LOG_LEVEL_DEBUG      6
#define ARCTOS_LOG_LEVEL_INFO       5
#define ARCTOS_LOG_LEVEL_WARNING    4
#define ARCTOS_LOG_LEVEL_SYSTEM     3
#define ARCTOS_LOG_LEVEL_ERROR      2
#define ARCTOS_LOG_LEVEL_CRITICAL   1
#define ARCTOS_LOG_LEVEL_NONE       0

#include <arctos/params.h>

namespace arctos {
namespace log {

namespace level {
    enum Level {
        kDebug    = ARCTOS_LOG_LEVEL_DEBUG,
        kInfo     = ARCTOS_LOG_LEVEL_INFO,
        kWarning  = ARCTOS_LOG_LEVEL_WARNING,
        kSystem   = ARCTOS_LOG_LEVEL_SYSTEM,
        kError    = ARCTOS_LOG_LEVEL_ERROR,
        kCritical = ARCTOS_LOG_LEVEL_CRITICAL,
        kNone     = ARCTOS_LOG_LEVEL_NONE
    };
}

#if ARCTOS_LOG_LEVEL >= ARCTOS_LOG_LEVEL_DEBUG
#define ARCTOS_LOG_DEBUG_AVAILABLE
__attribute__((__format__(__printf__, 1, 2))) size_t debug(const char *fmt, ...);
#else
inline size_t debug(...) { return 0; }
#endif

#if ARCTOS_LOG_LEVEL >= ARCTOS_LOG_LEVEL_INFO
#define ARCTOS_LOG_INFO_AVAILABLE
__attribute__((__format__(__printf__, 1, 2))) size_t info(const char *fmt, ...);
#else
inline size_t info(...) { return 0; }
#endif

#if ARCTOS_LOG_LEVEL >= ARCTOS_LOG_LEVEL_WARNING
#define ARCTOS_LOG_WARNING_AVAILABLE
__attribute__((__format__(__printf__, 1, 2))) size_t warning(const char *fmt, ...);
#else
inline size_t warning(...) { return 0; }
#endif

#if ARCTOS_LOG_LEVEL >= ARCTOS_LOG_LEVEL_SYSTEM
#define ARCTOS_LOG_SYSTEM_AVAILABLE
__attribute__((__format__(__printf__, 1, 2))) size_t system(const char *fmt, ...);
#else
inline size_t system(...) { return 0; }
#endif

#if ARCTOS_LOG_LEVEL >= ARCTOS_LOG_LEVEL_ERROR
#define ARCTOS_LOG_ERROR_AVAILABLE
__attribute__((__format__(__printf__, 1, 2))) size_t error(const char *fmt, ...);
#else
inline size_t error(...) { return 0; }
#endif

#if ARCTOS_LOG_LEVEL >= ARCTOS_LOG_LEVEL_CRITICAL
#define ARCTOS_LOG_CRITICAL_AVAILABLE
__attribute__((__format__(__printf__, 1, 2))) size_t critical(const char *fmt, ...);
#else
inline size_t critical(...) { while (true) {} }
#endif

} /* namespace log */
} /* namespace arctos */
#endif /* ARCTOS_LOG_LOGGING_HPP */
