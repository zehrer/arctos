/**
 * Copyright (c) 2019 Michael Zehrer
 * This file is part of ARCTOS
 *
 * @licence MIT
 * @brief   Printable base class
 * @author  Michael Zehrer
 */
#ifndef ARCTOS_PRINTABLE_HPP
#define ARCTOS_PRINTABLE_HPP

#include <stdint.h>
#include <stdarg.h>
#include <string.h>

namespace arctos {

/**
 * @brief Base for all those classes who need some print(f) extension
 */
class Printable {
public:
    /**
     * @brief Waits for all outgoing transmissions to complete
     */
    virtual void flush(void) = 0;

    /**
     * @brief Print a character.
     *
     * @param[in] c The character to print.
     * @return the value one
     */
    virtual size_t print(char c) = 0;

    /**
     * @brief Print a string.
     *
     * @param[in] str The string to print
     * @param[in] len The length of the input string
     * @return Number of chars which were really printed
     */
    virtual size_t print(const char *str, size_t len) = 0;

    /**
     * @brief Print a string.
     *
     * @param[in] str The string to print
     * @return Number of chars which were really printed
     */
    virtual size_t print(const char *str) { return this->print(str, strlen(str)); }

    /**
     * @brief Print a formatted string.
     *
     * @param[in] fmt The format string to display
     * @param[in] ... Format parameters
     * @return The length of the string
     */
    __attribute__((__format__(__printf__, 2, 3))) size_t printf(const char *fmt, ...);

    /**
     * @brief Print a formatted string.
     *
     * @param[in] fmt The format string to display
     * @param[in] vl Already initalized variadic list
     * @return The length of the string
     */
    size_t vprintf(const char *fmt, va_list vl);
};

} /* namespace arctos */
#endif /* ARCTOS_PRINTABLE_HPP */
