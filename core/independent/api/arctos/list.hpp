/**
 * @file
 * @copyright Copyright (c) 2018-2023 Michael Zehrer. This file is part of ARCTOS
 * @license MIT
 * @brief simple implementation of a (linked) list
 * @todo make thread-safe
 */
#ifndef ARCTOS_LIST_HPP
#define ARCTOS_LIST_HPP

#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>

/**
 * @brief List iterate macro
 * @param __type Data type of list nodes.
 * @param __list The list.
 */
#define ITERATE_LIST(__type,__list) \
      for (LinkedListItem<__type> *item = __list.getHead(); \
            item != nullptr; \
            item = item->getNext())

namespace arctos {

/**
 * @class LinkedListItem
 * @brief Item which could be inserted into a LinkedList
 * @param T data type of the content
 */
template<typename T> class LinkedListItem {
public:
    constexpr LinkedListItem()
        : next_(nullptr), content_(nullptr), rank_(0) {}

    constexpr LinkedListItem(T *content, uint32_t rank = 0)
        : next_(nullptr), content_(content), rank_(rank) {}

    T*   getContent() const { return this->content_; }
    void setContent(T *cnt) { this->content_ = cnt;  }

    LinkedListItem<T>* getNext() const { return this->next_; }
    void setNext(LinkedListItem<T> *next) { this->next_ = next; }

    uint32_t getRank() const { return this->rank_; }

protected:
    LinkedListItem<T> *next_;
    T *content_;
    uint32_t rank_;
};

/**
 * @class LinkedList
 * @brief (single) ordered linked list
 *
 * This is a simple implementation of a (single) linked list
 * with all items ordered downwards by their 'rank'.
 *
 * To perform a walk-through, simply call 'getHead()' on this
 * list and afterwards 'getNextItem()' on each individual item.
 *
 * @tparam T data type of ListItem content
 */
template<typename T> class LinkedList {
public:
   constexpr LinkedList() : head_(nullptr), size_(0) {}

   size_t             getSize() const { return this->size_; }
   LinkedListItem<T>* getHead() const { return this->head_; }

   /**
    * @brief Inserts the given 'item' into the linked list
    * @return true on success, false otherwise
    */
   bool insert(LinkedListItem<T> *item) {
      if (item == nullptr || item->getNext() != nullptr)
         return false;

      if (this->head_ == nullptr) {
         this->head_ = item;
         this->size_++;
         return true;
      }
      else if (this->head_->getRank() < item->getRank()) {
         item->setNext(this->head_);
         this->head_ = item;
         this->size_++;
         return true;
      }

      LinkedListItem<T> *cur = this->head_;
      do {
         LinkedListItem<T> *next = cur->getNext();

         if (next == nullptr) {
            cur->setNext(item);
            break;
         }
         else if (next->getRank() < item->getRank()) {
            item->setNext(next);
            cur->setNext(item);
            break;
         }

         cur = next;
      } while (true);

      this->size_++;
      return true;
   }

   /**
    * @brief Removes the given 'item' from the linked list.
    * @return true on success, false otherwise
    */
   bool remove(LinkedListItem<T> *item) {
      if (item == nullptr)
         return false;

      LinkedListItem<T> *cur = this->p_head;
      if (cur == item) {
         this->p_head = cur->getNext();
         this->size_--;
         return true;
      }

      while (cur != nullptr) {
         LinkedListItem<T> *next = cur->getNext();
         if (item == next) {
            cur->setNext(next->getNext());
            this->size_--;
            return true;
         }
         cur = next;
      }
      return false;
   }

protected:
   LinkedListItem<T> *head_;
   size_t size_;
};

} /* namespace arctos */
#endif /* ARCTOS_LIST_HPP */
