/* Original licence text from RODOS:

Copyright (c) 2008, German Aerospace Center (DLR)
Copyright (c) 2011, University of Wuerzburg
All rights reserved.

Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

* Redistributions of source code must retain the above copyright notice, this
  list of conditions and the following disclaimer.

* Redistributions in binary form must reproduce the above copyright notice, this
  list of conditions and the following disclaimer in the documentation and/or
  other materials provided with the distribution.

* Neither the name of the German Aerospace Center or the University of
  Würzburg nor the names of its contributors may be used to endorse or
  promote products derived from this software without specific
  prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/
/**
 * @file
 * @copyright Copyright (c) 2008 German Aerospace Center (DLR)
 * @license BSD
 * @brief based on code from Sergio Montenegro and Lutz Dittrich
 */
/**
 * @file
 * @copyright Copyright (c) 2018-2022 Michael Zehrer. This file is part of ARCTOS
 * @license MIT
 * @brief FIFO (first in first out) data structure
 */
#ifndef ARCTOS_FIFO_HPP
#define ARCTOS_FIFO_HPP

#include <stdbool.h>
#include <stddef.h>

namespace arctos {

// TODO Thread-safe

/**
 * @class Fifo
 * @brief simple FIFO (realized as ring buffer)
 *
 * A fifo where both sides (reader & writer) can work asynchronously.
 * If one side has more than one owner, the fifo has to be
 * protected using semaphores.
 *
 * @param T data type of fifo entries
 * @param size maximal number of entries
 */
template<typename T, size_t size>
class Fifo {
public:
    constexpr Fifo() : idx_write_(0), idx_read_(0) { }

    /**
     * @brief Insert one element into the buffer
     * @return true on success, false otherwise (fifo full)
     */
    bool put(const T &val) {
        size_t index = this->advanceIndex(this->idx_write_);

        if (index == this->idx_read_)
            return false;

        this->buffer_[this->idx_write_] = val;
        this->idx_write_ = index;
        return true;
    }

    /**
     * @brief Extract one element from the buffer
     * @return true on success, false otherwise (fifo empty)
     */
    bool get(T &val) {
        if (this->isEmpty())
            return false;

        val = this->buffer_[this->idx_read_];
        this->idx_read_ = advanceIndex(this->idx_read_);
        return true;
    }

    /**
     * @brief Retrieve the size of this fifo
     * @return Size of fifo
     */
    size_t getSize() const { return size; }

    /**
     * @brief Determines the number of elements inside this fifo
     * @return Number of elements in the fifo
     */
    size_t getElementCount() const {
        size_t r = this->idx_read_;
        size_t w = this->idx_write_;
        return ((r <= w) ? (w - r) : (size + 1 - r + w));
    }

    /**
     * @brief Determines the amount of free entries
     * @return Number of free entries
     */
    size_t getFreeSpaceCount() const {
        return size - getElementCount();
    }

    /**
     * @brief Checks if the list is full
     * @return true if the list is full, false otherwise
     */
    bool isFull() const {
        return (advanceIndex(this->idx_write_) == this->idx_read_);
    }

    /**
     * @brief Checks if the list is empty
     * @return true if the list is empty, false otherwise
     */
    bool isEmpty() const {
        return (this->idx_read_ == this->idx_write_);
    }

    /**
     * @brief Deletes all entries
     */
    void clear() {
        this->idx_read_  = 0;
        this->idx_write_ = 0;
    }

protected:
    T buffer_[size + 1];

    volatile size_t idx_write_;
    volatile size_t idx_read_;

    /**
     * @brief Advance index to the next position with
     *        an overflow to 0 to implement a ring
     */
    size_t advanceIndex(size_t index) const {
        return ((index < size) ? (index + 1) : 0);
    }
};


/**
 * @class BlockFifo
 * @brief Simple FIFO with direct block transfers
 *
 * A Fifo that lets you write or read continius Blocks of the stored type.
 * Gives out pointers to the internal space, so you can copy data directly from/to there.
 * Useful for DMA transfers.
 *
 * @param T data type of fifo entries
 * @param size maximal number of entries
 */
template<class T, size_t size>
class BlockFifo : public Fifo<T, size> {
public:
    constexpr BlockFifo() : Fifo<T, size>() { }

    /**
     * @brief Retrieve a pointer to the internal buffer
     * @param[out] max_size Maximal number of entries to be written.
     * @important 'max_size' may be updated to match internal constraints
     * @return Pointer to internal buffer or 'nullptr' if the buffer is full
     */
    T* getBufferToWrite(size_t &max_size) {
        size_t r = this->idx_read_;
        size_t w = this->idx_write_;

        // if the buffer is full
        if (r == this->advanceIndex(w)) {
            max_size = 0;
            return nullptr;
        }

        if (r > w)  // until r-1 possible
            max_size = r - w - 1;
        else {      // until end possible
            max_size = size + 1 - w;
            if (r == 0) { --max_size; }
        }
        return this->buffer_ + w;
    }

    /**
     * @brief Retrieve a pointer to the internal buffer
     * @param[out] max_size Maximal number of entries to be read.
     * @important 'max_size' may be updated to match internal constraints
     * @return Pointer to internal buffer or 'nullptr' if the buffer is empty
     */
    T* getBufferToRead(size_t &max_size) {
        size_t r = this->idx_read_;
        size_t w = this->idx_write_;

        // if the buffer is empty
        if (r == w) {
            max_size = 0;
            return nullptr;
        }

        if (r < w)  // until w-1 possible
            max_size = w - r;
        else        // until end possible
            max_size = size + 1 - r;

        return this->buffer_ + r;
    }

    /**
     * @brief Call this function after you have written into the buffer
     *        you got from 'getBufferToWrite(...)'
     */
    void writeConcluded(size_t size_written) {
        this->idx_write_ = this->advanceIndex(this->idx_write_, size_written);
    }

    /**
     * @brief Call this function after you have read from the buffer
     *        you got from 'getBufferToRead(...)'
     */
    void readConcluded(size_t size_read) {
        this->idx_read_ = this->advanceIndex(this->idx_read_, size_read);
    }

protected:
    size_t advanceIndex(size_t index, size_t count = 1) const {
        size_t new_index = index + count;
        while (new_index > size)
            new_index = new_index - size - 1;
        return new_index;
    }
};

} /* namespace arctos */
#endif /* ARCTOS_FIFO_HPP */
