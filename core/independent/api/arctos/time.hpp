/**
 * @file
 * @copyright Copyright (c) 2018-2021 Michael Zehrer. This file is part of ARCTOS
 * @license MIT
 * @brief (system) time API
 */
// FIXME: Overthink this API
#ifndef ARCTOS_TIME_HPP
#define ARCTOS_TIME_HPP
#include <stdint.h>

/** this implementation: nanoseconds about 150 Years from now */
#define END_OF_TIME   0x7FFFFFFFFFFFFFFFLL

#define NANOSECONDS    1LL
#define MICROSECONDS   (1000LL * NANOSECONDS)
#define MILLISECONDS   (1000LL * MICROSECONDS)
#define SECONDS        (1000LL * MILLISECONDS)
#define MINUTES        (60LL * SECONDS)
#define HOURS          (60LL * MINUTES)
#define DAYS           (24LL * HOURS)
#define WEEKS          (7LL  * DAYS)

namespace arctos {

// TODO
class TimeModel {
public:
    int64_t now();
    int64_t rough();
};

extern TimeModel sys_time;

// TODO overthink
#define NOW() ::arctos::sys_time.now()
#define ROUGH_NOW() ::arctos::sys_time.rough()

} /* namespace arctos */
#endif /* ARCTOS_TIME_HPP */
