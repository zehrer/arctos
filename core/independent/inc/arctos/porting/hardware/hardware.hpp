/**
 * @file
 * @copyright Copyright (c) 2018-2022 Michael Zehrer. This file is part of ARCTOS
 * @license MIT
 * @brief hardware related porting interface functions
 */
#ifndef ARCTOS_PORTING_HARDWARE_HARDWARE_HPP
#define ARCTOS_PORTING_HARDWARE_HARDWARE_HPP
#include <stdint.h>

namespace arctos {
namespace porting {
namespace hardware {

/**
 * @brief Early hardware initialization
 *
 * This function is intended to initialize certain hardware (e.g. debug-uart, ...)
 * @note This interface function is optional.
 */
void init(void);

} /* namespace hardware */
} /* namespace porting */
} /* namespace arctos */
#endif /* ARCTOS_PORTING_HARDWARE_HARDWARE_HPP */
