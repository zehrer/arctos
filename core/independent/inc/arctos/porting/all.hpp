/**
 * @file
 * @copyright Copyright (c) 2020-2022 Michael Zehrer. This file is part of ARCTOS
 * @license MIT
 * @brief include all porting interface headers
 * @note this file is intended to be shadowed by other
 *       <arctos/porting/all.hpp> down the tree
 */
#include <arctos/porting/independent.hpp>
