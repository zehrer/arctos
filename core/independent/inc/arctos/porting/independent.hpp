/**
 * @file
 * @copyright Copyright (c) 2020-2022 Michael Zehrer. This file is part of ARCTOS
 * @license MIT
 * @brief porting interface functions
 */
#ifndef ARCTOS_PORTING_INDEPENDENT_HPP
#define ARCTOS_PORTING_INDEPENDENT_HPP

#include "clock/clock.hpp"
#include "hardware/hardware.hpp"

namespace arctos {
namespace porting {

/**
 * @brief Porting interface initialization
 *
 * This function is intended to put all the other init-functions together
 * @note This interface function is optional.
 */
void init(void);

} /* namespace porting */
} /* namespace arctos */
#endif /* ARCTOS_PORTING_INDEPENDENT_HPP */
