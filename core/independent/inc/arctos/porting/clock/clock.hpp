/**
 * @file
 * @copyright Copyright (c) 2018-2022 Michael Zehrer. This file is part of ARCTOS
 * @license MIT
 * @brief system clock related porting interface functions
 */
#ifndef ARCTOS_PORTING_CLOCK_CLOCK_HPP
#define ARCTOS_PORTING_CLOCK_CLOCK_HPP
#include <stdint.h>

namespace arctos {
namespace porting {
namespace clock {

/**
 * @brief System clock initialization
 *
 * Configures, initializes and starts the system clock
 * @note This interface function is optional.
 */
void init(void);

void activateSystemTick(void);
void deactivateSystemTick(void);

bool activateTimeEvent(int64_t event);
void deactivateTimeEvent(void);

/**
 * @brief Determines the system time
 * @return Current system time in nanoseconds (startup time included)
 * @note EACH PORTATION MUST PROVIDE THIS FUNCTION!
 */
int64_t now(void);

/**
 * @brief Determines the system time.
 *        Compared to now(), rough() might be less accurate but more efficient
 *        (on some platforms)
 * @return Current system time in nanoseconds (startup time included)
 * @note This interface function is optional.
 */
int64_t rough(void);

} /* namespace clock */
} /* namespace porting */
} /* namespace arctos */
#endif /* ARCTOS_PORTING_CLOCK_CLOCK_HPP */
