/**
 * @file
 * @copyright Copyright (c) 2021-2024 Michael Zehrer. This file is part of ARCTOS
 * @license MIT
 * @brief system startup
 */
#ifndef ARCTOS_SYSTEM_STARTUP_HPP
#define ARCTOS_SYSTEM_STARTUP_HPP

namespace arctos {
namespace system {

/**
 * @brief initializes and starts the whole system
 */
[[noreturn]]
void startup(void) __asm__("arctos_system_startup");

} /* namespace system */
} /* namespace arctos */
#endif /* ARCTOS_SYSTEM_STARTUP_HPP */
