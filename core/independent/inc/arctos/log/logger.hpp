/**
 * Copyright (c) 2019-2020 Michael Zehrer
 * This file is part of ARCTOS
 *
 * @licence MIT
 * @brief   Simple logging implementation
 * @author  Michael Zehrer
 */
#ifndef ARCTOS_LOG_LOGGER_HPP
#define ARCTOS_LOG_LOGGER_HPP

#include <stdint.h>
#include <stdarg.h>

#include <arctos/log/logging.hpp>
#include <arctos/printable.hpp>

namespace arctos {
namespace log {

class Logger {
protected:
    Logger() {}

    Printable *destination_;

public:
    void setDestination(Printable *dest) { this->destination_ = dest; }
    size_t notify(level::Level lvl, const char *fmt, va_list vl);

    static Logger *instance() {
        static Logger log;
        return &log;
    }
};

} /* namespace log */
} /* namespace arctos */
#endif /* ARCTOS_LOG_LOGGER_HPP */
