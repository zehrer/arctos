/**
 * Copyright (c) 2020 Michael Zehrer
 * This file is part of ARCTOS
 *
 * @licence MIT
 * @brief   Common types and definitions
 * @author  Michael Zehrer
 */
#ifndef ARMV7AR_COMMON_HPP
#define ARMV7AR_COMMON_HPP

namespace arm {

#ifndef __R
#define __R     volatile const  /* read only    */
#endif
#ifndef __W
#define __W     volatile        /* write only   */
#endif
#ifndef __RW
#define __RW    volatile        /* read / write */
#endif

#ifndef BIT
  #define BIT(n) (1 << (n))
#endif
#ifndef STR
  #define STR_HELPER(x) #x
  #define STR(x) STR_HELPER(x)
#endif

#define __DMB(opt)  asm volatile ("dmb " #opt ::: "memory")
#define __DSB(opt)  asm volatile ("dsb " #opt ::: "memory")
#define __ISB(opt)  asm volatile ("isb " #opt ::: "memory")

} /* namespace arm */
#endif /* ARMV7AR_COMMON_HPP */
