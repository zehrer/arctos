/**
 * Copyright (c) 2020 Michael Zehrer
 * This file is part of ARCTOS
 *
 * @licence MIT
 * @brief   Application Program Status Register (APSR)
 * @see     ARMv7-A/R Architecture Reference Manual - Section A2.4
 * @author  Michael Zehrer
 */
#ifndef ARMV7AR_REGISTERS_APSR_HPP
#define ARMV7AR_REGISTERS_APSR_HPP
#include <arm/common.hpp>
#include <stdint.h>

namespace arm {
namespace registers {

union APSR_Type {
    uint32_t value;
    struct {
        uint32_t res0:16;   // bit:   0..15 Reserved, UNK/SBZP
        uint32_t GE:4;      // bit:  16..19 Greater than or Equal flag
        uint32_t res1:4;    // bit:  20..23 Reserved, UNK/SBZP
        uint32_t res2:3;    // bit:  24..26 Reserved, RAZ/SBZP
        uint32_t Q:1;       // bit:      27 Overflow or saturation flag
        uint32_t V:1;       // bit:      28 COND FLAG: Overflow
        uint32_t C:1;       // bit:      29 COND FLAG: Carry
        uint32_t Z:1;       // bit:      30 COND FLAG: Zero
        uint32_t N:1;       // bit:      31 COND FLAG: Negative
    } fields;
};

} /* namespace registers */
} /* namespace arm */
#endif /* ARMV7AR_REGISTERS_APSR_HPP */
