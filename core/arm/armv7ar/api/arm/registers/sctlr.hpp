/**
 * Copyright (c) 2020 Michael Zehrer
 * This file is part of ARCTOS
 *
 * @licence MIT
 * @brief   VMSA - System Control Register (SCTLR)
 * @see     ARMv7-A/R Architecture Reference Manual - Section B4.1.130
 * @author  Michael Zehrer
 */
#ifndef ARMV7AR_REGISTERS_SCTLR_HPP
#define ARMV7AR_REGISTERS_SCTLR_HPP
#include <arm/common.hpp>
#include <stdint.h>

namespace arm {
namespace registers {

union SCTLR_Type {
    uint32_t value;
    struct {
        uint32_t M:1;       // bit:       0 MMU enable
        uint32_t A:1;       // bit:       1 Alignment check enable
        uint32_t C:1;       // bit:       2 Unified/Data cache enable
        uint32_t res0:2;    // bit:    3..4 Reserved, RAO/SBOP
        uint32_t CP15BEN:1; // bit:       5 CP15 barrier enable
        uint32_t res1:1;    // bit:       6 Reserved, RAO/SBOP
        uint32_t res2:3;    // bit:    7..9 Reserved, RAZ/SBZP
        uint32_t SW:1;      // bit:      10 SWP and SWPB enable
        uint32_t Z:1;       // bit:      11 Branch prediction enable
        uint32_t I:1;       // bit:      12 Instruction cache enable
        uint32_t V:1;       // bit:      13 High exception vectors select
        uint32_t RR:1;      // bit:      14 Round Robin select
        uint32_t res3:1;    // bit:      15 Reserved, RAZ/SBZP
        uint32_t res4:1;    // bit:      16 Reserved, RAO/SBOP
        uint32_t HA:1;      // bit:      17 Hardware Access flag enable
        uint32_t res5:1;    // bit:      18 Reserved, RAO/SBOP
        uint32_t WXN:1;     // bit:      19 Write permission implies XN
        uint32_t UWXN:1;    // bit:      20 Unprivileged write permission implies PL1 XN
        uint32_t FI:1;      // bit:      21 Fast interrupts configuration enable
        uint32_t res6:2;    // bit:  22..23 Reserved, Reserved, RAO/SBOP
        uint32_t VE:1;      // bit:      24 Interrupt Vectors Enable
        uint32_t EE:1;      // bit:      25 Exception Endianness
        uint32_t res7:1;    // bit:      26 Reserved, RAZ/SBZP
        uint32_t NMFI:1;    // bit:      27 Non-maskable FIQ support (read-only)
        uint32_t TRE:1;     // bit:      28 TEX remap enable
        uint32_t AFE:1;     // bit:      29 Access flag enable
        uint32_t TE:1;      // bit:      30 Thumb Exception enable
        uint32_t res8:1;    // bit:      31 Reserved, UNK/SBZP
    } fields;

    static inline SCTLR_Type read() {
        SCTLR_Type val;
        asm volatile("mrc p15, #0, %[val], c1, c0, #0\n\t" : [val]"=r"(val.value));
        return val;
    }
    static inline void write(SCTLR_Type val) {
        asm volatile("mcr p15, #0, %[val], c1, c0, #0\n\t" :: [val]"r"(val.value));
    }
};

} /* namespace registers */
} /* namespace arm */
#endif /* ARMV7AR_REGISTERS_SCTLR_HPP */
