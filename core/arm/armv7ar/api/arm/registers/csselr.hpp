/**
 * Copyright (c) 2020 Michael Zehrer
 * This file is part of ARCTOS
 *
 * @licence MIT
 * @brief   Cache Size Selection Register (CSSELR) VMSA
 * @see     ARMv7-A/R Architecture Reference Manual - Section B4.1.41
 * @author  Michael Zehrer
 */
#ifndef ARMV7AR_REGISTERS_CSSELR_HPP
#define ARMV7AR_REGISTERS_CSSELR_HPP
#include <arm/common.hpp>
#include <stdint.h>

namespace arm {
namespace registers {

union CSSELR_Type {
    uint32_t value;
    struct {
        uint32_t InD:1;         // bit:       0 Instruction not Data
        uint32_t Level:3;       // bit:    1..3 (Cache level of required cache) - 1
        uint32_t res:28;        // bit:   4..31 Reserved, UNK/SBZP
    } fields;

    static inline CSSELR_Type read() {
        CSSELR_Type val;
        asm volatile("mrc p15, #2, %[val], c0, c0, #0\n\t" : [val]"=r"(val.value));
        return val;
    }
    static inline void write(CSSELR_Type val) {
        asm volatile("mcr p15, #2, %[val], c0, c0, #0\n\t" :: [val]"r"(val.value));
    }
};

} /* namespace registers */
} /* namespace arm */
#endif /* ARMV7AR_REGISTERS_CSSELR_HPP */
