/**
 * Copyright (c) 2020 Michael Zehrer
 * This file is part of ARCTOS
 *
 * @licence MIT
 * @brief   Cache Type Register (CTR) VMSA
 * @see     ARMv7-A/R Architecture Reference Manual - Section B4.1.42
 * @author  Michael Zehrer
 */
#ifndef ARMV7AR_REGISTERS_CTR_HPP
#define ARMV7AR_REGISTERS_CTR_HPP
#include <arm/common.hpp>
#include <stdint.h>

namespace arm {
namespace registers {

union CTR_Type {
    uint32_t value;
    struct {
        uint32_t IminLine:4;    // bit:    0..3 Log2 of the number of words in the smallest
                                //              cache line of all the instruction caches
                                //              that are controlled by the processor
        uint32_t zero0:10;      // bit:   4..13 RAZ
        uint32_t L1Ip:2;        // bit:  14..15 Level 1 instruction cache policy
        uint32_t DminLine:4;    // bit:  16..19 Log2 of the number of words in the smallest
                                //              cache line of all the data caches and unified
                                //              caches that are controlled by the processor
        uint32_t ERG:4;         // bit:  20..23 Exclusives Reservation Granule
        uint32_t CWG:4;         // bit:  24..27 Cache Write-back Granule
        uint32_t zero1:1;       // bit:      28 RAZ
        uint32_t format:3;      // bit:  29..31 Indicates the implemented CTR format
    } fields;

    static inline CTR_Type read() {
        CTR_Type val;
        asm volatile("mrc p15, #0, %[val], c0, c0, #1\n\t" : [val]"=r"(val.value));
        return val;
    }
};

} /* namespace registers */
} /* namespace arm */
#endif /* ARMV7AR_REGISTERS_CTR_HPP */
