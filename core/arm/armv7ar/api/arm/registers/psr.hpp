/**
 * Copyright (c) 2020 Michael Zehrer
 * This file is part of ARCTOS
 *
 * @licence MIT
 * @brief   Program status registers (PSR)
 * @see     ARMv7-A/R Architecture Reference Manual - Section B1.3.3
 * @author  Michael Zehrer
 */
#ifndef ARMV7AR_REGISTERS_PSR_HPP
#define ARMV7AR_REGISTERS_PSR_HPP
#include <arm/common.hpp>
#include <stdint.h>

namespace arm {
namespace registers {

namespace psr {
enum class Mode : uint8_t {
    kUser       = 0b10000,
    kFIQ        = 0b10001,
    kIRQ        = 0b10010,
    kSupervisor = 0b10011,
    kMonitor    = 0b10110, // with security extension
    kAbort      = 0b10111,
    kHyp        = 0b11010, // with virtualization extension
    kUndefined  = 0b11011,
    kSystem     = 0b11111
};
} /* namspace psr */


// The Current Program Status Register (CPSR) and the Saved Program Status Registers (SPSRs)
// are the same, regarding register format

union PSR_Type {
    uint32_t value;
    struct {
        uint32_t M:5;       // bit:    0..4 Mode field
        uint32_t T:1;       // bit:       5 Thumb execution state bit
        uint32_t F:1;       // bit:       6 MASK BIT: FIQ
        uint32_t I:1;       // bit:       7 MASK BIT: IRQ
        uint32_t A:1;       // bit:       8 MASK BIT: Asynchronous abort
        uint32_t E:1;       // bit:       9 Endianness execution state bit
        uint32_t IT_2to7:6; // bit:  10..15 If-Then execution state for Thumb
        uint32_t GE:4;      // bit:  16..19 Greater than or Equal flag
        uint32_t res0:4;    // bit:  20..23 Reserved RAZ/SBZP
        uint32_t J:1;       // bit:      24 Jazelle bit
        uint32_t IT_0to1:2; // bit:  25..26 If-Then execution state for Thumb
        uint32_t Q:1;       // bit:      27 Overflow or saturation flag
        uint32_t V:1;       // bit:      28 COND FLAG: Overflow
        uint32_t C:1;       // bit:      29 COND FLAG: Carry
        uint32_t Z:1;       // bit:      30 COND FLAG: Zero
        uint32_t N:1;       // bit:      31 COND FLAG: Negative
    } fields;

    /**
     * Checks the IRQ-Mask-Bit to find out if interrupts are enabled
     * @return TRUE = IRQ enabled; FALSE = otherwise
     */
    inline bool isIRQEnabled() {
        return (this->fields.I == 0 ? true : false);
    }
    /**
     * Checks the FIQ-Mask-Bit to find out if fast interrupts are enabled
     * @return TRUE = FIQ enabled; FALSE = otherwise
     */
    inline bool isFIQEnabled() {
        return (this->fields.F == 0 ? true : false);
    }
    /**
     * Checks whether interrupts or fast interrupts are (globally) enabled
     * @return TRUE = IRQ and/or FIQ enabled; FALSE = otherwise
     */
    inline bool areInterruptsEnabled() {
        return ((this->fields.I == 0 || this->fields.F == 0) ? true : false);
    }

    static inline PSR_Type readCPSR() {
        PSR_Type val;
        asm volatile("mrs %[val], cpsr_all\n\t" : [val]"=r"(val.value));
        return val;
    }
    static inline void writeCPSR(PSR_Type val) {
        asm volatile("msr cpsr_all, %[val]\n\t" :: [val]"r"(val.value));
    }
};

} /* namespace registers */
} /* namespace arm */
#endif /* ARMV7AR_REGISTERS_PSR_HPP */
