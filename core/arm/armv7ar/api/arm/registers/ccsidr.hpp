/**
 * Copyright (c) 2020 Michael Zehrer
 * This file is part of ARCTOS
 *
 * @licence MIT
 * @brief   Cache Size ID Register (CCSIDR) VMSA
 * @see     ARMv7-A/R Architecture Reference Manual - Section B4.1.19
 * @author  Michael Zehrer
 */
#ifndef ARMV7AR_REGISTERS_CCSIDR_HPP
#define ARMV7AR_REGISTERS_CCSIDR_HPP
#include <arm/common.hpp>
#include <stdint.h>

namespace arm {
namespace registers {

union CCSIDR_Type {
    uint32_t value;
    struct {
        uint32_t LineSize:3;        // bit:    0..2 Log2(Number of words in cache line)) - 2
        uint32_t Associativity:10;  // bit:   3..12 (Associativity of cache) - 1
        uint32_t NumSets:15;        // bit:  13..27 (Number of sets in cache) - 1
        uint32_t WA:1;              // bit:      28 write allocate support
        uint32_t RA:1;              // bit:      29 read-allocate support
        uint32_t WB:1;              // bit:      30 write-back support
        uint32_t WT:1;              // bit:      31 write-through support
    } fields;

    static inline CCSIDR_Type read() {
        CCSIDR_Type val;
        asm volatile("mrc p15, #1, %[val], c0, c0, #0\n\t" : [val]"=r"(val.value));
        return val;
    }
};

} /* namespace registers */
} /* namespace arm */
#endif /* ARMV7AR_REGISTERS_CCSIDR_HPP */
