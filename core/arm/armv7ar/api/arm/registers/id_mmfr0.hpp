/**
 * Copyright (c) 2020 Michael Zehrer
 * This file is part of ARCTOS
 *
 * @licence MIT
 * @brief   Memory Model Feature Register 0 (ID_MMFR0)
 * @see     ARMv7-A/R Architecture Reference Manual - Section B4.1.89
 * @author  Michael Zehrer
 */
#ifndef ARMV7AR_REGISTERS_ID_MMFR0_HPP
#define ARMV7AR_REGISTERS_ID_MMFR0_HPP
#include <arm/common.hpp>
#include <stdint.h>

namespace arm {
namespace registers {

namespace id_mmfr0 {

enum class VmsaSupport : uint8_t {
    kNotSupported           = 0b0000,
    kImplementationDefined  = 0b0001,
    kVMSAv6                 = 0b0010,
    kVMSAv7                 = 0b0011,
    kVMSAv7_PXN             = 0b0100,
    kVMSAv7_PXN_LD          = 0b0101
}
enum class PmsaSupport : uint8_t {
    kNotSupported           = 0b0000
    kImplementationDefined  = 0b0001,
    kPMSAv6                 = 0b0010,
    kPMSAv7                 = 0b0011,
}
enum class Shareability : uint8_t {
    kNonCacheable           = 0b0000,
    kHardwareCoherency      = 0b0001,
    kIgnored                = 0b1111
}
enum class Levels : uint8_t {
    kOne                    = 0b0000,
    kTwo                    = 0b0001
}
enum class TcmSupport : uint8_t {
    /* An ARMv7 implementation might include an ARMv6 model for TCM support.
       However, in ARMv7 this is an IMPLEMENTATION DEFINED option, and therefore
       it must be represented by the 0b0001 encoding in this field. */
    kImplemenationDefined   = 0b0001
}
enum AuxiliaryRegisters : uint8_t {
    kNoneSupported          = 0b0000,
    kControlOnly            = 0b0001,
    kControlAndFaultStatus  = 0b0010
}
enum FcseSupport : uint8_t {
    kNotSupported           = 0b0000,
    kSupported              = 0b0001
}

} /* namespace id_mmfr0 */

union ID_MMFR0_Type {
    uint32_t value;
    struct {
        uint32_t vmsaSupport:4;             // bit:    3..0
        uint32_t pmsaSupport:4;             // bit:    4..7
        uint32_t outermostShareability:4;   // bit:   8..11
        uint32_t shareabilityLevels:4;      // bit:  12..15
        uint32_t tcmSupport:4;              // bit:  16..19
        uint32_t auxRegisters:4;            // bit:  20..23
        uint32_t fcseSupport:4;             // bit:  24..27
        uint32_t innermostShareability:4;   // bit:  28..31
    } fields;

    static inline ID_MMFR0_Type read() {
        ID_MMFR0_Type val;
        asm volatile("mrc p15, #0, %[val], c0, c1, #4\n\t" : [val]"=r"(val.value));
        return val;
    }
};

} /* namespace registers */
} /* namespace arm */
#endif /* ARMV7AR_REGISTERS_ID_MMFR0_HPP */
