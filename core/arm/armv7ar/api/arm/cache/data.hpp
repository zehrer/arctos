/**
 * Copyright (c) 2020 Michael Zehrer
 * This file is part of ARCTOS
 *
 * @licence MIT
 * @brief   Data cache maintenance functions
 * @author  Michael Zehrer
 */
#ifndef ARMV7AR_CACHE_DATA_HPP
#define ARMV7AR_CACHE_DATA_HPP

#include <stdint.h>
#include <stddef.h>

namespace arm {
namespace cache {
namespace data {

/**
 * @brief Enable data cache
 */
void enable(void) asm("arm_cache_data_enable");

/**
 * @brief Disable data cache
 */
void disable(void) asm("arm_cache_data_disable");

/**
 * @brief Cleans the whole data cache
 */
void clean(void) asm("arm_cache_data_clean");

/**
 * @brief Cleans the cache line belonging to the given address
 */
void clean(uintptr_t addr);

/**
 * @brief Cleans the cache lines belonging to the given address range
 */
void clean(uintptr_t addr, uintptr_t addr_end);


/**
 * @brief Cleans and invalidates the whole data cache
 */
void cleanAndInvalidate(void) asm("arm_cache_data_clean_and_invalidate");

/**
 * @brief Cleans and invalidates the cache line belonging to the given address
 */
void cleanAndInvalidate(uintptr_t addr);

/**
 * @brief Cleans and invalidates the cache lines belonging to the given address range
 */
void cleanAndInvalidate(uintptr_t addr, uintptr_t addr_end);


/**
 * @brief Invalidates the whole data cache
 */
void invalidate(void) asm("arm_cache_data_invalidate");
/**
 * @brief Invalidates the cache line belonging to the given address
 */
void invalidate(uintptr_t addr);
/**
 * @brief Invalidates the cache lines belonging to the given address range
 */
void invalidate(uintptr_t addr, uintptr_t addr_end);

} /* namespace data */
} /* namespace cache */
} /* namespace arm */
#endif /* ARMV7AR_CACHE_DATA_HPP */
