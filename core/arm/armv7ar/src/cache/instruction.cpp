/**
 * Copyright (c) 2020 Michael Zehrer
 * This file is part of ARCTOS
 *
 * @licence MIT
 * @brief   Instruction cache maintenance functions
 * @author  Michael Zehrer
 */
#include <arm/cache/instruction.hpp>
#include <arm/registers/sctlr.hpp>

namespace arm {
namespace cache {
namespace instruction {

void enable(void) {
    __DMB();
    auto sctlr = ::arm::registers::SCTLR_Type::read();
    sctlr.fields.I = 1;
    sctlr.fields.Z = 1;
    ::arm::registers::SCTLR_Type::write(sctlr);
    __ISB();
}

void disable(void) {
    __DMB();
    auto sctlr = ::arm::registers::SCTLR_Type::read();
    sctlr.fields.I = 0;
    sctlr.fields.Z = 0;
    ::arm::registers::SCTLR_Type::write(sctlr);
    __ISB();
}

void invalidate(void) {
    // ICIALLU, Instruction cache invalidate all to PoU. Ignores Rt value
    asm volatile("mcr p15, #0, r0, c7, c5, #0\n\t");
}

} /* namespace instruction */
} /* namespace cache */
} /* namespace arm */
