/**
 * Copyright (c) 2020 Michael Zehrer
 * This file is part of ARCTOS
 *
 * @licence MIT
 * @brief   Data cache maintenance functions
 * @author  Michael Zehrer
 */
#include <arm/cache/data.hpp>
#include <arm/registers/ccsidr.hpp>
#include <arm/registers/csselr.hpp>
#include <arm/registers/ctr.hpp>
#include <arm/registers/sctlr.hpp>

namespace arm {
namespace cache {
namespace data {

void enable(void) {
    __DMB();
    auto sctlr = ::arm::registers::SCTLR_Type::read();
    sctlr.fields.C  = 1;
    ::arm::registers::SCTLR_Type::write(sctlr);
    __DSB();
}

void disable(void) {
    __DMB();
    auto sctlr = ::arm::registers::SCTLR_Type::read();
    sctlr.fields.C  = 0;
    ::arm::registers::SCTLR_Type::write(sctlr);
    __DSB();
}

static void cm_invalidate(uint32_t set_way) {
    asm volatile("mcr p15, #0, %[sw], c7, c6 , #2\n\t" :: [sw]"r"(set_way));
}
static void cm_clean(uint32_t set_way) {
    asm volatile("mcr p15, #0, %[sw], c7, c10, #2\n\t" :: [sw]"r"(set_way));
}
static void cm_cleanAndInvalidate(uint32_t set_way) {
    asm volatile("mcr p15, #0, %[sw], c7, c14, #2\n\t" :: [sw]"r"(set_way));
}

static void foreach_set_way(void (*cm)(uint32_t)) {
    ::arm::registers::CSSELR_Type cache_sel{ .value = 0 };
    do {
        ::arm::registers::CSSELR_Type::write(cache_sel);
        __ISB();
        auto cache_size = ::arm::registers::CCSIDR_Type::read();

        uint32_t num_sets_m1 = cache_size.fields.NumSets;
        uint32_t num_ways_m1 = cache_size.fields.Associativity;
        uint32_t set_shift   = cache_size.fields.LineSize + 4;
        uint32_t way_shift   = __builtin_clz(num_ways_m1);

        uint32_t set_way_level = (cache_sel.fields.Level << 1);
        int32_t cur_way = num_ways_m1;
        do {
            int32_t cur_set = num_sets_m1;
            do {
                uint32_t set_way = set_way_level | (cur_set << set_shift) | (cur_way << way_shift);
                cm(set_way);
                --cur_set;
            } while (cur_set >= 0);
            --cur_way;
        } while (cur_way >= 0);

        ++(cache_sel.fields.Level);
    } while (cache_sel.fields.Level < 2);
}

void invalidate(void) {
    foreach_set_way(&cm_invalidate);
}
void invalidate(uintptr_t addr) {
    // Invalidate data cache line by MVA to PoC
    asm volatile("mcr p15, #0, %[addr], c7, c6, #1\n\t" :: [addr]"r"(addr));
}
void invalidate(uintptr_t addr_start, uintptr_t addr_end) {
    auto ctr = ::arm::registers::CTR_Type::read();

    // min_linelen = 4 * 2^(DminLine) = size of smallest cache line
    uint32_t min_linelen = (4 << ctr.fields.DminLine);

    // align the start address to the line lenght
    addr_start = addr_start & (~(static_cast<uintptr_t>(min_linelen - 1)));
    while (addr_start < addr_end) {
        // Invalidate data cache line by MVA to PoC
        asm volatile("mcr p15, #0, %[addr], c7, c6, #1\n\t" :: [addr]"r"(addr_start));
        addr_start += min_linelen;
    }
}


void clean(void) {
    foreach_set_way(&cm_clean);
}
void clean(uintptr_t addr) {
    // Clean data or unified cache line by MVA to PoC
    asm volatile("mcr p15, #0, %[addr], c7, c10, #1\n\t" :: [addr]"r"(addr));
}
void clean(uintptr_t addr_start, uintptr_t addr_end) {
    auto ctr = ::arm::registers::CTR_Type::read();

    // min_linelen = 4 * 2^(DminLine) = size of smallest cache line
    uint32_t min_linelen = (4 << ctr.fields.DminLine);

    // align the start address to the line lenght
    addr_start = addr_start & (~(static_cast<uintptr_t>(min_linelen - 1)));
    while (addr_start < addr_end) {
        // Clean data or unified cache line by MVA to PoC
        asm volatile("mcr p15, #0, %[addr], c7, c10, #1\n\t" :: [addr]"r"(addr_start));
        addr_start += min_linelen;
    }
}


void cleanAndInvalidate(void) {
    foreach_set_way(&cm_cleanAndInvalidate);
}
void cleanAndInvalidate(uintptr_t addr) {
    // Clean and Invalidate data or unified cache line by MVA to PoC
    asm volatile("mcr p15, #0, %[addr], c7, c14, #1\n\t" :: [addr]"r"(addr));
}
void cleanAndInvalidate(uintptr_t addr_start, uintptr_t addr_end) {
    auto ctr = ::arm::registers::CTR_Type::read();

    // min_linelen = 4 * 2^(DminLine) = size of smallest cache line
    uint32_t min_linelen = (4 << ctr.fields.DminLine);

    // align the start address to the line lenght
    addr_start = addr_start & (~(static_cast<uintptr_t>(min_linelen - 1)));
    while (addr_start < addr_end) {
        // Clean and Invalidate data or unified cache line by MVA to PoC
        asm volatile("mcr p15, #0, %[addr], c7, c14, #1\n\t" :: [addr]"r"(addr_start));
        addr_start += min_linelen;
    }
}

} /* namespace data */
} /* namespace cache */
} /* namespace arm */
