/**
 * Copyright (c) 2020 Michael Zehrer
 * This file is part of ARCTOS
 *
 * @licence MIT
 * @brief   MMU maintenance functions
 * @author  Michael Zehrer
 */
#ifndef ARMV6_MMU_MAINTENANCE_HPP
#define ARMV6_MMU_MAINTENANCE_HPP

namespace arm {
namespace mmu {

/**
 * @brief Enable MMU
 */
void enable(void) asm("arm_mmu_enable");

/**
 * @brief Disable MMU
 */
void disable(void) asm("arm_mmu_disable");

} /* namespace mmu */
} /* namespace arm */
#endif /* ARMV6_MMU_MAINTENANCE_HPP */
