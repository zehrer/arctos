/**
 * Copyright (c) 2020 Michael Zehrer
 * This file is part of ARCTOS
 *
 * @licence MIT
 * @brief   Virtual Memory System Architecture (VMSA)
 * @author  Michael Zehrer
 */
#ifndef ARMV6_MMU_VMSA_HPP
#define ARMV6_MMU_VMSA_HPP

#include <stdint.h>
#include <stddef.h>

namespace arm {
namespace mmu {
namespace vmsa {

/******************************************************************************
 * First-level descriptors format (subpages disabled)
 * @see ARM ARM (Version 2005) - Section B4.7.4
 ******************************************************************************/

// 1 MB structured in an second level table
union CoarsePageTable {
    uint32_t value;
    struct {
        uint32_t one:1;             // bit:       0 MUST BE ONE!
        uint32_t zero:4;            // bit:    1..4 MUST BE ZERO!
        uint32_t domain:4;          // bit:    5..8 domain
        uint32_t IMP:1;             // bit:       9 IMPLEMENTATION DEFINED
        uint32_t table_base_addr:22;// bit:  10..31 Coarse page table base address
    } fields;
};

// 1 MB section
union Section {
    uint32_t value;
    struct {
        uint32_t zero0:1;           // bit:       0 MUST BE ZERO!
        uint32_t one:1;             // bit:       1 MUST BE ONE!
        uint32_t B:1;               // bit:       2 bufferable
        uint32_t C:1;               // bit:       3 cacheable
        uint32_t XN:1;              // bit:       4 execute-never bit
        uint32_t domain:4;          // bit:    5..8 domain
        uint32_t IMP:1;             // bit:       9 IMPLEMENTATION DEFINED
        uint32_t AP:2;              // bit:  10..11 access permissions
        uint32_t TEX:3;             // bit:  12..14 TEX memory region attribute
        uint32_t APX:1;             // bit:      15 access permissions extension
        uint32_t S:1;               // bit:      16 shareable
        uint32_t nG:1;              // bit:      17 not-global
        uint32_t zero1:2;           // bit:  18..19 MUST BE ZERO!
        uint32_t base_addr:12;      // bit:  20..31 section base address
    } fields;
};

// 16 MB supersection
union Supersection {
    uint32_t value;
    struct {
        uint32_t zero0:1;           // bit:       0 MUST BE ZERO!
        uint32_t one0:1;            // bit:       1 MUST BE ONE!
        uint32_t B:1;               // bit:       2 bufferable
        uint32_t C:1;               // bit:       3 cacheable
        uint32_t XN:1;              // bit:       4 execute-never bit
        uint32_t base_addr_36to39:4;// bit:    5..8 base address [39:36]
        uint32_t IMP:1;             // bit:       9 IMPLEMENTATION DEFINED
        uint32_t AP:2;              // bit:  10..11 access permissions
        uint32_t TEX:3;             // bit:  12..14 TEX memory region attribute
        uint32_t APX:1;             // bit:      15 access permissions extension
        uint32_t S:1;               // bit:      16 shareable
        uint32_t nG:1;              // bit:      17 not-global
        uint32_t one1:1;            // bit:      18 MUST BE ONE!
        uint32_t zero1:1;           // bit:      19 SHOULD BE ZERO!
        uint32_t base_addr_32to35:4;// bit:  20..23 Base address [35:32]
        uint32_t supersection_base_addr:8;// bit:  24..31 Supersection base address
    } fields;
};


/******************************************************************************
 * Descriptor table configuration
 ******************************************************************************/

// Memory region types according to Table B4-3
// (at Section B4.4.1 - ARM ARM (Version 2005))
enum class MemoryRegionType : uint32_t {
    //                                TEX           CB
    kStronglyOrdered             = (0b000 << 12) | (00 << 2),
    kSharedDevice                = (0b000 << 12) | (01 << 2),
    kWriteThroughNoWriteAllocate = (0b000 << 12) | (10 << 2),
    kWriteBackNoWriteAllocate    = (0b000 << 12) | (11 << 2),
    kNonCachable                 = (0b001 << 12) | (00 << 2),
    kWriteBackWriteAllocate      = (0b001 << 12) | (11 << 2),
    kNonSharedDevice             = (0b010 << 12) | (00 << 2)
};

class DescriptorTable {
private:
    uint32_t (&table_)[4096 * 4];

public:
    DescriptorTable(uint32_t (&destination)[4096 * 4]) :
        table_(destination) { }

    /**
     * @brief Creates page table entries for a given block of memory
     *
     * @param[in] physical_addr First physical address of the block
     * @param[in] virtual_addr First virtual address of the block
     * @param[in] length Length of the memory block (should be a multiple of 1MB)
     * @param[in] type Memory region type
     * @param[in] shareable Value of the VMSA shareable flag
     * @return Length of the mapped block
     */
    size_t map(
        uintptr_t physical_addr,
        uintptr_t virtual_addr,
        size_t length,
        MemoryRegionType type, bool shareable);

    /**
     * @brief Activates the descriptor table (but not the MMU!)
     *
     * @param[in] table Descriptor table to activate
     */
    static void enable(DescriptorTable &table);
};

} /* namespace vmsa */
} /* namespace mmu */
} /* namespace arm */
#endif /* ARMV6_MMU_VMSA_HPP */
