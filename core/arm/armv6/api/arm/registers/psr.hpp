/**
 * Copyright (c) 2020 Michael Zehrer
 * This file is part of ARCTOS
 *
 * @licence MIT
 * @brief   Program status registers (PSR)
 * @see     ARM ARM (Version 2005) - Section A2.5 (Program status registers)
 * @author  Michael Zehrer
 */
#ifndef ARMV6_REGISTERS_PSR_HPP
#define ARMV6_REGISTERS_PSR_HPP
#include <arm/common.hpp>
#include <stdint.h>

namespace arm {
namespace registers {

namespace psr {
enum class Mode : uint8_t {
    kUser       = 0b10000,
    kFIQ        = 0b10001,
    kIRQ        = 0b10010,
    kSupervisor = 0b10011,
    kAbort      = 0b10111,
    kUndefined  = 0b11011,
    kSystem     = 0b11111
};
} /* namspace psr */

union PSR_Type {
    uint32_t value;
    struct {
        uint32_t M:5;       // bit:    0..4 Mode field
        uint32_t T:1;       // bit:       5 Thumb execution state bit
        uint32_t F:1;       // bit:       6 MASK BIT: FIQ
        uint32_t I:1;       // bit:       7 MASK BIT: IRQ
        uint32_t A:1;       // bit:       8 MASK BIT: Asynchronous abort
        uint32_t E:1;       // bit:       9 Endianness execution state bit
        uint32_t res0:6;    // bit:  10..15 RESERVED
        uint32_t GE:4;      // bit:  16..19 Greater than or Equal flag
        uint32_t res1:4;    // bit:  20..23 RESERVED
        uint32_t J:1;       // bit:      24 Jazelle bit
        uint32_t res2:2;    // bit:  25..26 RESERVED
        uint32_t Q:1;       // bit:      27 Comulative saturation bit
        uint32_t V:1;       // bit:      28 COND FLAG: Overflow
        uint32_t C:1;       // bit:      29 COND FLAG: Carry
        uint32_t Z:1;       // bit:      30 COND FLAG: Zero
        uint32_t N:1;       // bit:      31 COND FLAG: Negative
    } fields;

    /**
     * Checks the IRQ-Mask-Bit to find out if interrupts are enabled
     * @return TRUE = IRQ enabled; FALSE = otherwise
     */
    inline bool isIRQEnabled() {
        return (this->fields.I == 0 ? true : false);
    }
    /**
     * Checks the FIQ-Mask-Bit to find out if fast interrupts are enabled
     * @return TRUE = FIQ enabled; FALSE = otherwise
     */
    inline bool isFIQEnabled() {
        return (this->fields.F == 0 ? true : false);
    }
    /**
     * Checks whether interrupts or fast interrupts are (globally) enabled
     * @return TRUE = IRQ and/or FIQ enabled; FALSE = otherwise
     */
    inline bool areInterruptsEnabled() {
        return ((this->fields.I == 0 || this->fields.F == 0) ? true : false);
    }

    static inline PSR_Type readCPSR() {
        PSR_Type val;
        asm volatile("mrs %[val], cpsr_all\n\t" : [val]"=r"(val.value));
        return val;
    }
    static inline void writeCPSR(PSR_Type val) {
        asm volatile("msr cpsr_all, %[val]\n\t" :: [val]"r"(val.value));
    }
};

} /* namespace registers */
} /* namespace arm */
#endif /* ARMV6_REGISTERS_PSR_HPP */
