/**
 * Copyright (c) 2020 Michael Zehrer
 * This file is part of ARCTOS
 *
 * @licence MIT
 * @brief   System Control coprocessor - Control register (SCTLR)
 * @see     ARM ARM (Version 2005) - Section B3.4.1 (Control register)
 * @author  Michael Zehrer
 */
#ifndef ARMV6_REGISTERS_SCTLR_HPP
#define ARMV6_REGISTERS_SCTLR_HPP
#include <arm/common.hpp>
#include <stdint.h>

namespace arm {
namespace registers {

#define SCTLR_M     BIT(0)
#define SCTLR_C     BIT(2)
#define SCTLR_Z     BIT(11)
#define SCTLR_I     BIT(12)
#define SCTLR_L2    BIT(26)

union SCTLR_Type {
    uint32_t value;
    struct {
        uint32_t M:1;       // bit:       0 MMU or Protection Unit enable
        uint32_t A:1;       // bit:       1 Strict allignment
        uint32_t C:1;       // bit:       2 L1 unified/data cache enable
        uint32_t W:1;       // bit:       3 Write buffer enable
        uint32_t P:1;       // bit:       4 ignore
        uint32_t D:1;       // bit:       5 ignore
        uint32_t L:1;       // bit:       6 ignore
        uint32_t B:1;       // bit:       7 configure big-endian memory system
        uint32_t S:1;       // bit:       8 System protection bit
        uint32_t R:1;       // bit:       9 ROM protection bit
        uint32_t F:1;       // bit:      10 IMPLEMENTATION DEFINED
        uint32_t Z:1;       // bit:      11 Program flow prediction enable
        uint32_t I:1;       // bit:      12 L1 instruction cache enable
        uint32_t V:1;       // bit:      13 High exception vectors select
        uint32_t RR:1;      // bit:      14 Cache predictable strategy
        uint32_t L4:1;      // bit:      15 DEPRECATED
        uint32_t res0:5;    // bit:  16..20 RESERVED
        uint32_t FI:1;      // bit:      21 Low interrupt latency configuration enable
        uint32_t U:1;       // bit:      22 Unaligned loads and stores enable
        uint32_t XP:1;      // bit:      23 Extended page table configure
        uint32_t VE:1;      // bit:      24 Configure vectored interrupts IMPLEMENTATION DEFINED
        uint32_t EE:1;      // bit:      25 Mixed Endian exception entry
        uint32_t L2:1;      // bit:      26 L2 unified cache enable
        uint32_t UNP_SBZP:1;// bit:  27..31 RESERVED
    } fields;

    static inline SCTLR_Type read() {
        SCTLR_Type val;
        asm volatile("mrc p15, #0, %[val], c1, c0, #0\n\t" : [val]"=r"(val.value));
        return val;
    }
    static inline void write(SCTLR_Type val) {
        asm volatile("mcr p15, #0, %[val], c1, c0, #0\n\t" :: [val]"r"(val.value));
    }
};

} /* namespace registers */
} /* namespace arm */
#endif /* ARMV6_REGISTERS_SCTLR_HPP */
