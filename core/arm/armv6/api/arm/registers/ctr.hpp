/**
 * Copyright (c) 2020 Michael Zehrer
 * This file is part of ARCTOS
 *
 * @licence MIT
 * @brief   Cache type register (CTR)
 * @see     ARM ARM (Version 2005) - Section B3.3.2
 * @author  Michael Zehrer
 */
#ifndef ARMV6_REGISTERS_CTR_HPP
#define ARMV6_REGISTERS_CTR_HPP
#include <arm/common.hpp>
#include <stdint.h>

namespace arm {
namespace registers {

namespace ctr {
/* Mapping of the cache line len field to byte lengths */
inline uint8_t const kCacheLineLenght[] = {
    /* 0b00 = */ 8,
    /* 0b01 = */ 16,
    /* 0b10 = */ 32,
    /* 0b11 = */ 64
};

/* Mapping of the cache size field to byte lengths */
inline uint32_t const kCacheSize[2][9] = {
    /* M == 0 */ {
        static_cast<uint32_t>(0.5  * 1024),
        static_cast<uint32_t>(1    * 1024),
        static_cast<uint32_t>(2    * 1024),
        static_cast<uint32_t>(4    * 1024),
        static_cast<uint32_t>(8    * 1024),
        static_cast<uint32_t>(16   * 1024),
        static_cast<uint32_t>(32   * 1024),
        static_cast<uint32_t>(64   * 1024),
        static_cast<uint32_t>(128  * 1024)
    },
    /* M == 1 */ {
        static_cast<uint32_t>(0.75 * 1024),
        static_cast<uint32_t>(1.5  * 1024),
        static_cast<uint32_t>(3    * 1024),
        static_cast<uint32_t>(6    * 1024),
        static_cast<uint32_t>(12   * 1024),
        static_cast<uint32_t>(24   * 1024),
        static_cast<uint32_t>(48   * 1024),
        static_cast<uint32_t>(96   * 1024),
        static_cast<uint32_t>(192  * 1024)
    }
};

/* Mapping of the cache assoc field */
inline uint8_t const kCacheAssociativity[2][8] = {
    /* M == 0 */ {
        1,  /*   1-way (direct mapped) */
        2,  /*   2-way */
        4,  /*   4-way */
        8,  /*   8-way */
        16, /*  16-way */
        32, /*  32-way */
        64, /*  64-way */
        128 /* 128-way */
    },
    /* M == 1 */ {
        0,  /*   cache absent */
        3,  /*   3-way */
        6,  /*   6-way */
        12, /*  12-way */
        24, /*  24-way */
        48, /*  48-way */
        96, /*  96-way */
        192 /* 192-way */
    }
};
} /* namespace ctr */

union CTR_Type {
    uint32_t value;
    struct {
        struct {
            uint32_t len:2;     // bit:    0..1 Cache line length
            uint32_t M:1;       // bit:       2 M bit
            uint32_t assoc:3;   // bit:    3..5 Cache associativty (tied with M)
            uint32_t size:4;    // bit:    6..9 Cache size (tied with M)
            uint32_t O:1;       // bit:      10 RESERVED
            uint32_t P:1;       // bit:      11 restriction applies
        } isize;
        struct {
            uint32_t len:2;     // bit:  12..13 Cache line length
            uint32_t M:1;       // bit:      14 M bit
            uint32_t assoc:3;   // bit:  15..17 Cache associativty (tied with M)
            uint32_t size:4;    // bit:  18..21 Cache size (tied with M)
            uint32_t O:1;       // bit:      22 RESERVED
            uint32_t P:1;       // bit:      23 restriction applies
        } dsize;
        uint32_t S:1;           // bit:      24 S == 0 unified cache; S == 1 separate instr. and data cache
        uint32_t ctype:4;       // bit:  25..28 Cache type value
        uint32_t zero:3;        // bit:  29..31 ZEROS
    } fields;

    static inline CTR_Type read() {
        CTR_Type val;
        asm volatile("mrc p15, #0, %[val], c0, c0, #1\n\t" : [val]"=r"(val.value));
        return val;
    }
};

} /* namespace registers */
} /* namespace arm */
#endif /* ARMV6_REGISTERS_CTR_HPP */
