/**
 * Copyright (c) 2020 Michael Zehrer
 * This file is part of ARCTOS
 *
 * @licence MIT
 * @brief   Common types and definitions
 * @author  Michael Zehrer
 */
#ifndef ARMV6_COMMON_HPP
#define ARMV6_COMMON_HPP

namespace arm {

#ifndef __R
#define __R     volatile const  /* read only    */
#endif
#ifndef __W
#define __W     volatile        /* write only   */
#endif
#ifndef __RW
#define __RW    volatile        /* read / write */
#endif

#ifndef BIT
  #define BIT(n) (1 << (n))
#endif
#ifndef STR
  #define STR_HELPER(x) #x
  #define STR(x) STR_HELPER(x)
#endif

#define __DMB()     asm volatile ("mcr p15, #0, %0, c7, c10, #5" :: "r"(0) : "memory")
#define __DSB()     asm volatile ("mcr p15, #0, %0, c7, c10, #4" :: "r"(0) : "memory")
#define __ISB()     asm volatile ("mcr p15, #0, %0, c7, c5 , #4" :: "r"(0) : "memory")

} /* namespace arm */
#endif /* ARMV6_COMMON_HPP */
