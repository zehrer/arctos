/**
 * Copyright (c) 2020 Michael Zehrer
 * This file is part of ARCTOS
 *
 * @licence MIT
 * @brief   Data cache maintenance functions
 * @author  Michael Zehrer
 */
#ifndef ARMV6_CACHE_DATA_HPP
#define ARMV6_CACHE_DATA_HPP

#include <stdint.h>
#include <stddef.h>

namespace arm {
namespace cache {
namespace data {

/**
 * @brief Enable data cache
 */
void enable(void) asm("arm_cache_data_enable");

/**
 * @brief Disable data cache
 */
void disable(void) asm("arm_cache_data_disable");

/**
 * @brief Invalidates the whole data cache
 */
void invalidate(void) asm("arm_cache_data_invalidate");

/**
 * @brief Invalidates the cache line belonging to the given address
 */
void invalidate(uintptr_t addr);

/**
 * @brief Invalidates the cache lines belonging to the given address range
 */
void invalidate(uintptr_t addr_start, uintptr_t addr_end);

} /* namespace data */
} /* namespace cache */
} /* namespace arm */
#endif /* ARMV6_CACHE_DATA_HPP */
