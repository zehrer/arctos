/**
 * Copyright (c) 2020 Michael Zehrer
 * This file is part of ARCTOS
 *
 * @licence MIT
 * @brief   Cache maintenance functions
 * @author  Michael Zehrer
 */
#ifndef ARMV6_CACHE_MAINTENANCE_HPP
#define ARMV6_CACHE_MAINTENANCE_HPP
#include "data.hpp"
#include "instruction.hpp"

namespace arm {
namespace cache {

/**
 * @brief Disables the cache in an controlled manner
 * In detail it cleans, invalidates and disables the whole cache system
 */
void shutdown(void) asm("arm_cache_shutdown");

} /* namespace cache */
} /* namespace arm */
#endif /* ARMV6_CACHE_MAINTENANCE_HPP */
