/**
 * Copyright (c) 2020 Michael Zehrer
 * This file is part of ARCTOS
 *
 * @licence MIT
 * @brief   Instruction cache maintenance functions
 * @author  Michael Zehrer
 */
#ifndef ARMV6_CACHE_INSTRUCTION_HPP
#define ARMV6_CACHE_INSTRUCTION_HPP

namespace arm {
namespace cache {
namespace instruction {

/**
 * @brief Enable instruction cache
 */
void enable(void) asm("arm_cache_instruction_enable");

/**
 * @brief Disable instruction cache
 */
void disable(void) asm("arm_cache_instruction_disable");

/**
 * @brief Invalidates the whole instruction cache
 */
void invalidate(void) asm("arm_cache_instruction_invalidate");

} /* namespace instruction */
} /* namespace cache */
} /* namespace arm */
#endif /* ARMV6_CACHE_INSTRUCTION_HPP */
