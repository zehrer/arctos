/**
 * Copyright (c) 2020 Michael Zehrer
 * This file is part of ARCTOS
 *
 * @licence MIT
 * @brief   Cache maintenance functions
 * @author  Michael Zehrer
 */
#include <arm/cache/maintenance.hpp>
#include <arm/registers/sctlr.hpp>
#include <arm/common.hpp>

namespace arm {
namespace cache {

void shutdown(void) {
    asm volatile(
        "mov r1, #0                     \n\t"
        "mcr p15, #0, r1, c7, c10,# 5   \n\t"// DMB
    "1:                                 \n\t"
        "mcr p15, #0, r1, c7, c10, #0   \n\t"// clean entire data cache
        "mrs r0, cpsr                   \n\t"
        "cpsid iaf                      \n\t"// disable interrupts
        "mrc p15, #0, r1, c7, c10, #6   \n\t"// r1 = cache dirty status register
        "ands r1, r1, #01               \n\t"// check if it is clean
        "mov r1, #0                     \n\t"
        "beq 2f                         \n\t"
        "msr cpsr, r0                   \n\t"// re-enable interrupts
        "b 1b                           \n\t"// clean the cache again
    "2:                                 \n\t"
        "mrc p15, #0, r2, c1, c0, #0    \n\t"// r2 = system control register
        "bic r2, r2, #" STR(SCTLR_C)  " \n\t"// disable L1 unified/data cache
        "bic r2, r2, #" STR(SCTLR_Z)  " \n\t"// disable Program flow prediction
        "bic r2, r2, #" STR(SCTLR_I)  " \n\t"// disable L1 instruction cache
        "bic r2, r2, #" STR(SCTLR_L2) " \n\t"// disable L2 unified cache
        "mcr p15, #0, r2, c1, c0, #0    \n\t"// system control register = r2
        "mcr p15, #0, r1, c7, c7, #0    \n\t"// invalidate both instruction and data caches or unified cache
        "mcr p15, #0, r1, c7, c10, #4   \n\t"// DSB
        "msr cpsr, r0                   \n\t"// re-enable interrupts
    :::"r0", "r1", "r2", "memory");
}

} /* namespace cache */
} /* namespace arm */
