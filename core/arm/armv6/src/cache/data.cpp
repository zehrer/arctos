/**
 * Copyright (c) 2020 Michael Zehrer
 * This file is part of ARCTOS
 *
 * @licence MIT
 * @brief   Data cache maintenance functions
 * @author  Michael Zehrer
 */
#include <arm/cache/data.hpp>
#include <arm/registers/sctlr.hpp>
#include <arm/registers/ctr.hpp>
#include <arm/common.hpp>

namespace arm {
namespace cache {
namespace data {

void enable(void) {
    __DMB();
    auto sctlr = ::arm::registers::SCTLR_Type::read();
    sctlr.fields.C  = 1;
    sctlr.fields.L2 = 1;
    ::arm::registers::SCTLR_Type::write(sctlr);
    __DSB();
}

void disable(void) {
    __DMB();
    auto sctlr = ::arm::registers::SCTLR_Type::read();
    sctlr.fields.C  = 0;
    sctlr.fields.L2 = 0;
    ::arm::registers::SCTLR_Type::write(sctlr);
    __DSB();
}

void invalidate(void) {
    // Invalidate entire data cache
    asm volatile("mcr p15, #0, %0, c7, c6, #0\n\t" :: "r"(0));
}

void invalidate(uintptr_t addr) {
    // Invalidate data cache line by MVA
    asm volatile("mcr p15, #0, %[addr], c7, c6, #1\n\t" :: [addr]"r"(addr));
}

void invalidate(uintptr_t addr_start, uintptr_t addr_end) {
    auto ctr = ::arm::registers::CTR_Type::read();
    uint8_t linelen = ::arm::registers::ctr::kCacheLineLenght[ctr.fields.dsize.len];

    // align the base address to the line lenght
    addr_start = addr_start & (~(static_cast<uintptr_t>(linelen - 1)));
    while (addr_start < addr_end) {
        asm volatile(
            "mcr p15, #0, %[addr], c7, c6, #1 \n\t" // Invalidate data cache line
        :: [addr]"r"(addr_start));
        addr_start += linelen;
    }
}

} /* namespace data */
} /* namespace cache */
} /* namespace arm */
