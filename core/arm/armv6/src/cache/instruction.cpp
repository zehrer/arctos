/**
 * Copyright (c) 2020 Michael Zehrer
 * This file is part of ARCTOS
 *
 * @licence MIT
 * @brief   Instruction cache maintenance functions
 * @author  Michael Zehrer
 */
#include <arm/cache/instruction.hpp>
#include <arm/registers/sctlr.hpp>
#include <arm/common.hpp>

namespace arm {
namespace cache {
namespace instruction {

void enable(void) {
    __DMB();
    auto sctlr = ::arm::registers::SCTLR_Type::read();
    sctlr.fields.I = 1;
    sctlr.fields.Z = 1;
    ::arm::registers::SCTLR_Type::write(sctlr);
    __ISB();
}

void disable(void) {
    __DMB();
    auto sctlr = ::arm::registers::SCTLR_Type::read();
    sctlr.fields.I = 0;
    sctlr.fields.Z = 0;
    ::arm::registers::SCTLR_Type::write(sctlr);
    __ISB();
}

void invalidate(void) {
    // According to ARM Ltd. a simple Invalidate Instruction Cache operation can fail.
    // Because of this, we need the following "patch"
    asm volatile(
        "mov r0, #0                 \n\t"
        "mrs r1, cpsr               \n\t"
        "cpsid ifa                  \n\t"
        ".rept 4                    \n\t"
        "mcr p15, #0, r0, c7, c5, #0\n\t" // Invalidate entire instruction cache
        ".endr                      \n\t"
        "msr cpsr_cx, r1            \n\t"
        ".rept 11                   \n\t"
        "nop                        \n\t"
        ".endr                      \n\t"
    ::: "r0", "r1", "memory");
}

} /* namespace instruction */
} /* namespace cache */
} /* namespace arm */
