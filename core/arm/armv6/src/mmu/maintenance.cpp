/**
 * Copyright (c) 2020 Michael Zehrer
 * This file is part of ARCTOS
 *
 * @licence MIT
 * @brief   MMU maintenance functions
 * @author  Michael Zehrer
 */
#include <arm/mmu/maintenance.hpp>
#include <arm/registers/sctlr.hpp>
#include <arm/common.hpp>

namespace arm {
namespace mmu {

void enable(void) {
    __DMB();
    auto sctlr = ::arm::registers::SCTLR_Type::read();
    sctlr.fields.M = 1;
    ::arm::registers::SCTLR_Type::write(sctlr);
    __DSB();
}

void disable(void) {
    __DMB();
    auto sctlr = ::arm::registers::SCTLR_Type::read();
    sctlr.fields.M = 0;
    ::arm::registers::SCTLR_Type::write(sctlr);
    __DSB();
}

} /* namespace mmu */
} /* namespace arm */
