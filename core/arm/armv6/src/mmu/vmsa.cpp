/**
 * Copyright (c) 2020 Michael Zehrer
 * This file is part of ARCTOS
 *
 * @licence MIT
 * @brief   Virtual Memory System Architecture (VMSA)
 * @author  Michael Zehrer
 */
#include <arm/mmu/vmsa.hpp>
#include <arm/registers/sctlr.hpp>
#include <arm/common.hpp>

namespace arm {
namespace mmu {
namespace vmsa {

size_t DescriptorTable::map(uintptr_t physical_addr, uintptr_t virtual_addr, size_t length,
        MemoryRegionType type, bool shareable) {
    Section sec;
    sec.value = static_cast<uint32_t>(type);
    sec.fields.one = 1;

    if (shareable)
        sec.fields.S = 1;

    // Only the top 12 bits are needed as base addresses
    uint32_t virtual_base = virtual_addr >> 20;
    uint32_t physical_base = physical_addr >> 20;
    uint32_t entries = length >> 20;
    length = entries << 20;

    for (; entries > 0; ++physical_base, ++virtual_base, --entries) {
        sec.fields.base_addr = physical_base;
        this->table_[virtual_base] = sec.value;
    }

    return length;
}

void DescriptorTable::enable(DescriptorTable &desc) {
    asm volatile (
        // (DACR) Domain Access Control Register
        // Set 11 to each domain
        // 11 = Manager. Accesses are not checked against the permission bits in the translation tables.
        "mvn r0, #0\n\t"
        "mcr p15, #0, r0, c3, c0, #0\n\t"
        // Set N = 0 in the Translation Table Base Control Register
        "mov r0, #0\n\t"
        "mcr p15, #0, r0, c2, c0, #2\n\t"
        // (TTBR0) Translation Table Base Register 0
        "mcr p15, #0, %[table_addr], c2, c0, #0\n\t"
        // (TLBIALL) TLB Invalidate All entries (value in r0 is ignored)
        "mcr p15, #0, r0, c8, c7, #0\n\t"
        :: [table_addr]"r"(&desc.table_) : "r0", "memory");
}

} /* namespace vmsa */
} /* namespace mmu */
} /* namespace arm */
