/**
 * @file
 * @copyright Copyright (c) 2017 STMicroelectronics. All rights reserved.
 * @license BSD 3-Clause
 * @attention based on the cmsis device peripheral access layer from STM32Cube
 */
/**
 * @file
 * @copyright Copyright (c) 2021 Michael Zehrer. This file is part of ARCTOS
 * @license MIT
 * @brief CMSIS-Core(M) Device Peripheral Access Layer Header File for STM32F4xx devices
 */
#ifndef SYSTEM_STM32F4XX_H
#define SYSTEM_STM32F4XX_H

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Exception / Interrupt Handler Function Prototype
 */
typedef void(*VECTOR_TABLE_Type)(void);

/**
 * @brief System Clock Frequency (Core Clock)
 *
 * This variable is updated in three ways:
 *   1) by calling CMSIS function SystemCoreClockUpdate()
 *   2) by calling HAL API function HAL_RCC_GetSysClockFreq()
 *   3) each time HAL_RCC_ClockConfig() is called to configure the system clock frequency
 *      Note: If you use this function to configure the system clock; then there
 *            is no need to call the 2 first functions listed above, since SystemCoreClock
 *            variable is updated automatically.
 */
extern uint32_t SystemCoreClock;

/**
 * @brief AHB prescalers table values
 */
extern const uint8_t AHBPrescTable[16];

/**
 * @brief APB prescalers table values
 */
extern const uint8_t APBPrescTable[8];

/**
 * @brief Setup the microcontroller system.
 *
 * Initialize the System and update the SystemCoreClock variable.
 */
extern void SystemInit(void);

/**
 * @brief Update SystemCoreClock variable.
 *
 * Updates the SystemCoreClock with current core Clock retrieved from cpu registers.
 */
extern void SystemCoreClockUpdate(void);

#ifdef __cplusplus
} /* extern "C" */
#endif

#endif /* SYSTEM_STM32F4XX_H */
