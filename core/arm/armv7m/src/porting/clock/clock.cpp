/**
 * @file
 * @copyright Copyright (c) 2022 Michael Zehrer. This file is part of ARCTOS
 * @license MIT
 * @brief default system clock implementation for ARMv7-M targets
 */
#include <stdint.h>
#include <arctos/params.h>
#include <arctos/porting/all.hpp>
#include <arctos/time.hpp>
#include CMSIS_DEVICE

namespace arctos {
namespace porting {
namespace clock {

extern "C" {
// System Core Clock Variable from CMSIS
extern uint32_t SystemCoreClock;

static volatile int64_t g_time_rough;
static          float   g_time_per_tick;
static          int64_t g_time_per_interrupt;
static          bool    g_schedule_on_sys_tick = false;

void SysTick_Handler() {
    /* update 'g_time_rough' */
    g_time_rough += g_time_per_interrupt;

    if (!g_schedule_on_sys_tick)
        return;

    /* reqest scheduling (scheduling is done via PEND_SV) */
    SCB->ICSR |= SCB_ICSR_PENDSVSET_Msk;
}
} // end extern "C"

void init(void) {
    // TODO RCC clock configuration?!

    g_time_rough            = 0UL;
    g_schedule_on_sys_tick  = false;

    double slot_size = static_cast<double>(ARCTOS_SCHEDULER_TIME_SLOT_SIZE) /
                       static_cast<double>(SECONDS);
    uint32_t ticks = static_cast<uint32_t>(static_cast<double>(SystemCoreClock) * slot_size);

    /* check if the reload value (and therefore the interval) is even possible */
    if ((ticks - 1) > SysTick_LOAD_RELOAD_Msk) {
        ticks = SysTick_LOAD_RELOAD_Msk;
    }

    g_time_per_tick      = static_cast<float>(SECONDS) / static_cast<float>(SystemCoreClock);
    g_time_per_interrupt = static_cast<int64_t>(g_time_per_tick * static_cast<float>(ticks));

    /* set reload register */
    SysTick->LOAD = ticks - 1;
    /* set Priority for Systick Interrupt */
    NVIC_SetPriority(SysTick_IRQn, 0);
    /* load the SysTick Counter Value */
    SysTick->VAL  = 0UL;
    SysTick->CTRL =
        /* use processor clock as source */
        SysTick_CTRL_CLKSOURCE_Msk |
        /* enable SysTick IRQ */
        SysTick_CTRL_TICKINT_Msk   |
        /* enable SysTick Timer */
        SysTick_CTRL_ENABLE_Msk;
}

void activateSystemTick(void) {
    g_schedule_on_sys_tick = true;
}
void deactivateSystemTick(void) {
    g_schedule_on_sys_tick = false;
}

int64_t now(void) {
    int64_t time_rough;
    uint32_t ticks_passed;

    /* read 'g_time_rough' twice, to make sure it has not changed while reading counter value */
    do {
        time_rough   = g_time_rough;
        ticks_passed = SysTick->LOAD - SysTick->VAL;
    } while (time_rough != g_time_rough);

    return time_rough + static_cast<int64_t>(static_cast<float>(ticks_passed) * g_time_per_tick);
}

int64_t rough(void) {
    return g_time_rough;
}

} /* namespace clock */
} /* namespace porting */
} /* namespace arctos */
