/**
 * @file
 * @copyright Copyright (c) 2022 Michael Zehrer. This file is part of ARCTOS
 * @license MIT
 * @brief atomic compare and exchange
 */
#include <stdint.h>
#include <arctos/porting/all.hpp>
#include CMSIS_DEVICE

namespace arctos {
namespace porting {

int32_t atomicCompareAndExchange(int32_t *v, int32_t o, int32_t n) {
    uint32_t prim = __get_PRIMASK();
    __disable_irq();

    int32_t prev = *v;
    if (prev == o)
        (*v) = n;

    if (!prim) {
        __enable_irq();
    }

    return prev;
}

int64_t atomicCompareAndExchange(int64_t *v, int64_t o, int64_t n) {
    uint32_t prim = __get_PRIMASK();
    __disable_irq();

    int64_t prev = *v;
    if (prev == o)
        (*v) = n;

    if (!prim) {
        __enable_irq();
    }

    return prev;
}

} /* namespace porting */
} /* namespace arctos */
