#
# This script is auto-generated
# DO NOT EDIT
#
# Generated on 2024-11-17 13:49
#
set(ARCTOS_TARGET                        "stm32")

################### BEGIN-TARGET: stm32 ####################
set(ARCTOS_LOG_LEVEL                     ARCTOS_LOG_LEVEL_DEBUG)
set(ARCTOS_LOG_ENABLE_COLOR              on)
set(ARCTOS_SCHEDULER_TIME_SLOT_SIZE      0x05F5E100)
set(ARM_ENABLE_FPU                       on)
set(ARM_EXECUTION_STATE                  AArch32)
set(STM_BOARD                            STM_BOARD_BLACK_PILL_F401V3)
set(STM_LOG                              STM_LOG_SERIAL_IDX02)

set(ARCTOS_NUMBER_OF_CORES               0x00000001)
set(ARCTOS_BOARDNAME                     "WeAct BlackPill (STM32F401) v3.0")
set(ARCTOS_SOC                           "STM32F401CE")
set(ARM_ARCHITECTURE_PROFILE             ARMv7E-M)
set(ARM_ARCHITECTURE_VERSION             0x00000007)
set(ARM_CORE                             CORTEX-M4)
target_link_options(iface_arctos INTERFACE LINKER:--defsym=__ROM_BASE=0x08000000)
target_link_options(iface_arctos INTERFACE LINKER:--defsym=__ROM_SIZE=0x00080000)
target_link_options(iface_arctos INTERFACE LINKER:--defsym=__RAM_BASE=0x20000000)
target_link_options(iface_arctos INTERFACE LINKER:--defsym=__RAM_SIZE=0x00018000)
set(HSI_VALUE                            0x00F42400)
set(HSE_VALUE                            0x017D7840)
set(LSI_VALUE                            0x00007D00)
set(LSE_VALUE                            0x00008000)

  #################### BEGIN-SEGMENT: arm ####################
    ################ BEGIN-SEGMENT: bare-metal #################
      ################ BEGIN-SEGMENT: independent ################
      add_subdirectory("${CMAKE_CURRENT_LIST_DIR}/../../core/independent")
      ################# END-SEGMENT: independent #################

    add_subdirectory("${CMAKE_CURRENT_LIST_DIR}/../../core/bare-metal")
    ################# END-SEGMENT: bare-metal ##################

  add_subdirectory("${CMAKE_CURRENT_LIST_DIR}/../../core/arm")
  ##################### END-SEGMENT: arm #####################

add_subdirectory("${CMAKE_CURRENT_LIST_DIR}/../../ports/stm32")
#################### END-TARGET: stm32 #####################

############### BEGIN-MODULE: system_run_arm ###############
set(DEFAULT_TASK_PRIORITY                0x0000000A)
set(DEFAULT_TASK_STACKSIZE               0x00000400)
set(MAX_NUMBER_OF_TASKS                  0x00000008)

  ################# BEGIN-MODULE: system_run #################
    ################### BEGIN-MODULE: system ###################
    set(UNIT "module_system")
    add_library(module_system OBJECT "${CMAKE_CURRENT_LIST_DIR}/arctos/system/params.h")
    add_subdirectory("${CMAKE_CURRENT_LIST_DIR}/../../modules/system")
    target_link_libraries(module_system iface_arctos)

    add_library(iface_module_system INTERFACE)
    target_link_libraries(iface_module_system INTERFACE module_system $<TARGET_OBJECTS:module_system>)
    #################### END-MODULE: system ####################

  set(UNIT "module_system_run")
  add_library(module_system_run OBJECT "${CMAKE_CURRENT_LIST_DIR}/arctos/system/run/params.h")
  add_subdirectory("${CMAKE_CURRENT_LIST_DIR}/../../modules/system/#run")
  target_link_libraries(module_system_run module_system)

  add_library(iface_module_system_run INTERFACE)
  target_link_libraries(iface_module_system_run INTERFACE module_system_run $<TARGET_OBJECTS:module_system_run>)
  ################## END-MODULE: system_run ##################

set(UNIT "module_system_run_arm")
add_library(module_system_run_arm OBJECT "${CMAKE_CURRENT_LIST_DIR}/arctos/system/run/arm/params.h")
add_subdirectory("${CMAKE_CURRENT_LIST_DIR}/../../modules/system/#run/#arm")
target_link_libraries(module_system_run_arm module_system_run)

add_library(iface_module_system_run_arm INTERFACE)
target_link_libraries(iface_module_system_run_arm INTERFACE module_system_run_arm $<TARGET_OBJECTS:module_system_run_arm>)
################ END-MODULE: system_run_arm ################

############## BEGIN-MODULE: io_serial_stm32 ###############
set(SERIAL_BUFFER_SIZE                   0x00000100)
set(SERIAL_ENABLE_IDX1                   on)
set(SERIAL_ENABLE_IDX2                   on)
set(SERIAL_ENABLE_IDX6                   on)

set(HAL_CORTEX_MODULE_ENABLED            on)
set(HAL_DMA_MODULE_ENABLED               on)
set(HAL_FLASH_MODULE_ENABLED             on)
set(HAL_GPIO_MODULE_ENABLED              on)
set(HAL_PWR_MODULE_ENABLED               on)
set(HAL_RCC_MODULE_ENABLED               on)
set(HAL_ADC_MODULE_ENABLED               off)
set(HAL_CAN_MODULE_ENABLED               off)
set(HAL_CAN_LEGACY_MODULE_ENABLED        off)
set(HAL_CRC_MODULE_ENABLED               off)
set(HAL_CEC_MODULE_ENABLED               off)
set(HAL_CRYP_MODULE_ENABLED              off)
set(HAL_DAC_MODULE_ENABLED               off)
set(HAL_DCMI_MODULE_ENABLED              off)
set(HAL_DMA2D_MODULE_ENABLED             off)
set(HAL_ETH_MODULE_ENABLED               off)
set(HAL_NAND_MODULE_ENABLED              off)
set(HAL_NOR_MODULE_ENABLED               off)
set(HAL_PCCARD_MODULE_ENABLED            off)
set(HAL_SRAM_MODULE_ENABLED              off)
set(HAL_SDRAM_MODULE_ENABLED             off)
set(HAL_HASH_MODULE_ENABLED              off)
set(HAL_EXTI_MODULE_ENABLED              off)
set(HAL_I2C_MODULE_ENABLED               off)
set(HAL_SMBUS_MODULE_ENABLED             off)
set(HAL_I2S_MODULE_ENABLED               off)
set(HAL_IWDG_MODULE_ENABLED              off)
set(HAL_LTDC_MODULE_ENABLED              off)
set(HAL_DSI_MODULE_ENABLED               off)
set(HAL_QSPI_MODULE_ENABLED              off)
set(HAL_RNG_MODULE_ENABLED               off)
set(HAL_RTC_MODULE_ENABLED               off)
set(HAL_SAI_MODULE_ENABLED               off)
set(HAL_SD_MODULE_ENABLED                off)
set(HAL_SPI_MODULE_ENABLED               off)
set(HAL_TIM_MODULE_ENABLED               off)
set(HAL_UART_MODULE_ENABLED              off)
set(HAL_USART_MODULE_ENABLED             off)
set(HAL_IRDA_MODULE_ENABLED              off)
set(HAL_SMARTCARD_MODULE_ENABLED         off)
set(HAL_WWDG_MODULE_ENABLED              off)
set(HAL_PCD_MODULE_ENABLED               off)
set(HAL_HCD_MODULE_ENABLED               off)
set(HAL_FMPI2C_MODULE_ENABLED            off)
set(HAL_FMPSMBUS_MODULE_ENABLED          off)
set(HAL_SPDIFRX_MODULE_ENABLED           off)
set(HAL_DFSDM_MODULE_ENABLED             off)
set(HAL_LPTIM_MODULE_ENABLED             off)
set(HAL_MMC_MODULE_ENABLED               off)

  ################# BEGIN-MODULE: io_serial ##################
    ##################### BEGIN-MODULE: io #####################
    set(UNIT "module_io")
    add_library(module_io OBJECT "${CMAKE_CURRENT_LIST_DIR}/arctos/io/params.h")
    add_subdirectory("${CMAKE_CURRENT_LIST_DIR}/../../modules/io")
    target_link_libraries(module_io iface_arctos)

    add_library(iface_module_io INTERFACE)
    target_link_libraries(iface_module_io INTERFACE module_io $<TARGET_OBJECTS:module_io>)
    ###################### END-MODULE: io ######################

  set(UNIT "module_io_serial")
  add_library(module_io_serial OBJECT "${CMAKE_CURRENT_LIST_DIR}/arctos/io/serial/params.h")
  add_subdirectory("${CMAKE_CURRENT_LIST_DIR}/../../modules/io/#serial")
  target_link_libraries(module_io_serial module_io)

  add_library(iface_module_io_serial INTERFACE)
  target_link_libraries(iface_module_io_serial INTERFACE module_io_serial $<TARGET_OBJECTS:module_io_serial>)
  ################## END-MODULE: io_serial ###################

  ################ BEGIN-MODULE: io_stm32hal #################
  set(UNIT "module_io_stm32hal")
  add_library(module_io_stm32hal OBJECT "${CMAKE_CURRENT_LIST_DIR}/arctos/io/stm32hal/params.h")
  add_subdirectory("${CMAKE_CURRENT_LIST_DIR}/../../modules/io/#stm32hal")
  target_link_libraries(module_io_stm32hal iface_arctos)

  add_library(iface_module_io_stm32hal INTERFACE)
  target_link_libraries(iface_module_io_stm32hal INTERFACE module_io_stm32hal $<TARGET_OBJECTS:module_io_stm32hal>)
  ################# END-MODULE: io_stm32hal ##################

set(UNIT "module_io_serial_stm32")
add_library(module_io_serial_stm32 OBJECT "${CMAKE_CURRENT_LIST_DIR}/arctos/io/serial/stm32/params.h")
add_subdirectory("${CMAKE_CURRENT_LIST_DIR}/../../modules/io/#serial/#stm32")
target_link_libraries(module_io_serial_stm32 module_io_serial)
target_link_libraries(module_io_serial_stm32 module_io_stm32hal)

add_library(iface_module_io_serial_stm32 INTERFACE)
target_link_libraries(iface_module_io_serial_stm32 INTERFACE module_io_serial_stm32 $<TARGET_OBJECTS:module_io_serial_stm32>)
############### END-MODULE: io_serial_stm32 ################

################ BEGIN-MODULE: io_pin_stm32 ################
set(PIN_ENABLE_GPIOA                     on)
set(PIN_ENABLE_GPIOB                     on)
set(PIN_ENABLE_GPIOC                     on)
set(PIN_ENABLE_GPIOD                     off)
set(PIN_ENABLE_GPIOE                     off)
set(PIN_ENABLE_GPIOF                     off)
set(PIN_ENABLE_GPIOG                     off)
set(PIN_ENABLE_GPIOH                     off)
set(PIN_ENABLE_GPIOI                     off)
set(PIN_ENABLE_GPIOJ                     off)
set(PIN_ENABLE_GPIOK                     off)
set(PIN_ENABLE_GPIOZ                     off)

set(HAL_CORTEX_MODULE_ENABLED            on)
set(HAL_DMA_MODULE_ENABLED               on)
set(HAL_FLASH_MODULE_ENABLED             on)
set(HAL_GPIO_MODULE_ENABLED              on)
set(HAL_PWR_MODULE_ENABLED               on)
set(HAL_RCC_MODULE_ENABLED               on)
set(HAL_ADC_MODULE_ENABLED               off)
set(HAL_CAN_MODULE_ENABLED               off)
set(HAL_CAN_LEGACY_MODULE_ENABLED        off)
set(HAL_CRC_MODULE_ENABLED               off)
set(HAL_CEC_MODULE_ENABLED               off)
set(HAL_CRYP_MODULE_ENABLED              off)
set(HAL_DAC_MODULE_ENABLED               off)
set(HAL_DCMI_MODULE_ENABLED              off)
set(HAL_DMA2D_MODULE_ENABLED             off)
set(HAL_ETH_MODULE_ENABLED               off)
set(HAL_NAND_MODULE_ENABLED              off)
set(HAL_NOR_MODULE_ENABLED               off)
set(HAL_PCCARD_MODULE_ENABLED            off)
set(HAL_SRAM_MODULE_ENABLED              off)
set(HAL_SDRAM_MODULE_ENABLED             off)
set(HAL_HASH_MODULE_ENABLED              off)
set(HAL_EXTI_MODULE_ENABLED              off)
set(HAL_I2C_MODULE_ENABLED               off)
set(HAL_SMBUS_MODULE_ENABLED             off)
set(HAL_I2S_MODULE_ENABLED               off)
set(HAL_IWDG_MODULE_ENABLED              off)
set(HAL_LTDC_MODULE_ENABLED              off)
set(HAL_DSI_MODULE_ENABLED               off)
set(HAL_QSPI_MODULE_ENABLED              off)
set(HAL_RNG_MODULE_ENABLED               off)
set(HAL_RTC_MODULE_ENABLED               off)
set(HAL_SAI_MODULE_ENABLED               off)
set(HAL_SD_MODULE_ENABLED                off)
set(HAL_SPI_MODULE_ENABLED               off)
set(HAL_TIM_MODULE_ENABLED               off)
set(HAL_UART_MODULE_ENABLED              off)
set(HAL_USART_MODULE_ENABLED             off)
set(HAL_IRDA_MODULE_ENABLED              off)
set(HAL_SMARTCARD_MODULE_ENABLED         off)
set(HAL_WWDG_MODULE_ENABLED              off)
set(HAL_PCD_MODULE_ENABLED               off)
set(HAL_HCD_MODULE_ENABLED               off)
set(HAL_FMPI2C_MODULE_ENABLED            off)
set(HAL_FMPSMBUS_MODULE_ENABLED          off)
set(HAL_SPDIFRX_MODULE_ENABLED           off)
set(HAL_DFSDM_MODULE_ENABLED             off)
set(HAL_LPTIM_MODULE_ENABLED             off)
set(HAL_MMC_MODULE_ENABLED               off)

  ################### BEGIN-MODULE: io_pin ###################
    ##################### BEGIN-MODULE: io #####################
    # module already present => nothing to do here
    ###################### END-MODULE: io ######################

  set(UNIT "module_io_pin")
  add_library(module_io_pin OBJECT "${CMAKE_CURRENT_LIST_DIR}/arctos/io/pin/params.h")
  add_subdirectory("${CMAKE_CURRENT_LIST_DIR}/../../modules/io/#pin")
  target_link_libraries(module_io_pin module_io)

  add_library(iface_module_io_pin INTERFACE)
  target_link_libraries(iface_module_io_pin INTERFACE module_io_pin $<TARGET_OBJECTS:module_io_pin>)
  #################### END-MODULE: io_pin ####################

  ################ BEGIN-MODULE: io_stm32hal #################
  # module already present => nothing to do here
  ################# END-MODULE: io_stm32hal ##################

set(UNIT "module_io_pin_stm32")
add_library(module_io_pin_stm32 OBJECT "${CMAKE_CURRENT_LIST_DIR}/arctos/io/pin/stm32/params.h")
add_subdirectory("${CMAKE_CURRENT_LIST_DIR}/../../modules/io/#pin/#stm32")
target_link_libraries(module_io_pin_stm32 module_io_pin)
target_link_libraries(module_io_pin_stm32 module_io_stm32hal)

add_library(iface_module_io_pin_stm32 INTERFACE)
target_link_libraries(iface_module_io_pin_stm32 INTERFACE module_io_pin_stm32 $<TARGET_OBJECTS:module_io_pin_stm32>)
################# END-MODULE: io_pin_stm32 #################

target_link_libraries(arctos PRIVATE
  iface_module_system
  iface_module_system_run
  iface_module_system_run_arm
  iface_module_io
  iface_module_io_serial
  iface_module_io_stm32hal
  iface_module_io_serial_stm32
  iface_module_io_pin
  iface_module_io_pin_stm32
)
