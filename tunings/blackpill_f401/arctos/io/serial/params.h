/**
 * @file
 * @brief  auto-generated params header
 *
 * Generated on 2024-11-17 13:49
 * DO NOT EDIT
 */
#ifndef ARCTOS_IO_SERIAL_PARAMS_H
#define ARCTOS_IO_SERIAL_PARAMS_H

#define SERIAL_BUFFER_SIZE                   0x00000100

#endif /* ARCTOS_IO_SERIAL_PARAMS_H */
