/**
 * @file
 * @brief  auto-generated params header
 *
 * Generated on 2024-11-17 13:49
 * DO NOT EDIT
 */
#ifndef ARCTOS_PARAMS_H
#define ARCTOS_PARAMS_H

// BEGIN of "stm_board"
#define STM_BOARD_STM32F411_DISCO            1
#define STM_BOARD_BLACK_PILL_F401V3          2
#define STM_BOARD_BLACK_PILL_F411V2          3
// END of "stm_board"

// BEGIN of "stm_log"
#define STM_LOG_NONE                         0
#define STM_LOG_SERIAL_IDX01                 1
#define STM_LOG_SERIAL_IDX02                 2
#define STM_LOG_SERIAL_IDX06                 6
// END of "stm_log"

#define ARCTOS_LOG_LEVEL                     ARCTOS_LOG_LEVEL_DEBUG
#define ARCTOS_LOG_ENABLE_COLOR
#define ARCTOS_SCHEDULER_TIME_SLOT_SIZE      0x05F5E100
#define ARM_ENABLE_FPU
#define ARM_EXECUTION_STATE                  AArch32
#define STM_BOARD                            STM_BOARD_STM32F411_DISCO
#define STM_LOG                              STM_LOG_SERIAL_IDX02

#define ARCTOS_NUMBER_OF_CORES               0x00000001
#define ARCTOS_BOARDNAME                     "STM32F411 Discovery Board"
#define ARCTOS_SOC                           "STM32F411VE"
#define ARM_ARCHITECTURE_PROFILE             ARMv7E-M
#define ARM_ARCHITECTURE_VERSION             0x00000007
#define ARM_CORE                             CORTEX-M4
#define HSI_VALUE                            0x00F42400
#define HSE_VALUE                            0x007A1200
#define LSI_VALUE                            0x00007D00
#define LSE_VALUE                            0x00008000

#endif /* ARCTOS_PARAMS_H */
