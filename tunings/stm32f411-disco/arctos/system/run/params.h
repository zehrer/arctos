/**
 * @file
 * @brief  auto-generated params header
 *
 * Generated on 2024-11-17 13:49
 * DO NOT EDIT
 */
#ifndef ARCTOS_SYSTEM_RUN_PARAMS_H
#define ARCTOS_SYSTEM_RUN_PARAMS_H

#define DEFAULT_TASK_PRIORITY                0x0000000A
#define DEFAULT_TASK_STACKSIZE               0x00000400
#define MAX_NUMBER_OF_TASKS                  0x00000008

#endif /* ARCTOS_SYSTEM_RUN_PARAMS_H */
