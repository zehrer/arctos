/**
 * @file
 * @copyright Copyright (c) 2022 Michael Zehrer. This file is part of ARCTOS
 * @license MIT
 * @brief hardware related porting interface functions
 */
#include <stdint.h>
#include <stddef.h>

#include <arctos/porting/all.hpp>
#include <arctos/log/logger.hpp>
#include <arctos/params.h>

#if __has_include (<arctos/io/stm32hal/init.hpp>)
#  include <arctos/io/stm32hal/init.hpp>
#  define MODULE_IO_STM32HAL_INIT
#endif

#if __has_include (<arctos/io/pin/init.hpp>)
#  include <arctos/io/pin/init.hpp>
#  define MODULE_IO_PIN_INIT
#endif

#if __has_include (<arctos/io/serial/init.hpp>)
#  include <arctos/io/serial/init.hpp>
#  define MODULE_IO_SERIAL_INIT
#endif

#if ARCTOS_LOG_LEVEL != ARCTOS_LOG_LEVEL_NONE && STM_LOG != STM_LOG_NONE
#  if __has_include (<arctos/io/serial/serial.hpp>)
#    include <arctos/io/serial/serial.hpp>
#  else
#    error "You must either disable logging or enable the serial module!"
#  endif
#endif

namespace arctos {
namespace porting {
namespace hardware {

using namespace arctos;

void init(void) {
#ifdef MODULE_IO_STM32HAL_INIT
    io::stm32hal::init();
#endif
#ifdef MODULE_IO_PIN_INIT
    io::pin::init();
#endif
#ifdef MODULE_IO_SERIAL_INIT
    io::serial::init();
#endif

#if ARCTOS_LOG_LEVEL != ARCTOS_LOG_LEVEL_NONE && STM_LOG != STM_LOG_NONE
#  if   STM_LOG == STM_LOG_SERIAL_IDX01
#    ifdef SERIAL_ENABLE_IDX1

    if (io::serial::usart01.begin() > 0)
        log::Logger::instance()->setDestination(&io::serial::usart01);

#    else
#      error "USART1 isn't enabled! Change logging destination or disable logging entirely."
#    endif

#  elif STM_LOG == STM_LOG_SERIAL_IDX02
#    ifdef SERIAL_ENABLE_IDX2

    if (io::serial::usart02.begin() > 0)
        log::Logger::instance()->setDestination(&io::serial::usart02);

#    else
#      error "USART2 isn't enabled! Change logging destination or disable logging entirely."
#    endif

#  elif STM_LOG == STM_LOG_SERIAL_IDX06
#    ifdef SERIAL_ENABLE_IDX6

    if (io::serial::usart06.begin() > 0)
        log::Logger::instance()->setDestination(&io::serial::usart06);

#    else
#      error "USART6 isn\'t enabled! Change logging destination or disable logging entirely."
#    endif
#  endif
#endif
}

} /* namespace hardware */
} /* namespace porting */
} /* namespace arctos */
