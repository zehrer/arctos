/**
 * @file
 * @copyright Copyright (c) 2018-2022 Michael Zehrer. This file is part of ARCTOS
 * @license MIT
 * @brief raspberry pi specific implementation of the porting interface functions
 */
#include <stddef.h>
#include <string.h>

#include <arctos/params.h>
#include <arctos/porting/all.hpp>
#include <arctos/log/logger.hpp>
#include <arctos/system/schedulable.hpp>
#include <arctos/system/scheduler.hpp>

#include <io/serial/all.hpp>

#include <raspberry/all.h>
#include <raspberry/context.hpp>
#include <raspberry/interrupts.hpp>

#include <arm/cache/maintenance.hpp>
#include <arm/mmu/maintenance.hpp>
#include <arm/mmu/vmsa.hpp>
#include <arm/registers/psr.hpp>

/**
 * pointer to the current context (used in switch.S)
 */
volatile uintptr_t g_cur_context;

namespace arctos {
namespace porting {

int32_t atomicCompareAndExchange(int32_t *v, int32_t o, int32_t n) {
    int32_t prev;
    uint32_t flags;

    asm volatile(
        "mrs %0, cpsr\n"
        "cpsid i"
        : "=r" (flags) : : "memory", "cc");

    prev = *v;
    if (prev == o)
        *v = n;

    asm volatile (
        "msr cpsr_c, %0"
        : : "r" (flags) : "memory", "cc");

    return prev;
}
int64_t atomicCompareAndExchange(int64_t *v, int64_t o, int64_t n) {
    int64_t prev;
    uint32_t flags;

    asm volatile(
        "mrs %0, cpsr\n"
        "cpsid i"
        : "=r" (flags) : : "memory", "cc");

    prev = *v;
    if (prev == o)
        *v = n;

    asm volatile (
        "msr cpsr_c, %0"
        : : "r" (flags) : "memory", "cc");

    return prev;
}



/***********************************************************************
 * context
 **********************************************************************/
namespace context {
uintptr_t create(uintptr_t stack, ::arctos::system::Schedulable *work) {
    // align to 4 byte boundary
    stack = stack & (~static_cast<uintptr_t>(3u));

    // mark bottom of stack (stack is growing downwards)
    *(reinterpret_cast<uint32_t*>(stack)) = 0xdeaddead;

    stack = stack - 4 - sizeof(Context);
    Context *ctx = reinterpret_cast<Context*>(stack);
    ctx->LR_swi      = reinterpret_cast<uintptr_t>(
        &::arctos::system::Schedulable::startupWrapper);
    ctx->LR_sys      = 0;
    ctx->SP          = stack;
    ctx->R12         = 0;
    ctx->R11         = 0;
    ctx->R10         = 0;
    ctx->R9          = 0;
    ctx->R8          = 0;
    ctx->R7          = 0;
    ctx->R6          = 0;
    ctx->R5          = 0;
    ctx->R4          = 0;
    ctx->R3          = 0;
    ctx->R2          = 0;
    ctx->R1          = 0;
    ctx->R0          = reinterpret_cast<uintptr_t>(work);
#ifdef ARM_ENABLE_FPU
    for (uint32_t i = 0; i < FPU_CONTEXT_SIZE; ++i)
        ctx->FPU[i] = 0;
#endif
    ctx->SPSR = ::arm::registers::PSR_Type::readCPSR();
    ctx->SPSR.fields.I = 0;
    ctx->SPSR.fields.F = 0;
    return stack;
}
} /* namespace context */



/***********************************************************************
 * special hardware stuff
 **********************************************************************/
namespace hardware {
enum LogDestination {
    kSerial00 = 0,
    kSerial01 = 1
};

// defined by the linker script
extern "C" uint32_t _mmu_level1_table_start_[4096 * 4];

void init(void) {
    arm::mmu::vmsa::DescriptorTable table(_mmu_level1_table_start_);
    table.map(0x00000000,
              0x00000000,
              PERIPHERALS_BASE,
              arm::mmu::vmsa::MemoryRegionType::kNonCachable,
              true);
    table.map(PERIPHERALS_BASE,
              PERIPHERALS_BASE,
              PERIPHERALS_SIZE,
              arm::mmu::vmsa::MemoryRegionType::kSharedDevice,
              true);
#if defined(BCM2836) || defined(BCM2837)
    table.map(ARM_LOCAL_BASE,
              ARM_LOCAL_BASE,
              0x00100000,
              arm::mmu::vmsa::MemoryRegionType::kNonCachable,
              true);
#endif
    arm::mmu::vmsa::DescriptorTable::enable(table);
    arm::mmu::enable();

#ifdef RPI_ENABLE_CACHE
    #warning The current implementation might not work with cache enabled!
    arm::cache::enableAll();
#endif

#if ARCTOS_LOG_LEVEL != ARCTOS_LOG_LEVEL_NONE
    arctos::io::serial::Serial *s = arctos::io::serial::get(RPI_LOG_DESTINATION);
    s->begin();
    arctos::log::Logger::instance()->setDestination(s);
#endif
}
} /* namespace hardware */



/***********************************************************************
 * system clock
 **********************************************************************/
namespace clock {
static  int32_t g_next_time_event_high = 0;
static uint32_t g_next_time_event_low  = 0;
static uint32_t g_next_time_event_iterations = 0;

class SystemTimer1Isr : public ::raspberry::Isr {
public:
    SystemTimer1Isr() : Isr(::raspberry::Interrupts::kSystemTimer1) { }

    void run() {
        // Write a one to the relevant bit to clear the match detect status bit
        // and the corresponding interrupt request line.
        // (BCM2835 ARM Peripherals - Page 172)
        SYSTEM_TIMER->CONTROL = SYSTEM_TIMER_CONTROL_MATCH1;

        SYSTEM_TIMER->COMPARE1 += (ARCTOS_SCHEDULER_TIME_SLOT_SIZE / 1000);
        ::arctos::system::Scheduler::getInstance()->onSystemTick(g_cur_context);
    }
};

class SystemTimer3Isr : public ::raspberry::Isr {
public:
    SystemTimer3Isr() : Isr(::raspberry::Interrupts::kSystemTimer3) { }

    void run() {
        // Clear the interrupt request line
        SYSTEM_TIMER->CONTROL = SYSTEM_TIMER_CONTROL_MATCH3;

        if (g_next_time_event_iterations == 0) {
            IRQ_CNTL->DISABLE1 = BIT(System_Timer3_IRQn);
            ::arctos::system::Scheduler::getInstance()->onTimeEvent(g_cur_context);
        }
        else if (--g_next_time_event_iterations == 0) {
            SYSTEM_TIMER->COMPARE3 = g_next_time_event_low;

            if (g_next_time_event_low <= SYSTEM_TIMER->CNT_LOW) {
                IRQ_CNTL->DISABLE1 = BIT(System_Timer3_IRQn);
                ::arctos::system::Scheduler::getInstance()->onTimeEvent(g_cur_context);
            }
        }
    }
};

static SystemTimer1Isr timer1;
static SystemTimer3Isr timer3;

void activateSystemTick() {
    // set compare value
    SYSTEM_TIMER->COMPARE1 = SYSTEM_TIMER->CNT_LOW + (ARCTOS_SCHEDULER_TIME_SLOT_SIZE / 1000);
    // reset counter interrupt
    SYSTEM_TIMER->CONTROL = SYSTEM_TIMER_CONTROL_MATCH1;
    // activate timer interrupt
    IRQ_CNTL->ENABLE1 = BIT(System_Timer1_IRQn);
}
void deactivateSystemTick() {
    // deactivate timer interrupt
    IRQ_CNTL->DISABLE1 = BIT(System_Timer1_IRQn);
    // reset counter interrupt
    SYSTEM_TIMER->CONTROL = SYSTEM_TIMER_CONTROL_MATCH1;
}

bool activateTimeEvent(int64_t event) {
    // deactivate timer interrupt
    IRQ_CNTL->DISABLE1 = BIT(System_Timer3_IRQn);

    g_next_time_event_high = static_cast< int32_t>(event >> 32);
    g_next_time_event_low  = static_cast<uint32_t>(event);
    int32_t cnt_high       = static_cast< int32_t>(SYSTEM_TIMER->CNT_HIGH);
    if (g_next_time_event_high < cnt_high)
        return false;

    bool event_activated = true;
    // NOTE: This loop is traversed twice at max
    while (true) {
        if (g_next_time_event_high == cnt_high) {
            SYSTEM_TIMER->COMPARE3 = g_next_time_event_low;
            SYSTEM_TIMER->CONTROL  = SYSTEM_TIMER_CONTROL_MATCH3;

            cnt_high = SYSTEM_TIMER->CNT_HIGH;
            if (g_next_time_event_high < cnt_high || g_next_time_event_low <= SYSTEM_TIMER->CNT_LOW)
                event_activated = false;
            else {
                g_next_time_event_iterations = 0;
                IRQ_CNTL->ENABLE1 = BIT(System_Timer3_IRQn);
            }
            break;
        }
        // g_next_time_event_high > cnt_high
        else {
            SYSTEM_TIMER->COMPARE3 = 0x0;
            SYSTEM_TIMER->CONTROL  = SYSTEM_TIMER_CONTROL_MATCH3;

            cnt_high = SYSTEM_TIMER->CNT_HIGH;
            if (g_next_time_event_high > cnt_high) {
                g_next_time_event_iterations = g_next_time_event_high - cnt_high;
                IRQ_CNTL->ENABLE1 = BIT(System_Timer3_IRQn);
                break;
            }
        }
    }
    return event_activated;
}
void deactivateTimeEvent(void) {
    // deactivate timer interrupt
    IRQ_CNTL->DISABLE1 = BIT(System_Timer3_IRQn);
    // reset counter interrupt
    SYSTEM_TIMER->CONTROL = SYSTEM_TIMER_CONTROL_MATCH3;
}

int64_t now() {
    int64_t now;
    uintptr_t system_timer_cnt_low_addr = SYSTEM_TIMER_CNT_LOW;
    asm volatile (
        "1:\n\t"
        // read the higher 32 bit of the timer to r0
        "ldr r0, [%[clow], #4]\n\t"
        // read the lower 32 bit of the timer
        "ldr %Q[now], [%[clow]]\n\t"
        // read the higher 32 bit of the timer again
        "ldr %R[now], [%[clow], #4]\n\t"
        // compare the two values of the high-register...
        "cmp r0, %R[now]\n\t"
        // ... and repeat the whole step if they are not equal
        "bne 1b\n\t"
        : [now]"=r"(now), [clow]"+r"(system_timer_cnt_low_addr)
        : 
        : "r0", "memory");
    return (now * 1000);
}
} /* namespace clock */
} /* namespace porting */
} /* namespace arctos */
