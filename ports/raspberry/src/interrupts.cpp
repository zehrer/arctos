/**
 * @file
 * @copyright Copyright (c) 2020-2022 Michael Zehrer. This file is part of ARCTOS
 * @license MIT
 * @brief interrupts
 */
#include <raspberry/interrupts.hpp>
#include <raspberry/all.h>

namespace raspberry {

class DummyIsr : public Isr {
public:
    DummyIsr() : Isr(Interrupts::kDummy) { }

    void run() {
        while (true) {
            // The dummy interrupt service routine isn't intended to be used
            // => hang
        }
    }
};
static DummyIsr dummy;


Isr *Isr::irqs_[static_cast<uint8_t>(Interrupts::kLast)] = { &dummy };

Isr::Isr(Interrupts irq) {
    Isr::irqs_[static_cast<uint8_t>(irq)] = this;
}

void Isr::handle() {
    uint32_t pend = IRQ_CNTL->PEND1;
    if (pend & BIT(System_Timer1_IRQn))
        Isr::irqs_[static_cast<uint8_t>(Interrupts::kSystemTimer1)]->run();
    if (pend & BIT(System_Timer3_IRQn))
        Isr::irqs_[static_cast<uint8_t>(Interrupts::kSystemTimer3)]->run();
    if (pend & BIT(Auxiliary_IRQn)) {
        // handle the auxiliaries interrupts
        uint32_t auxirq = AUXILIARY->IRQ;
        if (auxirq & AUXIRQ_MINIUART)
            Isr::irqs_[static_cast<uint8_t>(Interrupts::kUart1)]->run();
        else if (auxirq & AUXIRQ_SPI1)
            Isr::irqs_[static_cast<uint8_t>(Interrupts::kSpi1)]->run();
        else if (auxirq & AUXIRQ_SPI2)
            Isr::irqs_[static_cast<uint8_t>(Interrupts::kSpi2)]->run();
    }

    pend = IRQ_CNTL->PEND2;
    // most of the interrupts will be timer interrupts
    // => PEND2 is 0
    if (pend == 0)
        return;

    if (pend & BIT(SPI_I2C_SLV_IRQn - 32))
        Isr::irqs_[static_cast<uint8_t>(Interrupts::kSpi_I2c_Slave)]->run();

    if (pend & BIT(PWA0_IRQn - 32))
        Isr::irqs_[static_cast<uint8_t>(Interrupts::kPwa0)]->run();
    if (pend & BIT(PWA1_IRQn - 32))
        Isr::irqs_[static_cast<uint8_t>(Interrupts::kPwa1)]->run();

    if (pend & BIT(SMI_IRQn - 32))
        Isr::irqs_[static_cast<uint8_t>(Interrupts::kSmi)]->run();
    if (pend & BIT(GPIO0_IRQn - 32))
        Isr::irqs_[static_cast<uint8_t>(Interrupts::kGpio0)]->run();
    if (pend & BIT(GPIO1_IRQn - 32))
        Isr::irqs_[static_cast<uint8_t>(Interrupts::kGpio1)]->run();
    if (pend & BIT(GPIO2_IRQn - 32))
        Isr::irqs_[static_cast<uint8_t>(Interrupts::kGpio2)]->run();
    if (pend & BIT(GPIO_ALL_IRQn - 32))
        Isr::irqs_[static_cast<uint8_t>(Interrupts::kGpioAll)]->run();
    if (pend & BIT(I2C_IRQn - 32))
        Isr::irqs_[static_cast<uint8_t>(Interrupts::kI2c)]->run();
    if (pend & BIT(SPI_IRQn - 32))
        Isr::irqs_[static_cast<uint8_t>(Interrupts::kSpi0)]->run();
    if (pend & BIT(PCM_IRQn - 32))
        Isr::irqs_[static_cast<uint8_t>(Interrupts::kPcm)]->run();
    if (pend & BIT(UART_IRQn - 32))
        Isr::irqs_[static_cast<uint8_t>(Interrupts::kUart0)]->run();
}

} /* namespace raspberry */
