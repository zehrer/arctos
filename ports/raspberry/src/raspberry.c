/**
 * Copyright (c) 2018 Michael Zehrer
 * This file is part of ARCTOS
 *
 * @licence MIT
 * @brief   Some useful Raspberry Pi functions
 * @author  Michael Zehrer
 */
#include <raspberry/all.h>

#ifdef __cplusplus
extern "C" {
#endif

/***********************************************************************
 * Peripherals / Interrupts
 **********************************************************************/
void RPI_IRQ_Enable(IRQn_t irq_number) {
    if (irq_number < 32)
        IRQ_CNTL->ENABLE1 = BIT(irq_number);
    else
        IRQ_CNTL->ENABLE2 = BIT(irq_number - 32);
}
void RPI_IRQ_Disable(IRQn_t irq_number) {
    if (irq_number < 32)
        IRQ_CNTL->DISABLE1 = BIT(irq_number);
    else
        IRQ_CNTL->DISABLE2 = BIT(irq_number - 32);
}
bool RPI_IRQ_GetPending(IRQn_t irq_number) {
    if (irq_number < 32)
        return (IRQ_CNTL->PEND1 & BIT(irq_number));
    else
        return (IRQ_CNTL->PEND2 & BIT(irq_number - 32));
}

/***********************************************************************
 * Peripherals / GPIO
 **********************************************************************/
int RPI_GPIO_SetPinFunction(uint32_t pin, gpio_function_t function) {
    return RPI_GPIO_SetMultiplePinFunction(pin, pin, function);
}
int RPI_GPIO_SetMultiplePinFunction(uint32_t start_pin, uint32_t end_pin, gpio_function_t function) {
    if (start_pin > end_pin)
        return -2;
    else if (start_pin >= GPIO_NUMBER_OF_PINS || end_pin >= GPIO_NUMBER_OF_PINS)
        return -1;

    uint32_t start_fsreg  = (start_pin / 10);
    uint32_t end_fsreg    = (end_pin   / 10);
    
    for (uint32_t fsreg = start_fsreg; fsreg <= end_fsreg; ++fsreg) {
        volatile uint32_t *fs = ((volatile uint32_t *)GPIO_GPFSEL0) + fsreg;
        uint32_t fsval = *fs;

        uint32_t fsbits = (start_pin - (10 * fsreg)) * 3;
        while (fsbits <= 27 && fsbits <= (end_pin - (10 * fsreg)) * 3) {
            fsval &= ~(GPIO_GPFSEL_MASK << fsbits);
            fsval |= (function << fsbits);

            fsbits += 3;
        }
        *fs = fsval;
    }
    return 0;
}

int RPI_GPIO_SetPinPUD(uint32_t pin, gpio_pud_t pud) {
    return RPI_GPIO_SetMultiplePinPUD(pin, pin, pud);
}
int RPI_GPIO_SetMultiplePinPUD(uint32_t start_pin, uint32_t end_pin, gpio_pud_t pud) {
    if (start_pin > end_pin)
        return -2;
    else if (start_pin >= GPIO_NUMBER_OF_PINS || end_pin >= GPIO_NUMBER_OF_PINS)
        return -1;

    uint32_t pud_clk0_val = 0, pud_clk1_val = 0;
    volatile uint8_t i;

    GPIO->GPPUD = pud;

    for (uint32_t pin = start_pin; pin <= end_pin; ++pin) {
        if (pin < 32)
            pud_clk0_val |= BIT(pin);
        else
            pud_clk1_val |= BIT(pin - 32);
    }

    for (i = 0; i < 150; ++i) {}

    GPIO->GPPUDCLK0 = pud_clk0_val;
    GPIO->GPPUDCLK1 = pud_clk1_val;

    for (i = 0; i < 150; ++i) {}

    GPIO->GPPUDCLK0 = 0;
    GPIO->GPPUDCLK1 = 0;
    GPIO->GPPUD = 0;

    return 0;
}

int RPI_GPIO_SetPinEvent(uint32_t pin, gpio_pin_event_t event) {
    return RPI_GPIO_SetMultiplePinEvent(pin, pin, event);
}
int RPI_GPIO_SetMultiplePinEvent(uint32_t start_pin, uint32_t end_pin, gpio_pin_event_t event) {
    if (start_pin > end_pin)
        return -2;
    else if (start_pin >= GPIO_NUMBER_OF_PINS || end_pin >= GPIO_NUMBER_OF_PINS)
        return -1;

    uint32_t bank0 = 0, bank1 = 0;

    for (uint32_t pin = start_pin; pin <= end_pin; ++pin) {
        if (pin < 32)
            bank0 |= BIT(pin);
        else
            bank1 |= BIT(pin - 32);
    }

    /* clear all previous edge event settings... */
    GPIO->GPREN0 &= ~bank0;
    GPIO->GPREN1 &= ~bank1;

    GPIO->GPFEN0 &= ~bank0;
    GPIO->GPFEN1 &= ~bank1;

    GPIO->GPHEN0 &= ~bank0;
    GPIO->GPHEN1 &= ~bank1;

    GPIO->GPLEN0 &= ~bank0;
    GPIO->GPLEN1 &= ~bank1;

    GPIO->GPAREN0 &= ~bank0;
    GPIO->GPAREN1 &= ~bank1;

    GPIO->GPAFEN0 &= ~bank0;
    GPIO->GPAFEN1 &= ~bank1;

    /* ...and set the new one */
    switch(event) {
        case PE_RISING:
            GPIO->GPREN0 |= bank0;
            GPIO->GPREN1 |= bank1;
            break;
        case PE_RISING_AND_FALLING:
            GPIO->GPREN0 |= bank0;
            GPIO->GPREN1 |= bank1;
        case PE_FALLING:
            GPIO->GPFEN0 |= bank0;
            GPIO->GPFEN1 |= bank1;
            break;

        case PE_HIGH:
            GPIO->GPHEN0 |= bank0;
            GPIO->GPHEN1 |= bank1;
            break;
        case PE_LOW:
            GPIO->GPLEN0 |= bank0;
            GPIO->GPLEN1 |= bank1;
            break;

        case PE_ASYNC_RISING:
            GPIO->GPAREN0 |= bank0;
            GPIO->GPAREN1 |= bank1;
            break;

        case PE_ASYNC_FALLING:
            GPIO->GPAFEN0 |= bank0;
            GPIO->GPAFEN1 |= bank1;
            break;
        default:
            break;
    }
    return 0;
}

#ifdef __cplusplus
} // end extern "C"
#endif
