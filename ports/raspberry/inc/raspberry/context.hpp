/**
 * Copyright (c) 2018-2021 Michael Zehrer
 * This file is part of ARCTOS
 *
 * @licence MIT
 * @brief   Context structure
 * @author  Michael Zehrer
 */
#ifndef ARCTOS_RPI_CONTEXT_HPP
#define ARCTOS_RPI_CONTEXT_HPP
#include <stdint.h>

#include <arctos/params.h>
#include <arm/registers/psr.hpp>
#include <raspberry/all.h>

struct Context {
    ::arm::registers::PSR_Type SPSR;
#ifdef ARM_ENABLE_FPU
    uint32_t FPU[FPU_CONTEXT_SIZE];
#endif
    uint32_t R0;
    uint32_t R1;
    uint32_t R2;
    uint32_t R3;
    uint32_t R4;
    uint32_t R5;
    uint32_t R6;
    uint32_t R7;
    uint32_t R8;
    uint32_t R9;
    uint32_t R10;
    uint32_t R11;
    uint32_t R12;
    uint32_t SP;
    uint32_t LR_sys;
    uint32_t LR_swi;
};

#endif /* ARCTOS_RPI_CONTEXT_HPP */
