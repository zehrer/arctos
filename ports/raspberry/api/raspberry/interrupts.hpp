/**
 * @file
 * @copyright Copyright (c) 2020-2022 Michael Zehrer. This file is part of ARCTOS
 * @license MIT
 * @brief interrupts
 */
#ifndef ARCTOS_RPI_INTERRUPTS_HPP
#define ARCTOS_RPI_INTERRUPTS_HPP
#include <stdint.h>

#include <arctos/system/runnable.hpp>

namespace raspberry {

enum class Interrupts : uint8_t {
    kDummy = 0
    , kSystemTimer1
    , kSystemTimer3

    // Auxiliary handlers (only one interrupt line)
    , kUart1
    , kSpi1
    , kSpi2

    , kSpi_I2c_Slave

    , kPwa0
    , kPwa1

    , kSmi
    , kGpio0
    , kGpio1
    , kGpio2
    , kGpioAll
    , kI2c
    , kSpi0
    , kPcm

    , kUart0

    , kFirst = kSystemTimer1
    , kLast = kUart0
};

class Isr : public ::arctos::system::Runnable {
protected:
    static Isr *irqs_[static_cast<uint8_t>(Interrupts::kLast)];
    static void handle(void) asm("raspberry_Isr_handle");

public:
    Isr(Interrupts irq);
};

} /* namespace raspberry */
#endif /* ARCTOS_RPI_INTERRUPTS_HPP */
