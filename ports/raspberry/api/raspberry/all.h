/**
 * Copyright (c) 2018 Michael Zehrer
 * This file is part of ARCTOS
 *
 * @licence MIT
 * @brief   Definitions for the raspberry pi (several versions and models)
 * @author  Michael Zehrer
 */
#ifndef ARCTOS_RPI_ALL_H
#define ARCTOS_RPI_ALL_H

#include <arctos/params.h>

#ifndef BIT
  #define BIT(n) (1 << (n))
#endif
#ifndef STR
  #define STR_HELPER(x) #x
  #define STR(x) STR_HELPER(x)
#endif

/**
 * Raspberry Pi's:
 */
#define RASPBERRY_PI_A              1
#define RASPBERRY_PI_A_PLUS         2
#define RASPBERRY_PI_B              3
#define RASPBERRY_PI_B_PLUS         4
#define RASPBERRY_PI_B2             5
#define RASPBERRY_PI_B2v1_2         6
#define RASPBERRY_PI_B3             7
#define RASPBERRY_PI_ZERO           8
#define RASPBERRY_PI_ZERO_W         9

#if RPI_BOARD == RASPBERRY_PI_A      || RPI_BOARD == RASPBERRY_PI_A_PLUS || \
    RPI_BOARD == RASPBERRY_PI_B      || RPI_BOARD == RASPBERRY_PI_B_PLUS || \
    RPI_BOARD == RASPBERRY_PI_ZERO   || RPI_BOARD == RASPBERRY_PI_ZERO_W
  #define BCM2835
#elif RPI_BOARD == RASPBERRY_PI_B2
  #define BCM2836
#elif RPI_BOARD == RASPBERRY_PI_B2v1_2 || RPI_BOARD == RASPBERRY_PI_B3
  #define BCM2837
#else
  #error Raspberry-Pi model not specified!
#endif

#if    defined(BCM2835)
  #define PERIPHERALS_BASE            0x20000000
  #define PERIPHERALS_SIZE            0x01000000
  #define FPU_CONTEXT_SIZE            32
#elif defined(BCM2836)
  #define PERIPHERALS_BASE            0x3F000000
  #define PERIPHERALS_SIZE            0x01000000
  #define FPU_CONTEXT_SIZE            64
#elif defined(BCM2837)
  #define PERIPHERALS_BASE            0x3F000000
  #define PERIPHERALS_SIZE            0x01000000
  #define FPU_CONTEXT_SIZE            64
#endif

#ifdef __cplusplus
  #define    __R      volatile        /* defines 'read only' permissions */
#else
  #define    __R      volatile const  /* defines 'read only' permissions */
#endif
#define      __W      volatile        /* defines 'write only' permissions */
#define      __RW     volatile        /* defines 'read / write' permissions */

/***********************************************************************
 * Software interrupt numbers (SWI/SVC) used in this RODOS port
 **********************************************************************/
#define SVC_RUN_ACTIVE_CONTEXT      0
#define SVC_CONTEXT_SWITCH          1

/***********************************************************************
 * Peripherals
 **********************************************************************/
#include "peripherals/auxiliary.h"
#include "peripherals/gpio.h"
#include "peripherals/arm-timer.h"
#include "peripherals/system-timer.h"
#include "peripherals/interrupts.h"


/*-------------------NOT TESTED ON THE RASPBERRY PI 3 FROM HERE ON-------------------*/

/***********************************************************************
 * Power Management, Reset controller and Watchdog
 * (derived from "arch/arm/mach-bcm2708/include/mach/platform.h" of the Raspberry Pi Linux Kernel)
 **********************************************************************/
#define POWER_MANAGEMENT_BASE                   (PERIPHERALS_BASE + 0x00100000)
#define POWER_MANAGEMENT_RSTC                   (POWER_MANAGEMENT_BASE + 0x1c)
#define POWER_MANAGEMENT_RSTS                   (POWER_MANAGEMENT_BASE + 0x20)
#define POWER_MANAGEMENT_WDOG                   (POWER_MANAGEMENT_BASE + 0x24)

#define POWER_MANAGEMENT_WDOG_RESET             0000000000
#define POWER_MANAGEMENT_PASSWORD               0x5a000000
#define POWER_MANAGEMENT_WDOG_TIME_SET          0x000fffff
#define POWER_MANAGEMENT_RSTC_WRCFG_CLR         0xffffffcf
#define POWER_MANAGEMENT_RSTC_WRCFG_SET         0x00000030
#define POWER_MANAGEMENT_RSTC_WRCFG_FULL_RESET  0x00000020
#define POWER_MANAGEMENT_RSTC_RESET             0x00000102

/***********************************************************************
 * Broadcom Serial Controllers (BSC/I2C)
 **********************************************************************/
/* Only BSC0 is accessible from the RPi pi header.*/
#define BSC0_ADDR (PERIPHERALS_BASE + 0x00205000)
#define BSC1_ADDR (PERIPHERALS_BASE + 0x00804000)
#define BSC2_ADDR (PERIPHERALS_BASE + 0x00805000)

#define BSC_CLOCK_FREQ 250000000

#define BSC_REG_C 0x0
#define BSC_REG_S 0x4
#define BSC_REG_DLEN 0x8
#define BSC_REG_A 0xc
#define BSC_REG_FIFO 0x10
#define BSC_REG_DIV 0x14
#define BSC_REG_DEL 0x18
#define BSC_REG_CLKT 0x1c

/* I2C control flags */
#define BSC_I2CEN BIT(15)
#define BSC_INTR BIT(10)
#define BSC_INTT BIT(9)
#define BSC_INTD BIT(8)
#define BSC_ST BIT(7)
#define BSC_CLEAR BIT(4)
#define BSC_READ BIT(0)

/* I2C status flags */
#define BSC_TA BIT(0)   /** @brief Transfer active.*/
#define BSC_DONE BIT(1) /** @brief Transfer done.*/
#define BSC_TXW BIT(2)  /** @brief FIFO needs writing.*/
#define BSC_RXR BIT(3)  /** @brief FIFO needs reading.*/
#define BSC_TXD BIT(4)  /** @brief FIFO can accept data.*/
#define BSC_RXD BIT(5)  /** @brief FIFO contains data.*/
#define BSC_TXE BIT(6)  /** @brief FIFO empty.*/
#define BSC_RXF BIT(7)  /** @brief FIFO full.*/
#define BSC_ERR BIT(8)  /** @brief ACK error.*/
#define BSC_CLKT BIT(9) /** @brief Clock stretch timeout.*/

/* Rising/Falling Edge Delay Defaults.*/
#define BSC_DEFAULT_FEDL 0x30
#define BSC_DEFAULT_REDL 0x30

/* Clock Stretch Timeout Defaults.*/
#define BSC_DEFAULT_CLKT 0x40

#define CLEAR_STATUS BSC_CLKT|BSC_ERR|BSC_DONE

#define START_READ BSC_I2CEN|BSC_ST|BSC_CLEAR|BSC_READ
#define START_WRITE BSC_I2CEN|BSC_ST


#if defined(BCM2836) || defined(BCM2837)
/***********************************************************************
 * ARM local interrupt controller (from QA7_rev3.4 - Page 7-8)
 **********************************************************************/
#define ARM_LOCAL_BASE                      0x40000000
#define ARM_LOCAL_CONTROL                   (ARM_LOCAL_BASE + 0x000)
//004 unused
#define ARM_LOCAL_PRESCALER                 (ARM_LOCAL_BASE + 0x008)
#define ARM_LOCAL_GPU_INT_ROUTING           (ARM_LOCAL_BASE + 0x00C)
#define ARM_LOCAL_PM_ROUTING_SET            (ARM_LOCAL_BASE + 0x010)
#define ARM_LOCAL_PM_ROUTING_CLR            (ARM_LOCAL_BASE + 0x014)
//018 unused
#define ARM_LOCAL_TIMER_LS                  (ARM_LOCAL_BASE + 0x01C)
#define ARM_LOCAL_TIMER_MS                  (ARM_LOCAL_BASE + 0x020)
#define ARM_LOCAL_INT_ROUTING               (ARM_LOCAL_BASE + 0x024)
//028 unknown
#define ARM_LOCAL_AXI_COUNT                 (ARM_LOCAL_BASE + 0x02C)
#define ARM_LOCAL_AXI_IRQ                   (ARM_LOCAL_BASE + 0x030)
#define ARM_LOCAL_TIMER_CONTROL             (ARM_LOCAL_BASE + 0x034)
#define ARM_LOCAL_TIMER_WRITE               (ARM_LOCAL_BASE + 0x038)
//03C unused
#define ARM_LOCAL_TIMER_INT_CONTROL_CORE0   (ARM_LOCAL_BASE + 0x040)
#define ARM_LOCAL_TIMER_INT_CONTROL_CORE1   (ARM_LOCAL_BASE + 0x044)
#define ARM_LOCAL_TIMER_INT_CONTROL_CORE2   (ARM_LOCAL_BASE + 0x048)
#define ARM_LOCAL_TIMER_INT_CONTROL_CORE3   (ARM_LOCAL_BASE + 0x04C)

#define ARM_LOCAL_MAILBOX_INT_CONTROL_CORE0 (ARM_LOCAL_BASE + 0x050)
#define ARM_LOCAL_MAILBOX_INT_CONTROL_CORE1 (ARM_LOCAL_BASE + 0x054)
#define ARM_LOCAL_MAILBOX_INT_CONTROL_CORE2 (ARM_LOCAL_BASE + 0x058)
#define ARM_LOCAL_MAILBOX_INT_CONTROL_CORE3 (ARM_LOCAL_BASE + 0x05C)

#define ARM_LOCAL_IRQ_PENDING_CORE0         (ARM_LOCAL_BASE + 0x060)
#define ARM_LOCAL_IRQ_PENDING_CORE1         (ARM_LOCAL_BASE + 0x064)
#define ARM_LOCAL_IRQ_PENDING_CORE2         (ARM_LOCAL_BASE + 0x068)
#define ARM_LOCAL_IRQ_PENDING_CORE3         (ARM_LOCAL_BASE + 0x06C)

#define ARM_LOCAL_FIQ_PENDING_CORE0         (ARM_LOCAL_BASE + 0x070)
#define ARM_LOCAL_FIQ_PENDING_CORE1         (ARM_LOCAL_BASE + 0x074)
#define ARM_LOCAL_FIQ_PENDING_CORE2         (ARM_LOCAL_BASE + 0x078)
#define ARM_LOCAL_FIQ_PENDING_CORE3         (ARM_LOCAL_BASE + 0x07C)

#define ARM_LOCAL_MAILBOX0_SET_CORE0        (ARM_LOCAL_BASE + 0x080)
#define ARM_LOCAL_MAILBOX1_SET_CORE0        (ARM_LOCAL_BASE + 0x084)
#define ARM_LOCAL_MAILBOX2_SET_CORE0        (ARM_LOCAL_BASE + 0x088)
#define ARM_LOCAL_MAILBOX3_SET_CORE0        (ARM_LOCAL_BASE + 0x08C)

#define ARM_LOCAL_MAILBOX0_SET_CORE1        (ARM_LOCAL_BASE + 0x090)
#define ARM_LOCAL_MAILBOX1_SET_CORE1        (ARM_LOCAL_BASE + 0x094)
#define ARM_LOCAL_MAILBOX2_SET_CORE1        (ARM_LOCAL_BASE + 0x098)
#define ARM_LOCAL_MAILBOX3_SET_CORE1        (ARM_LOCAL_BASE + 0x09C)

#define ARM_LOCAL_MAILBOX0_SET_CORE2        (ARM_LOCAL_BASE + 0x0A0)
#define ARM_LOCAL_MAILBOX1_SET_CORE2        (ARM_LOCAL_BASE + 0x0A4)
#define ARM_LOCAL_MAILBOX2_SET_CORE2        (ARM_LOCAL_BASE + 0x0A8)
#define ARM_LOCAL_MAILBOX3_SET_CORE2        (ARM_LOCAL_BASE + 0x0AC)

#define ARM_LOCAL_MAILBOX0_SET_CORE3        (ARM_LOCAL_BASE + 0x0B0)
#define ARM_LOCAL_MAILBOX1_SET_CORE3        (ARM_LOCAL_BASE + 0x0B4)
#define ARM_LOCAL_MAILBOX2_SET_CORE3        (ARM_LOCAL_BASE + 0x0B8)
#define ARM_LOCAL_MAILBOX3_SET_CORE3        (ARM_LOCAL_BASE + 0x0BC)

#define ARM_LOCAL_MAILBOX0_CLR_CORE0        (ARM_LOCAL_BASE + 0x0C0)
#define ARM_LOCAL_MAILBOX1_CLR_CORE0        (ARM_LOCAL_BASE + 0x0C4)
#define ARM_LOCAL_MAILBOX2_CLR_CORE0        (ARM_LOCAL_BASE + 0x0C8)
#define ARM_LOCAL_MAILBOX3_CLR_CORE0        (ARM_LOCAL_BASE + 0x0CC)

#define ARM_LOCAL_MAILBOX0_CLR_CORE1        (ARM_LOCAL_BASE + 0x0D0)
#define ARM_LOCAL_MAILBOX1_CLR_CORE1        (ARM_LOCAL_BASE + 0x0D4)
#define ARM_LOCAL_MAILBOX2_CLR_CORE1        (ARM_LOCAL_BASE + 0x0D8)
#define ARM_LOCAL_MAILBOX3_CLR_CORE1        (ARM_LOCAL_BASE + 0x0DC)

#define ARM_LOCAL_MAILBOX0_CLR_CORE2        (ARM_LOCAL_BASE + 0x0E0)
#define ARM_LOCAL_MAILBOX1_CLR_CORE2        (ARM_LOCAL_BASE + 0x0E4)
#define ARM_LOCAL_MAILBOX2_CLR_CORE2        (ARM_LOCAL_BASE + 0x0E8)
#define ARM_LOCAL_MAILBOX3_CLR_CORE2        (ARM_LOCAL_BASE + 0x0EC)

#define ARM_LOCAL_MAILBOX0_CLR_CORE3        (ARM_LOCAL_BASE + 0x0F0)
#define ARM_LOCAL_MAILBOX1_CLR_CORE3        (ARM_LOCAL_BASE + 0x0F4)
#define ARM_LOCAL_MAILBOX2_CLR_CORE3        (ARM_LOCAL_BASE + 0x0F8)
#define ARM_LOCAL_MAILBOX3_CLR_CORE3        (ARM_LOCAL_BASE + 0x0FC)
#endif /* defined(BCM2836) || defined(BCM2837) */

#endif /* ARCTOS_RPI_ALL_H */
