/**
 * Copyright (c) 2018 Michael Zehrer
 * This file is part of ARCTOS
 *
 * @licence MIT
 * @brief   System timer interface
 * @author  Michael Zehrer
 */
#ifndef ARCTOS_RPI_PERIPHERALS_SYSTEM_TIMER_H
#define ARCTOS_RPI_PERIPHERALS_SYSTEM_TIMER_H

#include "../all.h"

/***********************************************************************
 * System-Timer definitions (from BCM2825 ARM Peripherals - Table 12.1 - Page 172)
 **********************************************************************/
#define SYSTEM_TIMER_BASE           (PERIPHERALS_BASE + 0x00003000)
/* System Timer Control/Status */
#define SYSTEM_TIMER_CONTROL        (SYSTEM_TIMER_BASE + 0x0)
/* System Timer Counter Lower 32 bits */
#define SYSTEM_TIMER_CNT_LOW        (SYSTEM_TIMER_BASE + 0x4)
/* System Timer Counter Higher 32 bits */
#define SYSTEM_TIMER_CNT_HIGH       (SYSTEM_TIMER_BASE + 0x8)
/* System Timer Compare 0 */
#define SYSTEM_TIMER_COMPARE0       (SYSTEM_TIMER_BASE + 0xc)
/* System Timer Compare 1 */
#define SYSTEM_TIMER_COMPARE1       (SYSTEM_TIMER_BASE + 0x10)
/* System Timer Compare 2 */
#define SYSTEM_TIMER_COMPARE2       (SYSTEM_TIMER_BASE + 0x14)
/* System Timer Compare 3 */
#define SYSTEM_TIMER_COMPARE3       (SYSTEM_TIMER_BASE + 0x18)

#define SYSTEM_TIMER_CONTROL_MATCH0 BIT(0) // Already used by the VideoCore GPU (Do not use)
#define SYSTEM_TIMER_CONTROL_MATCH1 BIT(1)
#define SYSTEM_TIMER_CONTROL_MATCH2 BIT(2) // Already used by the VideoCore GPU (Do not use)
#define SYSTEM_TIMER_CONTROL_MATCH3 BIT(3)

#ifndef __ASSEMBLY__
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef struct {
    __RW uint32_t CONTROL;
    __RW uint32_t CNT_LOW;
    __RW uint32_t CNT_HIGH;
    __RW uint32_t COMPARE0;
    __RW uint32_t COMPARE1;
    __RW uint32_t COMPARE2;
    __RW uint32_t COMPARE3;
} sys_timer_t;
#define SYSTEM_TIMER ((sys_timer_t *)SYSTEM_TIMER_BASE)

#ifdef __cplusplus
} // end extern "C"
#endif

#endif /* __ASSEMBLY__ */

#endif /* ARCTOS_RPI_PERIPHERALS_SYSTEM_TIMER_H */
