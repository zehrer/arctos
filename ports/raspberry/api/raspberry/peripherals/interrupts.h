/**
 * Copyright (c) 2018 Michael Zehrer
 * This file is part of ARCTOS
 *
 * @licence MIT
 * @brief   Interrupt interface
 * @author  Michael Zehrer
 */
#ifndef ARCTOS_RPI_PERIPHERALS_INTERRUPTS_H
#define ARCTOS_RPI_PERIPHERALS_INTERRUPTS_H

#include "../all.h"

/***********************************************************************
 * Interrupt definitions (from BCM2825 ARM Peripherals - Table 7.5 - Page 112)
 **********************************************************************/
#define IRQ_BASE                (PERIPHERALS_BASE + 0x0000B000)
#define IRQ_BASIC               (IRQ_BASE + 0x00000200)
#define IRQ_PEND1               (IRQ_BASE + 0x00000204)
#define IRQ_PEND2               (IRQ_BASE + 0x00000208)
#define IRQ_FIQ_CONTROL         (IRQ_BASE + 0x0000020C)
#define IRQ_ENABLE1             (IRQ_BASE + 0x00000210)
#define IRQ_ENABLE2             (IRQ_BASE + 0x00000214)
#define IRQ_ENABLE_BASIC        (IRQ_BASE + 0x00000218)
#define IRQ_DISABLE1            (IRQ_BASE + 0x0000021C)
#define IRQ_DISABLE2            (IRQ_BASE + 0x00000220)
#define IRQ_DISABLE_BASIC       (IRQ_BASE + 0x00000224)



#ifndef __ASSEMBLY__
#include <stdint.h>
#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef struct {
    __R  uint32_t BASIC;
    __R  uint32_t PEND1;
    __R  uint32_t PEND2;
    __RW uint32_t FIQ_CONTROL;
    __RW uint32_t ENABLE1;
    __RW uint32_t ENABLE2;
    __RW uint32_t ENABLE_BASIC;
    __RW uint32_t DISABLE1;
    __RW uint32_t DISABLE2;
    __RW uint32_t DISABLE_BASIC;
} interrupts_t;
#define IRQ_CNTL ((interrupts_t *)(IRQ_BASE + 0x00000200))

// IRQ lines of BCM2837 peripherals
// (IRQs 0-63 are those shared between the GPU and CPU, IRQs 64-95 are CPU-specific)
// IRQs 0 to 31 appear in the IRQ_PEND1 register
// (The definitions have many empty entries.
// These should not be enabled as they will interfere with the GPU operation.)
typedef enum {
    System_Timer0_IRQn = 0, // Already used by the VideoCore GPU (Do not use)
    System_Timer1_IRQn = 1,
    System_Timer2_IRQn = 2, // Already used by the VideoCore GPU (Do not use)
    System_Timer3_IRQn = 3,
    
    Auxiliary_IRQn     = 29, // AUX (UART1, SPI1 and SPI2)
    
    SPI_I2C_SLV_IRQn   = 43,

    PWA0_IRQn          = 45,
    PWA1_IRQn          = 46,

    SMI_IRQn           = 48,
    GPIO0_IRQn         = 49, // Bank0
    GPIO1_IRQn         = 50, // Bank1
    GPIO2_IRQn         = 51,
    GPIO_ALL_IRQn      = 52, // Any Bank (all banks interrupt to allow GPIO FIQ)
    I2C_IRQn           = 53,
    SPI_IRQn           = 54,
    PCM_IRQn           = 55,

    UART_IRQn          = 57
} IRQn_t;

void RPI_IRQ_Enable(IRQn_t irq_number);
void RPI_IRQ_Disable(IRQn_t irq_number);
bool RPI_IRQ_GetPending(IRQn_t irq_number);

#ifdef __cplusplus
} // end extern "C"
#endif

#endif /* __ASSEMBLY__ */

#endif /* ARCTOS_RPI_PERIPHERALS_INTERRUPTS_H */
