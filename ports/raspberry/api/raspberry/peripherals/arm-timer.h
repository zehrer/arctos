/**
 * Copyright (c) 2018 Michael Zehrer
 * This file is part of ARCTOS
 *
 * @licence MIT
 * @brief   ARM timer interface
 * @author  Michael Zehrer
 */
#ifndef ARCTOS_RPI_PERIPHERALS_ARM_TIMER_H
#define ARCTOS_RPI_PERIPHERALS_ARM_TIMER_H

#include "../all.h"

/***********************************************************************
 * ARM-Timer definitions (from BCM2825 ARM Peripherals - Table 14.2 - Page 196)
 **********************************************************************/
#define ARM_TIMER_BASE                  (PERIPHERALS_BASE + 0x0000B000)
#define ARM_TIMER_LOAD                  (ARM_TIMER_BASE + 0x400)
#define ARM_TIMER_VALUE                 (ARM_TIMER_BASE + 0x404)
#define ARM_TIMER_CONTROL               (ARM_TIMER_BASE + 0x408)
#define ARM_TIMER_IRQ_CLEAR             (ARM_TIMER_BASE + 0x40C)
#define ARM_TIMER_RAW_IRQ               (ARM_TIMER_BASE + 0x410)
#define ARM_TIMER_MASKED_IRQ            (ARM_TIMER_BASE + 0x414)
#define ARM_TIMER_RELOAD                (ARM_TIMER_BASE + 0x418)
#define ARM_TIMER_PREDIVIDER            (ARM_TIMER_BASE + 0x41C)
#define ARM_TIMER_FREE_RUNNING_COUNTER  (ARM_TIMER_BASE + 0x420)

/** 0 : 16-bit counters
    1 : 23-bit counter */
#define ARM_TIMER_CTRL_23BIT            ( 1 << 1 )

#define ARM_TIMER_CTRL_PRESCALE_1       ( 0 << 2 )
#define ARM_TIMER_CTRL_PRESCALE_16      ( 1 << 2 )
#define ARM_TIMER_CTRL_PRESCALE_256     ( 2 << 2 )

/** 0 : Timer interrupt disabled
    1 : Timer interrupt enabled */
#define ARM_TIMER_CTRL_INT_ENABLE       ( 1 << 5 )
#define ARM_TIMER_CTRL_INT_DISABLE      ( 0 << 5 )

/** 0 : Timer disabled
    1 : Timer enabled */
#define ARM_TIMER_CTRL_ENABLE           ( 1 << 7 )
#define ARM_TIMER_CTRL_DISABLE          ( 0 << 7 )



#ifndef __ASSEMBLY__
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef struct {
    /** The timer load register sets the time for the timer to count down.
        This value is loaded into the timer value register after the load
        register has been written or if the timer-value register has counted
        down to 0. */
    __RW uint32_t load;

    /** This register holds the current timer value and is counted down when
        the counter is running. It is counted down each timer clock until the
        value 0 is reached. Then the value register is re-loaded from the
        timer load register and the interrupt pending bit is set. The timer
        count down speed is set by the timer pre-divide register. */
    __R uint32_t value;

    /** The standard SP804 timer control register consist of 8 bits but in the
        BCM implementation there are more control bits for the extra features.
        Control bits 0-7 are identical to the SP804 bits, albeit some
        functionality of the SP804 is not implemented. All new control bits
        start from bit 8 upwards. */
    __RW uint32_t control;

    /** The timer IRQ clear register is write only. When writing this register
        the interrupt-pending bit is cleared. When reading this register it
        returns 0x544D5241 which is the ASCII reversed value for "ARMT". */
    __W uint32_t irq_clear;

    /** The raw IRQ register is a read-only register. It shows the status of
        the interrupt pending bit. 0 : The interrupt pending bits is clear.
        1 : The interrupt pending bit is set.

        The interrupt pending bits is set each time the value register is
        counted down to zero. The interrupt pending bit can not by itself
        generates interrupts. Interrupts can only be generated if the
        interrupt enable bit is set. */
    __R uint32_t raw_irq;

    /** The masked IRQ register is a read-only register. It shows the status
        of the interrupt signal. It is simply a logical AND of the interrupt
        pending bit and the interrupt enable bit. 0 : Interrupt line not
        asserted. 1 :Interrupt line is asserted, (the interrupt pending and
        the interrupt enable bit are set.)  */
    __R uint32_t masked_irq;

    /** This register is a copy of the timer load register. The difference is
        that a write to this register does not trigger an immediate reload of
        the timer value register. Instead the timer load register value is
        only accessed if the value register has finished counting down to
        zero. */
    __RW uint32_t reload;

    /** The Pre-divider register is not present in the SP804. The pre-divider
        register is 10 bits wide and can be written or read from. This
        register has been added as the SP804 expects a 1MHz clock which we do
        not have. Instead the pre-divider takes the APB clock and divides it
        down according to:

        timer_clock = apb_clock/(pre_divider+1)

        The reset value of this register is 0x7D so gives a divide by 126. */
    __RW uint32_t pre_divider;

    /** The free running counter is not present in the SP804. The free running
        counter is a 32 bits wide read only register. The register is enabled
        by setting bit 9 of the Timer control register. The free running
        counter is incremented immediately after it is enabled. The timer can
        not be reset but when enabled, will always increment and roll-over.

        The free running counter is also running from the APB clock and has
        its own clock pre-divider controlled by bits 16-23 of the timer
        control register.

        This register will be halted too if bit 8 of the control register is
        set and the ARM is in Debug Halt mode. */
    __RW uint32_t free_running_counter;
} arm_timer_t;
#define ARM_TIMER ((arm_timer_t *)(ARM_TIMER_BASE + 0x400))

#ifdef __cplusplus
} // end extern "C"
#endif

#endif /* __ASSEMBLY__ */

#endif /* ARCTOS_RPI_PERIPHERALS_ARM_TIMER_H */
