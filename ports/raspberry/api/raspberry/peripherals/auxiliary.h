/**
 * Copyright (c) 2018 Michael Zehrer
 * This file is part of ARCTOS
 *
 * @licence MIT
 * @brief   Auxiliary (MiniUART, SPI1, SPI2) interface
 * @author  Michael Zehrer
 */
#ifndef ARCTOS_RPI_PERIPHERALS_AUXILIARY_H
#define ARCTOS_RPI_PERIPHERALS_AUXILIARY_H

#include "../all.h"

/***********************************************************************
 * AUX (UART1 = miniUART, SPI1 and SPI2) (from BCM2825 ARM Peripherals - Section 2)
 **********************************************************************/
#define AUX_BASE                (PERIPHERALS_BASE + 0x00215000)
#define AUXIRQ                  (AUX_BASE)        // Auxiliary Interrupt status (3 bit)
#define AUXIRQ_MINIUART         BIT(0)
#define AUXIRQ_SPI1             BIT(1)
#define AUXIRQ_SPI2             BIT(2)

#define AUXENB                  (AUX_BASE + 0x04) // Auxiliary enables (3 bit)
#define AUXENB_MINIUART         BIT(0)
#define AUXENB_SPI1             BIT(1)
#define AUXENB_SPI2             BIT(2)

#define AUX_MU_IO_REG           (AUX_BASE + 0x40) // Mini Uart I/O Data (8 bit)
#define AUX_MU_IER_REG          (AUX_BASE + 0x44) // Mini Uart Interrupt Enable (8 bit)
#define AUX_MU_IIR_REG          (AUX_BASE + 0x48) // Mini Uart Interrupt Identify (8 bit)
#define AUX_MU_LCR_REG          (AUX_BASE + 0x4C) // Mini Uart Line Control (8 bit)
#define AUX_MU_MCR_REG          (AUX_BASE + 0x50) // Mini Uart Modem Control (8 bit)
#define AUX_MU_LSR_REG          (AUX_BASE + 0x54) // Mini Uart Line Status (8 bit)
#define AUX_MU_MSR_REG          (AUX_BASE + 0x58) // Mini Uart Modem Status (8 bit)
#define AUX_MU_SCRATCH          (AUX_BASE + 0x5C) // Mini Uart Scratch (8 bit)
#define AUX_MU_CNTL_REG         (AUX_BASE + 0x60) // Mini Uart Extra Control (8 bit)
#define AUX_MU_STAT_REG         (AUX_BASE + 0x64) // Mini Uart Extra Status (8 bit)
#define AUX_MU_BAUD_REG         (AUX_BASE + 0x68) // Mini Uart Baudrate (16 bit)

#define AUX_SPI0_CNTL0_REG      (AUX_BASE + 0x80) // SPI 1 Control register 0 (32 bit)
#define AUX_SPI0_CNTL1_REG      (AUX_BASE + 0x84) // SPI 1 Control register 1 (8 bit)
#define AUX_SPI0_STAT_REG       (AUX_BASE + 0x88) // SPI 1 Status (32 bit)
#define AUX_SPI0_IO_REG         (AUX_BASE + 0x90) // SPI 1 Data (32 bit)
#define AUX_SPI0_PEEK_REG       (AUX_BASE + 0x94) // SPI 1 Peek (16 bit)

#define AUX_SPI1_CNTL0_REG      (AUX_BASE + 0xC0) // SPI 2 Control register 0 (32 bit)
#define AUX_SPI1_CNTL1_REG      (AUX_BASE + 0xC4) // SPI 2 Control register 1 (8 bit)
#define AUX_SPI1_STAT_REG       (AUX_BASE + 0xC8) // SPI 2 Status (32 bit)
#define AUX_SPI1_IO_REG         (AUX_BASE + 0xD0) // SPI 2 Data (32 bit)
#define AUX_SPI1_PEEK_REG       (AUX_BASE + 0xD4) // SPI 2 Peek (16 bit)

/*
 * (!!) => see http://elinux.org/BCM2835_datasheet_errata
 */

// Mini Uart Interrupt Enable
#define AUX_MUIER_ENABLE_TXIRQ          0xA /* (!!) */
#define AUX_MUIER_ENABLE_RXIRQ          0x5 /* (!!) */

// Mini Uart Interrupt Identify
#define AUX_MUIIR_IRQ_PENDING           BIT(0)
#define AUX_MUIIR_IRQID_MASK            0x6
#define AUX_MUIIR_IRQID_TX_EMPTY        BIT(1)
#define AUX_MUIIR_IRQID_RX_READY        BIT(2)
#define AUX_MUIIR_CLEAR_RX_FIFO         BIT(1)
#define AUX_MUIIR_CLEAR_TX_FIFO         BIT(2)
#define AUX_MUIIR_ENABLE_FIFOS          0xC0

// Mini Uart Line Control
#define AUX_MULCR_DATA_SIZE_MASK        0x3
#define AUX_MULCR_DATA_SIZE_7BIT        0x0
#define AUX_MULCR_DATA_SIZE_8BIT        0x3 /* (!!) */
#define AUX_MULCR_BREAK                 BIT(6)
#define AUX_MULCR_DLAB                  BIT(7)

// Mini Uart Modem Control
#define AUX_MUMCR_RTS                   BIT(1)

// Mini Uart Line Status
#define AUX_MULSR_DATA_READY            BIT(0)
#define AUX_MULSR_RX_OVERRUN            BIT(1)
#define AUX_MULSR_TX_EMPTY              BIT(5)
#define AUX_MULSR_TX_IDLE               BIT(6)

// Mini Uart Modem Status
#define AUX_MUMSR_CTS                   BIT(5)

// Mini Uart Extra Control
#define AUX_MUCNTL_RX_ENABLE            BIT(0)
#define AUX_MUCNTL_TX_ENABLE            BIT(1)
#define AUX_MUCNTL_RTS_FLOW_ENABLE      BIT(2)
#define AUX_MUCNTL_CTS_FLOW_ENABLE      BIT(3)
#define AUX_MUCNTL_RTS_AUTO_MASK        0x30
#define AUX_MUCNTL_RTS_ASSERT_LEVEL     BIT(6)
#define AUX_MUCNTL_CTS_ASSERT_LEVEL     BIT(7)

// Mini Uart Extra Status
#define AUX_MUSTAT_SYMBOL_AVAILABLE     BIT(0)
#define AUX_MUSTAT_SPACE_AVAILABLE      BIT(1)
#define AUX_MUSTAT_RX_IDLE              BIT(2)
#define AUX_MUSTAT_TX_IDLE              BIT(3)
#define AUX_MUSTAT_RX_OVERRUN           BIT(4)
#define AUX_MUSTAT_TX_FIFO_FULL         BIT(5)
#define AUX_MUSTAT_RTS                  BIT(6)
#define AUX_MUSTAT_CTS                  BIT(7)
#define AUX_MUSTAT_TX_EMPTY             BIT(8)
#define AUX_MUSTAT_TX_DONE              BIT(9)
#define AUX_MUSTAT_RX_FIFO_LEVEL_POS    16
#define AUX_MUSTAT_RX_FIFO_LEVEL_MASK   ( 0xF << AUX_MUSTAT_RX_FIFO_LEVEL_POS )
#define AUX_MUSTAT_TX_FIFO_LEVEL_POS    24
#define AUX_MUSTAT_TX_FIFO_LEVEL_MASK   ( 0xF << AUX_MUSTAT_TX_FIFO_LEVEL_POS )

// TODO SPI



#ifndef __ASSEMBLY__
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef struct {
    __R  uint32_t IRQ;
    __RW uint32_t ENABLES;

    uint32_t res01[((0x40 - 0x04) / 4) - 1];

    __RW uint32_t MU_IO;
    __RW uint32_t MU_IER;
    __RW uint32_t MU_IIR;
    __RW uint32_t MU_LCR;
    __RW uint32_t MU_MCR;
    __R  uint32_t MU_LSR;
    __R  uint32_t MU_MSR;
    __RW uint32_t MU_SCRATCH;
    __RW uint32_t MU_CNTL;
    __R  uint32_t MU_STAT;
    __RW uint32_t MU_BAUD;

    uint32_t res02[(0x80 - 0x68) / 4];

    __RW uint32_t SPI0_CNTL0;
    __RW uint32_t SPI0_CNTL1;
    __RW uint32_t SPI0_STAT;
    __RW uint32_t SPI0_IO;
    __R  uint32_t SPI0_PEEK;

    uint32_t res03[(0xC0 - 0x94) / 4];

    __RW uint32_t SPI1_CNTL0;
    __RW uint32_t SPI1_CNTL1;
    __RW uint32_t SPI1_STAT;
    __RW uint32_t SPI1_IO;
    __R  uint32_t SPI1_PEEK;
} aux_t;
#define AUXILIARY ((aux_t *)AUX_BASE)

#ifdef __cplusplus
} // end extern "C"
#endif

#endif /* __ASSEMBLY__ */

#endif /* ARCTOS_RPI_PERIPHERALS_AUXILIARY_H */
