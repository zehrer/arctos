/**
 * Copyright (c) 2018 Michael Zehrer
 * This file is part of ARCTOS
 *
 * @licence MIT
 * @brief   GPIO interface
 * @author  Michael Zehrer
 */
#ifndef ARCTOS_RPI_PERIPHERALS_GPIO_H
#define ARCTOS_RPI_PERIPHERALS_GPIO_H

#include "../all.h"

/***********************************************************************
 * GPIO definitions (from BCM2825 ARM Peripherals - Section 6)
 **********************************************************************/
#define GPIO_BASE                   (PERIPHERALS_BASE + 0x00200000)

#define GPIO_NUMBER_OF_PINS         54

// GPIO Function selection
#define GPIO_GPFSEL0                (GPIO_BASE)        // GPIO Function Select 0
#define GPIO_GPFSEL1                (GPIO_BASE + 0x04) // GPIO Function Select 1
#define GPIO_GPFSEL2                (GPIO_BASE + 0x08) // GPIO Function Select 2
#define GPIO_GPFSEL3                (GPIO_BASE + 0x0C) // GPIO Function Select 3
#define GPIO_GPFSEL4                (GPIO_BASE + 0x10) // GPIO Function Select 4
#define GPIO_GPFSEL5                (GPIO_BASE + 0x14) // GPIO Function Select 5

// Pin Ouput Set Registers
#define GPIO_GPSET0                 (GPIO_BASE + 0x1C) // GPIO Pin Output Set 0
#define GPIO_GPSET1                 (GPIO_BASE + 0x20) // GPIO Pin Output Set 1
// Pin Output Clear Registers
#define GPIO_GPCLR0                 (GPIO_BASE + 0x28) // GPIO Pin Output Clear 0
#define GPIO_GPCLR1                 (GPIO_BASE + 0x2C) // GPIO Pin Output Clear 1
// Pin Level Registers
#define GPIO_GPLEV0                 (GPIO_BASE + 0x34) // GPIO Pin Level 0
#define GPIO_GPLEV1                 (GPIO_BASE + 0x38) // GPIO Pin Level 1
// Pin Event Detect Status Registers
#define GPIO_GPEDS0                 (GPIO_BASE + 0x40) // GPIO Pin Event Detect Status 0
#define GPIO_GPEDS1                 (GPIO_BASE + 0x44) // GPIO Pin Event Detect Status 1
// Pin Rising Edge Detect Enable Registers
#define GPIO_GPREN0                 (GPIO_BASE + 0x4C) // GPIO Pin Rising Edge Detect Enable 0
#define GPIO_GPREN1                 (GPIO_BASE + 0x50) // GPIO Pin Rising Edge Detect Enable 1
// Pin Falling Edge Detect Enable Registers
#define GPIO_GPFEN0                 (GPIO_BASE + 0x58) // GPIO Pin Falling Edge Detect Enable 0
#define GPIO_GPFEN1                 (GPIO_BASE + 0x5C) // GPIO Pin Falling Edge Detect Enable 1
// Pin High Detect Enable Registers
#define GPIO_GPHEN0                 (GPIO_BASE + 0x64) // GPIO Pin High Detect Enable 0
#define GPIO_GPHEN1                 (GPIO_BASE + 0x68) // GPIO Pin High Detect Enable 1
// Pin Low Detect Enable Registers
#define GPIO_GPLEN0                 (GPIO_BASE + 0x70) // GPIO Pin Low Detect Enable 0
#define GPIO_GPLEN1                 (GPIO_BASE + 0x74) // GPIO Pin Low Detect Enable 1
// Pin Async. Rising Edge Detect Registers
#define GPIO_GPAREN0                (GPIO_BASE + 0x7C) // GPIO Pin Async. Rising Edge Detect 0
#define GPIO_GPAREN1                (GPIO_BASE + 0x80) // GPIO Pin Async. Rising Edge Detect 1
// Pin Async. Falling Edge Detect Registers
#define GPIO_AFEN0                  (GPIO_BASE + 0x88) // GPIO Pin Async. Falling Edge Detect 0
#define GPIO_AFEN1                  (GPIO_BASE + 0x8C) // GPIO Pin Async. Falling Edge Detect 0
// Pin Pull-up/down Enable Register
#define GPIO_GPPUD                  (GPIO_BASE + 0x94) // GPIO Pin Pull-up/down Enable
// Pin Pull-up/down Enable Clock Registers (Must be used in conjunction with GPPUD)
#define GPIO_GPPUDCLK0              (GPIO_BASE + 0x98) // GPIO Pin Pull-up/down Enable Clock 0
#define GPIO_GPPUDCLK1              (GPIO_BASE + 0x9C) // GPIO Pin Pull-up/down Enable Clock 1

// Function Select Mask and Values
#define GPIO_GPFSEL_MASK            7 // 3 bit for each PIN
#define GPIO_GPFSEL_IN              0
#define GPIO_GPFSEL_OUT             1
#define GPIO_GPFSEL_ALT0            4
#define GPIO_GPFSEL_ALT1            5
#define GPIO_GPFSEL_ALT2            6
#define GPIO_GPFSEL_ALT3            7
#define GPIO_GPFSEL_ALT4            3
#define GPIO_GPFSEL_ALT5            2

// Pull-up/down Enable Mask and Values
#define GPIO_GPPUD_MASK             0x3
#define GPIO_GPPUD_DISABLE          0x0
#define GPIO_GPPUD_ENABLE_PULL_DOWN 0x1
#define GPIO_GPPUD_ENABLE_PULL_UP   0x2



#ifndef __ASSEMBLY__
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef struct {
    __RW uint32_t GPFSEL0;
    __RW uint32_t GPFSEL1;
    __RW uint32_t GPFSEL2;
    __RW uint32_t GPFSEL3;
    __RW uint32_t GPFSEL4;
    __RW uint32_t GPFSEL5;
    uint32_t res01;
    __W  uint32_t GPSET0;
    __W  uint32_t GPSET1;
    uint32_t res02;
    __W  uint32_t GPCLR0;
    __W  uint32_t GPCLR1;
    uint32_t res03;
    __W  uint32_t GPLEV0;
    __W  uint32_t GPLEV1;
    uint32_t res04;
    __W  uint32_t GPEDS0;
    __W  uint32_t GPEDS1;
    uint32_t res05;
    __W  uint32_t GPREN0;
    __W  uint32_t GPREN1;
    uint32_t res06;
    __W  uint32_t GPFEN0;
    __W  uint32_t GPFEN1;
    uint32_t res07;
    __W  uint32_t GPHEN0;
    __W  uint32_t GPHEN1;
    uint32_t res08;
    __W  uint32_t GPLEN0;
    __W  uint32_t GPLEN1;
    uint32_t res09;
    __W  uint32_t GPAREN0;
    __W  uint32_t GPAREN1;
    uint32_t res10;
    __W  uint32_t GPAFEN0;
    __W  uint32_t GPAFEN1;
    uint32_t res11;
    __W  uint32_t GPPUD;
    __W  uint32_t GPPUDCLK0;
    __W  uint32_t GPPUDCLK1;
    uint32_t res12[(0xB0 - 0xA0) / 4];
} gpio_t;

typedef enum {
    FS_INPUT  = GPIO_GPFSEL_IN,
    FS_OUTPUT = GPIO_GPFSEL_OUT,
    FS_ALT0   = GPIO_GPFSEL_ALT0,
    FS_ALT1   = GPIO_GPFSEL_ALT1,
    FS_ALT2   = GPIO_GPFSEL_ALT2,
    FS_ALT3   = GPIO_GPFSEL_ALT3,
    FS_ALT4   = GPIO_GPFSEL_ALT4,
    FS_ALT5   = GPIO_GPFSEL_ALT5
} gpio_function_t;

typedef enum {
    PUD_OFF   = GPIO_GPPUD_DISABLE,
    PUD_DOWN  = GPIO_GPPUD_ENABLE_PULL_DOWN,
    PUD_UP    = GPIO_GPPUD_ENABLE_PULL_UP
} gpio_pud_t;

typedef enum {
    PE_NONE = 0,

    PE_RISING,
    PE_FALLING,
    PE_RISING_AND_FALLING,

    PE_HIGH,
    PE_LOW,

    PE_ASYNC_RISING,
    PE_ASYNC_FALLING
} gpio_pin_event_t;

#define GPIO ((gpio_t *)GPIO_BASE)

int RPI_GPIO_SetPinFunction(uint32_t pin, gpio_function_t function);
int RPI_GPIO_SetMultiplePinFunction(uint32_t start_pin, uint32_t end_pin, gpio_function_t function);

int RPI_GPIO_SetPinPUD(uint32_t pin, gpio_pud_t pud);
int RPI_GPIO_SetMultiplePinPUD(uint32_t start_pin, uint32_t end_pin, gpio_pud_t pud);

int RPI_GPIO_SetPinEvent(uint32_t pin, gpio_pin_event_t event);
int RPI_GPIO_SetMultiplePinEvent(uint32_t start_pin, uint32_t end_pin, gpio_pin_event_t event);

#ifdef __cplusplus
} // end extern "C"
#endif

#endif /* __ASSEMBLY__ */

#endif /* ARCTOS_RPI_PERIPHERALS_GPIO_H */
